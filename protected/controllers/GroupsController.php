<?php

class GroupsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','view','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$uID = Yii::app()->user->id;
		$account_model = Accounts::model()->findbyAttributes(array('uid' => $uID));

		if (empty($account_model)) {
			$this->redirect(Yii::app()->request->urlReferrer);
		}else{
			$this->render('view',array(
				'model'=>$this->loadModel($id),
			));
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Groups;
		$uID = Yii::app()->user->id;
		$account_model = Accounts::model()->findbyAttributes(array('uid' => $uID));
		// $user_model = Users::model()->findbyAttributes(array('ID' => $uID));
		$roles = Yii::app()->db->createCommand()
						        ->from('roles')
						        ->queryAll();
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (!empty($account_model)) {
			if(isset($_POST['Groups']))
			{
				// SAVE DATA INTO GROUPS TABLE
				$model->attributes=$_POST['Groups'];
				$model->account_id = $account_model->ID;
				$model->user_id = $uID;

				if($model->save())
				{
					if (isset($_POST['role']) && !empty($_POST['role'])) {
						// SAVE ROLES INTO GROUPS ROLES TABLE ASSIGNED TO A GROUP
						foreach ($_POST['role'] as $role) {
							$groupsRolesModel = new GroupsRoles;
							$groupsRolesModel->group_id = $model->ID;
							$groupsRolesModel->role_id = $role;
							$groupsRolesModel->save(false);
						}
					}
					$this->redirect(array('index'));
				}
			}

			$this->render('create',array(
				'model'=>$model,
				'roles'=>$roles,
			));
		}else{
			$this->redirect(Yii::app()->request->urlReferrer);
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$uID = Yii::app()->user->id;
		$account_model = Accounts::model()->findbyAttributes(array('uid' => $uID));

		$model=$this->loadModel($id);

		$roles = Yii::app()->db->createCommand()
						        ->from('roles')
						        ->queryAll();

		$groupsRoles = Yii::app()->db->createCommand()
						        ->from('groups_roles')
						        ->where('group_id=:id', array(':id'=>$id))
						        ->queryAll();
						        
						        // echo "<pre>";
						        // print_r($groupsRoles);exit;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if (!empty($account_model)) {
			if(isset($_POST['Groups']))
			{
				$model->attributes=$_POST['Groups'];
				if($model->save()){
					foreach ($_POST['check'] as $value) {
						$groupsRolesModel = new GroupsRoles;
						$groupsRolesModel->group_id = $model->ID;
						$groupsRolesModel->role_id = $value;
						$groupsRolesModel->save();
					}
					$this->redirect(array('index','id'=>$model->ID));
				}
			}

			$this->render('update',array(
				'model'=>$model,
				'roles'=>$roles,
				'groupsRoles'=>$groupsRoles,
			));
		}else{
			$this->redirect(Yii::app()->request->urlReferrer);
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$uID = Yii::app()->user->id;
		$account_model = Accounts::model()->findbyAttributes(array('uid' => $uID));

		if (empty($account_model)) {
			$this->redirect(Yii::app()->request->urlReferrer);
		}else{
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax'])){
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$uID = Yii::app()->user->id;
		$account_model = Accounts::model()->findbyAttributes(array('uid' => $uID));

		if (empty($account_model)) {
			$this->redirect(Yii::app()->request->urlReferrer);
		}else{
			// $dataProvider=new CActiveDataProvider('Groups');
			$groups = Yii::app()->db->createCommand()
							        ->from('groups')
							        ->where('user_id=:id', array(':id'=>$uID))
							        ->order('ID DESC') 
							        ->queryAll();
			$this->render('index',array(
				'groups'=>$groups,
			));
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		// $uID = Yii::app()->user->id;
		// $account_model = Accounts::model()->findbyAttributes(array('uid' => $uID));

		// if (empty($account_model)) {
		// 	$this->redirect(Yii::app()->request->urlReferrer);
		// }else{
			$model=new Groups('search');
			$model->unsetAttributes();  // clear any default values
			if(isset($_GET['Groups'])){
				$model->attributes=$_GET['Groups'];
			}

			$this->render('admin',array(
				'model'=>$model,
			));
		// }
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Groups the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Groups::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Groups $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='groups-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
