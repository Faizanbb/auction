<?php

class UsersController extends Controller
{
	protected function beforeAction($action)
    {
    	$slug = Yii::app()->controller->action->id; // return only last part after slash(/)
    	$roles = Yii::app()->user->roles;
        if(in_array($slug, $roles))
        {
            return true;
        } else {
			Yii::app()->user->setFlash('saved', 'Sorry, you are not permitted to access this page');
            Yii::app()->request->redirect(Yii::app()->user->returnUrl);
        }
    }
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('create'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','update','update_user','profile','create_new_user'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$uID = Yii::app()->user->id;
		$admin = Users::model()->findbyAttributes(array('ID' => $uID));

		if ($admin->role == 'ADMIN') {
			$this->render('view',array(
				'model'=>$this->loadModel($id),
			));
		}else{
			$this->redirect(Yii::app()->request->urlReferrer);
		}
	}

	// USER PROFILE
	public function actionProfile()
	{
		$uID = Yii::app()->user->id; // session id
		$admin_account = Accounts::model()->findbyAttributes(array('uid' => $uID));

		if ($admin_account) {
			$this->render('profile',array(
				'model'=>$this->loadModel($uID),
				'account_model' => $admin_account,
			));
		}else{
			$this->render('profile',array(
				'model'=>$this->loadModel($uID),
			));
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate_new_user()
	{
		$uID = Yii::app()->user->id; // session id
		$admin = Users::model()->findbyAttributes(array('ID' => $uID));
		$admin_account = Accounts::model()->findbyAttributes(array('uid' => $uID));
		$groupModel = Yii::app()->db->createCommand()
						        ->from('groups')
						        ->where('user_id=:id', array(':id'=>$uID))
						        ->queryAll();
		// echo "<pre>";
		// print_r($admin);exit;
		$model=new Users;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if ($admin->role == 'ADMIN') {
			if(isset($_POST['Users']))
			{
				if ($admin_account) {
					if ($_POST['Users']['password'] == $_POST['password2'])
					{
						$model->attributes=$_POST['Users'];

						$model->accid = $admin_account->ID;
						$model->password = md5($_POST['password2']);
						$model->title = '';
						$model->phone = '';
						$model->mobile = '';
						$model->carriercode = 123;
						$model->mobileregion = '';
						$model->usergroup = $_POST['group'];
						$model->status = 'Pending';
						$model->role = 'USER';
						$model->logincount = 1;

						if($model->save(false)){
							Yii::app()->user->setFlash('saved', 'You have successfully created a new user');
							$this->redirect(array('view','id'=>$model->ID));
						}
					}else{
						Yii::app()->user->setFlash('failure', 'Password not maching!');
						$this->render('createNewUser',array(
							'model'=>$model,
						));
					}
				}else{
					Yii::app()->user->setFlash('failure', 'You are not authhorized to create new user!');
					$this->render('createNewUser',array(
						'model'=>$model,
					));
				}
			}

			$this->render('createNewUser',array(
				'model'=>$model,
				'group_model'=>$groupModel,
			));
		}else{
			$this->redirect(Yii::app()->request->urlReferrer);
		}
	}
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$uID = Yii::app()->user->id; // session id
		// $admin_account = Accounts::model()->findbyAttributes(array('uid' => $uID));
		
		$model = new Users;
		$account_model = new Accounts;

		if (empty($uID)) {
			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);
			if(isset($_POST['Users']))
			{
				if ($_POST['Users']['password'] == $_POST['password2'])
				{
					$model->attributes=$_POST['Users'];
					$model->accid = 1;
					$model->password = md5($_POST['password2']);
					$model->title = '';
					$model->phone = '';
					$model->mobile = '';
					$model->carriercode = 123;
					$model->mobileregion = '';
					$model->usergroup = '';
					$model->status = 'Pending';
					$model->role = 'ADMIN';
					$model->logincount = 0;

					if($model->save(false))
					{
						$account_model->uid = $model->ID;
						$account_model->country = $_POST['country'];
						$account_model->company = $_POST['company_name'];

						if ($account_model->save(false))
						{
							$model->accid = $account_model->ID;
							if($model->save(false))
							{
								Yii::app()->user->setFlash('saved', 'Your account has been created, login now');
								$this->redirect(array('site/login'));
								// $this->redirect(array('index','id'=>$model->ID));
							}
						}
					}
				}else{
					echo "Password is not maching";
				}
			}

			$this->render('create',array(
				'model'=>$model,
			));
		}
		else{
			$this->redirect(Yii::app()->request->urlReferrer);
		}
		
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate_user($id)
	{
		$uID = Yii::app()->user->id; // session id
		$model=$this->loadModel($id);
		$account_model = Accounts::model()->findbyAttributes(array('uid' => $uID));
		$groupModel = Yii::app()->db->createCommand()
						        ->from('groups')
						        ->where('user_id=:id', array(':id'=>$uID))
						        ->queryAll();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if (!empty($account_model))
		{
			if(isset($_POST['Users']))
			{
				$model->attributes=$_POST['Users'];
				$model->role = 'USER';
				$model->usergroup = $_POST['group'];
			
				if($model->save(false))
				{
					Yii::app()->user->setFlash('updated', 'User unpdated');
					$this->redirect(array('index'));
				}
			}

			$this->render('updateUser',array(
				'model'=>$model,
				// 'account_model'=>$account_model,
				'adminID'=>$account_model->ID,
				'groupModel'=>$groupModel,
			));
		}else{
			$this->redirect(Yii::app()->request->urlReferrer);
		}
	}
	
	public function actionUpdate($id)
	{
		$uID = Yii::app()->user->id; // session id

		$model = $this->loadModel($id); // User model

		$account_model = Accounts::model()->findbyAttributes(array('uid' => $id));
		$groupModel = Yii::app()->db->createCommand()
						        ->from('groups')
						        ->where('user_id=:id', array(':id'=>$uID))
						        ->queryAll();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if ($model->role == 'ADMIN')
		{
			if(isset($_POST['Users']))
			{
				$model->attributes=$_POST['Users'];

				$model->usergroup = $_POST['group'];

				$account_model->attributes=$_POST['Accounts'];
			
				if($model->save(false) && $account_model->save(false))
				{
					Yii::app()->user->setFlash('updated', 'Profile unpdated');
					$this->redirect(array('profile'));
				}
			}

			$this->render('update',array(
				'model'=>$model,
				'account_model'=>$account_model,
				'groupModel'=>$groupModel,
			));
		}
		else
		{
			if(isset($_POST['Users']))
			{
				$model->attributes=$_POST['Users'];
				// $model->usergroup = $model->usergroup;

				if($model->save(false))
				{
					Yii::app()->user->setFlash('updated', 'Profile unpdated');
					$this->redirect(array('profile'));
				}
			}

			$this->render('update',array(
				'model'=>$model,
				'groupModel'=>$groupModel,
			));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$uID = Yii::app()->user->id;
		$admin = User::model()->findbyAttributes(array('ID'=>$uID));
		if ($admin) {
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax'])){
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$uID = Yii::app()->user->id;
		$userAcc = Accounts::model()->findbyAttributes(array('uid'=>$uID));
		if (!empty($userAcc)) {
			// $users = Yii::app()->db->createCommand()
			// 				        ->select('u.*, g.name as g_name')
			// 				        ->from('users u')
			// 						->leftJoin('groups g', 'g.ID=u.usergroup')
			// 				        ->where('u.accid=:id', array(':id'=>$userAcc->ID))
			// 				        ->order('u.createdat DESC') 
			// 				        ->queryAll();
			$users = Yii::app()->db->createCommand()
									->from('users')
									->where('accid=:id', array(':id'=>$userAcc->ID))
									->order('createdat DESC')
									->queryAll();
			$this->render('index',array(
				'users'=>$users,
			));

		}else {
			// RETURN USER BACK, IF NOT ALLOWED
			$this->redirect(Yii::app()->request->urlReferrer);
			// Yii::app()->request->redirect(Yii::app()->user->returnUrl);
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Users('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Users']))
			$model->attributes=$_GET['Users'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Users the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Users $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
