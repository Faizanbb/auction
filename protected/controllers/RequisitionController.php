<?php

class RequisitionController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/req_layout';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','extlink'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('update','ajax','approve'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','list','updateStatus','DeleteReq','Approve'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
        
        public function actionAjax($action,$type){

            if($action=='delete'&&$type=='doc'){
                $reqDoc = RequisitionDocument::model()->findByPk($_POST['reqdid']);
                $reqDoc->delete();
                
//                $doc = Document::model()->findByPk($_POST['docid']);
//                $doc->delete();
                $data['status'] = true;
                echo json_encode($data);exit;
            }
            
        }
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionExtlink($id)
	{
            
		$this->layout = 'req_open_layout';
                $userEmail = 'faizan@businessbid.ae';	
		$model=new Requisition;
                $accountid = $id;       
//               how to contact us
                          if(isset($_POST['howContact']['name'])&&!empty($_POST['howContact']['name'])&&isset($_POST['howContact']['email'])&&isset($_POST['howContact']['email'])){
                    
//                    echo '<pre>';                    print_r($_POST);exit;
                    if($userEmail!=$_POST['howContact']['email']){
                        $cuser = Users::model()->findByAttributes(array('email'=>$_POST['howContact']['email']));
                        if(empty($cuser)){
                        $nuser = new Users;
                        $nuser->accid = $accountid;
                        $nuser->name = $_POST['howContact']['name'];
                        $nuser->email = $_POST['howContact']['email'];
                        $nuser->mobile = $_POST['phoneNumber'];
                        $nuser->carriercode = $_POST['carrierCode'];
                        $nuser->mobileregion = strtolower($_POST['defaultCountry']);
                        $nuser->password = md5('1234');
                        $nuser->title =$_POST['howContact']['department'] ;
                        $nuser->role = 3;
                        $nuser->createdat = date('Y-m-d H:i:s');
                        $nuser->save(false);
                                ;
                        $userid = $nuser->ID;
                        }else{
                            $userid = $cuser->ID;
                        }
                    }
                }
                
                
                if(isset($_POST['Requisition']))
		{
                        
		        $model->attributes=$_POST['Requisition'];
                        $model->user_id = $userid;
                        $model->account_id = $accountid;
                        $model->createdat = date("Y-m-d H:i:s");
                        $model->address = $_POST['Requisition']['address'];
			$model->status = 'PENDING';
                        
                        $files = CUploadedFile::getInstancesByName('Requisition[additional_documents]');
                        $dcount = 0;
                         if (isset($files) && count($files) > 0)
                            $dcount = 1;
                             
                        $model->additional_documents = $dcount;
                        
                        if($model->save()){
                            $reqId = $model->ID;
                                              
                            if(isset($_POST['Requisition']['Approver'])&& !empty($_POST['Requisition']['Approver'])){
                                
                                foreach($_POST['Requisition']['Approver'] as $app){
                                    if(!empty($app)){
                                        $reqapp = new RequisitionApprover;
                                        $req->account_id = $accountid;
                                        $req->user_id = $app;
                                        $req->req_id = $reqId;
                                        $req->save(false);
                                    }
                                }
                                
                            }
                        if (isset($files) && count($files) > 0) {
                            $dataFiles = array();
                            // go through each uploaded image
                            
                            foreach ($files as $image => $pic) {
                                    $ext = explode('/',$pic->type);
                                    $ext = $ext[1];
                                    
                                $filename = $userid.rand(9999,1000000).'.'.$ext;
                                if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/uploads/reqdocs/'.$filename)) {
                                    // add it to the main model now
                                    
                                    $document  = new Documents;
                                    $document->uid =   $userid;
                                    $document->name =   $filename;
                                    $document->typeoffile =   $pic->type;
                                    $document->url =   'uploads/reqdocs';
                                    $document->origname =   $pic->name;
                                    $document->size =   $pic->size;
                                    $document->save(false);
                                    $docid = $document->ID;
                                    
                                    $reqDoc = new RequisitionDocument;
                                    $reqDoc->uid = $userid;
                                    $reqDoc->did = $docid;
                                    $reqDoc->rid = $reqId;
                                    $reqDoc->save(false);
                                   
                                }
                                else{
                                    echo 'Cannot upload!';
                                }
                            }
                        }
                        Yii::app()->user->setFlash('extreStatus', "Your requisition has been submitted for review.<br>Questions, requests for additional information, and status updates will be sent to your email.");
                        $this->redirect(array('extlink','id'=>$id));
				
		}
                }
		$this->render('createext',array(
			'model'=>$model,
		));
	}
	public function actionCreate()
	{
            $userid= Yii::app()->user->ID;
            $accountid = Yii::app()->user->accountId;
			
		$model=new Requisition;
                $userModel = Users::model()->findByPk($userid);
                $userEmail = Yii::app()->user->email;
                
                if(isset($_POST['howContact']['name'])&&!empty($_POST['howContact']['name'])&&isset($_POST['howContact']['email'])&&isset($_POST['howContact']['email'])){
                    
//                    echo '<pre>';                    print_r($_POST);exit;
                    if($userEmail!=$_POST['howContact']['email']){
                        $cuser = Users::model()->findByAttributes(array('email'=>$_POST['howContact']['email']));
                        if(empty($cuser)){
                        $nuser = new Users;
                        $nuser->accid = $accountid;
                        $nuser->name = $_POST['howContact']['name'];
                        $nuser->email = $_POST['howContact']['email'];
                        $nuser->mobile = $_POST['phoneNumber'];
                        $nuser->carriercode = $_POST['carrierCode'];
                        $nuser->mobileregion = strtolower($_POST['defaultCountry']);
                        $nuser->password = md5('1234');
                        $nuser->title =$_POST['howContact']['department'] ;
                        $nuser->role = 3;
                        $nuser->createdat = date('Y-m-d H:i:s');
                        $nuser->save(false);
                                ;
                        $userid = $nuser->ID;
                        }else{
                            $userid = $cuser->ID;
                        }
                    }
                }
                                
		if(isset($_POST['Requisition']))
		{
                        $model->attributes=$_POST['Requisition'];
                        
                        $model->user_id = $userid;
                        $model->account_id = $accountid;
                        $model->createdat = date("Y-m-d H:i:s");
                        $model->address = $_POST['Requisition']['address'];
			$model->status = 'PENDING';
                        
                        $files = CUploadedFile::getInstancesByName('Requisition[additional_documents]');
                        $dcount = 0;
                         if (isset($files) && count($files) > 0)
                        $dcount = 1;
                             
                        $model->additional_documents = $dcount;
                        
                        if($model->save()){
                            $reqId = $model->ID;
                       
                            if(isset($_POST['Requisition']['Approver'])&& !empty($_POST['Requisition']['Approver'])){
                                
                                foreach($_POST['Requisition']['Approver'] as $app){
                                    if(!empty($app)){
                                        $reqapp = new RequisitionApprover;
                                        $req->account_id = $accountid;
                                        $req->user_id = $app;
                                        $req->req_id = $reqId;
                                        $req->save(false);
                                    }
                            }
                                
                            }
                        if (isset($files) && count($files) > 0) {
                            $dataFiles = array();
                            // go through each uploaded image
                            
                            foreach ($files as $image => $pic) {
                                    $ext = explode('/',$pic->type);
                                    $ext = $ext[1];
                                    
                                $filename = $userid.rand(9999,1000000).'.'.$ext;
                                if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/uploads/reqdocs/'.$filename)) {
                                    // add it to the main model now
                                    
                                    $document  = new Documents;
                                    $document->uid =   $userid;
                                    $document->name =   $filename;
                                    $document->typeoffile =   $pic->type;
                                    $document->url =   'uploads/reqdocs';
                                    $document->origname =   $pic->name;
                                    $document->size =   $pic->size;
                                    $document->save(false);
                                    $docid = $document->ID;
                                    
                                    $reqDoc = new RequisitionDocument;
                                    $reqDoc->uid = $userid;
                                    $reqDoc->did = $docid;
                                    $reqDoc->rid = $reqId;
                                    $reqDoc->save(false);
                                   
                                }
                                else{
                                    echo 'Cannot upload!';
                                }
                            }
                        }
                        
                        $this->redirect(array('list'));
				
		}
                }
		$this->render('create',array(
			'model'=>$model,
			'user'=>$userModel,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
           
            	$model=$this->loadModel($id);
                $criteria = new CDbCriteria;
                $criteria->with = array('d');
                $criteria->condition = "t.rid=$id";
                $reqDocs = RequisitionDocument::model()->findall($criteria);
                $approver = RequisitionApprover::model()->findAllByAttributes(array('req_id'=>$id));
//                 how to contact us
                $userEmail = 'faizan@businessbid.ae';
                $userid = 1;
                $accountid = 1;
                
                if(isset($_POST['howContact']['name'])&&!empty($_POST['howContact']['name'])&&isset($_POST['howContact']['email'])&&isset($_POST['howContact']['email'])){
                    if($userEmail!=$_POST['howContact']['email']){
                        $cuser = Users::model()->findByAttributes(array('email'=>$_POST['howContact']['email']));
                        if(empty($cuser)){

                            $nuser = new Users;
                            $nuser->accid = $accountid;
                            $nuser->name = $_POST['howContact']['name'];
                            $nuser->email = $_POST['howContact']['email'];
                            $nuser->mobile = $_POST['phoneNumber'];
                            $nuser->carriercode = $_POST['carrierCode'];
                            $nuser->mobileregion = strtolower($_POST['defaultCountry']);
                            $nuser->password = md5('1234');
                            $nuser->title =$_POST['howContact']['department'] ;
                            $nuser->role = 3;
                            $nuser->createdat = date('Y-m-d H:i:s');
                            $nuser->save(false);
                            
                            $userid = $nuser->ID;
                        }else{
                            $userid = $cuser->ID;
                        }
                    }
                }
                
                if(isset($_POST['Requisition']))
		{
                    
                    $model->attributes=$_POST['Requisition'];
                    
                    $files = CUploadedFile::getInstancesByName('Requisition[additional_documents]');
                    if (isset($files) && count($files) > 0){
                    $dcount = 1;
                    $model->additional_documents = $dcount;
                    }
                    $model->user_id = $userid;
                    if($model->save()){
                        $reqId = $model->ID;
                                               
                        if(isset($_POST['Requisition']['Approver'])&& !empty($_POST['Requisition']['Approver'])){
                            foreach($_POST['Requisition']['Approver'] as $app){
                                if(!empty($app)){
                                        $reqapp = new RequisitionApprover;
                                        $req->account_id = $accountid;
                                        $req->user_id = $app;
                                        $req->req_id = $reqId;
                                        $req->save(false);
                                    }
                            }

                        }
                    if (isset($files) && count($files) > 0) {
                        $dataFiles = array();
                        foreach ($files as $image => $pic) {
                                $ext = explode('/',$pic->type);
                                $ext = $ext[1];

                            $filename = $userid.rand(99999,100000).'.'.$ext;
                            if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/uploads/reqdocs/'.$filename)) {
                                // add it to the main model now

                                $document  = new Documents;
                                $document->uid =   $userid;
                                $document->name =   $filename;
                                $document->typeoffile =   $pic->type;
                                $document->url =   'uploads/reqdocs';
                                $document->origname =   $pic->name;
                                $document->size =   $pic->size;
                                $document->save(false);
                                $docid = $document->ID;

                                $reqDoc = new RequisitionDocument;
                                $reqDoc->uid = $userid;
                                $reqDoc->did = $docid;
                                $reqDoc->rid = $reqId;
                                $reqDoc->save(false);

                            }
                            else{
                                echo 'Cannot upload!';
                            }
                        }
                    }
                    }

                    $this->redirect(array('update','id'=>$model->ID));
				
		}
                $userModel = Users::model()->findByPk($model->user_id);

		$this->render('update',array(
                    'model'=>$model,
                    'reqDocs'=>$reqDocs,
                    'user'=>$userModel,
                    'approver'=>$approver,
		));
	}
	public function actionupdateStatus()
	{
		
		if(isset($_POST))
		{
                    $data['status'] = false;
			$id= $_POST['id'];
			$status= $_POST['status'];
                        
			$model=$this->loadModel($id);
                        if($status==1)
                            $status='APPROVED';
                        
                        $model->status = $status;
                            if($model->save())
                               $data['status']=true ;
                            
                    if($status=='RFI'||$status=='RFQ'||$status=='AUCTION'){
                        $requisition = Requisition::model()->findByPk($id);
                       $userid = Yii::app()->user->ID;
                       $accountid = Yii::app()->user->accountId;
                        $request = new Requests;
                        $request->name = $requisition->name;
                        $request->description = $requisition->description;
                        $request->uid = $userid;
                        $request->accid = $accountid;
                        $request->createdat = date('Y-m-d H:i:s');
                        $request->save(false);
                        $requestUrl = $request->ID;
                        if($status=='RFI')
                        $data['redURL'] = Yii::app()->createUrl('requests/rfi',array('event'=>$requestUrl));
                        if($status=='RFQ')
                        $data['redURL'] = Yii::app()->createUrl('requests/rfq',array('event'=>$requestUrl));
                        if($status=='AUCTION')
                        $data['redURL'] = Yii::app()->createUrl('requests/auction',array('event'=>$requestUrl));
                        
                    }
                           echo json_encode($data);exit;
		}
	}

        
	public function actionDeleteReq()
	{
		
		if(isset($_POST))
		{
//                    echo 123;exit;
                    $data['status']=false ;
                    $id= $_POST['id'];
                    $reqDoc = RequisitionDocument::model()->findAllByAttributes(array('rid'=>$id));
                    foreach($reqDoc as $rd){
                    $rd->delete();
                    
                    }
                    if($this->loadModel($id)->delete())
                       $data['status']=true ;
                    echo json_encode($data);exit;
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Requisition');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionList()
	{
//            echo '<pre>';print_r($_POST);exit;
            $accountid = yii::app()->user->accountId;
            $model = Requisition::model()->findAllByAttributes(array('status'=>array('PENDING','APPROVED'),'account_id'=>$accountid));
            if(isset($_GET['Requisition']))
			$model->attributes=$_GET['Requisition'];
            
            $this->render('newlist',array(
                    'model'=>$model,
            ));
                
	}
	/**
	 * Approve Requisition.
	 */
	public function actionApprove()
	{
            $accountid = yii::app()->user->accountId;
            $model = Requisition::model()->findAllByAttributes(array('status'=>array('PENDING','APPROVED'),'account_id'=>$accountid));
            if(isset($_GET['Requisition']))
			$model->attributes=$_GET['Requisition'];

                $this->render('approveReq',array(
			'model'=>$model,
		));
	}
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Requisition('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Requisition']))
			$model->attributes=$_GET['Requisition'];

                 
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Requisition the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Requisition::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Requisition $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='requisition-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
