<?php

class SupplierController extends Controller
{
    public $layout='//layouts/supplier';
    public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('Index','event'),
				
			),
                    );
        }
        
	public function actionIndex()
	{
            $this->layout = 'supplier';
            
            $userid= Yii::app()->user->ID;
            //$accountid = Yii::app()->user->accountId;
                        
            $reqnotaccepted = RequestSupplier::model()->with('req')->findAllByAttributes(array('supplier_id'=>$userid));
            
            $rq_html = Core::makeGrid($reqnotaccepted);
            
            $this->render('index',array('req_html'=>$rq_html));
	}
        public function actionEvent($event,$id){
            if(isset($id)&&!empty($id))
            $supplierId = base64_decode($id);
            else 
              $supplierId = Yii::app()->user->ID;
//            echo $supplierId;exit;
            $requestId = base64_decode($event);
           
            $requestSupplier = RequestSupplier::model()->findByAttributes(array('supplier_id'=>$supplierId,'req_id'=>$requestId));
//            print_r($requestSupplier);exit;
            $model = Requests::model()->findByPk($requestId);
            if(isset($requestSupplier)){
                 $requestSupplier->viewed = 1;
            if(isset($_POST)&& !empty($_POST)){
                if(isset($_POST['accept'])&&$_POST['accept']=='Accept'){
                    $requestSupplier->accept = 1;
                }else if(isset($_POST['decline'])&&$_POST['decline']=='Decline'){
                    $requestSupplier->accept = 2;
                }
               
            } 
            $requestSupplier->save(false);
            }
            
             $units= Units::model()->findAll();
            $supReqDocs = SupplierReqDoc::model()->findAllByAttributes(array('uid'=>$supplierId,'rid'=>$requestId));
            foreach($units as $u){
                     $unitList[$u->ID]=$u->name;      
                    }
//                  exit;
                if($model->reqtype=='RFQ'){    
		$this->render('rfqevent',array(
                    'model'=>$model,
                    'requestSup'=>$requestSupplier,
                    'unitList'=>$unitList,
                    'supReqDocs'=>$supReqDocs
                ));
                }
                else if($model->reqtype=='RFI'){    
		$this->render('rfievent',array(
                    'model'=>$model,
                    'requestSup'=>$requestSupplier,
                    'unitList'=>$unitList,
                    'supReqDocs'=>$supReqDocs
                ));
                }
                else if($model->reqtype=='AUCTION'){    
		$this->render('auctionevent',array(
                    'model'=>$model,
                    'requestSup'=>$requestSupplier,
                    'unitList'=>$unitList,
                    'supReqDocs'=>$supReqDocs
                ));
                }
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}