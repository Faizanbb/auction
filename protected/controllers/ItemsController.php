<?php

class ItemsController extends Controller
{
	protected function beforeAction($action)
	{
		$uID = Yii::app()->user->id;
		$supperAdmin = Users::model()->findbyAttributes(array('role'=>'SUPPERADMIN', 'ID'=>$uID));
		if ($supperAdmin) {
			return true;
		}else{
			Yii::app()->user->setFlash('saved', 'Sorry, you are not permitted to access this page');
			Yii::app()->request->redirect(Yii::app()->user->returnUrl);
		}
	}
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','create','update','admin','delete','create_heading','attachFile','import_excel','export_csv','changes','download_template'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionChanges()
	{
		if (isset($_REQUEST['id2'])) {
			$itemID = $_REQUEST['id2'];
			$itemQty = $_REQUEST['qty2'];
			$itemItmpr = $_REQUEST['itmpr2'];
			$getItem = Yii::app()->db->createCommand()
							        ->leftjoin('units as u', 'u.ID=i.unit')
							        ->select('i.*,u.ID as unitid,u.name as unitname,u.parent')
							        ->from('items i')
						        	->where('i.ID='.$itemID)
							        ->queryRow();
			echo '
					<p style="color: #37377d;"><b>'.$getItem["name"].'</b></p>
					<p><b>Item Number :</b> '.$getItem["lotnum"].' Roll</p>
					<p><b>Unit : </b>'.$getItem["unitname"].'</p>
					<p><b>Qty : </b>'.$getItem["quantity"].'</p>
					<p><b>Spec.Files</b> <a href="#" class="btn btn-primary btn-sm">View</a></p>
					<p><b>Price :</b> AED '.$getItem["itemprice"].'</p>
				';
		}else{
			$itemID = $_REQUEST['id'];
			$itemQty = $_REQUEST['qty'];
			$itemItmpr = $_REQUEST['itmpr'];
			// echo $itemID." ".$itemQty." ".$itemItmpr;
			$getItem = Items::model()->findByPK($itemID);
			$getItem->quantity = $itemQty;
			$getItem->itemprice = $itemItmpr;
			if ($getItem->save(false))
			{
				$item = Yii::app()->db->createCommand()
							        ->leftjoin('units as u', 'u.ID=i.unit')
							        ->select('i.*,u.ID as unitid,u.name as unitname,u.parent')
							        ->from('items i')
						        	->where('i.ID='.$itemID)
							        ->queryRow();
				echo '<tr>
						<td><br><b>'.$item['name'].'</b></td>
						<td><br><b>'.$item['lotnum'].'</b></td>
						<td><br><a href="#" class="btn btn-primary btn-smm">View</a></td>
						<td><br>'.$item['unitname'].'</td>
						<td class="td"><br>
							<input type="number" name="quantity" style="display: none;">
							<span><b>'.$item['quantity'].'</b></span>
						</td>
						<td class="td">
							<a id="edit" style="cursor: pointer;">Edit</a><br>
							<input type="text" name="itemprice" style="display: none;">
							<span><b>AED '.$item['itemprice'].'</b></span>
						</td>
						<td class="savebtn" style="display: none;"><br>
							<button class="btn btn-sm btn-primary" type="submit">Save</button>
						</td>
					</tr>';
			}
		}
	}
	
	public function actionDownload_template()
	{
		$path = Yii::getPathOfAlias('webroot.uploads.items').'/items_template/template.xlsx';
		if (file_exists($path)) {
			header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
		    header('Content-Disposition: attachment; filename='.basename($path));
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($path));
		    ob_clean();
		    flush();
		    readfile($path);
		    exit;
		}
	}
	
	public function actionExport_csv()
	{
		$itemsModel = Yii::app()->db->createCommand()
							        ->from('items')
							        ->where('parentid !=0') 
							        ->queryAll();
		function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
		    // open raw memory as file so no temp files needed, you might run out of memory though
		    $f = fopen('php://memory', 'w'); 
		    // loop over the input array
		    foreach ($array as $line) { 
		        // generate csv lines from the inner arrays
		        fputcsv($f, $line, $delimiter); 
		    }
		    // reset the file pointer to the start of the file
		    fseek($f, 0);
		    // tell the browser it's going to be a csv file
		    header('Content-Type: application/csv');
		    // tell the browser we want to save it instead of displaying it
		    header('Content-Disposition: attachment; filename="'.$filename.'";');
		    // make php send the generated csv lines to the browser
		    fpassthru($f);
		}
		array_to_csv_download($itemsModel,	"download.csv");
	}
	
	public function actionImport_excel()
	{
		if (isset($_FILES) && !empty($_FILES))
		{
            $files = $_FILES['myFile'];

            $objPhpExcel = new PHPExcel();
           
            $excelReader = PHPExcel_IOFactory::createReaderForFile($files['tmp_name']);

            $excelObj = $excelReader->load($files['tmp_name']);

            $rowIterator = $excelObj->getActiveSheet()->getRowIterator();

            $sheet = $excelObj->getActiveSheet();

            $array_data = array();

			foreach($rowIterator as $row)
			{
			    $rowIndex = $row->getRowIndex();
			    $array_data[$rowIndex] = array('A'=>'', 'B'=>'','C'=>'','D'=>'','E'=>'','F'=>'','G'=>'','H'=>'','I'=>'');
				    $cell = $sheet->getCell('A' . $rowIndex);
				    $array_data[$rowIndex]['A'] = $cell->getCalculatedValue();
				    $cell = $sheet->getCell('B' . $rowIndex);
				    $array_data[$rowIndex]['B'] = $cell->getCalculatedValue();
				    $cell = $sheet->getCell('C' . $rowIndex);
				    $array_data[$rowIndex]['C'] = $cell->getCalculatedValue();
				    $cell = $sheet->getCell('D' . $rowIndex);
				    $array_data[$rowIndex]['D'] = $cell->getCalculatedValue();
				    $cell = $sheet->getCell('E' . $rowIndex);
				    $array_data[$rowIndex]['E'] = $cell->getCalculatedValue();
				    $cell = $sheet->getCell('F' . $rowIndex);
				    $array_data[$rowIndex]['F'] = $cell->getCalculatedValue();
				    $cell = $sheet->getCell('G' . $rowIndex);
				    $array_data[$rowIndex]['G'] = $cell->getCalculatedValue();
				    $cell = $sheet->getCell('H' . $rowIndex);
				    $array_data[$rowIndex]['H'] = $cell->getCalculatedValue();
				    $cell = $sheet->getCell('I' . $rowIndex);
				    $array_data[$rowIndex]['I'] = $cell->getCalculatedValue();
			}

			// echo "<pre>";
			// print_r($array_data);exit;

			// Count # of uploaded files in array
			// $total = count($array_data);
			$uID = Yii::app()->user->id;
			$supperAdmin = Users::model()->findbyAttributes(array('role'=>'SUPPERADMIN', 'ID'=>$uID));
            $count = 1;
			foreach ($array_data as $value)
			{
			    if ($count > 1) {
					$items = new Items;

					$items->uid = $uID;
					$items->accid = $supperAdmin->accid;
					$items->lotnum = $value['A'];
					$items->name = $value['B'];
					$items->description = $value['C'];
					$items->notes = $value['D'];
					$getUnit = Units::model()->findbyAttributes(array('name'=>$value['E']));
					$items->unit = $getUnit->ID;
					$items->quantity = $value['F'];
					$items->saved = 'YES';
					$items->status = 1;
					$items->itemprice = $value['G'];
					$items->unit_label = $value['H'];
					$items->unit_amount = $value['I'];
	                $items->save(false); 
					// echo "<pre>";
					// print_r($items);exit;
			    }
			    $count ++;
			}
			Yii::app()->user->setFlash('saved', 'Excel file successfully imported.');
			Yii::app()->request->redirect(Yii::app()->request->urlReferrer);
		}
		$this->render('importFromExcel');
	}

	// ATTACH NEW FILE, IF THERE IS NO FILE WITH ITEM
	public function actionAttachFile($id)
	{
		$uID = Yii::app()->user->id;
		$supperAdmin = Users::model()->findbyAttributes(array('role'=>'SUPPERADMIN', 'ID'=>$uID));

		$getItem = Yii::app()->db->createCommand()
						        ->leftjoin('units as u', 'u.ID=i.unit')
						        ->select('i.*,u.ID as unitid,u.name as unitname,u.parent')
						        ->from('items i')
					        	->where('i.ID='.$id)
						        ->queryRow();
		$getItemDocs = Yii::app()->db->createCommand()
					        ->join('documents as d', 'd.ID=itd.doc_id')
					        ->select('itd.item_id,itd.doc_id,d.ID as did,d.name as dname,d.origname,d.typeoffile')
					        ->from('item_documents as itd')
					        ->where('itd.item_id='.$id)
					        ->queryAll();
		if ($supperAdmin) {
			if (isset($_FILES) && !empty($_FILES)) {

				$path = Yii::app()->urlManager->parseUrl(Yii::app()->request); // return only last controller and action (controller/action)

				// Count # of uploaded files in array
				$total = count($_FILES['fileToUpload']['name']);

				for ($i=0; $i < $total; $i++)
				{
					$documents = new Documents;
					$rand = rand(000000, 999999).'.'.pathinfo($_FILES['fileToUpload']['name'][$i], PATHINFO_EXTENSION);
                    $documents->uid = $uID;
                    $documents->name = $rand;
                    $documents->url = $path;
                    $documents->typeoffile = $_FILES['fileToUpload']['type'][$i];
                    $documents->origname = $_FILES['fileToUpload']['name'][$i];
                    $documents->size = $_FILES['fileToUpload']['size'][$i];

                    move_uploaded_file($_FILES['fileToUpload']['tmp_name'][$i], Yii::getPathOfAlias('webroot.uploads.items').'/'.$rand);
                    if($documents->save()){
                    	// insert data into ItemDocuments
	                    $itemDocs = new ItemDocuments;
	                    $itemDocs->item_id = $id;
	                    $itemDocs->doc_id = $documents->ID;
	                    $itemDocs->save(); 
					}
				}
				$this->render('view',array(
					'model'=>$this->loadModel($id),
					'getItem'=>$getItem,
					'getItemDocs'=>$getItemDocs,
				));
			}
			$this->render('attachFile');
		}
	}

	public function actionSearch()
	{
		$query = $_GET['query'];

		if(strlen($query) >= 1)
		{
			$sql="SELECT * FROM items WHERE  name LIKE '%" . $query . "%' OR unit LIKE '%" . $query  ."%' OR itemprice LIKE '%" . $query  ."%' OR quantity LIKE '%" . $query  ."%' OR lotnum LIKE '%" . $query  ."%' ";
			$items = Yii::app()->db->createCommand($sql)->queryAll();
			$this->render('index',array(
				'items'=>$items,
			));
		}else{
			Yii::app()->user->setFlash('error', 'At least enter one digit!');
			Yii::app()->request->redirect(Yii::app()->request->urlReferrer);
		}
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$uID = Yii::app()->user->id;
		$supperAdmin = Users::model()->findbyAttributes(array('role'=>'SUPPERADMIN', 'ID'=>$uID));

		$getItem = Yii::app()->db->createCommand()
								        ->leftjoin('units as u', 'u.ID=i.unit')
								        ->select('i.*,u.ID as unitid,u.name as unitname,u.parent')
								        ->from('items i')
							        	->where('i.ID='.$id)
								        ->queryRow();
		// echo "<pre>";
		// print_r($getItem['unitname']);exit;
		$getItemDocs = Yii::app()->db->createCommand()
							        ->join('documents as d', 'd.ID=itd.doc_id')
							        ->select('itd.item_id,itd.doc_id,d.ID as did,d.name as dname,d.origname,d.typeoffile')
							        ->from('item_documents as itd')
							        ->where('itd.item_id='.$id)
							        ->queryAll();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if ($supperAdmin) {
			$this->render('view',array(
				'model'=>$this->loadModel($id),
				'getItem'=>$getItem,
				'getItemDocs'=>$getItemDocs,
			));
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate_heading()
	{
		$uID = Yii::app()->user->id;
		$supperAdmin = Users::model()->findbyAttributes(array('role'=>'SUPPERADMIN', 'ID'=>$uID));
		$model=new Items;

		if ($supperAdmin)
		{
			if(isset($_POST['Items']))
			{
				$model->attributes=$_POST['Items'];
				$model->description=$_POST['Items']['description'];
				$model->notes=$_POST['Items']['notes'];
				$model->uid = $uID;
				$model->accid = $supperAdmin->accid;
				$model->parentid = 0;
				$model->lotnum = 0;
				$model->unit = 0;
				$model->quantity = 0;
				$model->saved = 'YES';
				$model->status = 0;
				if($model->save()){
					$this->redirect(array('admin'));
				}
			}

			$this->render('createHeading',array(
				'model'=>$model
			));
		}
	}

	public function actionCreate()
	{
		$uID = Yii::app()->user->id;
		$supperAdmin = Users::model()->findbyAttributes(array('role'=>'SUPPERADMIN', 'ID'=>$uID));
		// Get all units and put in multi dimensional array
		$units= Units::model()->findAll();
        foreach($units as $u) {
        	if($u->parent==0) {
	            foreach ($units as $us) {
	                if($us->parent==$u->ID) {
	                	$unitList[$u->name][$us->ID]=$us->name;        
	                }
	            }
            }
        }
        // Get all categories and put it into array
        $categories= Categories::model()->findAll();
        foreach ($categories as $cat) {
        	$categoriesList[$cat->ID] = $cat->name;
        }
		// Get all suppliers
		$suppliers = Yii::app()->db->createCommand()
									->join('users u', 'sa.user_id = u.ID')
									->select('u.ID, u.name, sa.ID as sid, sa.user_id')
							        ->from('supplier_accounts sa')
							        ->queryAll();
		$model=new Items;

		if ($supperAdmin)
		{
			if(isset($_POST['Items']))
			{
                // ITEM DATA
				$model->uid = $uID;
				$model->accid = $supperAdmin->accid;
				$model->attributes=$_POST['Items'];
				$model->saved = 'YES';
				$model->status = $_POST['status'];

				if($model->save())
				{
					// take last inserted item id
					$modelID = $model->ID;
					// Save item into CategoriesItems table
					$categoriesItems = new CategoriesItems;
					$categoriesItems->cat_id = $model->parentid;
					$categoriesItems->item_id = $modelID;
					$categoriesItems->save();

					// $path = Yii::app()->getBaseUrl(true); // Get URL
					$path = Yii::app()->urlManager->parseUrl(Yii::app()->request); // return only last controller and action (controller/action)

	                $docsArray = ['application/pdf','image/png','image/jpg','image/jpeg','image/gif','image/bmp'];
	                $imgArray = ['image/png','image/jpg','image/jpeg','image/gif','image/bmp'];
	                
						
					// Insert data into supplier_items table
					if (isset($_POST['suppliers']))
					{
						foreach ($_POST['suppliers'] as $sup)
						{
							$suppItemModel=new SupplierItems;
							$suppItemModel->item_id = $modelID;
							$suppItemModel->supplier_id = $sup;
							$suppItemModel->item_num = $model->lotnum;
							$suppItemModel->price = $model->itemprice;
							$suppItemModel->description = $model->description;
							$suppItemModel->save();
						}
					}

	                if(isset($_FILES['item_docs']))
	                {
	                	// Count # of uploaded files in array
						$total = count($_FILES['item_docs']['name']);

						for ($i=0; $i < $total; $i++)
						{
							$docsModel1 = new Documents;
							$rand = rand(000000, 999999).'.'.pathinfo($_FILES['item_docs']['name'][$i], PATHINFO_EXTENSION);
		                    $docsModel1->uid = $uID;
		                    $docsModel1->name = $rand;
		                    $docsModel1->url = $path;
		                    $docsModel1->typeoffile = $_FILES['item_docs']['type'][$i];
		                    $docsModel1->origname = $_FILES['item_docs']['name'][$i];
		                    $docsModel1->size = $_FILES['item_docs']['size'][$i];

	                        move_uploaded_file($_FILES['item_docs']['tmp_name'][$i], Yii::getPathOfAlias('webroot.uploads.items').'/'.$rand);
	                        $docsModel1->save(); // DOCUMENT SAVED

	                        $itemDocsModel1 = new ItemDocuments;
	                        $itemDocsModel1->item_id = $modelID;
	                        $itemDocsModel1->doc_id = $docsModel1->ID;
	                        $itemDocsModel1->save(); 
						}
	                }
	                if(isset($_FILES['item_image']))
	                {
	                	// Count # of uploaded files in array
						$total = count($_FILES['item_image']['name']);

						for ($i=0; $i < $total; $i++) 
						{
							$docsModel2 = new Documents;
		                	$rand = rand(000000, 999999).'.'.pathinfo($_FILES['item_image']['name'][$i], PATHINFO_EXTENSION);
		                	$docsModel2->uid = $uID;
		                    $docsModel2->name = $rand;
		                    $docsModel2->url = $path;
		                    $docsModel2->typeoffile = $_FILES['item_image']['type'][$i];
		                    $docsModel2->origname = $_FILES['item_image']['name'][$i];
		                    $docsModel2->size = $_FILES['item_image']['size'][$i];

	                        move_uploaded_file($_FILES['item_image']['tmp_name'][$i], Yii::getPathOfAlias('webroot.uploads.items').'/'.$rand);
	                        $docsModel2->save();

							$itemDocsModel2 = new ItemDocuments;
	                        
	                        $itemDocsModel2->item_id = $modelID;
	                        $itemDocsModel2->doc_id = $docsModel2->ID;
	                        $itemDocsModel2->save();
						}
	                }

					if($model->saved == 'YES'){
						$this->redirect(array('admin'));
					}
				}
			}

			$this->render('create',array(
				'model'=>$model,
				'unitList'=>$unitList,
				'categoriesList'=>$categoriesList,
				'suppliers'=>$suppliers,
			));
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$uID = Yii::app()->user->id;
		$supperAdmin = Users::model()->findbyAttributes(array('role'=>'SUPPERADMIN', 'ID'=>$uID));
		// Get all supplers
		$suppliers = Users::model()->findAllbyAttributes(array('role'=>'SUPPLIER'));
		// Get all selected suppliers and put into an array
		$getSuppliers = Yii::app()->db->createCommand()
							        ->from('supplier_items')
							        ->where('item_id='.$id)
							        ->queryAll();
		$checkedSup = array();
      	foreach ($getSuppliers as $value) {
      		$checkedSup[] = $value['supplier_id'];
      	}
      	// Get all units and put in multi dimensional array
 		$units= Units::model()->findAll();
        foreach($units as $u) {
        	if($u->parent==0) {
	            foreach ($units as $us) {
	                if($us->parent==$u->ID) {
	                	$unitList[$u->name][$us->ID]=$us->name;        
	                }
	            }
            }
        }
		// Get all categories
		$categories= Categories::model()->findAll();
        foreach ($categories as $cat) {
        	$categoriesList[$cat->ID] = $cat->name;
        }
        // Get selected item
		$model=$this->loadModel($id);
		// Get all documents, images
		$getItemDocs = Yii::app()->db->createCommand()
							        ->join('item_documents as itd', 'i.ID=itd.item_id')
							        ->join('documents as d', 'd.ID=itd.doc_id')
							        ->select('i.ID,i.status,itd.item_id,itd.doc_id,d.ID as did,d.name as dname,d.origname,d.typeoffile')
							        ->from('items as i')
							        ->where('i.ID='.$id)
							        ->queryAll();
		if ($supperAdmin) {
			if (isset($_POST['Items']))
			{
                // ITEM DATA
				$model->attributes=$_POST['Items'];
				$model->status = $_POST['status'];

				if ($model->save())
				{
					// take last inserted item id
					$modelID = $model->ID;
					// Get CategoriesItem whose item id is already stored
					$categoriesItems = CategoriesItems::model()->findbyAttributes(array('item_id'=>$id));
					// If the incoming parentid(selected category) has updated, then update the cat_id as well
					if (isset($categoriesItems) && !empty($categoriesItems) && $categoriesItems->cat_id != $model->parentid)
					{
						$categoriesItems->cat_id = $model->parentid;
						$categoriesItems->save();
					}else{
						$categoriesItems = new CategoriesItems;
						$categoriesItems->cat_id = $model->parentid;
						$categoriesItems->item_id = $modelID;
						$categoriesItems->save();
					}

					// Insert data into supplier_items table
					if (isset($_POST['suppliers']))
					{
						foreach ($_POST['suppliers'] as $sup)
						{
							$suppItemModel = SupplierItems::model()->findbyAttributes(array('item_id'=>$modelID, 'supplier_id'=>$sup));
							// Insert only those supplers which are not already exists
							if (empty($suppItemModel)) {
								$suppItemModel=new SupplierItems;
								$suppItemModel->item_id = $modelID;
								$suppItemModel->supplier_id = $sup;
								$suppItemModel->item_num = $model->lotnum;
								$suppItemModel->price = $model->itemprice;
								$suppItemModel->description = $model->description;
								$suppItemModel->save();
							}
						}
					}

					// return only last controller and action (controller/action)
					$path = Yii::app()->urlManager->parseUrl(Yii::app()->request);

	                if(isset($_FILES['item_docs']))
	                {
	                	$documentsName = $_FILES['item_docs']['name'];
	                	// Count # of uploaded files in array
						$total = count($documentsName);

						for ($i=0; $i < $total; $i++)
						{
							$docsModel1 = Yii::app()->db->createCommand()
												        ->leftjoin('documents as d', 'd.ID=itd.doc_id')
												        ->select('itd.*,d.origname,d.ID as did')
												        ->from('item_documents as itd')
												        ->where('d.origname='."'".$documentsName[$i]."'")
												        ->andWhere('itd.item_id='.$modelID)
												        ->queryRow();
							// Insert only those documents which are not already exists
							if (empty($docsModel1)) {
								$docsModel1 = new Documents;
								$rand = rand(000000, 999999).'.'.pathinfo($documentsName[$i], PATHINFO_EXTENSION);
			                    $docsModel1->uid = $uID;
			                    $docsModel1->name = $rand;
			                    $docsModel1->url = $path;
			                    $docsModel1->typeoffile = $_FILES['item_docs']['type'][$i];
			                    $docsModel1->origname = $documentsName[$i];
			                    $docsModel1->size = $_FILES['item_docs']['size'][$i];

		                        move_uploaded_file($_FILES['item_docs']['tmp_name'][$i], Yii::getPathOfAlias('webroot.uploads.items').'/'.$rand);
		                        $docsModel1->save(); // DOCUMENT SAVED

		                        $itemDocsModel1 = new ItemDocuments;
		                        $itemDocsModel1->item_id = $modelID;
		                        $itemDocsModel1->doc_id = $docsModel1->ID;
		                        $itemDocsModel1->save(); 
							}
						}
	                }
	                if(isset($_FILES['item_image']))
	                {
	                	$itemImages = $_FILES['item_image']['name'];
	                	// Count # of uploaded files in array
						$total = count($itemImages);

						for ($i=0; $i < $total; $i++) 
						{
							$docsModel2 = Yii::app()->db->createCommand()
												        ->leftjoin('documents as d', 'd.ID=itd.doc_id')
												        ->select('itd.*,d.origname,d.ID as did')
												        ->from('item_documents as itd')
												        ->where('d.origname='."'".$itemImages[$i]."'")
												        ->andWhere('itd.item_id='.$modelID)
												        ->queryRow();
							// Insert only those photos which are not already exists
							if (empty($docsModel2)) {
								$docsModel2 = new Documents;
			                	$rand = rand(000000, 999999).'.'.pathinfo($itemImages[$i], PATHINFO_EXTENSION);
			                	$docsModel2->uid = $uID;
			                    $docsModel2->name = $rand;
			                    $docsModel2->url = $path;
			                    $docsModel2->typeoffile = $_FILES['item_image']['type'][$i];
			                    $docsModel2->origname = $itemImages[$i];
			                    $docsModel2->size = $_FILES['item_image']['size'][$i];

		                        move_uploaded_file($_FILES['item_image']['tmp_name'][$i], Yii::getPathOfAlias('webroot.uploads.items').'/'.$rand);
		                        $docsModel2->save();

								$itemDocsModel2 = new ItemDocuments;
		                        
		                        $itemDocsModel2->item_id = $modelID;
		                        $itemDocsModel2->doc_id = $docsModel2->ID;
		                        $itemDocsModel2->save();
							}
						}
	                }
                }

				if($model->saved == 'YES'){
					$this->redirect(array('admin'));
				}
			}
			$this->render('updateSubItems',array(
				'model'=>$model,
				'categoriesList'=>$categoriesList,
				'getItemDocs'=>$getItemDocs,
				'suppliers'=>$suppliers,
				'checkedSup'=>$checkedSup,
				'unitList'=>$unitList,
			));
		}
	}

	public function actionRemove($id)
	{
		$doc = Documents::model()->findbyAttributes(array('ID' => $id));
		$path = Yii::getPathOfAlias('webroot.uploads.items').'/'.$doc->name;
		if (file_exists($path)) {
			unlink($path);
		}
		ItemDocuments::model()->findbyAttributes(array('doc_id' => $id))->delete();
		Documents::model()->findbyAttributes(array('ID' => $id))->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])){
			// Yii::app()->user->setFlash('error', 'Foreign key constraints error!');
			Yii::app()->request->redirect(Yii::app()->request->urlReferrer);
			// $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$item = Yii::app()->db->createCommand()
							        ->join('item_documents as itd', 'i.ID=itd.item_id')
							        ->join('documents as d', 'd.ID=itd.doc_id')
							        ->select('i.*,itd.ID as itdocID,itd.item_id,itd.doc_id,d.ID as dID,d.name as dname,d.origname,d.typeoffile')
							        ->from('items as i')
							        ->where('i.ID='.$id)
							        ->queryAll();
		foreach ($item as $i) {
			$path = Yii::getPathOfAlias('webroot.uploads.items').'/'.$i['dname'];
			if (file_exists($path)) {
				unlink($path);
			}
			if (!empty($i['dID'])) {
				ItemDocuments::model()->findbyAttributes(array('doc_id' => $i['dID']))->delete();
				Documents::model()->findbyAttributes(array('ID' => $i['dID']))->delete();
			}
		}
		$supItems = Yii::app()->db->createCommand()
							        ->join('supplier_items as si', 'si.item_id=i.ID')
							        ->select('i.ID,si.item_id')
							        ->from('items as i')
							        ->where('i.ID='.$id)
							        ->queryAll();
		foreach ($supItems as $s) {
			SupplierItems::model()->findbyAttributes(array('item_id' => $s['item_id']))->delete();
		}
        // echo "<pre>";
        // print_r($supItems);exit;
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax'])){
			Yii::app()->request->redirect(Yii::app()->request->urlReferrer);
			// $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$uID = Yii::app()->user->id;
		$supperAdmin = Users::model()->findbyAttributes(array('role'=>'SUPPERADMIN', 'ID'=>$uID));
		$suppliers = Yii::app()->db->createCommand()
									->join('users u', 'sa.user_id = u.ID')
									->select('u.ID, u.name, sa.ID as sid, sa.user_id')
							        ->from('supplier_accounts sa')
							        ->queryAll();
		if ($supperAdmin) 
		{
			// Get all items assigned to a supplier
			if (isset($_GET['supId']) && !empty($_GET['supId']))
			{
				// Get supplier id from url
				$supID = $_REQUEST['supId'];
				$items = Yii::app()->db->createCommand()
								        ->leftjoin('supplier_items si', 'si.item_id = i.ID')
								        ->leftjoin('units as u', 'u.ID=i.unit')
								        ->select('i.*,si.item_id,u.ID as unitid,u.name as unitname')
								        ->from('items i')
								        ->where('si.supplier_id='.$supID)
								        ->order('i.createdat DESC')
								        ->queryAll();
				$documents = Yii::app()->db->createCommand()
									        ->join('item_documents as itd', 'i.ID=itd.item_id')
									        ->join('documents as d', 'd.ID=itd.doc_id')
									        ->select('i.*,itd.item_id,itd.doc_id,d.ID as did,d.typeoffile')
									        ->from('items as i')
									        ->queryAll();
				$doc = array();
				$itemID = array();
				foreach ($documents as $k) 
				{
					$doc[] = $k['typeoffile'];
					$itemID[] = $k['ID'];
				}
				if (Yii::app()->request->isAjaxRequest) {
					$table = $this->renderPartial('filter',array('items'=>$items,'doc'=>$doc,'itemID'=>$itemID,'suppliers'=>$suppliers));
					echo $table;
	                Yii::app()->end();
	            }
			} 
			// Get all active or inactive items
			if (isset($_GET['activeInactive']) && !empty($_GET['activeInactive']))
			{
				$activeInactive = $_REQUEST['activeInactive'];
				if ($activeInactive == 1) {
					// GET ALL ACTIVE ITEMS
					$items = Yii::app()->db->createCommand()
									        ->leftjoin('units as u', 'u.ID=i.unit')
									        ->select('i.*,u.ID as unitid,u.name as unitname,u.parent')
									        ->from('items i')
										    ->where('i.status=1')
										    ->order('i.createdat DESC')
									        ->queryAll();
				}
				if ($activeInactive == 2) {
					// GET ALL INACTIVE ITEMS
					$items = Yii::app()->db->createCommand()
									        ->leftjoin('units as u', 'u.ID=i.unit')
									        ->select('i.*,u.ID as unitid,u.name as unitname,u.parent')
									        ->from('items i')
									        ->where('i.status=0')
										    ->order('i.createdat DESC')
									        ->queryAll();
				}
				$documents = Yii::app()->db->createCommand()
									        ->join('item_documents as itd', 'i.ID=itd.item_id')
									        ->join('documents as d', 'd.ID=itd.doc_id')
									        ->select('i.*,itd.item_id,itd.doc_id,d.ID as did,d.typeoffile')
									        ->from('items as i')
									        ->queryAll();
				$doc = array();
				$itemID = array();
				foreach ($documents as $k) 
				{
					$doc[] = $k['typeoffile'];
					$itemID[] = $k['ID'];
				}
				if (Yii::app()->request->isAjaxRequest) {
					$table = $this->renderPartial('filter',array('items'=>$items,'doc'=>$doc,'itemID'=>$itemID,'suppliers'=>$suppliers));
					echo $table;
	                Yii::app()->end();
	            }
			}
			// else{ // Else Get all companies items
				$items = Yii::app()->db->createCommand()
								        ->leftjoin('units as u', 'u.ID=i.unit')
								        ->select('i.*,u.ID as unitid,u.name as unitname,u.parent')
								        ->from('items i')
									    ->order('i.createdat DESC')
								        ->queryAll();
				$documents = Yii::app()->db->createCommand()
								        ->join('item_documents as itd', 'i.ID=itd.item_id')
								        ->join('documents as d', 'd.ID=itd.doc_id')
								        ->select('i.*,itd.item_id,itd.doc_id,d.ID as did,d.typeoffile')
								        ->from('items as i')
								        ->queryAll();
				$doc = array();
				$itemID = array();
				foreach ($documents as $k) 
				{
					$doc[] = $k['typeoffile'];
					$itemID[] = $k['ID'];
				}
				if (isset($_GET['view'])) {
					$table = $this->renderPartial('filter',array('items'=>$items,'doc'=>$doc,'itemID'=>$itemID,'suppliers'=>$suppliers));
					echo $table;
	                Yii::app()->end();
				}
				$this->render('index',array('items'=>$items,'doc'=>$doc,'itemID'=>$itemID,'suppliers'=>$suppliers));
			// }
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$uID = Yii::app()->user->id;
		$supperAdmin = Users::model()->findbyAttributes(array('role'=>'SUPPERADMIN', 'ID'=>$uID));
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if ($supperAdmin) {
			$model=new Items('search');
			$model->unsetAttributes();  // clear any default values
			if(isset($_GET['Items'])){
				$model->attributes=$_GET['Items'];
			}

			$this->render('admin',array(
				'model'=>$model,
			));
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Items the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Items::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Items $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='items-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
