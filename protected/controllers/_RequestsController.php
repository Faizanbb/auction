<?php

class RequestsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/req_layout';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view','supplierview','fillq'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','secques','index','questionnaire','list','deletereq','duplicate','publish','rfi','rfq','requestbidstatus','resendsupplierEmail','reverseauction','rfqoverview','auctionoverview'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
        public function actionresendsupplierEmail(){
            $reqSupId = $_POST['reqSupplier'];
//            echo $requestId;exit;
            $criteria = new CDbCriteria;
            $criteria->with = array('supplier','req','req.u','req.u.acc');
            $criteria->condition = "t.ID=$reqSupId";
            $request = RequestSupplier::model()->findall($criteria);
            $request= $request[0];
//            print_r($request);exit;
            $supplierEmail = $request->supplier->email;
            $supplierName = $request->supplier->name;
            $timezone = Timezone::model()->findByPk($request->req->timezone);
            $subjectMessage = "Request Email";
            $bodyMessage = $this->renderpartial('../emails/supplier/sendRequest',array('model'=>$request->req,'timezone'=>$timezone),true);
            
            $this->sendSupplierEmail($supplierEmail,$supplierName,$subjectMessage,$bodyMessage);
            
            
            exit;
        }
        public function actionDeleteReq()
	{
		
		if(isset($_POST))
		{
//                    echo 123;exit;
                    $data['status']=false ;
                    $id= $_POST['id'];
                    $reqQuest = SectionQuestions::model()->findAllByAttributes(array('rid'=>$id));
                    foreach($reqQuest as $rq){
                    $optlist = Optionlist::model()->findAllByAttributes(array('qid'=>$rq->ID));
                        foreach($optlist as $optl){
                            $optl->delete();
                        }
                    $rq->delete();
                    }
                    $optlist = DocumentRequest::model()->findAllByAttributes(array('rid'=>$id));
                        foreach($optlist as $optl){
                            $optl->delete();
                        }
                        
                    if($this->loadModel($id)->delete())
                       $data['status']=true ;
                    echo json_encode($data);exit;
		}
	}
        public function actionDuplicate(){
            $userid = Yii::app()->user->ID;
            $id = $_POST['id'];
            $criteria = new CDbCriteria;
            $criteria->with = array('documentRequests.d','requestItems.item','sectionQuestions','sectionQuestions.optionlists');
            $criteria->condition = "t.ID=$id";
            $request = Requests::model()->findall($criteria);
            $request = $request[0];
//            echo '<pre>';            print_r($request->sectionQuestions );
//            exit;
            $clone= new Requests;
            $clone->attributes = $request->attributes;
            $clone->uid = $userid;
            $clone->status = 0;
            $clone->createdat = date('Y-m-d H:i:s');
            $clone->save(false);
            $crequestid = $clone->ID;
            
            if(isset($request->sectionQuestions)&&!empty($request->sectionQuestions)){
                  
                foreach($request->sectionQuestions as $reqques){
                    
                    if($reqques->parentid==0){
                        
                        $newrequestQues= new SectionQuestions;
                        $newrequestQues->attributes  = $reqques->attributes;
                        $newrequestQues->rid = $crequestid;
                        $newrequestQues->uid = $userid;
                        $newrequestQues->createdat = date('Y-m-d H:i:s');
                        $newrequestQues->save(false);
                        $newqid = $newrequestQues->ID;
                        foreach($request->sectionQuestions as $subques){
                            if($subques->parentid==$reqques->ID){
                                
                                $newrequestQues= new SectionQuestions;
                                $newrequestQues->attributes  = $subques->attributes;
                                $newrequestQues->rid = $crequestid;
                                $newrequestQues->uid = $userid;
                                $newrequestQues->parentid = $newqid;
                                $newrequestQues->createdat = date('Y-m-d H:i:s');
                                $newrequestQues->save(false);
                                $subquesid = $newrequestQues->ID;
                    
                                if(isset($subques->optionlists)&&!empty($subques->optionlists)){
                                    foreach($subques->optionlists as $rqolist){
                                        $newoptlist = new Optionlist;
                                        $newoptlist->attributes = $rqolist->attributes;
                                        $newoptlist->qid = $subquesid;
                                        $newoptlist->save(false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        
            if(isset($request->documentRequests)&&!empty($request->documentRequests)){
                foreach($request->documentRequests as $reqitem){
                    $newrequestDoc= new DocumentRequest;
                    $newrequestDoc->attributes  = $reqitem->attributes;
                    $newrequestDoc->rid = $crequestid;
                    $newrequestDoc->uid = $userid;
                    $newrequestDoc->save(false);
                }
            }
            
            if(isset($request->requestItems)&&!empty($request->requestItems)){
                foreach($request->requestItems as $reqitem){
                    $newrequestItems = new RequestItems;
                    $newrequestItems->attributes  = $reqitem->attributes;
                    $newrequestItems->request_id = $crequestid;
                    $newrequestItems->save(false);
                }
            }
            
            $this->redirect(array('list'));
        }
        public function actionPublish(){
//            Yii::import('extensions.phpexcel.PHPExcel',true);
            $supplier = $_POST['Suppliers'];
            $accountid = Yii::app()->user->accountId;
            if(isset($supplier['requestId'])&&!empty($supplier['requestId'])){
            $requestid = $supplier['requestId'];
           
                    $criteria = new CDbCriteria;
                    $criteria->with = array('u','u.acc');
                    $criteria->condition = "t.ID=$requestid";
                    $requestD = Requests::model()->findAll($criteria);
                    $requestD= $requestD[0];
                    
                    $subjectMessage = "Request Email";
                    $timezone = Timezone::model()->findByPk($requestD->timezone);
                    
                    $bodyMessage = $this->renderpartial('../emails/supplier/sendRequest',array('model'=>$requestD,'timezone'=>$timezone),true);
               
//                echo 123;exit;
                $request = Requests::model()->findByPk($requestid);
                $request->privacy = $supplier['privacy'];
                $request->status = 1;
                $request->save(false);
                $requestType = $request->reqtype;
                if($supplier['type']=='manual'){
                    foreach($supplier['manual'] as $supplier){
                        
                        $user = Users::model()->findByAttributes(array('email'=>$supplier));
                        if(empty($user)){
                            $account = new Accounts;
                            $account->status=  'active';
                            $account->save(false);
                            $accid = $account->ID;
                            
                            $user = new Users;
                            $user->accid = $accid;
                            $user->email = $supplier;
                            $user->password = md5('1234');
                            $user->createdat = date("Y-m-d H:i:s");
                            $user->role = 'SUPPLIER';
                            $user->save(false);
                            
                            $account->uid = $user->ID;
                            $account->save(false);
                            
                            $supplierAccount = new SupplierAccounts;
                            $supplierAccount->acc_id = $accountid;
                            $supplierAccount->user_id = $user->ID;
                            $supplierAccount->save(false);
                            
                        
                        }
                        $supplierId = $user->ID;
                        
                        
                        $reqSupplier = new RequestSupplier;
                        $reqSupplier->supplier_id = $supplierId;
                        $reqSupplier->req_id = $requestid;
                        $reqSupplier->save(false);
                        
                        $supplierEmail = $user->email;
                        $supplierName = $user->name;
                    if(isset($supplier['cusEmail']) && !empty($supplier['cusEmail']) && $supplier['cusEmail']=='yes'){
                    
                        $subjectMessage = $supplier['subject'];
                        $bodyMessage = $supplier['emailbody'];
                        $participationlink =Yii::app()->createAbsoluteUrl('requests/supplierview',array('id'=> base64_encode($user->ID),'event'=> base64_encode($requestid)),'http');

                        $bodyMessage = str_replace('{want_to_participate}', $participationlink, $bodyMessage);
                        $bodyMessage = str_replace('{dont_want_to_participate}', $participationlink, $bodyMessage);

    //                   print_r($bodyMessage); exit;
                    }
                        $this->sendSupplierEmail($supemail,$supname,$subjectMessage,$bodyMessage);
                    }
                }
                else if($supplier['type']=='pickgroup'){
                
               if(isset($supplier['pickgroup'])&&!empty($supplier['pickgroup'])){
                $suppliersArray = array();
                   foreach($supplier['pickgroup'] as $pikgpId){
                       $groupSuppliers = SupplierGroupRel::model()->findAllByAttributes(array('supgroup_id'=>$pikgpId));
                       foreach ($groupSuppliers as $gs){
                           $suppliersArray[] = $gs->supplier_id;
                       }
                    }
                    
                    $suppliersIdArray = array_unique($suppliersArray);
                    foreach($suppliersIdArray as $supplierId){
                        $suplierData = Users::model()->findByPk($supplierId);
                        
                        $supplierEmail = $suplierData->email;
                        $supplierName = $suplierData->name;
                        $supplierId = $suplierData->ID;
                        
                        $reqSupplier = new RequestSupplier;
                        $reqSupplier->supplier_id = $supplierId;
                        $reqSupplier->req_id = $requestid;
                        $reqSupplier->save(false);
                    if(isset($supplier['cusEmail']) && !empty($supplier['cusEmail']) && $supplier['cusEmail']=='yes'){
                    
                        $subjectMessage = $supplier['subject'];
                        $bodyMessage = $supplier['emailbody'];
                        $participationlink =Yii::app()->createAbsoluteUrl('requests/supplierview',array('id'=>base64_encode($supplierId),'event'=> base64_encode($requestid)),'http');

                        $bodyMessage = str_replace('{want_to_participate}', $participationlink, $bodyMessage);
                        $bodyMessage = str_replace('{dont_want_to_participate}', $participationlink, $bodyMessage);

    //                   print_r($bodyMessage); exit;
                    }
                      $this->sendSupplierEmail($supplierEmail,$supplierName,$subjectMessage,$bodyMessage);
                    }
               }
               
            }
                else if($supplier['type']=='pickdatabase'){
                    if(isset($supplier['pfpd'])&&!empty($supplier['pfpd'])){
 //                   print_r($supplier['pfpd']);exit;
                    foreach($supplier['pfpd'] as $supId){
                        $suplierData = Users::model()->findByPk($supId);

                        $supplierEmail = $suplierData->email;
                        $supplierName = $suplierData->name;
                        $supplierId = $suplierData->ID;

                        $reqSupplier = new RequestSupplier;
                        $reqSupplier->supplier_id = $supplierId;
                        $reqSupplier->req_id = $requestid;
                        $reqSupplier->save(false);
                        if(isset($supplier['cusEmail']) && !empty($supplier['cusEmail']) && $supplier['cusEmail']=='yes'){
                    
                        $subjectMessage = $supplier['subject'];
                        $bodyMessage = $supplier['emailbody'];
                        $participationlink =Yii::app()->createAbsoluteUrl('requests/supplierview',array('id'=>base64_encode($supplierId),'event'=> base64_encode($requestid)),'http');

                        $bodyMessage = str_replace('{want_to_participate}', $participationlink, $bodyMessage);
                        $bodyMessage = str_replace('{dont_want_to_participate}', $participationlink, $bodyMessage);

    //                   print_r($bodyMessage); exit;
                    }
                      $this->sendSupplierEmail($supplierEmail,$supplierName,$subjectMessage,$bodyMessage);
                    }
                }
               
                }
                else if($supplier['type']=='upload'){
                    $files = CUploadedFile::getInstancesByName('Suppliers[uploaddoc]');
//                    echo '<pre>'; print_r($files);exit;
                    $files = $files[0];
                    $objPhpExcel = new PHPExcel();
                   
                    $excelReader = PHPExcel_IOFactory::createReaderForFile($files->tempName);;
                    $excelObj = $excelReader->load($files->tempName);
                    $worksheet = $excelObj->getSheet(0);
                    $lastRow = $worksheet->getHighestRow();
//                    echo '<pre>';                    print_r($worksheet);exit;
                    for ($row = 1; $row < $lastRow; $row++) {
                        
                        $supplierName  =  $worksheet->getCell('A'.$row)->getValue();

                        $supplierEmail =  $worksheet->getCell('B'.$row)->getValue();

                        $supplierComp =  $worksheet->getCell('C'.$row)->getValue();
                        
                        if($this->checkEmail($supplierEmail)){
                        
                        $user = Users::model()->findByAttributes(array('email'=>$supplierEmail));
                       
                        if(empty($user)){
                            $account = new Accounts;
                            $account->status=  'active';
                            $account->save(false);
                            $accountid = $account->ID;
                             
                            $user = new Users;
                            $user->accid = $accountid;
                            $user->email = $supplierEmail;
                            $user->password = md5('1234');
                            $user->createdat = date("Y-m-d H:i:s");
                            $user->role = 'SUPPLIER';
                            $user->save(false);
                           
                            $account->uid = $user->ID;
                            $account->save(false);
                            
                            $supplierAccount = new SupplierAccounts;
                            $supplierAccount->acc_id = $accountid;
                            $supplierAccount->user_id = $user->ID;
                            $supplierAccount->save(false);
                            
                        
                        }
                        $supplierId = $user->ID;
                        
                        
                        $reqSupplier = new RequestSupplier;
                        $reqSupplier->supplier_id = $supplierId;
                        $reqSupplier->req_id = $requestid;
                        $reqSupplier->save(false);
                        
                        $supplierEmail = $user->email;
                        $supplierName = $user->name;
                        if(isset($supplier['cusEmail']) && !empty($supplier['cusEmail']) && $supplier['cusEmail']=='yes'){

                            $subjectMessage = $supplier['subject'];
                            $bodyMessage = $supplier['emailbody'];
                            $participationlink =Yii::app()->createAbsoluteUrl('requests/supplierview',array('id'=>base64_encode($supplierId),'event'=> base64_encode($requestid)),'http');

                            $bodyMessage = str_replace('{want_to_participate}', $participationlink, $bodyMessage);
                            $bodyMessage = str_replace('{dont_want_to_participate}', $participationlink, $bodyMessage);

        //                   print_r($bodyMessage); exit;
                        }
                        $this->sendSupplierEmail($supemail,$supname,$subjectMessage,$bodyMessage);
                            
                        }
                             
                    }

                    
                }
                

            }
            
           $saveEmail = new RequestInvitationMessage;
           $saveEmail->uid= Yii::app()->user->ID;
           $saveEmail->rid= $requestid;
           $saveEmail->name= $subjectMessage;
           $saveEmail->email_message= $bodyMessage;
           $saveEmail->createdat = date('Y-m-d H:i:s');
           $saveEmail->save(false);
            Yii::app()->user->setFlash('success', "your request for $requestType is published");
            $this->redirect(array('list'));
        }
        protected  function checkEmail($email) {
            if ( strpos($email, '@') !== false ) {
               $split = explode('@', $email);
               return (strpos($split['1'], '.') !== false ? true : false);
            }
            else {
               return false;
            }
         }
        protected function sendSupplierEmail($vendorEmail,$vendorName,$subject,$body)
        {

            $url     = 'https://api.sendgrid.com/';
            $user    = 'businessbid';
            $pass    = '!3zxih1x0';
            


            $message = array(
                        'api_user'  => $user,
                        'api_key'   => $pass,
                        'to'        => $vendorEmail,
                        'subject'   => $subject,
                        'html'      => $body,
                        'text'      => $body,
                        'from'      => 'support@businessbid.ae',
                    );


            $request =  $url.'api/mail.send.json';


            $sess = curl_init($request);
            curl_setopt ($sess, CURLOPT_POST, true);
            curl_setopt ($sess, CURLOPT_POSTFIELDS, $message);
            curl_setopt($sess, CURLOPT_HEADER,false);
            curl_setopt($sess, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($sess);
            curl_close($sess);
            //echo $response;exit;
        }
        
	public function actionQuestionnaire($action,$id)
	{
                $userid = Yii::app()->user->ID;
                $accountid = Yii::app()->user->accountId;
                if($action =='fetchSecQues'){
                    $questionnaireModel = SectionQuestions::model()->findAllByAttributes(array('rid'=>$id));
                    $this->renderPartial('questionnaire',array(
                            'model'=>$questionnaireModel,
                    ));
                }
                else if($action =='newSection'){
                    $model = new SectionQuestions;
                    $this->renderPartial('_newSection',array(
                            'model'=>$model,
                            'requestId'=>$id,
                    ));
                    
                }
                else if($action =='getAllItemsEdit'){
                    $request_id = $_POST['reqId'];
                    
                    $units= Units::model()->findAll();
                    foreach($units as $u){
                        if($u->parent==0){
                        foreach ($units as $us){
                            if($us->parent==$u->ID){
                            $unitList[$u->name][$us->ID]=$us->name;        
                            }
                        }
                        }

                    }
                    
                    $criteria = new CDbCriteria;
                    $criteria->with = array('reqItemDocs','reqItemDocs.doc');
                    $criteria->condition = "t.request_id =$request_id";
                     $items= RequestItems::model()->findAll($criteria);
                    
                    $this->renderPartial('_listItemsDatabase',array(
                            'unitList'=>$unitList,
                            'items'=>$items
                    ));
                    
                }
                else if($action =='getExtDataItems'){
                    $request_id = $_POST['reqId'];
                    $initCount = $_POST['initCount'];
                    $secCount = count($_POST['ditems']);
                    $units= Units::model()->findAll();
                    foreach($units as $u){
                        if($u->parent==0){
                        foreach ($units as $us){
                            if($us->parent==$u->ID){
                            $unitList[$u->name][$us->ID]=$us->name;        
                            }
                        }
                        }

                    }
//                    exit;
                    $ids = $_POST['ditems'];
                    $criteria = new CDbCriteria;
                    $criteria->with = array('itemDocuments');
                    $criteria->addInCondition('t.ID',$ids);
                      
                    $items = Items::model()->findAll($criteria);
                    
                    foreach($items as $preItem){
                       
                        $reqItem = new RequestItems;
                        $reqItem->name = $preItem->name;
                        $reqItem->request_id = $request_id;
                        $reqItem->description = $preItem->description;
                        $reqItem->unit_label = $preItem->unit_label;
                        $reqItem->item_number = $preItem->lotnum;
                        $reqItem->unit_amount = $preItem->unit_amount;
                        $reqItem->unit_id = $preItem->unit;
                        $reqItem->price = $preItem->itemprice;
                        $reqItem->quantity = $preItem->quantity;
                        $reqItem->save(false);
                        $reqItemId = $reqItem->ID;
                        $newIds[] = $reqItemId;
                        foreach($preItem->itemDocuments as $piid){
                            $reqitemdoc = new ReqItemDoc;
                            $reqitemdoc->reqitem_id = $reqItemId;
                            $reqitemdoc->doc_id = $piid->doc_id;
                            $reqitemdoc->save(false);
                        }
                        
                    }
                    
                    $criteria = new CDbCriteria;
                    $criteria->with = array('reqItemDocs','reqItemDocs.doc');
                    $criteria->addInCondition('t.ID',$newIds);
                    $items= RequestItems::model()->findAll($criteria);
                    
                    $this->renderPartial('_listItemsDatabase',array(
                            'initCount'=>$initCount,
                            'secCount'=>$secCount,
                            'unitList'=>$unitList,
                            'items'=>$items
                    ));
                    
                }
                else if($action =='getPreviewItems'){
//                      echo 123;exit;
                    $request_id = $_POST['reqId'];
                    
                   
                    
//                    echo 123;exit;
                    $criteria = new CDbCriteria;
                    $criteria->with = array('reqItemDocs','reqItemDocs.doc');
                    $criteria->condition = "t.request_id= $request_id";
                    $items= RequestItems::model()->findAll($criteria);
                     $units= Units::model()->findAll();
                    foreach($units as $u){
                     $unitList[$u->ID]=$u->name;      
                    }
//                    exit;
                    $this->renderPartial('_listItemsViewMode',array(
                            'items'=>$items,
                            'unitList'=>$unitList
                    ));
                    
                }
                else if($action =='getExtItems'){
                    $initCount = $_POST['initCount'];
                    $secCount = $_POST['secCount'];
                    $units= Units::model()->findAll();
                    foreach($units as $u){
                        if($u->parent==0){
                        foreach ($units as $us){
                            if($us->parent==$u->ID){
                            $unitList[$u->name][$us->ID]=$us->name;        
                            }
                        }
                        }

                    }
                    $this->renderPartial('_listItems',array(
                            'initCount'=>$initCount,
                            'secCount'=>$secCount,
                            'unitList'=>$unitList
                    ));
                    
                }
                else if($action =='viewAttachFile'){
                    
                    $reqItems = '';
                    if(isset( $_POST['item'])&&isset($_POST['requestid'])){
                        $itemId = $_POST['item'];
                        $requestId = $_POST['requestid'];
                        
                        $criteria = new CDbCriteria;
                        $criteria->with = array('item');
                        $criteria->condition = "t.request_id=$requestId AND t.item_id=$itemId";
                        $reqItems = RequestItems::model()->findall($criteria);
                    
                    }
                    
                    $model = new SectionQuestions;
                    $this->renderPartial('_listRequestItems',array(
                            'reqItems'=>$reqItems,
                    ));
                    
                }
                else if($action =='getNewTemplateForm'){
                    $model = new TemplateQuestionnaire;
                    $this->renderPartial('_newTemplateQuestionnaire',array(
                            'model'=>$model,
                    ));
                    
                }
                else if($action =='getQTemplates'){
                    
                    $model = TemplateQuestionnaire::model()->findAllByAttributes(array('accid'=>$accountid));
//                    print_r($model);exit;
                    $this->renderPartial('_getLoadTemplates',array(
                            'model'=>$model,
                            'requestId'=>$id,
                    ));
                    
                }
                else if($action =='savenewTemplate'){
                    
                    $model = new TemplateQuestionnaire;
                    $model->attributes = $_POST['TemplateQuestionnaire'];
                    $model->uid = $userid;
                    $model->accid= $accountid;
                    $model->createdat = date('Y-m-d H:i:s');
                    $model->save(false);
                    $temQId = $model->ID;
                    $criteria = new CDbCriteria;
                    $criteria->with = array('sectionQuestions','sectionQuestions.optionlists');
                    $criteria->condition = "t.ID=$id";
                    $request = Requests::model()->findall($criteria);
                    $request = $request[0];
//                    print_r($request); exit;
                    if(isset($request->sectionQuestions)&&!empty($request->sectionQuestions)){
                  
                        foreach($request->sectionQuestions as $reqques){

                            if($reqques->parentid==0){

                                $newrequestQues= new TemplateSectionQuestions;
                                $newrequestQues->attributes  = $reqques->attributes;
                                $newrequestQues->tempid = $temQId;
                                $newrequestQues->createdat = date('Y-m-d H:i:s');
                                $newrequestQues->save(false);
                                $newqid = $newrequestQues->ID;
                                foreach($request->sectionQuestions as $subques){
                                    if($subques->parentid==$reqques->ID){

                                        $newrequestQues= new TemplateSectionQuestions;
                                        $newrequestQues->attributes  = $subques->attributes;
                                        $newrequestQues->tempid = $temQId;
                                        $newrequestQues->parentid = $newqid;
                                        $newrequestQues->createdat = date('Y-m-d H:i:s');
                                        $newrequestQues->save(false);
                                        $subquesid = $newrequestQues->ID;

                                        if(isset($subques->optionlists)&&!empty($subques->optionlists)){
                                            foreach($subques->optionlists as $rqolist){
                                                $newoptlist = new TemplateOptionlist;
                                                $newoptlist->attributes = $rqolist->attributes;
                                                $newoptlist->qid = $subquesid;
                                                $newoptlist->save(false);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    
                }
                else if($action =='usenewTemplate'){
                    
                    $templateId = $_POST['Templates'];
                    $reqid = $_POST['reqId'];
                        if(isset($_POST['reqId']) && !empty($_POST['reqId'])){
                        $allQ = SectionQuestions::model()->findAllByAttributes(array('rid'=>$_POST['reqId']));
                        foreach($allQ as $q){
                        $OptList = Optionlist::model()->findAllByAttributes(array('qid'=>$q->ID));
                        foreach($OptList as $optlist){
                            $optlist->delete();
                        }
                            $q->delete();
                        }
                    }
                    
                    $criteria = new CDbCriteria;
                    $criteria->with = array('templateSectionQuestions','templateSectionQuestions.templateOptionlists');
                    $criteria->condition = "t.ID=$templateId";
                    $request = TemplateQuestionnaire::model()->findAll($criteria);
                   $request = $request[0];
                    echo '<pre>';            print_r($request) ;
        //            exit;

                    
                    if(isset($request->templateSectionQuestions)&&!empty($request->templateSectionQuestions)){
                        
                        foreach($request->templateSectionQuestions as $reqques){

                            if($reqques->parentid==0){

                                $newrequestQues= new SectionQuestions;
                                $newrequestQues->attributes  = $reqques->attributes;
                                $newrequestQues->rid = $reqid;
                                $newrequestQues->uid = $userid;
                                $newrequestQues->createdat = date('Y-m-d H:i:s');
                                $newrequestQues->save(false);
                                $newqid = $newrequestQues->ID;
                                foreach($request->templateSectionQuestions as $subques){
                                    if($subques->parentid==$reqques->ID){

                                        $newrequestQues= new SectionQuestions;
                                        $newrequestQues->attributes  = $subques->attributes;
                                        $newrequestQues->rid = $reqid;
                                        $newrequestQues->uid = $userid;
                                        $newrequestQues->parentid = $newqid;
                                        $newrequestQues->createdat = date('Y-m-d H:i:s');
                                        $newrequestQues->save(false);
                                        $subquesid = $newrequestQues->ID;

                                        if(isset($subques->templateOptionlists)&&!empty($subques->templateOptionlists)){
                                            foreach($subques->templateOptionlists as $rqolist){
                                                $newoptlist = new Optionlist;
                                                $newoptlist->attributes = $rqolist->attributes;
                                                $newoptlist->qid = $subquesid;
                                                $newoptlist->save(false);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    
                    
                }
                else if($action =='savenewSection'){
                    
                    $model = new SectionQuestions;
                    $model->parentid = 0;
                    $model->uid = $userid;
                    $model->attributes = $_POST['SectionQuestions'];
                    $model->createdat = date('Y-m-d H:i:s');
                    $model->save(false);
                    echo 'done';
                 
                }
                else if($action =='addQuestion'){
                    $secid = $_GET['secId'];
                    $model = new SectionQuestions;
                    $this->renderPartial('_newQuestion',array(
                        'model'=>$model,
                        'requestId'=>$id,
                        'secid'=>$secid,
                    ));
                    
                }
                else if($action =='deleteQuestion'){
                    $qid = $_GET['qId'];
                    $secid = $_GET['secId'];
                    
                    $model = SectionQuestions::model()->findByPK($qid);
                    if(!empty($model)){
                    $optionlist = Optionlist::model()->findAllByAttributes(array('qid'=>$model->ID));
                    
                    if(isset($optionlist) && !empty($optionlist)){
                        foreach($optionlist as $optlist){
                            $optlist->delete();
                        }
                    }
                    
                    $model->delete();
                    }
                    
                }
                else if($action =='editQuestion'){
                    $qid = $_GET['qId'];
                    $secid = $_GET['secId'];
                    
                    $optionList = Optionlist::model()->findAllByAttributes(array('qid'=>$qid));
                    $model = SectionQuestions::model()->findByPK($qid);
                    $this->renderPartial('_newQuestion',array(
                        'model'=>$model,
                        'requestId'=>$id,
                        'secid'=>$secid,
                        'qid'=>$qid,
                        'optionset'=>$optionList,
                    ));
                    
                }
                else if($action =='savenewQuestion'){
                    if(isset($_POST['SectionQuestions']['questid'])&&!empty($_POST['SectionQuestions']['questid']))
                    $model = SectionQuestions::model()->findByPk($_POST['SectionQuestions']['questid']);
                    else if(!isset($_POST['SectionQuestions']['questid'])&&empty($_POST['SectionQuestions']['questid']))
                    $model = new SectionQuestions;
                    
//                    echo '<pre>';print_r($_POST);exit;
                    $model->uid = $userid;
                    $model->attributes = $_POST['SectionQuestions'];
                    $model->createdat = date('Y-m-d H:i:s');
                    $model->save(false);
                    $qid = $model->ID;
                    
                    if(isset($_POST['OptionValue'])&&!empty($_POST['OptionValue'])){
                        $optionList = Optionlist::model()->findAllByAttributes(array('qid'=>$qid));
                        foreach($optionList as $qts){
                            $qts->delete();
                        }
                        if(isset($_POST['SectionQuestions']['qtype'])&&!empty(($_POST['SectionQuestions']['qtype']))&& (($_POST['SectionQuestions']['qtype'])==2||($_POST['SectionQuestions']['qtype'])==3)){
                            foreach($_POST['OptionValue'] as $ov){
                                $option = new Optionlist;
                                $option->qid = $qid;
                                $option->value = $ov;
                                $option->save(false);
                            }
                        }
                    }
                 
                }
                else if($action =='saveQuestionAnswers'){
                   
//                    $requestid = $_POST['Request']['rid'];
//                    echo '<pre>';print_r($_POST);exit;
                    $requestId  = $_POST['requestId'];
                    
                    $supplierId  = base64_decode($_POST['supplierId']);
                    $supplierId = trim($supplierId);
//                    echo $supplierId.'-';exit;
                    $itemPrices  = $_POST['Items'];
                    $questAnswers  = $_POST['Question'];
                    
                foreach ($questAnswers  as $key=>$value){
//                    echo $key.'<br>';
                    if(is_array($value))
                        $value = implode(' ,', $value);
                    $answer = new QuestionsAnswers;
                    $answer->uid= $supplierId;;
                    $answer->qid= $key;
                    $answer->ques_ans = $value;
                    $answer->rid = $requestId;
                    $answer->save(false);
                    $answerId = $answer->ID;
                    $files = CUploadedFile::getInstancesByName('Question['.$key.'][doc]');
                    
                    if (isset($files) && count($files) > 0)
                        {
                            $dataFiles = array();
                            // go through each uploaded image
                            
                            foreach ($files as $image => $pic) {
                                
                                $ext = explode('/',$pic->type);
                                $ext = $ext[1];
                                    
                                $filename = $supplierId.rand(9999,1000000).'.'.$ext;
                                if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/uploads/requests/'.$filename)) {
                                    // add it to the main model now
                                    
                                    $document  = new Documents;
                                    $document->uid =   $supplierId;
                                    $document->name =   $filename;
                                    $document->typeoffile =   $pic->type;
                                    $document->url =   'uploads/requests';
                                    $document->origname =   $pic->name;
                                    $document->size =   $pic->size;
                                    $document->save(false);
                                    $docid = $document->ID;
                                    
                                    $ansDoc = new QuestanswersDocument;
                                    $ansDoc->qans_id = $answerId;
                                    $ansDoc->doc_id = $docid;
                                    $ansDoc->save(false);
                                   
                                }
                                else{
                                    echo 'Cannot upload!';
                                }
                            }
                        }
                }
                
                foreach($itemPrices as $key=>$value){
                    $riPrice = new RequestItemPrices;
                    $riPrice->uid = $supplierId;
                    $riPrice->rid = $requestId;
                    $riPrice->reqitem_id = $key;
                    $riPrice->price = $value['price'];
                    $riPrice->save(false);
                    
                }
                        
                        echo 'done';exit;
                }
                else if($action =='saveManualQuestionAnswers'){
                   
//                    $requestid = $_POST['Request']['rid'];
//                    echo '<pre>';print_r($_POST);exit;
                    $requestId  = $_POST['requestId'];
                    
                    $supplierId  = ($_POST['supplierId']);
                   
//                    echo $supplierId.'-';exit;
                    $questAnswers  = $_POST['Question'];
                    
                foreach ($questAnswers  as $key=>$value){
//                    echo $key.'<br>';
                     
                        $value = $value['answer'];
                   
                    if(is_array($value))
                        $value = implode(' ,', $value);
                    if(isset(value['answerId'])&&!empty(value['answerId']))
                    $answer = QuestionsAnswers::model()->findByPk(value['answerId']);
                    else
                    $answer = new QuestionsAnswers;
                    
                    $answer->uid= $supplierId;;
                    $answer->qid= $key;
                    $answer->ques_ans = $value;
                    $answer->rid = $requestId;
                    $answer->save(false);
                    $answerId = $answer->ID;
                    $files = CUploadedFile::getInstancesByName('Question['.$key.'][doc]');
                    
                    if (isset($files) && count($files) > 0)
                        {
                            $dataFiles = array();
                            // go through each uploaded image
                            
                            foreach ($files as $image => $pic) {
                                
                                $ext = explode('/',$pic->type);
                                $ext = $ext[1];
                                    
                                $filename = $supplierId.rand(9999,1000000).'.'.$ext;
                                if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/uploads/requests/'.$filename)) {
                                    // add it to the main model now
                                    
                                    $document  = new Documents;
                                    $document->uid =   $supplierId;
                                    $document->name =   $filename;
                                    $document->typeoffile =   $pic->type;
                                    $document->url =   'uploads/requests';
                                    $document->origname =   $pic->name;
                                    $document->size =   $pic->size;
                                    $document->save(false);
                                    $docid = $document->ID;
                                    
                                    $ansDoc = new QuestanswersDocument;
                                    $ansDoc->qans_id = $answerId;
                                    $ansDoc->doc_id = $docid;
                                    $ansDoc->save(false);
                                   
                                }
                                else{
                                    echo 'Cannot upload!';
                                }
                            }
                        }
                }
                
                        
                        echo 'done';exit;
                }
                else if($action =='saveSupQuestionAnswers'){
                   
//                    $requestid = $_POST['Request']['rid'];
//                    echo '<pre>';print_r($_POST);exit;
                    $requestId  = $_POST['requestId'];
                    
                    $supplierId  = ($_POST['supplierId']);
                   
//                    echo $supplierId.'-';exit;
                    $questAnswers  = $_POST['Question'];
//                    echo '<pre>';                    print_r($_POST);exit;
                foreach ($questAnswers  as $key=>$value){
//                    echo $key.'<br>';
                        
                        $value = $value['answer'];
                   
                    if(is_array($value))
                        $value = implode(' ,', $value);
                    if(isset($value['answerId'])&&!empty($value['answerId']))
                    $answer = QuestionsAnswers::model()->findByPk($value['answerId']);
                    else
                    $answer = new QuestionsAnswers;
                    
                    $answer->uid= $supplierId;;
                    $answer->qid= $key;
                    $answer->ques_ans = $value;
                    $answer->rid = $requestId;
                    $answer->save(false);
                    $answerId = $answer->ID;
                    $files = CUploadedFile::getInstancesByName('Question['.$key.'][doc]');
                    
                    if (isset($files) && count($files) > 0)
                        {
                            $dataFiles = array();
                            // go through each uploaded image
                            
                            foreach ($files as $image => $pic) {
                                
                                $ext = explode('/',$pic->type);
                                $ext = $ext[1];
                                    
                                $filename = $supplierId.rand(9999,1000000).'.'.$ext;
                                if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/uploads/requests/'.$filename)) {
                                    // add it to the main model now
                                    
                                    $document  = new Documents;
                                    $document->uid =   $supplierId;
                                    $document->name =   $filename;
                                    $document->typeoffile =   $pic->type;
                                    $document->url =   'uploads/requests';
                                    $document->origname =   $pic->name;
                                    $document->size =   $pic->size;
                                    $document->save(false);
                                    $docid = $document->ID;
                                    
                                    $ansDoc = new QuestanswersDocument;
                                    $ansDoc->qans_id = $answerId;
                                    $ansDoc->doc_id = $docid;
                                    $ansDoc->save(false);
                                   
                                }
                                else{
                                    echo 'Cannot upload!';
                                }
                            }
                        }
                }
                
                        
                        echo 'done';exit;
                }
                else if($action =='saverequestDocument'){
                   
                    $requestid = $_POST['Request']['rid'];
                    $files = CUploadedFile::getInstancesByName('Documents[name]');
                    if (isset($files) && count($files) > 0)
                        {
                            $dataFiles = array();
                            // go through each uploaded image
                            
                            foreach ($files as $image => $pic) {
                                
                                $ext = explode('/',$pic->type);
                                $ext = $ext[1];
                                    
                                $filename = $userid.rand(9999,1000000).'.'.$ext;
                                if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/uploads/requests/'.$filename)) {
                                    // add it to the main model now
                                    
                                    $document  = new Documents;
                                    $document->uid =   $userid;
                                    $document->name =   $filename;
                                    $document->typeoffile =   $pic->type;
                                    $document->url =   'uploads/requests';
                                    $document->origname =   $pic->name;
                                    $document->size =   $pic->size;
                                    $document->save(false);
                                    $docid = $document->ID;
                                    
                                    $reqDoc = new DocumentRequest;
                                    $reqDoc->uid = $userid;
                                    $reqDoc->did = $docid;
                                    $reqDoc->rid = $requestid;
                                    $reqDoc->save(false);
                                   
                                }
                                else{
                                    echo 'Cannot upload!';
                                }
                            }
                        }
                        $criteria = new CDbCriteria;
                        $criteria->with = array('d');
                        $criteria->condition = "t.rid=$requestid";
                        $datadoc = DocumentRequest::model()->findall($criteria);
                        
                        $count = 0;
                        foreach($datadoc as $doc){
                            $data[$count]['ID'] = $doc->ID;
                            $data[$count]['name'] = $doc->d->origname;
                            $data[$count]['did'] = $doc->d->ID;
                            $data[$count]['src'] = Yii::app()->theme->baseUrl;
                            $count++;
                        }
                        
                        echo json_encode($data);exit;
                }
                else if($action =='saveSupDocument'){
                   
                    $requestid = $_POST['Supplier']['rid'];
                    $userid = $_POST['Supplier']['sid'];
                    
                    $files = CUploadedFile::getInstancesByName('Documents[name]');
                    if (isset($files) && count($files) > 0)
                        {
                            $dataFiles = array();
                            // go through each uploaded image
                            
                            foreach ($files as $image => $pic) {
                                
                                $ext = explode('/',$pic->type);
                                $ext = $ext[1];
                                    
                                $filename = $userid.rand(9999,1000000).'.'.$ext;
                                if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/uploads/requests/'.$filename)) {
                                    // add it to the main model now
                                    
                                    $document  = new Documents;
                                    $document->uid =   $userid;
                                    $document->name =   $filename;
                                    $document->typeoffile =   $pic->type;
                                    $document->url =   'uploads/requests';
                                    $document->origname =   $pic->name;
                                    $document->size =   $pic->size;
                                    $document->save(false);
                                    $docid = $document->ID;
                                    
                                    $reqDoc = new SupplierReqDoc;
                                    $reqDoc->uid = $userid;
                                    $reqDoc->did = $docid;
                                    $reqDoc->rid = $requestid;
                                    $reqDoc->save(false);
                                   
                                }
                                else{
                                    echo 'Cannot upload!';
                                }
                            }
                        }
                        $criteria = new CDbCriteria;
                        $criteria->condition = "t.rid=$requestid";
                        $datadoc = SupplierReqDoc::model()->findall($criteria);
                        
                        $count = 0;
                        foreach($datadoc as $doc){
                            $data[$count]['ID'] = $doc->ID;
                            $data[$count]['name'] = $doc->d->origname;
                            $data[$count]['did'] = $doc->d->ID;
                            $data[$count]['src'] = Yii::app()->theme->baseUrl;
                            $count++;
                        }
                        
                        echo json_encode($data);exit;
                }
                else if($action =='deleteDocument'){
                    $reqdid = $_POST['reqdid'];
                    $docid = $_POST['docid'];
                    $model = DocumentRequest::model()->findByPk($reqdid);
                    $model->delete();
             
                }
                else if($action =='saveRFIRequest'){
//                    print_r($_POST['Requests']);exit;
                    if(isset($_POST['Requests'])&&!empty(($_POST['Requests']))){
                        if(isset($_POST['Requests']['rid'])&&!empty($_POST['Requests']['rid'])){
                            $model= Requests::model()->findByPk($_POST['Requests']['rid']);
                        }else
                            $model = new Requests;
                           
                            $model->attributes = $_POST['Requests'];
                            $model->uid = $userid;
                            $model->accid = $accountid;
                            $model->endtime = date('Y-m-d H:i:s', strtotime($_POST['Requests']['endtime']));
                            $model->createdat = date("Y-m-d H:i:s");
                            $model->reqtype = "RFI";
                            $model->save(false);
                            
                            echo $model->ID;exit;
                }
             
                }
                else if($action =='saveRFQRequest'){
//                    print_r($_POST['Requests']);exit;
                    if(isset($_POST['Requests'])&&!empty(($_POST['Requests']))){
                        if(isset($_POST['Requests']['rid'])&&!empty($_POST['Requests']['rid'])){
                            $model= Requests::model()->findByPk($_POST['Requests']['rid']);
                        }else
                            $model = new Requests;
                           
                            $model->attributes = $_POST['Requests'];
                            $model->uid = $userid;
                            $model->accid = $accountid;
                            $model->endtime = date('Y-m-d H:i:s', strtotime($_POST['Requests']['endtime']));
                            $model->createdat = date("Y-m-d H:i:s");
                            $model->reqtype = "RFQ";
                            $model->save(false);
                            
                            echo $model->ID;exit;
                }
             
                }
                else if($action =='saveAuctionRequest'){
//                    print_r($_POST['Requests']);exit;
                    if(isset($_POST['Requests'])&&!empty(($_POST['Requests']))){
                        if(isset($_POST['Requests']['rid'])&&!empty($_POST['Requests']['rid'])){
                            $model= Requests::model()->findByPk($_POST['Requests']['rid']);
                        }else
                            $model = new Requests;
                           
                            $model->attributes = $_POST['Requests'];
                            $model->uid = $userid;
                            $model->accid = $accountid;
                            $model->createdat = date("Y-m-d H:i:s");
                            $model->reqtype = "AUCTION";
                            $model->save(false);
                            
                            echo $model->ID;exit;
                }
             
                }
                else if($action =='deleteAllQuestion'){
//                    print_r($_POST['Requests']);exit;
                    if(isset($_POST['reqId']) && !empty($_POST['reqId'])){
                        $allQ = SectionQuestions::model()->findAllByAttributes(array('rid'=>$_POST['reqId']));
                        foreach($allQ as $q){
                        $OptList = Optionlist::model()->findAllByAttributes(array('qid'=>$q->ID));
                        foreach($OptList as $optlist){
                            $optlist->delete();
                        }
                            $q->delete();
                        }
                    }
             
                }
                else if($action =='submitRFQItems'){
                    
                    if(isset($_POST['Items'])){
                    $items = $_POST['Items'];
                    $requestid = $id;
                    $createdat = date("Y-m-d H:i:s");
//                    print_r($items);exit;
                    
                    foreach($items as $item){
                    
                    if(!isset($item['id'])&&empty($item['id'])){ 
                        $reqitem = new RequestItems;
                        $reqitem->request_id = $id;
                       
                    }else
                        $reqitem = RequestItems::model()->findByPk($item['id']);
//                    echo '-'. count($items).'-';print_r($item);exit;
                        $reqitem->item_number = $item['itemnumber'];
                        $reqitem->name = $item['name'];
                        $reqitem->quantity = $item['quantity'];
                        $reqitem->price = $item['price'];
                        $reqitem->unit_label = $item['unit_label'];
                        $reqitem->unit_amount = $item['unit_amount'];
                        $reqitem->unit_id = $item['unit'];
                        $reqitem->save(false);
                        $itemId = $reqitem->ID;
                    
                        $files = CUploadedFile::getInstancesByName('Items['.$item['cc'].'][documents]');
                    
                    if (isset($files) && count($files) > 0)
                        {
                            $dataFiles = array();
                            // go through each uploaded image
                            
                            foreach ($files as $image => $pic) {
                                
                                $ext = explode('/',$pic->type);
                                $ext = $ext[1];
                                    
                                $filename = $userid.rand(9999,1000000).'.'.$ext;
                                if ($pic->saveAs(Yii::getPathOfAlias('webroot').'/uploads/requests/'.$filename)) {
                                    // add it to the main model now
                                    
                                    $document  = new Documents;
                                    $document->uid =   $userid;
                                    $document->name =   $filename;
                                    $document->typeoffile =   $pic->type;
                                    $document->url =   'uploads/requests';
                                    $document->origname =   $pic->name;
                                    $document->size =   $pic->size;
                                    $document->save(false);
                                    $docid = $document->ID;
                                    
                                    $reqDoc = new ReqItemDoc;
                                    $reqDoc->reqitem_id = $itemId;
                                    $reqDoc->doc_id = $docid;
                                    $reqDoc->save(false);
                                   
                                }
                                else{
                                    echo 'Cannot upload!';
                                }
                            }
                        }
                    }
                        
                    echo 'done';exit;
                    
                    }
                }
                else if($action=='deleteItemRFQRequest'){
                    $itemId = $_POST['itemId'];
                    $reqid = $id;
                    $reqitemdoc = ReqItemDoc::model()->findAllByAttributes(array('reqitem_id'=>$itemId));
                    foreach($reqitemdoc as $rqidoc){
                        $rqidoc->delete();
                    }
                    $reqitemdoc = RequestItemPrices::model()->findAllByAttributes(array('reqitem_id'=>$itemId));
                    foreach($reqitemdoc as $rqidoc){
                        $rqidoc->delete();
                    }
                    $reqItem  = RequestItems::model()->findByPk($itemId);
                    $reqItem->delete();
                    
                }
                else if($action=='deleteItemDoc'){
                    $itemId = $_POST['itDocId'];
                    $itemDoc  = ReqItemDoc::model()->findByPk($itemId);
                    $itemDoc->delete();
                    
                }
                else if($action=='getDatabaseItems'){
                    
                    $criteria = new CDbCriteria;
                    $criteria->with = array('user','user.supplierItems','user.supplierItems.item');
                    $criteria->condition = "t.acc_id=$accountid";
                    $supplierItems = SupplierAccounts::model()->findAll($criteria);
//                    echo '<pre>';print_r($supplierItems);exit;
                    $criteria = new CDbCriteria;
                    $criteria->with = array('categoriesItems','categoriesItems.item');
                    $criteria->condition = "t.acc_id=$accountid";
                    $categoryItems = Categories::model()->findAll($criteria);
                    
                    $this->renderPartial('_pickItemsDatabase',array(
                        'supplierItems'=>$supplierItems,
                        'categoryItems'=>$categoryItems,
                    ));

                }
                else if($action=='getBiddingSupplierData'){
//                    print_r( $_POST);exit;
                   $requestid = $_POST['reqId'];
                   $supplierid = $_POST['userId'];
                
                    $criteria = new CDbCriteria;
                    $criteria->with = array('requestSuppliers.supplier.acc','requestItems.requestItemPrices','sectionQuestions.questionsAnswers1.questanswersDocuments.doc');
                    $criteria->condition = "t.ID=$requestid AND requestSuppliers.supplier_id=$supplierid AND requestItemPrices.uid=$supplierid";
                    $suplier = Requests::model()->findAll($criteria);
                    $suplier = $suplier[0];
//                    echo '<pre>';print_r($suplier);exit;
                    
                    $this->renderPartial('_biddingCompanyData',array(
                        'model'=>$suplier,
                        'supplierId'=>$supplierid
                    ));

                }
                else if($action=='getSupplierQuestionnaireComp'){
//                    print_r( $_POST);exit;
                   $requestid = $id;
                  
                    $criteria = new CDbCriteria;
                    $criteria->with = array('requestSuppliers.supplier.acc','requestItems.requestItemPrices','sectionQuestions.questionsAnswers1.questanswersDocuments.doc');
                    $criteria->condition = "t.ID=$requestid";
                    $suplier = Requests::model()->findAll($criteria);
                    $suplier = $suplier[0];
                   
                    
//                    echo '<pre>';print_r($suplier);exit;
                    $this->renderPartial('_getRFQQSComparison',array(
                        'model'=>$suplier,
//                        'unitList'=>$unitList
                    ));

                }
                else if($action=='getSupplierBidsComp'){
//                    print_r( $_POST);exit;
                   $requestid = $id;
                  
                    $criteria = new CDbCriteria;
                    $criteria->with = array('requestSuppliers.supplier.acc','requestItems.requestItemPrices','sectionQuestions.questionsAnswers1.questanswersDocuments.doc');
                    $criteria->condition = "t.ID=$requestid";
                    $suplier = Requests::model()->findAll($criteria);
                    $suplier = $suplier[0];
//                    echo '<pre>';print_r($suplier);exit;
                     $units= Units::model()->findAll();
                    foreach($units as $u){
                            $unitList[$u->ID]=$u->name;        
                        }

                    $this->renderPartial('_getRFQBidsComparison',array(
                        'model'=>$suplier,
                        'unitList'=>$unitList
                    ));

                }
                else if($action=='getSupplierABidsComp'){
//                    print_r( $_POST);exit;
                   $requestid = $id;
                  
                    $criteria = new CDbCriteria;
                    $criteria->with = array('requestSuppliers.supplier.acc','requestItems.requestItemPrices','sectionQuestions.questionsAnswers1.questanswersDocuments.doc');
                    $criteria->condition = "t.ID=$requestid";
                    $suplier = Requests::model()->findAll($criteria);
                    $suplier = $suplier[0];
//                    echo '<pre>';print_r($suplier);exit;
                     $units= Units::model()->findAll();
                    foreach($units as $u){
                            $unitList[$u->ID]=$u->name;        
                        }

                    $this->renderPartial('_getAUCBidsComparison',array(
                        'model'=>$suplier,
                        'unitList'=>$unitList
                    ));

                }
                else if($action=='getSupplierAPreBidsComp'){
//                    print_r( $_POST);exit;
                   $requestid = $id;
                  
                    $criteria = new CDbCriteria;
                    $criteria->with = array('requestSuppliers.supplier.acc','requestItems.requestItemPrices','sectionQuestions.questionsAnswers1.questanswersDocuments.doc');
                    $criteria->condition = "t.ID=$requestid";
                    $suplier = Requests::model()->findAll($criteria);
                    $suplier = $suplier[0];
//                    echo '<pre>';print_r($suplier);exit;
                     $units= Units::model()->findAll();
                    foreach($units as $u){
                            $unitList[$u->ID]=$u->name;        
                        }

                    $this->renderPartial('_getAUCPreBidsComparison',array(
                        'model'=>$suplier,
                        'unitList'=>$unitList
                    ));

                }
                else if($action=='getEnterBidManual'){
//                    print_r( $_POST);exit;
                   $requestid = $_POST['reqId'];
                   $supplierid = $_POST['supplierId'];
                  
                    $criteria = new CDbCriteria;
                    $criteria->with = array('requestItems.requestItemPrices');
                    $criteria->condition = "t.ID=$requestid AND requestItemPrices.uid=$supplierid";
                    $reqItems = Requests::model()->findAll($criteria);
//                    echo '<pre>';                    print_r($reqItems);exit;
                    if(isset($reqItems)){
                    $reqItems = $reqItems[0];
                    }else{
                    $criteria = new CDbCriteria;
                    $criteria->with = array('requestItems');
                    $criteria->condition = "t.ID=$requestid";
                    $reqItems = Requests::model()->findAll($criteria);
                       
                    }
                        
                   
                     $units= Units::model()->findAll();
                    foreach($units as $u){
                            $unitList[$u->ID]=$u->name;        
                        }

                    $this->renderPartial('_getSupplierCompEBid',array(
                        'model'=>$reqItems,
                        'unitList'=>$unitList,
                        'supplierId'=>$supplierid
                    ));

                }
                else if($action=='saveManualItemPrice'){
//                    print_r( $_POST);exit;
                    
                   $item = $_POST['ItemPrice'];
                  
                   foreach($item as $itm){
                       if(isset($itm['reqItemPriceId'])&&!empty($itm['reqItemPriceId'])){
                            $itmprice = RequestItemPrices::model()->findByPk($itm['reqItemPriceId']);
                            $itmprice->price = $itm['price'];
                            $itmprice->save(false);
                       }else{
                            $itmprice = new RequestItemPrices;
                            $itmprice->reqitem_id = $itm['reqItemId'];
                            $itmprice->uid = $_POST['supplierId'];
                            $itmprice->rid = $_POST['requestId'];
                            $itmprice->price = $itm['price'];
                            $itmprice->save(false);
                           
                       }
                       
                   }
                   exit;
                }
                else if($action=='inviteNewSupplier'){
//                    print_r( $_POST);exit;
                    $requestid  = $_POST['reqId'];
                    if(isset($_POST['supplier'])&&!empty($_POST['supplier'])){
 //                   print_r($supplier['pfpd']);exit;
                    foreach($_POST['supplier'] as $supId){
                        $suplierData = Users::model()->findByPk($supId);

                        $supplierEmail = $suplierData->email;
                        $supplierName = $suplierData->name;
                        $supplierId = $suplierData->ID;

                        $reqSupplier = new RequestSupplier;
                        $reqSupplier->supplier_id = $supplierId;
                        $reqSupplier->req_id = $requestid;
                        $reqSupplier->save(false);
                       
                        $emailBody = RequestInvitationMessage::model()->findByAttributes(array('rid'=>$requestid));
                        $subjectMessage = $emailBody->name;
                        $bodyMessage = $emailBody->email_message;
                        $participationlink =Yii::app()->createAbsoluteUrl('requests/supplierview',array('id'=>base64_encode($supplierId),'event'=> base64_encode($requestid)),'http');

                        $bodyMessage = str_replace('{want_to_participate}', $participationlink, $bodyMessage);
                        $bodyMessage = str_replace('{dont_want_to_participate}', $participationlink, $bodyMessage);

                        
                      $this->sendSupplierEmail($supplierEmail,$supplierName,$subjectMessage,$bodyMessage);
                    }
                }
                   exit;
                }
                else if($action=='acceptItemBid'){
                    
                    $requestId  = $_POST['requestId'];
                    $supplierId  = $_POST['supplierId'];
                    $requestItemId  = $_POST['requestIId'];
                    $bidStatus  = $_POST['bstatus'];
                     $reqItemPrice =  RequestItemprices::model()->findByAttributes(array('uid'=>$supplierId,'rid'=>$requestId,'reqitem_id'=>$requestItemId));
                    if(empty($reqItemPrice))
                     $reqItemPrice = new RequestItemprices;
                     
                    if($bidStatus==1||$bidStatus==0){
                    $reqItemPrice->uid = $supplierId; 
                    $reqItemPrice->rid = $requestId; 
                    $reqItemPrice->reqitem_id = $requestItemId; 
                    $reqItemPrice->status = $bidStatus;
                    $reqItemPrice->save(false);
                    }else if($bidStatus==2){
                       
                        $reqItemPrice->status =2;
                        $reqItemPrice->price = $_POST['price'];
                        $reqItemPrice->save(false);
                    }
                }
                else if($action=='deleteQuestAnswerDoc'){
                    $questansdocId = $_POST['questansDocId'];
                    $questansDoc  = QuestanswersDocument::model()->findByPk($questansdocId);
                    $questansDoc->delete();
                    
                }
                else if($action=='getSupplierAuctionRank'){
                    $supplierId = $_POST['supId'];
                    $requestId = $_POST['reqId'];

                    $requestSupplier = RequestSupplier::model()->findByAttributes(array('supplier_id'=>$supplierId,'req_id'=>$requestId));

                    $model = Requests::model()->findByPk($requestId);
                    $count = 0;
                    foreach($requestSupplier->req->requestItems as $reqitem){
                        $curSupcurItem = '';
                        $allPricesofItem= array();
                        foreach($reqitem->requestItemPrices as $reqiprice){
                            if($reqiprice->uid==$supplierId){
                                $supPriceExist=true; $curSupcurItem= $reqiprice ;
                                
                            }
                            $allPricesofItem[] = $reqiprice->price;
                        }
                        
                        $maxv = max($allPricesofItem);
                        $max = array_keys($allPricesofItem, max($allPricesofItem));

                        $min = array_keys($allPricesofItem, min($allPricesofItem));
                        $minv = min($allPricesofItem);
                        $rankm = '';
                        if($curSupcurItem->status==2){
                        if($curSupcurItem->price==$minv)
                           $rankm =  '<div class="rank-green">'.($min[0]+1).'</div>';
                        else if($curSupcurItem->price==$maxv)
                            $rankm=  '<div class="rank-red">'.($max[0]+1).'</div>';
                        else
                           $rankm =  '<div class="rank-gray">'.($min[0]+1).'</div>';
                        }
                        $data[$count]['id'] = $curSupcurItem->ID;
                        
                        
                        $data[$count]['rank'] = $rankm ;
                        
                        $count++;
                    }
                   echo json_encode($data);exit;
                    
                }
                
                exit;
	}
        public function actionSupplierview($id,$event){
//            echo base64_encode(21);exit;
            $event = base64_decode($event);
//            echo $id;exit;
            $this->layout = 'req_open_layout';
            $criteria = new CDbCriteria;
            $criteria->with = array('documentRequests.d','requestItems.reqItemDocs','sectionQuestions','sectionQuestions.optionlists','u','acc');
            $criteria->condition = "t.ID=$event";
            $request = Requests::model()->findall($criteria);
            $request = $request[0];
//        echo '<pre>';    print_r($request);exit;
            if($request->privacy=='public'){
            $this->render('supplierView',array(
                'model'=>$request,
                'userid'=>$id
            ));
            }else if( $request->privacy=='restricted'){
            $this->render('supplierViewPrivate',array(
                'model'=>$request,
                'userid'=>$id
            ));
            }
            
        }
        public function actionFillQ($id,$event){
            
            $event = base64_decode($event);
//            echo $id;exit;
            $this->layout = 'req_open_layout';
            $criteria = new CDbCriteria;
            $criteria->with = array('documentRequests.d','requestItems.reqItemDocs','sectionQuestions','sectionQuestions.optionlists','u','acc');
            $criteria->condition = "t.ID=$event";
            $request = Requests::model()->findall($criteria);
            $request = $request[0];
//          echo '<pre>';          print_r($request);exit;
            $this->render('supplierViewQuestionnaire',array(
                'model'=>$request,
                'userid'=>$id
            ));
            
        }
	public function actionRfi($event)
	{
            $userid = Yii::app()->user->ID;
            $accountid = Yii::app()->user->accountId;
            $qcount= 0;
            
            $supplierGroups = Suppliergroup::model()->findAllByAttributes(array('acc_id'=>$accountid));
             $criteria=new CDbCriteria;
             $criteria->with = array('user','user.acc');
             $criteria->condition = "t.acc_id=$accountid";
            $accountSupplier = SupplierAccounts::model()->findAll($criteria);
//            echo '<pre>';            print_r($accountSupplier);exit;
            if(isset($event)&&!empty($event)){
            $criteria=new CDbCriteria;
            $criteria->condition = "t.parentid!=0 AND t.rid=$event";
            $questionnaireModel = SectionQuestions::model()->findAll($criteria);
//            echo '<pre>';            print_r($questionnaireModel);exit;
            $qcount = count($questionnaireModel);
            }
//            echo 123;exit;
            $reqDocs = '';
            $docmodel = new Documents;
                if(isset($event)&&!empty($event)){
                    $criteria = new CDbCriteria;
                    $criteria->with = array('d');
                    $criteria->condition = "t.rid=$event";
                    $reqDocs = DocumentRequest::model()->findall($criteria);
                    $model= Requests::model()->findByPk($event);
                
                }else {
                    $model=new Requests;
                    
                }
                
                $timezone= Timezone::model()->findAll();

                $docmodel = new Documents;
                
                $this->render('index',array(
			'requestId'=>$event,
			'model'=>$model,
			'docmodel'=>$docmodel,
			'reqDocs'=>$reqDocs,
			'docModel'=>$docmodel,
			'timezone'=>$timezone,
			'questcount'=>$qcount,
			'supplierGroups'=>$supplierGroups,
			'accountSuppliers'=>$accountSupplier,
		));
	}
	public function actionRfq($event)
	{
            $userid = Yii::app()->user->ID;
            $accountid = Yii::app()->user->accountId;
            $qcount= 0;
            
            $supplierGroups = Suppliergroup::model()->findAllByAttributes(array('acc_id'=>$accountid));
             $criteria=new CDbCriteria;
             $criteria->with = array('user','user.acc');
             $criteria->condition = "t.acc_id=$accountid";
            $accountSupplier = SupplierAccounts::model()->findAll($criteria);

            if(isset($event)&&!empty($event)){
            $criteria=new CDbCriteria;
            $criteria->condition = "t.parentid!=0 AND t.rid=$event";
            $questionnaireModel = SectionQuestions::model()->findAll($criteria);

            $qcount = count($questionnaireModel);
            }

            $reqDocs = '';
            $docmodel = new Documents;
                if(isset($event)&&!empty($event)){
                    $criteria = new CDbCriteria;
                    $criteria->with = array('d');
                    $criteria->condition = "t.rid=$event";
                    $reqDocs = DocumentRequest::model()->findall($criteria);
                    $model= Requests::model()->findByPk($event);
                                // items of request
//                    echo 123;exit
                $criteria = new CDbCriteria;
                $criteria->with = array('reqItemDocs','reqItemDocs.doc');
                $criteria->condition = "t.request_id=$event";
                $items= RequestItems::model()->findAll($criteria);
//               echo '<pre>'; print_r($items);exit;
                }else {
                    $items='';
                    $model=new Requests;
                }
                
                $timezone= Timezone::model()->findAll();
                $units= Units::model()->findAll();
                foreach($units as $u){
                    if($u->parent==0){
                    foreach ($units as $us){
                        if($us->parent==$u->ID){
                        $unitList[$us->ID]=$us->name;        
                        }
                    }
                    }
                    
                }
//                echo '<pre>';print_r($unitList);exit;
                $docmodel = new Documents;
                
                $this->render('indexRfq',array(
			'requestId'=>$event,
			'model'=>$model,
			'docmodel'=>$docmodel,
			'reqDocs'=>$reqDocs,
			'docModel'=>$docmodel,
			'timezone'=>$timezone,
			'questcount'=>$qcount,
			'supplierGroups'=>$supplierGroups,
			'accountSuppliers'=>$accountSupplier,
			'items'=>$items,
			'unitList'=>$unitList,
		));
	}
	public function actionReverseAuction($event)
	{
            $userid = Yii::app()->user->ID;
            $accountid = Yii::app()->user->accountId;
            $qcount= 0;
            
            $supplierGroups = Suppliergroup::model()->findAllByAttributes(array('acc_id'=>$accountid));
             $criteria=new CDbCriteria;
             $criteria->with = array('user','user.acc');
             $criteria->condition = "t.acc_id=$accountid";
            $accountSupplier = SupplierAccounts::model()->findAll($criteria);

            if(isset($event)&&!empty($event)){
            $criteria=new CDbCriteria;
            $criteria->condition = "t.parentid!=0 AND t.rid=$event";
            $questionnaireModel = SectionQuestions::model()->findAll($criteria);

            $qcount = count($questionnaireModel);
            }

            $reqDocs = '';
            $docmodel = new Documents;
                if(isset($event)&&!empty($event)){
                    $criteria = new CDbCriteria;
                    $criteria->with = array('d');
                    $criteria->condition = "t.rid=$event";
                    $reqDocs = DocumentRequest::model()->findall($criteria);
                    $model= Requests::model()->findByPk($event);
                                // items of request
//                    echo 123;exit
                $criteria = new CDbCriteria;
                $criteria->with = array('reqItemDocs','reqItemDocs.doc');
                $criteria->condition = "t.request_id=$event";
                $items= RequestItems::model()->findAll($criteria);
//               echo '<pre>'; print_r($items);exit;
                }else {
                    $model=new Requests;
                     $items='';
                }
                
                $timezone= Timezone::model()->findAll();
                $units= Units::model()->findAll();
                foreach($units as $u){
                    if($u->parent==0){
                    foreach ($units as $us){
                        if($us->parent==$u->ID){
                        $unitList[$us->ID]=$us->name;        
                        }
                    }
                    }
                    
                }
//                echo '<pre>';print_r($unitList);exit;
                $docmodel = new Documents;
                
                $this->render('indexAuction',array(
			'requestId'=>$event,
			'model'=>$model,
			'docmodel'=>$docmodel,
			'reqDocs'=>$reqDocs,
			'docModel'=>$docmodel,
			'timezone'=>$timezone,
			'questcount'=>$qcount,
			'supplierGroups'=>$supplierGroups,
			'accountSuppliers'=>$accountSupplier,
			'items'=>$items,
			'unitList'=>$unitList,
		));
	}        
        public function actionRequestBidStatus($id){
           $accountId = Yii::app()->user->accountId;
            $criteria = new CDbCriteria;
            $criteria->with = array('supplier');
            $criteria->condition = "t.req_id=$id";
            $request = RequestSupplier::model()->findall($criteria);
            $requestModel = Requests::model()->findByPk($id);
//            print_r($request);exit;
        $data['listhtml'] =  $this->renderPartial('_requestListBidStats',array(
                'model'=>$request,
                'requestId'=>$id,
                'requestModel'=>$requestModel,
            ),true);
            $criteria = new CDbCriteria;
            $criteria->with = array('acc','user');
            $criteria->condition = "t.acc_id=$accountId";
            $mySuppliers = SupplierAccounts::model()->findall($criteria);
            $option ='';
            foreach ($mySuppliers as $mysup){
                $option .= '<option value="'.$mysup->user->ID.'">'.$mysup->user->name.'</option>';
            }
        $data['selecthtml'] = $option;
        $data['requestId'] = $id;
        echo json_encode($data);exit;
            
        }
        public function actionList(){
            $accountId = Yii::app()->user->accountId;
            $model = Requests::model()->findAll();
            $accountId = Yii::app()->user->accountId;
            $criteria = new CDbCriteria;
            $criteria->with = array('acc','user');
            $criteria->condition = "t.acc_id=$accountId";
            $mySuppliers = SupplierAccounts::model()->findall($criteria);
//            print_r($mySuppliers);exit;
            $this->render('requestList',array(
                'model'=>$model,
                'mySuppliers'=>$mySuppliers
            ));
        }
	public function actionSecques()
	{
                $requestid = 3;
                $questionnaireModel = SectionQuestions::model()->findAllByAttributes(array('rid'=>$requestid));
                    $requestid = 1; 
                    $criteria = new CDbCriteria;
                    $criteria->with = array('d');
                    $criteria->condition = "t.rid=$requestid";
                    $reqDocs = DocumentRequest::model()->findall($criteria);
                 
                    $model = new Documents;
		$this->render('secques',array(
			'requestId'=>$requestid,
			'model'=>$model,
			'reqDocs'=>$reqDocs,
		));
	}        
        
        public function actionrfqOverview($event){
            $userid = Yii::app()->user->ID;
            $accountid  = Yii::app()->user->accountId;
            $criteria = new CDbCriteria;
            $criteria->with  = array('requestSuppliers.supplier.acc','requestItemPrices');
            $criteria->condition = "t.ID=$event AND t.accid=$accountid";
            $model = Requests::model()->findAll($criteria);
            $model = $model[0];
//            echo '<pre>';            print_r($model);exit;
                    $bidsGraph = array();
                    $itemTPrice = array();
                    $count = 0;
                    //get all suppliers and their info Along with their total bid
                    $maxBid = 0;
                    $minBid = 0;
                    $tprice = 0 ;
                    $expectedBudget = 0;
                    $maxminarray = array();
                    foreach($model->requestSuppliers as $reqSupplier) {
                        //supplier company name
                        $bidsGraph[$count]['companyname'] = $reqSupplier->supplier->acc->company;
                        $bidsGraph[$count]['name'] = $reqSupplier->supplier->name;
                        $bidsGraph[$count]['ID'] = $reqSupplier->supplier->acc->ID;
                        $bidsGraph[$count]['requestid'] = $event;
                        $bidsGraph[$count]['userid'] = $reqSupplier->supplier->ID;
                        $supplierId = $reqSupplier->supplier->ID;
                        $requestId = $model->ID;
                        $criteria = new CDbCriteria;
                        $criteria->with  = array('reqitem');
                        $criteria->condition = "t.uid=$supplierId AND t.rid=$requestId";
                        $itemprices = RequestItemPrices::model()->findAll($criteria);
                        
                        if(isset($itemprices)&&!empty($itemprices)){

                        if(isset($itemprices)&&!empty($itemprices)){
                            foreach($itemprices as $itemprice){
                                $bidsGraph[$count]['bidTime'] = $itemprice->createdat;
                                $tprice +=$itemprice->reqitem->quantity*$itemprice->price;
                                $expectedBudget +=$itemprice->reqitem->price;
                            }
                        }
                        //total items price/bid
                        $bidsGraph[$count]['itemTPrice'] = $tprice;
                        $bidsGraph[$count]['class'] = '';
                        $bidsGraph[$count]['width'] = '100';
                        // used to get index of min max values;
                        $maxminarray[$count] = $tprice;
                        $bidsGraph[$count]['bidstatus'] = '1';
                        }else{
                        $bidsGraph[$count]['bidstatus'] = '0';
                        }
                        
                        $count++;
                        }
                        if(isset($maxminarray)&&!empty($maxminarray)){
                        $max = array_keys($maxminarray, max($maxminarray));
                        $max = $max[0];
                        $min = array_keys($maxminarray, min($maxminarray));
                        $min = $min[0];
                       
//                        echo '<pre>';                        print_r($maxminarray);exit;
//                        echo $min;exit;
                        // put min price supplier class
                        $bidsGraph[$min]['class'] = 'sup-min-bid';
                        $maxBid = $bidsGraph[$max]['itemTPrice'];
                        $minBid = $bidsGraph[$min]['itemTPrice'];
                        foreach($bidsGraph as $key=>$value){
                            if(isset($value['itemTPrice']))
                            $bidsGraph[$key]['width'] = round(($value['itemTPrice']/$bidsGraph[$max]['itemTPrice'])*100,2);
                            else
                                $bidsGraph[$key]['width'] =0;

                            
                        }
                        }
                        
                $this->render('rfqOverview',array(
                    'model'=>$model,
                    'bidsGraph'=>$bidsGraph,
                    'maxbid'=>$maxBid,
                    'minBid'=>$minBid,
                    'expectedBudget'=>$expectedBudget
			
		));
            
        }
        public function actionauctionOverview($event){
            $userid = Yii::app()->user->ID;
            $accountid  = Yii::app()->user->accountId;
            $criteria = new CDbCriteria;
            $criteria->with  = array('requestSuppliers.supplier.acc','requestItemPrices');
            $criteria->condition = "t.ID=$event AND t.accid=$accountid";
            $model = Requests::model()->findAll($criteria);
            $model = $model[0];
//            echo '<pre>';            print_r($model);exit;
                    $bidsGraph = array();
                    $itemTPrice = array();
                    $count = 0;
                    //get all suppliers and their info Along with their total bid
                    $maxBid = 0;
                    $minBid = 0;
                    $tprice = 0 ;
                    $expectedBudget = 0;
                    $maxminarray = array();
                    foreach($model->requestSuppliers as $reqSupplier) {
                        //supplier company name
                        $bidsGraph[$count]['companyname'] = $reqSupplier->supplier->acc->company;
                        $bidsGraph[$count]['name'] = $reqSupplier->supplier->name;
                        $bidsGraph[$count]['ID'] = $reqSupplier->supplier->acc->ID;
                        $bidsGraph[$count]['requestid'] = $event;
                        $bidsGraph[$count]['userid'] = $reqSupplier->supplier->ID;
                        $supplierId = $reqSupplier->supplier->ID;
                        $requestId = $model->ID;
                        $criteria = new CDbCriteria;
                        $criteria->with  = array('reqitem');
                        $criteria->condition = "t.uid=$supplierId AND t.rid=$requestId";
                        $itemprices = RequestItemPrices::model()->findAll($criteria);
                        if(isset($itemprices)&&!empty($itemprices)){

                        if(isset($itemprices)&&!empty($itemprices)){
                            foreach($itemprices as $itemprice){
                                $bidsGraph[$count]['bidTime'] = $itemprice->createdat;
                                $tprice +=$itemprice->reqitem->quantity*$itemprice->price;
                                $expectedBudget +=$itemprice->reqitem->price;
                            }
                        }
                        //total items price/bid
                        $bidsGraph[$count]['itemTPrice'] = $tprice;
                        $bidsGraph[$count]['class'] = '';
                        $bidsGraph[$count]['width'] = '100';
                        // used to get index of min max values;
                        $maxminarray[$count] = $tprice;
                        $bidsGraph[$count]['bidstatus'] = '1';
                        }else{
                        $bidsGraph[$count]['bidstatus'] = '0';
                        }
                        
                        $count++;
                        }
                        if(isset($maxminarray)&&!empty($maxminarray)){
                        $max = array_keys($maxminarray, max($maxminarray));
                        $max = $max[0];
                        $min = array_keys($maxminarray, min($maxminarray));
                        $min = $min[0];
                       
                        
                        
                        // put min price supplier class
                        $bidsGraph[$min]['class'] = 'sup-min-bid';
                        $maxBid = $bidsGraph[$max]['itemTPrice'];
                        $minBid = $bidsGraph[$min]['itemTPrice'];
                        foreach($bidsGraph as $key=>$value){
                            if(isset($value['itemTPrice']))
                            $bidsGraph[$key]['width'] = round(($value['itemTPrice']/$bidsGraph[$max]['itemTPrice'])*100,2);
                            else
                                $bidsGraph[$key]['width'] =0;

                            
                        }
                        }
                        
                $this->render('auctionOverview',array(
                    'model'=>$model,
                    'bidsGraph'=>$bidsGraph,
                    'maxbid'=>$maxBid,
                    'minBid'=>$minBid,
                    'expectedBudget'=>$expectedBudget
			
		));
            
        }
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($slug)
	{
		$model=new Requests;
		$timezone= Timezone::model()->findAll();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
		if(isset($_POST['Requests']))
		{
//                    echo '<pre>';                    print_r($_POST);exit;
                    $userid  = 1;
                    $accountid  = 1;
			$model->attributes=$_POST['Requests'];
			$model->uid=$userid;
			$model->createdat=date('Y-m-d H:i:s');
			$model->endtime=date('Y-m-d H:i:s', strtotime($_POST['Requests']['endtime']));
			$model->accid=$accountid;
			$model->reqtype='RFI';
			$model->status=0;
			if($model->save())
				$this->redirect(array('view','id'=>$model->ID));
                        
		}

		$this->render('create',array(
			'model'=>$model,
			'timezone'=>$timezone,
		));
	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                $timezone= Timezone::model()->findAll();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Requests']))
		{
			$model->attributes=$_POST['Requests'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->ID));
		}

		$this->render('update',array(
			'model'=>$model,
			'timezone'=>$timezone,
		));
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Requests('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Requests']))
			$model->attributes=$_GET['Requests'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Requests the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Requests::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Requests $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='requests-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
