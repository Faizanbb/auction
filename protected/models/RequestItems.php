<?php

/**
 * This is the model class for table "request_items".
 *
 * The followings are the available columns in table 'request_items':
 * @property integer $ID
 * @property integer $request_id
 * @property string $name
 * @property string $description
 * @property string $item_number
 * @property string $unit_label
 * @property integer $unit_amount
 * @property integer $unit_id
 * @property integer $quantity
 * @property double $price
 *
 * The followings are the available model relations:
 * @property ReqItemDoc[] $reqItemDocs
 * @property RequestItemPrices[] $requestItemPrices
 * @property Requests $request
 */
class RequestItems extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'request_items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('request_id, name, description, item_number, unit_label, unit_amount, unit_id, quantity, price', 'required'),
			array('request_id, unit_amount, unit_id, quantity', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('name, description, unit_label', 'length', 'max'=>150),
			array('item_number', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, request_id, name, description, item_number, unit_label, unit_amount, unit_id, quantity, price', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'reqItemDocs' => array(self::HAS_MANY, 'ReqItemDoc', 'reqitem_id'),
			'requestItemPrices' => array(self::HAS_MANY, 'RequestItemPrices', 'reqitem_id'),
			'request' => array(self::BELONGS_TO, 'Requests', 'request_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'request_id' => 'Request',
			'name' => 'Name',
			'description' => 'Description',
			'item_number' => 'Item Number',
			'unit_label' => 'Unit Label',
			'unit_amount' => 'Unit Amount',
			'unit_id' => 'Unit',
			'quantity' => 'Quantity',
			'price' => 'Price',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('request_id',$this->request_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('item_number',$this->item_number,true);
		$criteria->compare('unit_label',$this->unit_label,true);
		$criteria->compare('unit_amount',$this->unit_amount);
		$criteria->compare('unit_id',$this->unit_id);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('price',$this->price);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RequestItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
