<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $ID
 * @property integer $accid
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $title
 * @property string $phone
 * @property string $mobile
 * @property integer $carriercode
 * @property string $mobileregion
 * @property string $usergroup
 * @property string $createdat
 * @property string $status
 * @property string $role
 * @property integer $logincount
 * @property string $lastaccess
 * @property string $updatedat
 *
 * The followings are the available model relations:
 * @property DocumentRequest[] $documentRequests
 * @property Documents[] $documents
 * @property Groups[] $groups
 * @property Items[] $items
 * @property Package[] $packages
 * @property QAnswer[] $qAnswers
 * @property QuestionsDocuments[] $questionsDocuments
 * @property RequestInvitationMessage[] $requestInvitationMessages
 * @property RequestSupplier[] $requestSuppliers
 * @property Requests[] $requests
 * @property Requisition[] $requisitions
 * @property RequisitionApprover[] $requisitionApprovers
 * @property RequisitionDocument[] $requisitionDocuments
 * @property Roles[] $roles
 * @property SectionQuestions[] $sectionQuestions
 * @property SupplierAccounts[] $supplierAccounts
 * @property SupplierGroupRel[] $supplierGroupRels
 * @property SupplierItems[] $supplierItems
 * @property Suppliergroup[] $suppliergroups
 * @property Accounts $acc
 */
class Users extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// array('accid, email, password, title, phone, mobile, carriercode, mobileregion, usergroup, createdat, role, logincount, lastaccess, updatedat', 'required'),
			array('accid, email, password, title, phone, mobile, mobileregion', 'required'),
			array('accid, carriercode, logincount', 'numerical', 'integerOnly'=>true),
			array('name, email, password, title, phone, mobile, usergroup', 'length', 'max'=>350),
			array('mobileregion', 'length', 'max'=>10),
			array('status', 'length', 'max'=>8),
			array('role', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, accid, name, email, password, title, phone, mobile, carriercode, mobileregion, usergroup, createdat, status, role, logincount, lastaccess, updatedat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'documentRequests' => array(self::HAS_MANY, 'DocumentRequest', 'uid'),
			'documents' => array(self::HAS_MANY, 'Documents', 'uid'),
			'groups' => array(self::HAS_MANY, 'Groups', 'user_id'),
			'items' => array(self::HAS_MANY, 'Items', 'uid'),
			'packages' => array(self::HAS_MANY, 'Package', 'uid'),
			'qAnswers' => array(self::HAS_MANY, 'QAnswer', 'uid'),
			'questionsDocuments' => array(self::HAS_MANY, 'QuestionsDocuments', 'uid'),
			'requestInvitationMessages' => array(self::HAS_MANY, 'RequestInvitationMessage', 'uid'),
			'requestSuppliers' => array(self::HAS_MANY, 'RequestSupplier', 'supplier_id'),
			'requests' => array(self::HAS_MANY, 'Requests', 'uid'),
			'requisitions' => array(self::HAS_MANY, 'Requisition', 'user_id'),
			'requisitionApprovers' => array(self::HAS_MANY, 'RequisitionApprover', 'user_id'),
			'requisitionDocuments' => array(self::HAS_MANY, 'RequisitionDocument', 'uid'),
			'roles' => array(self::HAS_MANY, 'Roles', 'user_id'),
			'sectionQuestions' => array(self::HAS_MANY, 'SectionQuestions', 'uid'),
			'supplierAccounts' => array(self::HAS_MANY, 'SupplierAccounts', 'user_id'),
			'supplierGroupRels' => array(self::HAS_MANY, 'SupplierGroupRel', 'supplier_id'),
			'supplierItems' => array(self::HAS_MANY, 'SupplierItems', 'supplier_id'),
			'suppliergroups' => array(self::HAS_MANY, 'Suppliergroup', 'user_id'),
			'acc' => array(self::BELONGS_TO, 'Accounts', 'accid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'accid' => 'Accid',
			'name' => 'Name',
			'email' => 'Email',
			'password' => 'Password',
			'title' => 'Title',
			'phone' => 'Phone',
			'mobile' => 'Mobile',
			'carriercode' => 'Carriercode',
			'mobileregion' => 'Mobileregion',
			'usergroup' => 'Usergroup',
			'createdat' => 'Createdat',
			'status' => 'Status',
			'role' => 'Role',
			'logincount' => 'Logincount',
			'lastaccess' => 'Lastaccess',
			'updatedat' => 'Updatedat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('accid',$this->accid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('carriercode',$this->carriercode);
		$criteria->compare('mobileregion',$this->mobileregion,true);
		$criteria->compare('usergroup',$this->usergroup,true);
		$criteria->compare('createdat',$this->createdat,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('role',$this->role,true);
		$criteria->compare('logincount',$this->logincount);
		$criteria->compare('lastaccess',$this->lastaccess,true);
		$criteria->compare('updatedat',$this->updatedat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
