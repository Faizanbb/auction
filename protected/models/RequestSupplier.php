<?php

/**
 * This is the model class for table "request_supplier".
 *
 * The followings are the available columns in table 'request_supplier':
 * @property integer $ID
 * @property integer $supplier_id
 * @property integer $req_id
 * @property integer $accept
 * @property integer $viewed
 * @property string $createdat
 *
 * The followings are the available model relations:
 * @property Users $supplier
 * @property Requests $req
 */
class RequestSupplier extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'request_supplier';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('supplier_id, req_id, accept, viewed, createdat', 'required'),
			array('supplier_id, req_id, accept, viewed', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, supplier_id, req_id, accept, viewed, createdat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'supplier' => array(self::BELONGS_TO, 'Users', 'supplier_id'),
			'req' => array(self::BELONGS_TO, 'Requests', 'req_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'supplier_id' => 'Supplier',
			'req_id' => 'Req',
			'accept' => 'Accept',
			'viewed' => 'Viewed',
			'createdat' => 'Createdat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('req_id',$this->req_id);
		$criteria->compare('accept',$this->accept);
		$criteria->compare('viewed',$this->viewed);
		$criteria->compare('createdat',$this->createdat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RequestSupplier the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
