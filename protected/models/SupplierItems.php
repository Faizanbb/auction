<?php

/**
 * This is the model class for table "supplier_items".
 *
 * The followings are the available columns in table 'supplier_items':
 * @property integer $ID
 * @property integer $item_id
 * @property integer $supplier_id
 * @property string $item_num
 * @property double $price
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Items $item
 * @property Users $supplier
 */
class SupplierItems extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'supplier_items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_id, supplier_id, item_num, price, description', 'required'),
			array('item_id, supplier_id', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('item_num', 'length', 'max'=>20),
			array('description', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, item_id, supplier_id, item_num, price, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'item' => array(self::BELONGS_TO, 'Items', 'item_id'),
			'supplier' => array(self::BELONGS_TO, 'Users', 'supplier_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'item_id' => 'Item',
			'supplier_id' => 'Supplier',
			'item_num' => 'Item Num',
			'price' => 'Price',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('item_num',$this->item_num,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SupplierItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
