<?php

/**
 * This is the model class for table "items".
 *
 * The followings are the available columns in table 'items':
 * @property integer $ID
 * @property integer $uid
 * @property integer $accid
 * @property integer $parentid
 * @property integer $lotnum
 * @property string $name
 * @property string $description
 * @property string $notes
 * @property integer $unit
 * @property integer $quantity
 * @property string $saved
 * @property string $createdat
 * @property integer $status
 * @property double $itemprice
 * @property string $unit_label
 * @property integer $unit_amount
 *
 * The followings are the available model relations:
 * @property CategoriesItems[] $categoriesItems
 * @property ItemDocuments[] $itemDocuments
 * @property Users $u
 * @property Accounts $acc
 */
class Items extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, accid, parentid, lotnum, name, description, notes, unit, quantity, saved, status, itemprice, unit_label, unit_amount', 'required'),
			array('uid, accid, parentid, lotnum, unit, quantity, status, unit_amount', 'numerical', 'integerOnly'=>true),
			array('itemprice', 'numerical'),
			array('name', 'length', 'max'=>350),
			array('saved', 'length', 'max'=>3),
			array('unit_label', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, uid, accid, parentid, lotnum, name, description, notes, unit, quantity, saved, createdat, status, itemprice, unit_label, unit_amount', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categoriesItems' => array(self::HAS_MANY, 'CategoriesItems', 'item_id'),
			'itemDocuments' => array(self::HAS_MANY, 'ItemDocuments', 'item_id'),
			'u' => array(self::BELONGS_TO, 'Users', 'uid'),
			'acc' => array(self::BELONGS_TO, 'Accounts', 'accid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'uid' => 'Uid',
			'accid' => 'Accid',
			'parentid' => 'Parentid',
			'lotnum' => 'Lotnum',
			'name' => 'Name',
			'description' => 'Description',
			'notes' => 'Notes',
			'unit' => 'Unit',
			'quantity' => 'Quantity',
			'saved' => 'Saved',
			'createdat' => 'Createdat',
			'status' => 'Status',
			'itemprice' => 'Itemprice',
			'unit_label' => 'Unit Label',
			'unit_amount' => 'Unit Amount',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('accid',$this->accid);
		$criteria->compare('parentid',$this->parentid);
		$criteria->compare('lotnum',$this->lotnum);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('unit',$this->unit);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('saved',$this->saved,true);
		$criteria->compare('createdat',$this->createdat,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('itemprice',$this->itemprice);
		$criteria->compare('unit_label',$this->unit_label,true);
		$criteria->compare('unit_amount',$this->unit_amount);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Items the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
