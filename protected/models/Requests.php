<?php

/**
 * This is the model class for table "requests".
 *
 * The followings are the available columns in table 'requests':
 * @property integer $ID
 * @property integer $uid
 * @property integer $accid
 * @property string $name
 * @property string $description
 * @property string $endtime
 * @property string $timezone
 * @property string $starttime
 * @property integer $exttime
 * @property integer $lastperiod
 * @property integer $highestbid
 * @property integer $lowestbid
 * @property string $tiedbid
 * @property string $reqtype
 * @property string $createdat
 * @property integer $status
 * @property string $privacy
 * @property string $termscond
 * @property string $currency
 * @property integer $mcurrency
 * @property string $payterms
 * @property integer $isauction
 * @property string $endsin
 *
 * The followings are the available model relations:
 * @property DocumentRequest[] $documentRequests
 * @property QAnswer[] $qAnswers
 * @property QuestionsAnswers[] $questionsAnswers
 * @property RequestItemPrices[] $requestItemPrices
 * @property RequestItems[] $requestItems
 * @property RequestSupplier[] $requestSuppliers
 * @property Users $u
 * @property Accounts $acc
 * @property SectionQuestions[] $sectionQuestions
 * @property SupplierReqDoc[] $supplierReqDocs
 */
class Requests extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'requests';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, accid, name, description, endtime, timezone, starttime, exttime, lastperiod, highestbid, lowestbid, tiedbid, reqtype, createdat, status, privacy, termscond, currency, mcurrency, payterms, isauction, endsin', 'required'),
			array('uid, accid, exttime, lastperiod, highestbid, lowestbid, status, mcurrency, isauction', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>350),
			array('timezone', 'length', 'max'=>250),
			array('tiedbid', 'length', 'max'=>11),
			array('reqtype', 'length', 'max'=>7),
			array('privacy', 'length', 'max'=>10),
			array('currency', 'length', 'max'=>50),
			array('endsin', 'length', 'max'=>8),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, uid, accid, name, description, endtime, timezone, starttime, exttime, lastperiod, highestbid, lowestbid, tiedbid, reqtype, createdat, status, privacy, termscond, currency, mcurrency, payterms, isauction, endsin', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'documentRequests' => array(self::HAS_MANY, 'DocumentRequest', 'rid'),
			'qAnswers' => array(self::HAS_MANY, 'QAnswer', 'rid'),
			'questionsAnswers' => array(self::HAS_MANY, 'QuestionsAnswers', 'rid'),
			'requestItemPrices' => array(self::HAS_MANY, 'RequestItemPrices', 'rid'),
			'requestItems' => array(self::HAS_MANY, 'RequestItems', 'request_id'),
			'requestSuppliers' => array(self::HAS_MANY, 'RequestSupplier', 'req_id'),
			'u' => array(self::BELONGS_TO, 'Users', 'uid'),
			'acc' => array(self::BELONGS_TO, 'Accounts', 'accid'),
			'sectionQuestions' => array(self::HAS_MANY, 'SectionQuestions', 'rid'),
			'supplierReqDocs' => array(self::HAS_MANY, 'SupplierReqDoc', 'rid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'uid' => 'Uid',
			'accid' => 'Accid',
			'name' => 'Name',
			'description' => 'Description',
			'endtime' => 'Endtime',
			'timezone' => 'Timezone',
			'starttime' => 'Starttime',
			'exttime' => 'Exttime',
			'lastperiod' => 'Lastperiod',
			'highestbid' => 'Highestbid',
			'lowestbid' => 'Lowestbid',
			'tiedbid' => 'Tiedbid',
			'reqtype' => 'Reqtype',
			'createdat' => 'Createdat',
			'status' => 'Status',
			'privacy' => 'Privacy',
			'termscond' => 'Termscond',
			'currency' => 'Currency',
			'mcurrency' => 'Mcurrency',
			'payterms' => 'Payterms',
			'isauction' => 'Isauction',
			'endsin' => 'Endsin',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('accid',$this->accid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('endtime',$this->endtime,true);
		$criteria->compare('timezone',$this->timezone,true);
		$criteria->compare('starttime',$this->starttime,true);
		$criteria->compare('exttime',$this->exttime);
		$criteria->compare('lastperiod',$this->lastperiod);
		$criteria->compare('highestbid',$this->highestbid);
		$criteria->compare('lowestbid',$this->lowestbid);
		$criteria->compare('tiedbid',$this->tiedbid,true);
		$criteria->compare('reqtype',$this->reqtype,true);
		$criteria->compare('createdat',$this->createdat,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('privacy',$this->privacy,true);
		$criteria->compare('termscond',$this->termscond,true);
		$criteria->compare('currency',$this->currency,true);
		$criteria->compare('mcurrency',$this->mcurrency);
		$criteria->compare('payterms',$this->payterms,true);
		$criteria->compare('isauction',$this->isauction);
		$criteria->compare('endsin',$this->endsin,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Requests the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
