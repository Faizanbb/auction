<?php

/**
 * This is the model class for table "documents".
 *
 * The followings are the available columns in table 'documents':
 * @property integer $ID
 * @property integer $uid
 * @property string $name
 * @property string $url
 * @property string $typeoffile
 * @property string $origname
 * @property integer $size
 *
 * The followings are the available model relations:
 * @property DocumentRequest[] $documentRequests
 * @property Users $u
 * @property ItemDocuments[] $itemDocuments
 * @property QuestanswersDocument[] $questanswersDocuments
 * @property ReqItemDoc[] $reqItemDocs
 * @property RequisitionDocument[] $requisitionDocuments
 */
class Documents extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'documents';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, name, url, typeoffile, origname, size', 'required'),
			array('uid, size', 'numerical', 'integerOnly'=>true),
			array('name, typeoffile, origname', 'length', 'max'=>250),
			array('url', 'length', 'max'=>350),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, uid, name, url, typeoffile, origname, size', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'documentRequests' => array(self::HAS_MANY, 'DocumentRequest', 'did'),
			'u' => array(self::BELONGS_TO, 'Users', 'uid'),
			'itemDocuments' => array(self::HAS_MANY, 'ItemDocuments', 'doc_id'),
			'questanswersDocuments' => array(self::HAS_MANY, 'QuestanswersDocument', 'doc_id'),
			'reqItemDocs' => array(self::HAS_MANY, 'ReqItemDoc', 'doc_id'),
			'requisitionDocuments' => array(self::HAS_MANY, 'RequisitionDocument', 'did'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'uid' => 'Uid',
			'name' => 'Name',
			'url' => 'Url',
			'typeoffile' => 'Typeoffile',
			'origname' => 'Origname',
			'size' => 'Size',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('typeoffile',$this->typeoffile,true);
		$criteria->compare('origname',$this->origname,true);
		$criteria->compare('size',$this->size);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Documents the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
