<?php

/**
 * This is the model class for table "request_item_prices".
 *
 * The followings are the available columns in table 'request_item_prices':
 * @property integer $ID
 * @property integer $uid
 * @property integer $rid
 * @property integer $reqitem_id
 * @property double $price
 * @property string $createdat
 *
 * The followings are the available model relations:
 * @property Requests $r
 * @property Users $u
 * @property RequestItems $reqitem
 */
class RequestItemPrices extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'request_item_prices';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, rid, reqitem_id, price, createdat', 'required'),
			array('uid, rid, reqitem_id', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, uid, rid, reqitem_id, price, createdat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'r' => array(self::BELONGS_TO, 'Requests', 'rid'),
			'u' => array(self::BELONGS_TO, 'Users', 'uid'),
			'reqitem' => array(self::BELONGS_TO, 'RequestItems', 'reqitem_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'uid' => 'Uid',
			'rid' => 'Rid',
			'reqitem_id' => 'Reqitem',
			'price' => 'Price',
			'createdat' => 'Createdat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('rid',$this->rid);
		$criteria->compare('reqitem_id',$this->reqitem_id);
		$criteria->compare('price',$this->price);
		$criteria->compare('createdat',$this->createdat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RequestItemPrices the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
