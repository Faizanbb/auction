<?php

/**
 * This is the model class for table "requisition".
 *
 * The followings are the available columns in table 'requisition':
 * @property integer $ID
 * @property integer $user_id
 * @property integer $account_id
 * @property string $name
 * @property string $description
 * @property string $delivery_date
 * @property integer $quantity
 * @property string $capex_opex
 * @property integer $budgeted
 * @property string $budget
 * @property string $budget_currency
 * @property string $cost_center
 * @property integer $additional_documents
 * @property string $address
 * @property string $createdat
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property RequisitionApprover[] $requisitionApprovers
 * @property RequisitionDocument[] $requisitionDocuments
 */
class Requisition extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'requisition';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, account_id, name, description, delivery_date, quantity, capex_opex, budgeted, budget, budget_currency, cost_center, additional_documents, address, createdat, status', 'required'),
			array('user_id, account_id, quantity, budgeted, additional_documents', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>250),
			array('capex_opex, cost_center', 'length', 'max'=>100),
			array('budget', 'length', 'max'=>15),
			array('budget_currency', 'length', 'max'=>20),
			array('status', 'length', 'max'=>8),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, user_id, account_id, name, description, delivery_date, quantity, capex_opex, budgeted, budget, budget_currency, cost_center, additional_documents, address, createdat, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'requisitionApprovers' => array(self::HAS_MANY, 'RequisitionApprover', 'req_id'),
			'requisitionDocuments' => array(self::HAS_MANY, 'RequisitionDocument', 'rid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'user_id' => 'User',
			'account_id' => 'Account',
			'name' => 'Name',
			'description' => 'Description',
			'delivery_date' => 'Delivery Date',
			'quantity' => 'Quantity',
			'capex_opex' => 'Capex Opex',
			'budgeted' => 'Budgeted',
			'budget' => 'Budget',
			'budget_currency' => 'Budget Currency',
			'cost_center' => 'Cost Center',
			'additional_documents' => 'Additional Documents',
			'address' => 'Address',
			'createdat' => 'Createdat',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('account_id',$this->account_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('delivery_date',$this->delivery_date,true);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('capex_opex',$this->capex_opex,true);
		$criteria->compare('budgeted',$this->budgeted);
		$criteria->compare('budget',$this->budget,true);
		$criteria->compare('budget_currency',$this->budget_currency,true);
		$criteria->compare('cost_center',$this->cost_center,true);
		$criteria->compare('additional_documents',$this->additional_documents);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('createdat',$this->createdat,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Requisition the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
