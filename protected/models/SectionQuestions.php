<?php

/**
 * This is the model class for table "section_questions".
 *
 * The followings are the available columns in table 'section_questions':
 * @property integer $ID
 * @property integer $uid
 * @property integer $rid
 * @property integer $parentid
 * @property string $name
 * @property string $description
 * @property string $qtype
 * @property string $mandatory
 * @property string $document
 * @property integer $prequalify
 * @property string $createdat
 *
 * The followings are the available model relations:
 * @property Optionlist[] $optionlists
 * @property QAnswer[] $qAnswers
 * @property QuestionsAnswers[] $questionsAnswers
 * @property QuestionsAnswers[] $questionsAnswers1
 * @property QuestionsDocuments[] $questionsDocuments
 * @property Users $u
 * @property Requests $r
 */
class SectionQuestions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'section_questions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, rid, parentid, name, description, qtype, mandatory, document, prequalify, createdat', 'required'),
			array('uid, rid, parentid, prequalify', 'numerical', 'integerOnly'=>true),
			array('qtype', 'length', 'max'=>250),
			array('mandatory', 'length', 'max'=>3),
			array('document', 'length', 'max'=>350),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, uid, rid, parentid, name, description, qtype, mandatory, document, prequalify, createdat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'optionlists' => array(self::HAS_MANY, 'Optionlist', 'qid'),
			'qAnswers' => array(self::HAS_MANY, 'QAnswer', 'qid'),
			'questionsAnswers' => array(self::HAS_MANY, 'QuestionsAnswers', 'uid'),
			'questionsAnswers1' => array(self::HAS_MANY, 'QuestionsAnswers', 'qid'),
			'questionsDocuments' => array(self::HAS_MANY, 'QuestionsDocuments', 'qid'),
			'u' => array(self::BELONGS_TO, 'Users', 'uid'),
			'r' => array(self::BELONGS_TO, 'Requests', 'rid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'uid' => 'Uid',
			'rid' => 'Rid',
			'parentid' => 'Parentid',
			'name' => 'Name',
			'description' => 'Description',
			'qtype' => 'Qtype',
			'mandatory' => 'Mandatory',
			'document' => 'Document',
			'prequalify' => 'Prequalify',
			'createdat' => 'Createdat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('uid',$this->uid);
		$criteria->compare('rid',$this->rid);
		$criteria->compare('parentid',$this->parentid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('qtype',$this->qtype,true);
		$criteria->compare('mandatory',$this->mandatory,true);
		$criteria->compare('document',$this->document,true);
		$criteria->compare('prequalify',$this->prequalify);
		$criteria->compare('createdat',$this->createdat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SectionQuestions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
