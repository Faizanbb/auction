<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $_id;
    private $_name;
    // private $email;
    // private $password;
    // public $usertype;
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{	
            // print_r($this->username);exit;
            $user = Users::model()->findbyAttributes(array('email' => $this->username));
            //echo '<pre>';print_r();exit;
            
            //Login Authentication & Validation
            if ($user === null)
            {
                $this->errorCode = self::ERROR_USERNAME_INVALID;
            }
            else if ($user->password!= md5($this->password))
            {
            	$this->errorCode = self::ERROR_PASSWORD_INVALID;
            }
            else
            {   
                
                $groupRolesArray = GroupsRoles::model()->findAllbyAttributes(array('group_id' => $user->usergroup));

                $arr = array();
                foreach ($groupRolesArray as $value) {
                    $arr[] = $value->role_id;
                }
                //$implodeArr = implode("',", $arr);

                $roles = Yii::app()->db->createCommand()
                        ->select('GROUP_CONCAT(slug) as s')
                        ->from('roles')
                        ->where(array('in', 'ID', $arr))
                        ->queryRow();

                $assigned_roles = explode(',', $roles['s']);
                
                $this->_id = $user->ID;
                // $this->_accid = $user->accid;
                $this->username = $user->email;
                $this->_name = $user->name;
                $this->setState('name', $user->name);
                $this->setState('id', $this->_id); //Store user type for sepration
                $this->setState('ID', $this->_id); //Store user type for sepration
                $this->setState('accountId', $user->accid); //Store user account id for sepration
                $this->setState('email', $user->email); //Store user account id for sepration
                $this->setState('roles', $assigned_roles); // store user assigned roles
                $this->setState('usertype', $user->role);//Stores usertype in session
                // if($user->usertype=='3'){
                //     $this->setState('role', 'Vendor');
                // }elseif($user->usertype=='2'){
                //     $this->setState('role', 'Buyer');
                // }
                
                $this->errorCode = self::ERROR_NONE;
            }
            return !$this->errorCode;



		// $users=array(
		// 	// username => password
		// 	'demo'=>'demo',
		// 	'admin'=>'admin',
		// );

		// if(!isset($users[$this->username]))
		// 	$this->errorCode=self::ERROR_USERNAME_INVALID;
		// elseif($users[$this->username]!==$this->password)
		// 	$this->errorCode=self::ERROR_PASSWORD_INVALID;
		// else
		// 	$this->errorCode=self::ERROR_NONE;
		// return !$this->errorCode;
	}
}