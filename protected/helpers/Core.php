<?php

class Core
{
    public static function makeGrid($data)
    {
        $rec_html = '';
        if($data){
            
            foreach($data as $record){
                
                $user = Users::model()->with('acc')->findByPk($record->req->uid);
                $end_date = date("F d, Y",strtotime($record->req->endtime));
                $timezone = Timezone::model()->findByPk($record->req->timezone);
                //$tname = $timezone->name;
                //echo '<pre>';print_r($tname);exit;
                $rec_html .= '<tr>
                <td valign="middle">'.$record->req->name.'</td>
                <td valign="middle">Host: <strong>'.$user->name.'</strong><br>
                  Company: <strong>'.$user->acc->company.'</strong></td>
                <td valign="middle">'.$end_date.'</td>
                <td valign="middle"><a href="'.Yii::app()->createUrl('supplier/event',array('event'=> base64_encode($record->req->ID),'id'=>'')).'" class="accept-btn">View</a></td>
              </tr>';
                
            }
            
        }else{
            
            $rec_html = '<td colspan="4"> No Record Found. </td>';
                    
        }
        
        return $rec_html;
    }
        
}
?>