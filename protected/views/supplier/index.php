<?php
/* @var $this SupplierController */

$this->breadcrumbs=array(
	'Supplier',
);
?>

<!--content part start-->
<section class="content">
  <div class="content-inner ra-supplierview">

    <div class="tab-block active"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/invitations-icon.png" alt="reports"><span>Invitations</span></div>
    <div class="tab-open">
      <div class="tab-heading">
        <h1>Invitations </h1>
      </div>
      <div class="rfq-overview-outer">
        <div class="invitations-outer">
          <p>Please find below a list of Auctions you have been invited to participate in. Please select Accept if you wish to participate. Once accepted, you can read the rules of the Auction in the Overview tab.</p>
          <div class="invitations-table-outer">
            <div class="table-scroll">
            <table class="auction-description-table tablewidth100">
              <tr>
                <th>Auction Description <a href="#" data-toggle="tooltip" title="" data-original-title="Auction Description"><img src="dist/images/icon-b.png" alt=""></a></th>
                <th>Host Company <a href="#" data-toggle="tooltip" title="" data-original-title="Host Company"><img src="dist/images/icon-b.png" alt=""></a></th>
                <th>End Event <a href="#" data-toggle="tooltip" title="" data-original-title="Deadlines"><img src="dist/images/icon-b.png" alt=""></a></th>
                <th>Actions <a href="#" data-toggle="tooltip" title="" data-original-title="Status"><img src="dist/images/icon-b.png" alt=""></a></th>
              </tr>
              <?php echo $req_html?>
            </table>
            </div>
          </div>
        </div>
      </div>
    </div>
        
  </div>
</section>
<!--content part end--> 