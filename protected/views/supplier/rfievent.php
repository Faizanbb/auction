<?php 
$supplierId = $requestSup->supplier_id;
$requestId = $model->ID;
//echo '<pre>'; print_r($requestSup);exit;
?>  

<div class="tab-block invitations-block active"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/invitations-icon.png" alt="reports"><span>Invitations</span></div>
    <div class="tab-open">
      <div class="tab-heading">
        <h1>Invitations </h1>
      </div>
      <div class="rfq-overview-outer">
        <div class="invitations-outer">
          <p>Please find below a list of Auctions you have been invited to participate in. Please select Accept if you wish to participate. Once accepted, you can read the rules of the Auction in the Overview tab.</p>
          <div class="invitations-table-outer">
          <div class="table-scroll">
              <form class="form" method="POST" >
                  <table class="auction-description-table">
            <tr>
            <th>Auction Description <a href="javascript:void(0)" data-toggle="tooltip" title="" data-original-title="Auction Description"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/icon-b.png" alt=""></a></th>
            <th>Host Company <a href="javascript:void(0)" data-toggle="tooltip" title="" data-original-title="Host Company"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/icon-b.png" alt=""></a></th>
            <th>Deadlines <a href="javascript:void(0)" data-toggle="tooltip" title="" data-original-title="Deadlines"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/icon-b.png" alt=""></a></th>
            <th>Status <a href="javascript:void(0)" data-toggle="tooltip" title="" data-original-title="Status"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/icon-b.png" alt=""></a></th>
            </tr>
            <tr>
            <td valign="middle"><?php echo $model->name; ?></td>
            <td valign="middle">Host: <strong><?php echo $model->u->name; ?></strong><br>
            Company: <strong><?php echo $model->acc->company; ?></strong></td>
            <td valign="middle">Start Time: <br>
            <?php echo date("M d, Y H:i", strtotime($model->starttime)); ?> UTC + 04</td>
            <td valign="middle">
                <?php  if($requestSup->accept==1){?> 
                    Accepted
                <?php  }else if($requestSup->accept==2){?> 
                    Declined
                <?php  }else{?> 
                <input type="submit" id="AcceptJob" name="accept" class="accept-btn myBtn_multi" value="Accept" /><br>
                <input type="submit" id="rejectJob" class="decline-btn" name="decline" value="Decline" />
                <?php } ?>
            </td>
            </tr>
            <tr>
            <td valign="middle">&nbsp;</td>
            <td valign="middle">&nbsp;</td>
            <td valign="top" style="padding:0;">End Time  <br>
            <?php echo date("M d, Y H:i", strtotime($model->endtime)); ?></td>
            <td valign="middle">&nbsp;</td>
            </tr>
            </table>
              </form>
            
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-block"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/overview.png" alt="reports"><span>Overview</span></div>
    <div class="tab-open ">
      <div class="tab-heading">
        <h1>Overview</h1>
      </div>
      <div class="rfq-overview-outer">
        <ul class="tabs">
          <li class="active gen-info" rel="tab1">General Information</li>
       
          <li rel="tab2" class="pre-bid">Pre-Bid</li>
     
        </ul>
        <div class="tab_container">
          <h3 class="tab_drawer_heading d_active" rel="tab1">General Information</h3>
          <div id="tab1" class="tab_content" style="display: block;">
            <div class="supplierview-Information-outer">
              <div class="supplierview-Information-top">
                <h4>Description</h4>
                <div class="supplierview-Information-detail">
                  <p><?php echo $model->description; ?></p>
                </div>
              </div>
              <div class="supplierview-Information-top">
                <h4>Host Company Details</h4>
                <div class="supplierview-Information-detail">
                  <ul>
                    <li>Contact Name: <strong><?php echo $model->u->name; ?></strong></li>
                    <li>Company: <strong><?php echo $model->acc->company; ?></strong></li>
                    <li>E-mail: <a href="mailto:<?php echo $model->u->email; ?>"><?php echo $model->u->email; ?></a></li>
                    <li>Phone: <a href="tel:+971505342349;">+<?php echo $model->u->carriercode.$model->u->mobile;; ?></a></li>
                  </ul>
                </div>
              </div>
              <div class="supplierview-Information-top">
                <h4>Auction Summary</h4>
                <div class="supplierview-Information-detail">
                  <ul>
                    <li>Type of Auction: <strong>Reverse Auction</strong></li>
                    <li>Currency: <strong><?php echo $model->currency; ?></strong></li>
                    <li>Payment Terms: <strong><?php echo $model->payterms; ?></strong></li>
                    <li>Order Type: <strong><?php if($model->endsin=='order') echo 'Ends in Purchase Order'; else echo'Ends in Contract';?></strong></li>
                    <li>Automatic Extension Period: <strong><?php echo date('i', $model->exttime); ?> Minutes</strong></li>
                    <li>Applicable During Last: <strong><?php echo date('i', $model->exttime); ?> Minutes</strong></li>
                    <li>Minimum Bid Change: <strong><?php echo $model->lowestbid; ?> %</strong></li>
                    <li>Maximum Bid Change: <strong><?php echo $model->highestbid; ?> %</strong></li>
                    <li>"Tied Bid" Options: <strong><?php if($model->tiedbid=='equalbest') echo 'Equal Best';  if($model->tiedbid=='equalbest') echo 'Equal Worst';  else if($model->tiedbid=='bytimestamp') echo 'Seprate by time stamp';?> </strong></li>
                  </ul>
                </div>
              </div>
              <div class="supplierview-Information-top">
                <h4>Deadlines</h4>
                <div class="supplierview-Information-detail">
                  <ul>
                    <li>Questionnaire Deadline:
                        <p><?php echo date('M d, Y H:i', strtotime($model->starttime))?> UTC + 04</p>
                    </li>
                    <li>Auction Start:
                        <p><?php echo date('M d, Y H:i', strtotime($model->starttime))?> UTC + 04</p>
                    </li>
                    <li>Auction End:
                        <p><?php echo date('M d, Y H:i', strtotime($model->endtime))?> UTC + 04</p>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="supplierview-Information-top">
                <h4>Terms and Conditions</h4>
                <div class="supplierview-Information-detail">
                  <p><?php echo $model->termscond; ?></p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right text-right margin-top">
                <input name="" type="button" onclick="$('.invitations-block').trigger('click')" class="back-btn back-btn-2" value="Back">
                <input name="" type="button" onclick="$('.pre-bid').trigger('click')"  class="questionnaire-btn" value="Next">
              </div>
            </div>
          </div>
          <!-- #tab1 -->
          <h3 class="tab_drawer_heading" rel="tab2">Pre-Bid</h3>
          <div id="tab2" class="tab_content" style="display: none;">
            <div class="list-iteam">
              <h2>LIST OF ITEMS</h2>
              <div class="list-iteam-table">
                <table>
                  <tr class="tr-bold">
                    <td>Item Name</td>
                    <td>Quantity</td>
                    <td>Unit</td>
                    <td>Unit Price</td>
                    <td>Item Total</td>
                    <td>Enter New Bid</td>
                    <td>Rank</td>
                  </tr>
                  <?php  foreach($requestSup->req->requestItems as $reqitem){?>
                  <tr  data-sid="<?php echo $requestSup->supplier_id?>" data-rid="<?php echo $requestSup->req->ID?>" data-reqitem="<?php echo $reqitem->ID; ?>">
                    <td><strong><i class="fa fa-caret-down"></i> <?php echo $reqitem->name; ?></strong></td>
                    <td> <span class="quantity"><?php echo $reqitem->quantity; ?></span></td>
                    <td><?php echo $unitList[$reqitem->unit_id]; ?></td>
                    <td >
                     
                        
                    <?php  
                    
                        $curSupcurItem = '';
                        foreach($reqitem->requestItemPrices as $reqiprice){
                        $supPriceExist = false;
                        if($reqiprice->uid==$supplierId){$supPriceExist=true; $curSupcurItem= $reqiprice ;}
                        }
                        ?>
                        
                        
                        <?php if(!empty($curSupcurItem)){ ?>
                    <div class="aed pre-price-box <?php if($reqiprice->status==0 ) echo 'nodisplay'; ?>">
                            
                        <div class="aed-left">Aed</div>
                        <div class="aed-right"> <input  type="text" value="0" class="price-init noborder width-100 text-center pre-price  <?php if($reqiprice->status==2 ) echo 'nodisplay'; ?>" /> <?php if($reqiprice->status==2) echo $reqiprice->price; ?></div>
                    </div>
                        <?php }else{ ?>
                         <div class="aed pre-price-box nodisplay">
                            
                            <div class="aed-left">Aed</div>
                            <div class="aed-right"> <input  type="text" value="0" class="price-init noborder width-100 text-center pre-price " /></div>
                        </div>
                        <?php } ?>

                    </td>
                    <td>
                        <span class="item-total">
                        <?php
                            if(!empty($curSupcurItem)){?> 
                        <?php if($curSupcurItem->status==2) echo $curSupcurItem->price*$reqitem->quantity ?>
                        <?php } ?>
                            </span> 
                    </td>
                    <td class="action-btns"> 
                    <?php  if($requestSup->accept==1){
                        
                            if(!empty($curSupcurItem)){
                                
                                if($curSupcurItem->status==0){ ?>   
                            <a  href="javascript:void(0)" class="decline-btn ">Rejected</a>
                        <?php }else if($curSupcurItem->status==2){ ?>   
                       Bid Received 
                        <?php }else if($curSupcurItem->status==1){ ?>
                            <a href="#" data-status="2" class="accept-btn placeRejBid  submtiBid">Submit Bid</a>
                            <?php
                        }  } else{ ?>
                       
                    <a  data-status="1" href="javascript:void(0)" class="accept-btn placeRejBid">Place Bid</a><br>
                    <a  data-status="0" href="javascript:void(0)" class="decline-btn placeRejBid">Reject Bid</a>                        <?php } } ?>
                     
                      </td>
                    <td>&nbsp;</td>
                  </tr>
                  <?php } ?>
                </table>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right text-right margin-top">
                <div class="col-lg-3">
                  <div class="row">
                    <input name="" type="button" onclick="$('.gen-info').trigger('click')"  class="back-btn back-btn-2" value="Back">
                    <input name="" type="button"  onclick="$('#rejectJob').trigger('click')" class="decline-event-btn" value="Decline Event">
                  </div>
                </div>
                <div class="col-lg-2 pull-right">
                  <div class="row">
                    
                     <?php if($requestSup->accept!=1){?>  
                    <input name="" type="button" onclick="$('#AcceptJob').trigger('click')" class="questionnaire-btn" value="Accept">
                    <?php }else{ ?>
                     <input name="" type="button" onclick="$('.questionnaire-block').trigger('click')"  class="questionnaire-btn" value="Next">
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- #tab2 --> 
        </div>
      </div>
    </div>
    <div class="tab-block questionnaire-block <?php  if($requestSup->accept!=1) echo 'request-block disabled-block'; ?>"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/questionnaire-icon.png" alt="reports"> <span>Questionnaire</span></div>
    <div class="tab-open">
    <div class="tab-heading">
        <h1>Questionnaire</h1>
      </div>
      <div class="rfq-overview-outer">
     <form class="" id="questionAnswers" method="POST" enctype="multipart/form-data">
          <input type="hidden" name="requestId" id="reqid" value="<?php echo $requestSup->req->ID;?>" /> 
           <input type="hidden" name="supplierId" id="supid" value="<?php echo $supplierId;?>" />
    <?php
      $scount = 1;
      foreach($model->sectionQuestions as $sq)
      {
          if($sq->parentid==0){
              $qcount = 1;
      ?>
    <div class="questionnaire-general-information mb-0">
        
        <h3><?php echo $scount.'. '.$sq->name; ?></h3>
        <?php foreach($model->sectionQuestions as $q)
        {
          if($q->parentid==$sq->ID){
        ?>
        <div class="questionnaire-general-inner">

            <div class="questionnaire-general-top">
                <h4><?php echo $scount.'.'.$qcount.'. '.$q->name?></h4>
                <p><?php echo $q->description; ?></p>
                <div class="general-answer">
                     <?php 
                     $answer = '';
                     foreach($q->questionsAnswers1 as $qans){
                         if($qans->uid==$supplierId){
                             $answer=$qans->ques_ans;
                             $answerData=$qans;
                         }
                     }
                    if(isset($answerData)&&!empty($answerData))
                        echo '<input type="hidden" name="Question['.$q->ID.'][answerId]" value="'.$answerData->ID.'" />';
                   
                     ?>
                    
                    
                    
                    <?php   if($q->qtype==0){ ?> 
                    
                    <select <?php if(!empty($answer)) echo $answer ?>  <?php  if($q->mandatory=='Yes') echo 'required'; ?> name="Question[<?php echo $q->ID; ?>][answer]" class="form-control validate-field"> 
                        <option  value="">Select one</option> 
                        <option <?php if($answer=='Yes') echo 'selected'?>>Yes</option> 
                        <option <?php if($answer=='No') echo 'selected'?>>No</option>
                    </select>
                    
                    <?php  }
                    else if($q->qtype==1){  ?> 
                    <textarea name="Question[<?php echo $q->ID?>][answer]" cols="" rows=""  <?php  if($q->mandatory=='Yes') echo 'required'; ?>  placeholder="Enter your answer here..." class="answer"><?php if(!empty($answer)) echo $answer ?></textarea> 
                        <?php  }
                        elseif($q->qtype==2){  ?> 
                    <div class="form-group">
                    <select  name="Question[<?php echo $q->ID?>][answer]" class="form-control validate-field"  <?php  if($q->mandatory=='Yes') echo 'required'; ?> > 
                        <option value="">Select one</option> 
                        <?php if(isset($q->optionlists)&& !empty($q->optionlists)){ 
                            foreach($q->optionlists as $optl){?>
                        <option <?php if($answer==$optl->value) echo 'selected'?> ><?php echo $optl->value; ?></option>
                        <?php } }  ?>
                    </select>  
                    </div >
                    <?php  }
                    elseif($q->qtype==3){  ?> 
                   <div class="purchase-outer <?php  if($q->mandatory=='Yes') echo 'required'; ?>">
                        <div class="error error-checkbox"></div>

                         <ul class="checkbox-quest   ">
                           <?php if(isset($q->optionlists)&& !empty($q->optionlists)){ 
                                         foreach($q->optionlists as $optl){ 
                                             $checked = '';
                                         if(!empty($answer) && $optl->value==$answer){
                                            $checked = 'selected'; 
                                         }
                                             ?>
                          <li>
                              <div class="pacific-checkbox">
                                         <div class="custom-checkbox">
                                             <input name="Question[<?php echo $q->ID?>][answer][]" value='<?php echo $optl->value;?>' <?php if(!empty($checked)) echo 'checked';?> id="cbaq<?php echo $optl->ID;?>" type="checkbox" />
                                           <label  for="cbaq<?php echo $optl->ID;?>"></label>
                                         </div>
                                       </div><?php echo $optl->value;?>
                          </li>
                          <?php } } ?>
                         </ul>


                    </div>
                    <?php  } ?>
                    <?php if($q->document==1){ ?>
                    <div class="drag-answer">
                        <div data-id='<?php echo $q->ID; ?>' class="drag-answer-here drag-doc">
                         Drag & Drop your file here
                        </div>
                         <div class="attachment">
                             <label for="Question_<?php echo $q->ID;?>_doc" class="add-attachment-bnt">+ Add attachment</label>  
                        </div>
                        <div class="view-files ">
                        <div class="drag-documents-list">
                            <?php
                                $this->widget('CMultiFileUpload', array(
                                  'name'=>'Question['.$q->ID.'][doc]',
                                  'accept'=>'jpg|png|PNG',
                                  'remove'=> '[x]',
                                  'duplicate'=>'Already Selected',
                                  'denied'=>'File is not allowed',
                                  'options'=>array(
                                    'afterFileSelect'=>'function(e, v, m){ }',
                                    ),
                                  'max'=>10, // max 10 files
                                    'htmlOptions'=>array('class'=>' nodisplayimp ','multiple'=>'multiple','enctype'=>'multipart/form-data','onchange'=>'insertDocuemnt(this)')

                                ));
                            ?>
                              
                             
                            <ul >
                          
                           </ul>
                        
                                        <?php 
//                                       echo '<pre>'; print_r($q);exit;
            if(isset($q->questionsAnswers1)&&!empty($q->questionsAnswers1)){
                foreach($q->questionsAnswers1 as $qans){ 
                    if($qans->uid==$supplierId){
                        
            ?>
                <input type="hidden" name="Question[<?php echo $q->ID?>][answerId]"  value="<?php echo $qans->ID;?>" />
            
               
                                       <?php 
//                                               echo '<pre>';print_r($answers->questanswersDocuments);exit;
                                       if(isset($qans->questanswersDocuments )&&!empty($qans->questanswersDocuments )){ foreach($qans->questanswersDocuments as $doc){ ?>
                                       <li>
                                           <div class="drag-documents-leftbar"><strong><?php echo $doc->doc->origname;?></strong><span>File size : <?php echo $doc->doc->size/1000; ?> KB</span></div>
                                           <div class="drag-documents-right"><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/pin.png" alt="pin"></a><a download href="<?php echo $doc->doc->url.'/'.$doc->doc->name; ?>"  >View</a> <a data-qid='<?php echo $doc->ID?>' class='delete-af-doc' href="javascript:void(0)"  >remove</a> </div>
                                        </li>
                                       <?php } } ?>
                  
                            
                           
            <?php } } }  ?>  
                       </div>                 
                   </div>
                    </div>
                    <?php } ?>
                    
                </div>
            </div>

            </div>

      
        <?php $qcount++;}
        
          } ?> 
    </div>
            

        <?php  $scount++;}   } ?>

    <div class="request">
    <div class="request-btn-outer">
   <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right text-right margin-top">
                <div class="col-lg-3">
                  <div class="row">
                    <input name="" type="button" onclick="$('.gen-info').trigger('click')"  class="back-btn back-btn-2" value="Back">
                   
                  </div>
                </div>
                <div class="col-lg-2 pull-right">
                  <div class="row">
                    
                     
                    <input name="" type="submit" class="questionnaire-btn" value="Next">
                  </div>
                </div>
              </div>
            </div>
    </div>
   </div>

  
  </form>
      </div>
    </div>
    
    <div class="tab-block documentation-block <?php  if($requestSup->accept!=1) echo 'request-block disabled-block'; ?> "><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/documents.png" alt="reports"> <span>Documentation</span></div>
    <div class="tab-open">
      <div class="tab-heading">
        <h1 class="doc-heading">Documentation</h1>
      </div>
      <div class="requisition-inner">
        <div class="general-Information">
            
    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'document-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
        'clientOptions'=>array(
        'validateOnSubmit'=>true,
        ),
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),

)); 
  
?>
            <input type='hidden' name='Supplier[sid]' value='<?php echo $supplierId; ?>'/>
            <input type='hidden' name='Supplier[rid]' value="<?php echo $requestId; ?>" />
          <div class="text-center ">
               <div class="drag-documents doc-drag-section">
        <div class="drag-documents-input doc-drag-section drag-doc-here ">Drag documents here</div>
        <label for="Documents_name" id="Documents_name_label" class="approver-btn">Add document...</label>
         <?php
                    $this->widget('CMultiFileUpload', array(
                      'name'=>'Documents[name]',
                      'accept'=>'jpg|png|PNG',
                      'remove'=> '[x]',
                      'duplicate'=>'Already Selected',
                      'denied'=>'File is not allowed',
                      'options'=>array(
                        'afterFileSelect'=>'function(e, v, m){ console.log(m);'
                          . 'if($("javascript:void(0)"+m.current.id+"-li").length==0){'
                          . 'var data = " <li id=\'"+m.current.id+"-li\'>  <div class=\'drag-documents-leftbar\'><strong>"+v+" </strong></div><div class=\'drag-documents-right \'><a href=\'#\'><img src=\'./images/pin.png\' alt=\'pin\'></a><a href=\'javascript:void(0)\' class=\'unploded-doc\' onclick=\' dcd(this) \'  data-uuid=\'"+m.current.id+"\'  >Remove</a></div></li>";   $(".doc-drag-section .drag-documents-list ul").append(data);  '
                          . '}else{'
                          . ' var curtxt =  $("javascript:void(0)"+m.current.id+"-li").find(".drag-documents-leftbar").find("strong").text();  $("javascript:void(0)"+m.current.id+"-li").find(".drag-documents-leftbar").find("strong").text(curtxt+" ,"+v);'
                          . '}'
                          . ';fileLabelloyality();checkdocadded();}',
                          
                        ),
                      'max'=>10, // max 10 files
                        'htmlOptions'=>array('class'=>' nodisplayimp add-docs','multiple'=>'multiple','enctype'=>'multipart/form-data')

                ));
        ?>
        <div class="drag-documents-list">
         <ul> <?php 
             if(isset($supReqDocs) && !empty($supReqDocs)){
//                    print_r($reqDocs);exit;
                    foreach($supReqDocs as $d){
                       
                ?>
         <li>
         <div class="drag-documents-leftbar">
             <strong><?php echo $d->d->origname;?> </strong>
             <!--<span>File size : 12KB</span>-->
         </div>
             <div class="drag-documents-right "><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl?>/dist/images/pin.png" alt="pin"></a><a href="javascript:void(0)" class="delete-doc"  data-id="<?php echo $d->ID?>" data-did="<?php echo$d->d->ID;?>">Remove</a></div>
         </li>
         <?php   }
                }
                ?>
         </ul>
         </div>
        </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right text-right margin-top">
                <div class="col-lg-3">
                  <div class="row">
                    <input name="" type="button" onclick="$('.questionnaire-block').trigger('click')"  class="back-btn back-btn-2" value="Back">
                   
                  </div>
                </div>
                <div class="col-lg-2 pull-right">
                  <div class="row">
                    
                     
                    <input name="" type="submit" id="savedocuments" class="questionnaire-btn" value="Next">
                  </div>
                </div>
              </div>
            </div>
    </div>

	 </div>
          <?php $this->endWidget(); ?>
        </div>
      </div>
    <div class="tab-block auction-block getAuctionView <?php  if($requestSup->accept!=1) echo 'request-block disabled-block'; ?>"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/auction.png" alt="reports"> <span>Auctions</span></div>
    <div class="tab-open">
      <div class="tab-heading">
        <h1 class="auction-head">Auction</h1>
      </div>
      <div class="rfq-overview-outer">
          <div class="auction-item-list">
              <?php 
$supplierId = $requestSup->supplier_id;
$requestId = $model->ID;

?>

<div class="list-iteam">
   
    <h2>LIST OF ITEMS</h2>
    <div class="list-iteam-table">
      <table>
        <tr class="tr-bold">
          <td>Item Name</td>
          <td>Quantity</td>
          <td>Unit</td>
          <td>Unit Price</td>
          <td>Item Total</td>
          <td>Enter New Bid</td>
          <td>Rank</td>
        </tr>
        <?php foreach($requestSup->req->requestItems as $reqitem){?>
        <tr  data-sid="<?php echo $requestSup->supplier_id?>" data-rid="<?php echo $requestSup->req->ID?>" data-reqitem="<?php echo $reqitem->ID; ?>">
          <td><strong><i class="fa fa-caret-down"></i> <?php echo $reqitem->name; ?></strong></td>
          <td> <span class="quantity"><?php echo $reqitem->quantity; ?></span></td>
          <td><?php echo $unitList[$reqitem->unit_id];  ?></td>
          <td >


          <?php  

              $curSupcurItem = '';
              $allPricesofItem= array();
              
              foreach($reqitem->requestItemPrices as $reqiprice){
              $supPriceExist = false;
              if($reqiprice->uid==$supplierId){$supPriceExist=true; $curSupcurItem= $reqiprice ;}
              $allPricesofItem[] = $reqiprice->price;
              }
              ?>


              <?php if(!empty($curSupcurItem)){ ?>
                    <div class="aed pre-price-box <?php if($reqiprice->status==0 ) echo 'nodisplay'; ?>">

                        <div class="aed-left">Aed</div>
                        <div class="aed-right"> <input  type="text" value="<?php echo $curSupcurItem->price; ?>" class="price-init noborder width-100 text-center pre-price  <?php if($reqiprice->status==2 ) echo 'nodisplay'; ?>" /><span class="on-price"><?php if($reqiprice->status==2) echo $reqiprice->price; ?></span> </div>
                    </div>
              <?php }?>

          </td>
          <td>
              <span class="item-total">
              <?php
                  if(!empty($curSupcurItem)){?> 
              <?php if($curSupcurItem->status==2) echo $curSupcurItem->price*$reqitem->quantity ?>
              <?php } ?>
                  </span> 
          </td>
          <td class="action-btns"> 
          <?php  if($requestSup->accept==1){

                  if(!empty($curSupcurItem)){

                      if($curSupcurItem->status==0){ ?>   
                  <a  href="javascript:void(0)" class="decline-btn ">Rejected</a>
              <?php }else if($curSupcurItem->status==2){ ?>   
            <a  data-status="1" href="javascript:void(0)" class="accept-btn placeBid">Place Bid</a><br>
              <?php }else if($curSupcurItem->status==1){ ?>
                  <a href="#" data-status="2" class="accept-btn placeRejBid  placeBid">Submit Bid</a>
                  <?php
              }  } else{ ?>

          <a  data-status="1" href="javascript:void(0)" class="accept-btn placeRejBid">Place Bid</a><br>
          <a  data-status="0" href="javascript:void(0)" class="decline-btn placeRejBid">Reject Bid</a>                        <?php } }  ?>

            </td>
            
            <td data-id="<?php  if(!empty($curSupcurItem)) echo $curSupcurItem->ID ?>" class="item-rank">
            <?php
           if(!empty($curSupcurItem)){
            if($curSupcurItem->status==2){
            $maxv = max($allPricesofItem);
            $max = array_keys($allPricesofItem, max($allPricesofItem));
            
            $min = array_keys($allPricesofItem, min($allPricesofItem));
            $minv = min($allPricesofItem);
         
            if($curSupcurItem->price==$minv)
               echo '<div class="rank-green">'.($min[0]+1).'</div>';
            else if($curSupcurItem->price==$maxv)
               echo '<div class="rank-red">'.($max[0]+1).'</div>';
            else
               echo '<div class="rank-gray">'.($min[0]+1).'</div>';
            }
           }
            
              ?>
            </td>
        </tr>
        <?php } ?>
      </table>
    </div>
   
</div>
              
          </div>
        <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right text-right margin-top">
                <div class="col-lg-3">
                  <div class="row">
                    <input name="" type="button" onclick="$('.documentation-block').trigger('click')"  class="back-btn back-btn-2" value="Back">
                   
                  </div>
                </div>
                <div class="col-lg-2 pull-right">
                  <div class="row">
                    
                     
                    <input name="" type="button"  class="questionnaire-btn" value="Finish">
                  </div>
                </div>
              </div>
            </div>
      </div>
    </div>
       <div class="loading-gif">
        <img src="<?php echo Yii::app()->request->baseUrl?>/images/loading.gif" />
    </div>
    
    <script>
        var prevNowPlaying = null;
        getItemRank();
        function getItemRank(){
        var requestId = $("#reqid").val();
        var supplierId = $("#supid").val();
        var exdata = {'reqId':requestId,'supId':supplierId};

        $.ajax({
            type  :'POST',
            url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array("action"=>"getSupplierAuctionRank","id"=>'')) ?>',
            data     : exdata,
            success:function(data){
             data = JSON.parse(data);
                for(var i=0;i<data.length;i++){
                    $('.item-rank').each(function(){
                        
                        if($(this).data('id')==data[i].id){
                            $(this).html(data[i].rank);
                            }
                    });
                }
            },
            Error:function(data){
            alert("some thing went wrong please contact admin");
              },
            beforeSend: function() {
                // setting a timeout
            
            },
            complete: function() {
             
                if(prevNowPlaying) {
                    clearInterval(prevNowPlaying);
                }
               
                    prevNowPlaying = setInterval(function () {
                    getItemRank();
                   }, 5000);
                
            },   
           
       });
    }
    $(document).on('click','.placeBid',function(e){

            e.preventDefault();
            var trparent = $(this).parent().parent();
            
            var supId = $(trparent).data('sid');
            var reqId = $(trparent).data('rid');
            var reqItemId = $(trparent).data('reqitem');
            var status = $(this).data('status');
            var pprice = $(trparent).find('.pre-price').val();
            
            
            var exdata = {'supplierId':supId,'requestId':reqId,'requestIId':reqItemId,'bstatus':status,'price':pprice};
             if(status==1){
                $(trparent).find('.pre-price-box  .pre-price').css('display','inline-block');
                $(trparent).find('.pre-price-box').find('.on-price').css('display','none');
               $(trparent).find('.action-btns').html('<a href="#" data-status="2" class="accept-btn placeBid  submtiBid">Submit Bid</a>');
               
                }else if(status==2){
            var el = $(this);
            $.ajax({
                type  :'POST',
                url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array("action"=>"acceptItemBid","id"=>'')) ?>'+reqId,
                data     : exdata,
                success:function(data){

                
                      $(trparent).find('.pre-price-box .pre-price').css('display','none');
                      $(trparent).find('.pre-price-box').find('.on-price').css('display','block');
                      $(trparent).find('.pre-price-box').find('.on-price').text(pprice);
                   $(el).parent().html('<a  data-status="1" href="javascript:void(0)" class="accept-btn placeBid">Place Bid</a>');

                },
                Error:function(data){
                alert("some thing went wrong please contact admin");
                  },
                beforeSend: function() {
                    // setting a timeout
                    $('.loading-gif').show();
                },
                complete: function() {
                    $('.loading-gif').hide();
                },   

           });
        }
    });
    $(document).on('click','.placeRejBid',function(e){

            e.preventDefault();
            var trparent = $(this).parent().parent();
            
            var supId = $(trparent).data('sid');
            var reqId = $(trparent).data('rid');
            var reqItemId = $(trparent).data('reqitem');
            var status = $(this).data('status');
            var pprice = $(trparent).find('.pre-price').val();
            
            
            var exdata = {'supplierId':supId,'requestId':reqId,'requestIId':reqItemId,'bstatus':status,'price':pprice};
            var el = $(this);
        $.ajax({
            type  :'POST',
            url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array("action"=>"acceptItemBid","id"=>'')) ?>'+reqId,
            data     : exdata,
            success:function(data){
           
              if(status==1){
                $(trparent).find('.pre-price-box').css('display','inline-block');
               $(el).parent().html('<a href="#" data-status="2" class="accept-btn placeRejBid  submtiBid">Submit Bid</a>');
                }else if(status==0){
                    $(el).parent().html('<a href="javascript:void(0)" class="rejected-btn myBtn_multi">Rejected</a>');
                   
                }else if(status==2){
                   $(trparent).find('.pre-price-box .aed-right').html(pprice);
                   $(trparent).find('.action-btns').html('Bid Received ');
                   
                }
            },
            Error:function(data){
            alert("some thing went wrong please contact admin");
              },
            beforeSend: function() {
                // setting a timeout
                $('.loading-gif').show();
            },
            complete: function() {
                $('.loading-gif').hide();
            },   
           
       });
    });
    $(document).on('keyup','.pre-price',function(){
        var trparent = $(this).parent().parent().parent().parent();
        console.log(trparent);
        var totalprice = parseInt($(trparent).find('.quantity').text())*parseInt($(this).val());
        $(trparent).find('.item-total').text(totalprice);
       
    });
    $(document).on('click','.delete-af-doc',function(){
    var status = confirm("Are you sure you want to delete it.");   
    var curElement = $(this);
    
    if( status){
       var id = $(this).data('qid'); 
       var extdata = {'questansDocId':id};
       $.ajax({
                beforeSend: function() {
                    // setting a timeout
                    $('.loading-gif').show();
                },
                complete: function() {
                    $('.loading-gif').hide();
                },          type  :'POST',
          url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array('action'=>'deleteQuestAnswerDoc','id'=>''))?>',
          data  :extdata,
          success:function(data){
                $(curElement).parent().parent().remove();
         },
          Error:function(data){
          alert("some thing went wrong please contact admin");
            }
           
       });
    
    }
    });
    $(document).on('submit','#questionAnswers',function(e){
       e.preventDefault();
       // validation : check if required checkboxes are checked;
       var validationStatus = true;
       $('.purchase-outer.required').each(function(){
           var parent = $(this);
           
        if($(parent).find('input[type=checkbox]:checked').length<=0){
            
            $(parent).find('.error-checkbox').text('Please select atleast one option');
            scrollBox($(parent).find('.error-checkbox'));
            validationStatus =  false;
            
        }
        
       
        });
         var requestId = $('#reqid').val();
         if(validationStatus){
             $(parent).find('.error-checkbox').text('');
        $.ajax({
                beforeSend: function() {
                    // setting a timeout
                    $('.loading-gif').show();
                },
                complete: function() {
                    $('.loading-gif').hide();
                },     
                type     :'POST',
            url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'saveSupQuestionAnswers','id'=>'')); ?>'+requestId,
            data     : new FormData( this ),
            processData: false,
            contentType: false,
            success  :function(data){
            $('.documentation-block').trigger('click');
            scrollBox($('.doc-heading'));
             $('.loading-gif').hide();
            },
            error    : function(){
                 alert('some thing went wrong');
                   $('.loading-gif').hide();
             }
        })
        }
   });
    $(document).on('submit','#document-form',function(e){
       e.preventDefault();
         var requestId = $('#reqid').val();
        
        $.ajax({
                beforeSend: function() {
                    // setting a timeout
                    $('.loading-gif').show();
                },
                complete: function() {
                    $('.loading-gif').hide();
                },            type     :'POST',
            url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'saveSupDocument','id'=>'')); ?>'+requestId,
            data     : new FormData( this ),
            processData: false,
            contentType: false,
            success  :function(data){
                var avdata = JSON.parse(data);
                var apdata= '';
                for(var i=0;i<avdata.length;i++){
                 var apdata =apdata+'<li>         <div class="drag-documents-leftbar">             <strong>'+avdata[i].name+'</strong>           </div>             <div class="drag-documents-right "><a href="javascript:void(0)"><img src="'+avdata[i].src+'/dist/images/pin.png" alt="pin"></a><a href="javascript:void(0)" class="delete-doc"  data-id="'+avdata[i].ID+'>" data-did="'+avdata[i].did+'">Remove</a></div>         </li>';
             }
               
                $(".doc-drag-section .drag-documents-list ul").html('');
                $(".doc-drag-section .drag-documents-list ul").append(apdata);
                $('#Documents_name-wrap').find('.add-docs').each(function(){
                    if(!$(this).is(':last-child'))
                        $(this).remove();
                });
                
                $('.auction-block').trigger("click");
                scrollBox($('.auction-head'));
            },
            error    : function(){
                 alert('some thing went wrong');
             }
        })
     
   });
        </script>
        
        
        <!--drag drop documents-->
        
<script>
    function checkdocadded(){
        if($('.add-docs').length > 1){
          if($('.doc-req-edit').hasClass('docsedit')){
              $('.doc-req-edit').removeClass('docsedit')
          } 
        } 
      }
var isAdvancedUpload = function() {
  var div = document.createElement('div');
  return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
}();

var $form = $('.drag-doc');

if (isAdvancedUpload) {
  $form.addClass('has-advanced-upload');
  var droppedFiles ;
  $form.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
    e.preventDefault();
    e.stopPropagation();
  })
  .on('dragover dragenter', function() {
    $form.addClass('is-dragover');
  })
  .on('dragleave dragend drop', function() {
    $form.removeClass('is-dragover');
  })
  .on('drop', function(e) {
    droppedFiles = e.originalEvent.dataTransfer.files;
    var lilen  =  $(this).siblings('.view-files').find(".doc-drag-section .drag-documents-list ul li").length;
       var qustid = $(this).data("id");
     
       var datapfirst = "<li id='dorp-li-"+lilen+"'><div class='drag-documents-leftbar'><strong>";
       var dataplast=" </strong><!--<span>File size : 12KB</span>--></div><div class='drag-documents-right '><a href='#'><img src='<?php echo Yii::app()->theme->baseUrl?>/dist/images/pin.png' alt='pin'></a><a href='javascript:void(0)' onclick='deleteDrop(this)' class='delete-drop add-docs' >Remove</a></div> <input type='file'class='nodisplayimp' id='doc_elment_"+lilen+"' multiple='multiple' name='Question["+qustid+"][doc][]' /></li>";   
      var datapmiddle = "";
    for(var i = 0; i<droppedFiles.length;  i++){
        if(i==0)
        datapmiddle = droppedFiles[i].name;
        else
        datapmiddle =datapmiddle+" ,"+ droppedFiles[i].name;
         
       
    }
    var datap= datapfirst + datapmiddle+ dataplast;
     ;
    $(this).siblings('.view-files').find(".drag-documents-list ul").append(datap);
    
     $("#dorp-li-"+lilen).find('input[type="file"]')
        .prop("files",droppedFiles);

    checkdocadded();

//    
  });
}
   function deleteDrop(el){
       $(el).parent().parent().remove(); 
    };

    function insertDocuemnt(item){
        
       $(item).parent().find('li.ext').remove();
    for(var i=0;i<item.files.length;i++){
        
        var size = item.files[i].size/1000;
        var dataap = ' <li class="ext"> <div class="drag-documents-leftbar"><strong>'+item.files[i].name+' </strong><span>File size : '+size+'KB</span></div> <div class="drag-documents-right"><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/pin.png" alt="pin"></a><a class="delete-bf-doc" href="javascript:void(0)">Remove</a></div> </li>';

        $(item).parent().append(dataap);
    }

    }
    $(document).on("click",'.delete-bf-doc',function(){
        $(this).parent().parent().remove();
    });
     
</script>

<script>
      function dcd(el){
                $(el).parent().parent().remove();
        var nid = $(el).data('uuid');
        $('#'+nid).remove();
        fileLabelloyality();
    };
    function fileLabelloyality(){
       var id =  $('#Documents_name-wrap').find('input').last().attr('id');
        console.log(id);
        $('#Documents_name_label').attr('for',id);
        
    }
    function deleteDrop(el){
       $(el).parent().parent().remove(); 
    };

  $(document).on('click','.delete-doc',function(){
    var status = confirm("Are you sure you want to delete it.");   
    var curElement = $(this);
    
    if( status){
       var id = $(this).data('id'); 
       var did = $(this).data('did'); 
       var extdata = {'reqdid':id,'docid':did};
       $.ajax({
                beforeSend: function() {
                    // setting a timeout
                    $('.loading-gif').show();
                },
                complete: function() {
                    $('.loading-gif').hide();
                },          type  :'POST',
          url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array('action'=>'deleteDocument','id'=>''))?>',
          data  :extdata,
          success:function(data){
                $(curElement).parent().parent().remove();
         },
          Error:function(data){
          alert("some thing went wrong please contact admin");
            }
           
       });
    
    }
    });
    
</script>
<script>


var $form = $('.drag-doc-here');
var isAdvancedUpload = function() {
  var div = document.createElement('div');
  return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
}();
if (isAdvancedUpload) {
  $form.addClass('has-advanced-upload');
  var droppedFiles ;
  $form.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
    e.preventDefault();
    e.stopPropagation();
  })
  .on('dragover dragenter', function() {
    $form.addClass('is-dragover');
  })
  .on('dragleave dragend drop', function() {
    $form.removeClass('is-dragover');
  })
  .on('drop', function(e) {
      
    droppedFiles = e.originalEvent.dataTransfer.files;
    var lilen  =  $(".doc-drag-section .drag-documents-list ul li").length;
    
       var datapfirst = "<li id='dorp-li-"+lilen+"'><div class='drag-documents-leftbar'><strong>";
       var dataplast=" </strong><!--<span>File size : 12KB</span>--></div><div class='drag-documents-right '><a href='#'><img src='<?php echo Yii::app()->theme->baseUrl?>/dist/images/pin.png' alt='pin'></a><a href='javascript:void(0)' onclick='deleteDrop(this)' class='delete-drop add-docs' >Remove</a></div> <input type='file'class='nodisplayimp' id='doc_elment_"+lilen+"' multiple='multiple' name='Documents[name][]' /></li>";   
      var datapmiddle = "";
    for(var i = 0; i<droppedFiles.length;  i++){
        if(i==0)
        datapmiddle = droppedFiles[i].name;
        else
        datapmiddle =datapmiddle+" ,"+ droppedFiles[i].name;
         
       
    }
    var datap= datapfirst + datapmiddle+ dataplast;
    $(".doc-drag-section .drag-documents-list ul").append(datap);
    
     $("#dorp-li-"+lilen).find('input[type="file"]')
        .prop("files",droppedFiles);

    checkdocadded();

//    
  });
}

</script>