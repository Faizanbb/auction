<link href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/css/style.css" rel="stylesheet">
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/css/stylesheet.css" rel="stylesheet" type="text/css">

<div class="preview-invitation-email-content">
    <div class="invitation-email-content-top">
    <p>Hi</p><br>
    <p>I am inviting you to participate on a request below. Click the name of the request to see details and please submit your bid through our online platform.</p>
    <p><span><?php echo $model->name; ?></span>
        The deadline for making bids is: <?php echo date('M d, Y H:i', strtotime($model->endtime)); ?>  <?php if(isset($timezone))echo $timezone->name; ?></p>
    <p><strong>Description of the request:</strong><br>
    Annual Contract for Cleaning Services for Office 503, Indigo Icon Tower, Cluster F, JLT. Dubai.</p>
    </div>

    <div class="invitation-email-content-top">
        <p><strong>If you would like to participate in this request, please <a href="<?php echo Yii::app()->createAbsoluteUrl('requests/supplierview',array('id'=> base64_encode($model->ID)),'http');?>">click here</a> so we will know to wait for your bid.</strong>

    However if this request is of no interest to you, <span>just let me know.</span></p>
    <div class="participate-btn">
    <a href="#">Want to Participate</a> <a href="#">Dont want to Participat</a>
    </div>
    </div>
    <div class="address">
    Kind regards,<br>
    <?php echo $model->u->name;?> <br>
    <?php echo $model->u->acc->company;?><br>
    <?php echo "+".$model->u->carriercode.$model->u->mobile;?>
    </div>
</div>