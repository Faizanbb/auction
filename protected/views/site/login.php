<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<div class="sign-up-outer">
<h1>Login</h1>

<!-- <p>Please fill out the following form with your login credentials:</p> -->
<div class="sign-up-form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
	<!-- Flash messages -->
	<?php
		foreach (Yii::app()->user->getFlashes() as $type => $flash) {
			echo "<div class='{$type}' style='color:red;'>{$flash}</div>";
		}
	?>
	<br>
	<!-- <p class="note">Fields with <span class="required">*</span> are required.</p> -->

	<div class="row">
		<?php //echo $form->labelEx($model,'email',array('class'=>'sign-up-input')); ?>
		<?php echo $form->textField($model,'email',array('class'=>'sign-up-input','placeholder'=>'Email')); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('class'=>'sign-up-input','placeholder'=>'Password')); ?>
		<?php echo $form->error($model,'password'); ?>
		
	</div>
        
        
        
	<div class="row rememberMe">
            <span class="borderspan">		
                <?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
            </span>
            
            <span class="spantext">Remember me next time</span>
            
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row buttons loginbutton">
		<?php echo CHtml::submitButton('Login',array('class'=>'register-btn login-button')); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
</div>