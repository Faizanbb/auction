<?php foreach ($items as $item){?>
             <tr>
                <td><strong><i class="fa fa-caret-down"></i> <?php echo $item->name;?></strong></td>
                <td><?php echo $item->item_number;?></td>
                         <td >
            
            <button type="button" class="attach-button viewAttachFile view-attach-btn myBtn_multi">View</button>
            
            <div class="modal modal_multi " id="viewAttachFile" > 
            <div class="modal-content questionnaire-popup"> 
                <span class="close close_multi" data-dismiss='modal'><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/close-btn.png" alt="close"></span>
                  <h3>View Files</h3>

                  <div class="questionnaire-popup-inner">
                    <div class="view-files">
                        <div class="drag-documents-list">
                            <?php
                                $this->widget('CMultiFileUpload', array(
                                  'name'=>'Items['.$item->ID.'][documents]',
                                  'accept'=>'jpg|png|PNG',
                                  'remove'=> '[x]',
                                  'duplicate'=>'Already Selected',
                                  'denied'=>'File is not allowed',
                                  'options'=>array(
                                    'afterFileSelect'=>'function(e, v, m){ }',
                                    ),
                                  'max'=>10, // max 10 files
                                    'htmlOptions'=>array('class'=>' nodisplayimp ','multiple'=>'multiple','enctype'=>'multipart/form-data','onchange'=>'insertDocuemnt(this)')

                                ));
                            ?>
                              
                             <!--<label  for='Items_<?php echo $item->ID;?>_documents' class="attach-field" >+ Attach File</label>-->
                          
                            <ul>
                               <?php 
//                               echo '<pre>';print_r($item->reqItemDocs);exit;
                               if(isset($item->reqItemDocs)&&!empty($item->reqItemDocs)){ foreach($item->reqItemDocs as $doc){ ?>
                               <li>
                                   <div class="drag-documents-leftbar"><strong><?php echo $doc->doc->origname;?></strong><span>File size : <?php echo $doc->doc->size/1000; ?> KB</span></div>
                                   <div class="drag-documents-right"><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/pin.png" alt="pin"></a><a href="javascript:void(0)" class="delete-af-doc" data-idid="<?php echo $doc->ID?>" >Remove</a></div>
                                </li>
                               <?php } } ?>
                           </ul>
                        </div>
                   </div>
                    <div class="question-form-btn">
                    
                    <!--<input  type="button" data-dismiss="modal" class="questionnaire-button" value="Save">-->
                      <input  type="button" class="close-btn" data-dismiss='modal' value="Cancel">
                    </div>
                  </div>

            </div>
            </div>
             </td>
                <td><?php echo $unitList[$item->unit_id];?></td>
                <td><div class="input-group spinner">
                        <input disabled="" type="text" class="form-control" value="<?php echo $item->quantity;?>">
                    <div class="input-group-btn-vertical">
                      <button class="btn" type="button"><i class="fa fa-caret-down"></i></button>
                      <button class="btn" type="button"><i class="fa fa-caret-up"></i></button>
                    </div>
                  </div></td>
                <td><a href="javascript:void(0)"  class="edit-delete edit-items"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> <a data-id="<?php echo $item->ID;?>"    href="javascript:void(0)" class="price-delete delete-af-row"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    <input name="" disabled="" value="<?php echo $item->price;?>" type="text" class="price-input"></td>
              </tr>
             
             <?php } ?>