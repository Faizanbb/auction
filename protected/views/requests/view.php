<?php
/* @var $this RequestsController */
/* @var $model Requests */

$this->breadcrumbs=array(
	'Requests'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Requests', 'url'=>array('index')),
	array('label'=>'Create Requests', 'url'=>array('create')),
	array('label'=>'Update Requests', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Requests', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Requests', 'url'=>array('admin')),
);
?>

<h1>View Requests #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'uid',
		'name',
		'description',
		'endtime',
		'timezone',
		'starttime',
		'exttime',
		'lastperiod',
		'highestbid',
		'lowestbid',
		'reqtype',
		'createdat',
	),
)); ?>
