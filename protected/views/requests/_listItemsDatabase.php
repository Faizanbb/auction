<?php foreach ($items as $item){?>
             
             
        <tr data-rid ="<?php echo $item->ID;?> " >
            <td><input  type="text" class="item-name-input" value="<?php echo $item->name;?>" name='Items[<?php echo $item->ID;?>][name]'></td>
         <td><input  type="text" class="item-number-input" value="<?php echo $item->item_number;?>" name='Items[<?php echo $item->ID;?>][itemnumber]'></td>
         <td >
            
            <button type="button" class="attach-button viewAttachFile view-attach-btn myBtn_multi">View</button>
            
            <div class="modal modal_multi " id="viewAttachFile" > 
            <div class="modal-content questionnaire-popup"> 
                <span class="close close_multi" data-dismiss='modal'><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/close-btn.png" alt="close"></span>
                  <h3>View Files</h3>

                  <div class="questionnaire-popup-inner">
                    <div class="view-files">
                        <div class="drag-documents-list">
                            <?php
                                $this->widget('CMultiFileUpload', array(
                                  'name'=>'Items['.$item->ID.'][documents]',
                                  'accept'=>'jpg|png|PNG',
                                  'remove'=> '[x]',
                                  'duplicate'=>'Already Selected',
                                  'denied'=>'File is not allowed',
                                  'options'=>array(
                                    'afterFileSelect'=>'function(e, v, m){ }',
                                    ),
                                  'max'=>10, // max 10 files
                                    'htmlOptions'=>array('class'=>' nodisplayimp ','multiple'=>'multiple','enctype'=>'multipart/form-data','onchange'=>'insertDocuemnt(this)')

                                ));
                            ?>
                              
                             <label  for='Items_<?php echo $item->ID;?>_documents' class="attach-field" >+ Attach File</label>
                          
                            <ul>
                               <?php 
//                               echo '<pre>';print_r($item->reqItemDocs);exit;
                               if(isset($item->reqItemDocs)&&!empty($item->reqItemDocs)){ foreach($item->reqItemDocs as $doc){ ?>
                               <li>
                                   <div class="drag-documents-leftbar"><strong><?php echo $doc->doc->origname;?></strong><span>File size : <?php echo $doc->doc->size/1000; ?> KB</span></div>
                                   <div class="drag-documents-right"><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/pin.png" alt="pin"></a><a href="javascript:void(0)" class="delete-af-doc" data-idid="<?php echo $doc->ID?>" >Remove</a></div>
                                </li>
                               <?php } } ?>
                           </ul>
                        </div>
                   </div>
                    <div class="question-form-btn">
                            <input type="hidden" value="<?php echo $item->ID;?>" name="Items[<?php echo $item->ID;?>][cc]" />
                            <input type="hidden" value="<?php echo $item->ID;?>" name="Items[<?php echo $item->ID;?>][id]" />
                            <input type="hidden" value="<?php echo $item->ID;?>" name="Items[<?php echo $item->ID;?>][reqItem]" />
                    <input  type="button" data-dismiss="modal" class="questionnaire-button" value="Save">
                      <input  type="button" class="close-btn" data-dismiss='modal' value="Cancel">
                    </div>
                  </div>

            </div>
            </div>
         </td>
        <td><input type="button" value="Add" class="add-button myBtn_multi viewaddunit">
        <div class="modal modal_multi "> 
        
        <!-- Modal content -->
        <div class="modal-content questionnaire-popup unit-popup"> <span  data-dismiss="modal" class="close close_multi"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/close-btn.png" data-dismiss="modal" alt="close"></span>
        <h3>Unit</h3>
        <div class="unit-outer">
       
        <div class="unit-label">
        <label>Label</label>
        <input value="<?php echo $item->unit_label;?>" name="Items[<?php echo $item->ID;?>][unit_label]"  type="text" class="unit-label-input">
        </div>
        <div class="unit-amount">
        <label>Amount</label>
        <div class="input-group spinner">
        <input value="<?php echo $item->unit_amount;?>"  name="Items[<?php echo $item->ID;?>][unit_amount]"  type="text" class="form-control" value="1">
        <div class="input-group-btn-vertical">
        <button class="btn btn-inc" type="button"><i class="fa fa-caret-down"></i></button>
        <button class="btn btn-dec" type="button"><i class="fa fa-caret-up"></i></button>
        </div>
        </div>
        </div>
        <div class="unit-label">
        <label>Unit</label>
        
        <div class="drp">
        <?php 
        
        echo CHtml::dropDownList('Items['.$item->ID.'][unit]', $item->unit_id, $unitList); ?>
        </div>
        </div>
        </div>
        <div class="question-form-btn">
        <input  type="button"  data-dismiss="modal"  class="questionnaire-button" value="Save">
        <input  type="button"  data-dismiss="modal"  class="close-btn" value="Cancel">
        </div>
        </div>
        </div>
        </td>
        <td>
        <div class="input-group spinner">
        <input type="text" class="form-control" value="<?php echo $item->quantity;?>" name='Items[<?php echo $item->ID;?>][quantity]' >
        <div class="input-group-btn-vertical">
        <button class="btn btn-inc" type="button"><i class="fa fa-caret-down"></i></button>
        <button class="btn btn-dec" type="button"><i class="fa fa-caret-up"></i></button>
        </div>
        </div>
        </td>
        <td>
        <a href="javascript:void(0)" data-id="<?php echo $item->ID;?>"   class="price-delete delete-af-row"><i class="fa fa-trash" aria-hidden="true"></i></a>
            <input  type="text" name='Items[<?php echo $item->ID;?>][price]' value="<?php echo $item->price;?>" class="price-input">
        </td>
        </tr>
             <?php } ?>