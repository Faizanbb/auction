<?php
    /* @var $this RequestsController */
    /* @var $model Requests */
    /* @var $form CActiveForm */
    $scount=1;
//    foreach($model as $sq){
//        if($sq->parentid==0){
//            echo '<div class="section" data-secid= "'.$sq->ID.'"><h5>sNo. '.$scount.' Section '.$sq->name.' </h5>';
//            $qcount = 1;
//            foreach($model as $q){
//                if($sq->ID==$q->parentid){
//                    echo '<div class="questions"><button class="btn btn-primary edit-question" data-qid="'.$q->ID.'" data-secid="'.$sq->ID.'">edit</button>';
//                    echo '<div class="questions"><button class="btn btn-danger delete-question" data-qid="'.$q->ID.'" data-secid="'.$sq->ID.'">Delete</button>';
//                    echo '<p>qNo. '.$qcount.' </p>';
//                    echo '<p>name '.$q->name.' </p>';
//                    echo '<p>description '.$q->description.' </p></div>';
//                    $qcount++;
//                }
//            }
//            echo '<button class="btn btn-primary addQuestion" data-secid="'.$sq->ID.'" >Add Question</button>';
//            echo '<div class="addQuestionContent"></div></div></div>';
//            
//            $qcount++;
//        }
//    }

?>
            <div class="modal-content questionnaire-popup"> <span class="close close_multi"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/close-btn.png" data-dismiss='modal' alt="close"></span>
              <!--<h3>Pacific Trade Corp.</h3>-->
              <div class="questionnaire-popup-inner">
                <div class="question-section">
                  <div class="add-section">
                    <div class="add-section-leftbar"> <a href="javascript:void(0)" class="add-section-btn" id='addSection'>Add Section</a> <a href="javascript:void(0)" class="preview-section-btn clear clear-all-questions">Clear All Questions</a> </div>
                    <div class="add-section-rightbar"><a href="javascript:viod()" id="loadTemplates" class="load-template-btn">Load Template</a></div>
                  </div>
                    
                    <?php   $seccount = 1;  foreach($model as $sq){
                        if($sq->parentid==0){ ?>
                    <div class="pacific-general-information section" data-secid='<?php echo $sq->ID?>'>
                    <ul>
                      <li>
                        <div class="pacific-heading">
                          <h4><strong><?php echo $seccount;?>. <?php echo $sq->name;?></strong>
                              <a href="javascript:void(0)" class="load-template-btn addQuestion" data-secid="<?php echo $sq->ID;?>" >Add New Question</a></h4>
                        </div>
                      </li>
                    </ul>
                    <div class="pacific-list">
                      <ul>
                                                <?php  $qcount = 1;
                foreach($model as $q){
                    if($sq->ID==$q->parentid){?>
                        <li>
                          <div class="pacific-checkbox">
                            <div class="custom-checkbox">
                              <input id="cb<?php echo $seccount.$qcount;?>" type="checkbox">
                              <label for="cb<?php echo $seccount.$qcount;?>"></label>
                            </div>
                          </div>
                          <div class="pacific-right">
                            <h4><?php echo $seccount.'.'.$qcount.'.';?> <?php echo $q->name; ?>
                              <div class="edits-btn">
                                  <a href="javascript:void(0)" class="edit-button edit-question" data-qid="<?php echo $q->ID;?>" data-secid="<?php echo $sq->ID;?>"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/edit-icon.png" alt="edit"></a> 
                                  <a href="javascript:void(0)" class="edit-button delete-question" data-qid="<?php echo $q->ID;?>" data-secid="<?php echo $sq->ID;?>"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/del-icon.png" alt="del" ></a>
                              </div>
                            </h4>
                            <p><?php echo $q->description;?></p>
                            <a href="javascript:void(0)" class="text-btn">
                                <?php 
                                
                                    if($q->qtype==0){
                                        echo 'Yes/No';
                                    }elseif($q->qtype==1){
                                        echo 'Text';
                                    
                                    }elseif($q->qtype==2){
                                        echo 'Choose From List';
                                    }elseif($q->qtype==3){
                                        echo 'Checkbox';
                                    } 
                                ?>
                            </a> </div>
                        </li>
                        
                <?php $qcount++;} } $seccount++;?>
                      </ul>
                    </div>
                    </div>
                    <?php } }?>
                  
                    
                    <div class='col-sm-12 ' id='addSectionContent'>
                    
                    </div>
  
                  
                </div>
                <div class="question-form-btn">
                  <input name="" data-dismiss='modal' type="button" class="close-btn" value="Close">
                  <input name="" type="button" class="save-btn" id="save-as-template" value="Save As">
                </div>
              </div>
            </div>



