<?php
/* @var $this RequestsController */
/* @var $model Requests */
/* @var $form CActiveForm */


?>

<div class='row'>
    <div class='col-sm-12 ' id='questSectionContent'>
        
    </div>
</div>



<button class="btn btn-primary" id='questSection'>Fetch Questionnaire</button>

<script>
$(document).ready(function(){
   
   $('#questSection').on('click',function(){
       $.ajax({
           type     :'GET',
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'fetchSecQues','id'=>$requestId)); ?>',
           success  :function(data){
               
                   $('#questSectionContent').html(data);
         
           },
           error    : function(){
           alert('some thing went wrong');
            }
       })
   });
   $('#getRequestDocument').on('click',function(){
       $.ajax({
           type     :'GET',
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'requestDocument','id'=>$requestId)); ?>',
           success  :function(data){
               
                   $('#questSectionContent').html(data);
         
           },
           error    : function(){
           alert('some thing went wrong');
            }
       })
   });
   $(document).on('click','#addSection',function(){

        $.ajax({
            type     : 'GET',
            url      : '<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'newSection','id'=>$requestId)); ?>',
            success  : function(data){
                 $('#addSectionContent').html(data);
            },
            error    : function(){
                alert('some thing went wrong');
            }
        })
   });
   $(document).on('click','#savenewSection',function(){
        var formData = $("#section-form").serialize();
       $.ajax({
           type     :'POST',
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'savenewSection','id'=>$requestId)); ?>',
           data     : formData,
           success  :function(data){
                $('#questSection').trigger('click');
           },
           error    : function(){
                alert('some thing went wrong');
            }
       })
   });
   $(document).on('submit','#document-form',function(e){
        $.ajax({
            type     :'POST',
            url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'saverequestDocument','id'=>$requestId)); ?>',
            data     : new FormData( this ),
            processData: false,
            contentType: false,
            success  :function(data){
                $('#getRequestDocument').trigger("click");
            },
            error    : function(){
                 alert('some thing went wrong');
             }
        })
         e.preventDefault();
   });
   $(document).on('click','#savenewQuestion',function(){
        var formData = $("#question-form").serialize();
       $.ajax({
           type     :'POST',
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'savenewQuestion','id'=>$requestId)); ?>',
           data     : formData,
           success  :function(data){
                $('#questSection').trigger('click');
           },
           error    : function(){
           alert('some thing went wrong');
            }
       })
   });
   $(document).on('click','.addQuestion',function(){
   
        var qsecid = $(this).data('secid');
        var ndata = {'secId':qsecid};
        $.ajax({
           type     :'GET',
           data     : ndata,
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'addQuestion','id'=>$requestId)); ?>',
           success  :function(data){
                $('.section').each(function(){
                   
                    if($(this).data('secid')==qsecid){
                        $(this).find('.addQuestionContent').html(data);
                    }
                    
                });
           },
           error    : function(){
           alert('some thing went wrong');
            }
        });
   });
   $(document).on('click','.edit-question',function(){
   
        var qid = $(this).data('qid');
        var qsecid = $(this).data('secid');
        var ndata = {'qId':qid,'secId':qsecid};
        $.ajax({
           type     :'GET',
           data     : ndata,
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'editQuestion','id'=>$requestId)); ?>',
           success  :function(data){
                   
                $('.section').each(function(){
                    if($(this).data('secid')==qsecid)
                    $(this).find('.addQuestionContent').html(data);;
                });
           },
           error    : function(){
           alert('some thing went wrong');
            }
        });
   });
   $(document).on('click','.delete-question',function(){
   
        var qid = $(this).data('qid');
        var qsecid = $(this).data('secid');
        var ndata = {'qId':qid,'secId':qsecid};
        $.ajax({
           type     :'GET',
           data     : ndata,
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'deleteQuestion','id'=>$requestId)); ?>',
           success  :function(data){
                   
                $('#questSection').trigger('click');
           },
           error    : function(){
           alert('some thing went wrong');
            }
        });
   });
   
});    
</script>




<script src="<?php echo Yii::app()->theme->baseUrl; ?>/vendor/multifile/jquery.MultiFile.js"></script>
<?php
/* @var $this RequisitionController */
/* @var $model Requisition */
/* @var $form CActiveForm */
?>


    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'document-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
        'clientOptions'=>array(
        'validateOnSubmit'=>true,
        ),
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),

)); 
  
?>

<div class="form">
<div class="col-lg-12 requisition-form-inner">
        <div class="row">
        <h2>Additional documents <?php if($model->isNewRecord!=1){?><a href="javascript:void(0)" class="edit-btn doc-req-edit docsedit">Save</a><?php } ?></h2>
        
        
        <div class="requisition-inner text-center">
               <div class="drag-documents">
        <div class="drag-documents-input">Drag documents here</div>
        <label for="Documents_name" id="Documents_name_label" class="approver-btn">Add document...</label>
         <?php
                    $this->widget('CMultiFileUpload', array(
                      'model'=>$model,
                      'attribute'=>'name',
                      'accept'=>'jpg|png|PNG',
                      'remove'=> '[x]',
                      'duplicate'=>'Already Selected',
                      'denied'=>'File is not allowed',
                      'options'=>array(
                        'afterFileSelect'=>'function(e, v, m){ console.log(m);'
                          . 'if($("#"+m.current.id+"-li").length==0){'
                          . 'var data = " <li id=\'"+m.current.id+"-li\'>  <div class=\'drag-documents-leftbar\'><strong>"+v+" </strong></div><div class=\'drag-documents-right \'><a href=\'#\'><img src=\'./images/pin.png\' alt=\'pin\'></a><a href=\'javascript:void(0)\' class=\'unploded-doc\' onclick=\' dcd(this) \'  data-uuid=\'"+m.current.id+"\'  >Remove</a></div></li>";   $(".drag-documents-list ul").append(data);  '
                          . '}else{'
                          . ' var curtxt =  $("#"+m.current.id+"-li").find(".drag-documents-leftbar").find("strong").text();  $("#"+m.current.id+"-li").find(".drag-documents-leftbar").find("strong").text(curtxt+" ,"+v);'
                          . '}'
                          . ';fileLabelloyality();checkdocadded();}',
                          
                        ),
                      'max'=>10, // max 10 files
                        'htmlOptions'=>array('class'=>' nodisplayimp add-docs','multiple'=>'multiple','enctype'=>'multipart/form-data')

                ));
        ?>
        <div class="drag-documents-list">
         <ul> <?php
            
             if(isset($reqDocs)){
//                    print_r($reqDocs);exit;
                    foreach($reqDocs as $d){
                       
                ?>
         <li>
         <div class="drag-documents-leftbar">
             <strong><?php echo $d->d->origname;?> </strong>
             <!--<span>File size : 12KB</span>-->
         </div>
             <div class="drag-documents-right "><a href="#"><img src="<?php echo Yii::app()->theme->baseUrl?>/dist/images/pin.png" alt="pin"></a><a href="javascript:void(0)" class="delete-doc"  data-id="<?php echo $d->ID?>" data-did="<?php echo$d->d->ID;?>">Remove</a></div>
         </li>
         <?php   }
                }
                ?>
         </ul>
         </div>
        </div>
            <div class="error phone-error-container"></div>
            <input type="hidden" value='<?php echo $requestId; ?>' name='Request[rid]' />
            <button type='submit' id='savedocuments' class='submit-approval'>Submit doc</button>
            
	 </div>
        </div>
        
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
        function checkdocadded(){
         if($('.add-docs').length > 1){
           if($('.doc-req-edit').hasClass('docsedit')){
               $('.doc-req-edit').removeClass('docsedit')
           } 
         } 
       }
var isAdvancedUpload = function() {
  var div = document.createElement('div');
  return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
}();

var $form = $('.drag-documents-input');

if (isAdvancedUpload) {
  $form.addClass('has-advanced-upload');
  var droppedFiles ;
  $form.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
    e.preventDefault();
    e.stopPropagation();
  })
  .on('dragover dragenter', function() {
    $form.addClass('is-dragover');
  })
  .on('dragleave dragend drop', function() {
    $form.removeClass('is-dragover');
  })
  .on('drop', function(e) {
    droppedFiles = e.originalEvent.dataTransfer.files;
    var lilen  =  $(".drag-documents-list ul li").length;
       var datapfirst = "<li id='dorp-li-"+lilen+"'><div class='drag-documents-leftbar'><strong>";
       var dataplast=" </strong><!--<span>File size : 12KB</span>--></div><div class='drag-documents-right '><a href='#'><img src='<?php echo Yii::app()->theme->baseUrl?>/dist/images/pin.png' alt='pin'></a><a href='javascript:void(0)' onclick='deleteDrop(this)' class='delete-drop add-docs' >Remove</a></div> <input type='file'class='nodisplayimp' id='doc_elment_"+lilen+"' multiple='multiple' name='Documents[name][]' /></li>";   
      var datapmiddle = "";
    for(var i = 0; i<droppedFiles.length;  i++){
        if(i==0)
        datapmiddle = droppedFiles[i].name;
        else
        datapmiddle =datapmiddle+" ,"+ droppedFiles[i].name;
         
       
    }
    var datap= datapfirst + datapmiddle+ dataplast;
    $(".drag-documents-list ul").append(datap);
    
     $("#dorp-li-"+lilen).find('input[type="file"]')
        .prop("files",droppedFiles);

    checkdocadded();

//    
  });
}

</script>
<script>
      function dcd(el){
                $(el).parent().parent().remove();
        var nid = $(el).data('uuid');
        $('#'+nid).remove();
        fileLabelloyality();
    };
    function fileLabelloyality(){
       var id =  $('#Documents_name-wrap').find('input').last().attr('id');
        console.log(id);
        $('#Documents_name_label').attr('for',id);
        
    }
    function deleteDrop(el){
       $(el).parent().parent().remove(); 
    };
$(document).ready(function(){
//    var el = $('.MultiFile-wrap');
//    el.attr("id",'cfile-wrapper-id');
//    
    
    $('.delete-doc').on('click',function(){
    var status = confirm("Are you sure you want to delete it.");   
    var curElement = $(this);
    
    if( status){
       var id = $(this).data('id'); 
       var did = $(this).data('did'); 
       var extdata = {'reqdid':id,'docid':did};
       $.ajax({
          type  :'POST',
          url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array('action'=>'deleteDocument'))?>',
          data  :extdata,
          success:function(data){
              var data = JSON.parse(data);
              console.log(data);
              if(data.status)
                $(curElement).parent().parent().remove();
         },
          Error:function(data){
          alert("some thing went wrong please contact admin");
            }
           
       });
    
    }
    });
    
})  ;  
    
</script>



<script>
    $(document).on('click','.addMoreOption',function(){
        var apenddata = '<input type="text" placeholder="value...." name="OptionValue[]"/><br>';
        $(this).parent().append(apenddata); 
        
    });
    function checkOptions(val){
        if(val==2||val==3){
        var apenddata = '<button class="btn btn-default addMoreOption" type="button">Add More</button><input type="text" placeholder="value...." name="OptionValue[]"/>';
        $('.addOptionContent').html(apenddata); 
            
        }else{
            $(".addOptionContent").htm('');
        }
    }
    
</script>