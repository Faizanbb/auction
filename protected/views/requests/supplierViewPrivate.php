
<section class="requisition-form-outer text-center bg-gray">
 <div class="container-fluid">
   <div class="row">
   <div class="rfi-privateaccess">
    <div class="make-bid">
    <a href="#" class="make-bid-btn">make bid</a> <a href="#" class="decline-bid-btn">decline</a>
    </div>
    <div class="reprehenderit-outer">
     <h1>Duis aute irure dolor in reprehenderit</h1>
     <h1><?php echo $model->name?></h1>
    <div class="reprehenderit-list">
    <ul>
    <li>
    <div class="reprehenderit-icon"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/company-icon.png" alt=""></div>
    <div class="reprehenderit-detail"><p>Company</p><span><?php echo $model->acc->company; ?> </span></div>
    </li>
    <li>
    <div class="reprehenderit-icon"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/user.png" alt="user"></div>
    <div class="reprehenderit-detail"><p>Contact Person</p><span><?php echo $model->u->name; ?>  |  <a href="#"><?php echo '+'.$model->u->carriercode.''.$model->u->mobile; ?></a></span></div>
    </li>
    </ul>
    </div>
    <div class="time-detail">
     <ul>
      <li>Time Left:  <span>Deactive</span></li>
      <li>  End date: <span><?php echo date('M d, Y H:i', strtotime($model->endtime)); ?>  (UTC+04:00)</span></li>
     </ul>
    </div>
    
    
    <div class="reprehenderit-tabs">
     <ul class="tabs">
  <li class="active" rel="tab1">DETAILS</li>
  <li rel="tab2">DOCUMENTS</li>
  <li rel="tab3">FILL IN QUESTIONNAIRE</li>
</ul>
<div class="tab_container">
  <h3 class="d_active tab_drawer_heading" rel="tab1">DETAILS</h3>
  <div id="tab1" class="tab_content">
  <div class="reprehenderit-tabs-detail">
  <p class="mh-300">    <?php echo $model->description; ?></p>
<input name="" value="Next" class="next-btn" type="button">
<div class="reprehenderit-address">
	<h4>Enter email address to access RFI</h4>
    <div class="reprehenderit-address-outer">
    	<form action="" method="get">
        <input name="" class="address-filed" value="" type="text">
        <input name="" type="button" value="Send" class="address-btn">
        </form>
    </div>
</div>
  </div>
  </div>
  <!-- #tab1 -->
  <h3 class="tab_drawer_heading" rel="tab2">DOCUMENTS</h3>
  <div id="tab2" class="tab_content">
      <div class="reprehenderit-tabs-detail">
          <div class="mh-300">
          <?php if(isset($model->documentRequests)&&!empty($model->documentRequests)){?>
         <div class="documents-detail">
            <?php 

                foreach($model->documentRequests as $mdr){
            ?>
        <div class="documents-detail-inner">
        <div class="documents-detail-leftbar"><?php echo $mdr->d->origname; ?></div>
        <div class="documents-detail-rightbar">
        <ul>
            <li><?php  $type = explode('/', $mdr->d->typeoffile) ; echo $type[1];?></li>
        <li><?php echo $mdr->d->size/1000?> KB.</li>
        <li><a href="<?php echo Yii::app()->baseUrl.'/'.$mdr->d->url.'/'.$mdr->d->name;?>" >View</a></li>
        </ul>
        </div>
        </div>

            <?php } ?>


        </div>
          <?php } ?>
          </div>
        <input name="" value="Next" class="next-btn" type="button">
        <div class="reprehenderit-address">
            <h4>Enter email address to access RFI</h4>
            <div class="reprehenderit-address-outer">
                <form action="" method="get">
                <input name="" class="address-filed" value="" type="text">
                <input name="" type="button" value="Send" class="address-btn">
                </form>
            </div>
        </div>
      </div>
  </div>
  <!-- #tab2 -->
  <h3 class="tab_drawer_heading" rel="tab3">FILL IN QUESTIONNAIRE</h3>
  <div id="tab3" class="tab_content">
  	 <div class="reprehenderit-tabs-detail">
		<div class="materials-outer">
            <table>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
              </tr>
              <?php
              $scount = 1;
              foreach($model->sectionQuestions as $sq)
              {
                  if($sq->parentid==0){
                      $qcount = 1;
              ?>
              <tr class="gray-background">
               <td class="orange-color"><?php echo $scount;?>.</td>
               <td class="orange-color"><?php echo $sq->name?></td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
              </tr>
              <?php foreach($model->sectionQuestions as $q)
              {
                  if($q->parentid==$sq->ID){
                ?>
              <tr>
               <td valign="top"><?php echo $scount.'.'.$qcount;?></td>
               <td valign="top"><strong><?php echo $q->name?></strong>
               <p><?php echo $q->description?></p></td>
               <td>&nbsp;</td>
               <td valign="top"><?php   if($q->qtype==0){echo 'Yes/No';}elseif($q->qtype==1){echo 'Text';}elseif($q->qtype==2){echo 'Choose From List';}elseif($q->qtype==3){echo 'Checkbox';} ?></td>
              </tr>

                  <?php $qcount++;}} $scount++;} } ?>
            </table>
    </div>
        <input name="" value="Submit" class="next-btn" type="button">
    </div>
  </div>
  <!-- #tab3 -->
  
</div>
<!-- .tab_container -->
    </div>
    
    
    </div>
   </div>
  </div>
 </div>
</section>
<!--requisition form part end-->
