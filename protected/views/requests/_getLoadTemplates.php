<?php
/* @var $this RequestsController */
/* @var $model Requests */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'use-template-question-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
    )); ?>
            <div class="modal-content questionnaire-popup"> <span class="close close_multi"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/close-btn.png" data-dismiss='modal' alt="close"></span>
              <h3>Available Templates</h3>
              <div class="questionnaire-popup-inner">
                <div class="question-section">

                    <div class="question-form supplier-list">
                       <div class="hide-div supplier-box  pickgroup">
                        <ul>
                        <?php if(isset($model)&&!empty($model)){
                            foreach($model as $sa){
                            ?>
                              <li>
                                  <div class="pacific-checkbox">
                                  <div class="custom-checkbox">
                                   <input id="sg<?php echo $sa->ID;?>" value="<?php echo $sa->ID;?>" type="radio" name="Templates"><label for="sg<?php echo $sa->ID;?>"></label></div>
                                 </div> <span><?php echo $sa->name;?></span> 
                                 <!--<button type="submit" class="btn add-btn">Add</button>-->
                              </li>
                            <?php } }else{ echo '<li> No Group Found <li>'; } ?>
                        </ul>
                        </div>
                    </div>
                </div>
                  <input type="hidden" name="reqId" value="<?php echo $requestId;?>" />
                <div class="question-form-btn">
                  <input name="" type="button"  data-dismiss='modal'  class=" close-btn close-btn-question" value="Back">
                  <input name="" type="button" id='putNewTemplateQ' class="save-btn" value="Save">
                </div>
              </div>
            </div>
<!-- form -->

<?php $this->endWidget(); ?>

</div>