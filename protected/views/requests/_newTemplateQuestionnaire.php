<?php
/* @var $this RequestsController */
/* @var $model Requests */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'template-question-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
    )); ?>
            <div class="modal-content questionnaire-popup"> <span class="close close_multi"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/close-btn.png" data-dismiss='modal' alt="close"></span>
              <h3>create Template</h3>
              <div class="questionnaire-popup-inner">
                <div class="question-section">

                    <div class="question-form">
                        <label>New Template</label>
                            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>350,'class'=>'form-control')); ?>
                            <?php echo $form->error($model,'name'); ?>
                      
                    </div>
                </div>
                
                <div class="question-form-btn">
                  <input name="" type="button"  data-dismiss='modal'  class=" close-btn close-btn-question" value="Back">
                  <input name="" type="button" id='savenewTemplateQ' class="save-btn" value="Save">
                </div>
              </div>
            </div>
<!-- form -->

<?php $this->endWidget(); ?>

</div>