
<div class="tab-block auction-overview active"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/overview.png" alt="reports">Auction <br>overview</div>
    <div class="tab-open">
      <div class="tab-heading">
        <h1>Auction overview </h1>
      </div>
      <div class="rfq-overview-outer">
        <ul class="tabs">
          <li class="active" rel="tab1">General Information</li>
          <li rel="tab2" class="getSupPreBComp">Pre-Bid</li>
          <li rel="tab3" class="">Suppliers</li>
        </ul>
        <div class="tab_container">
          <h3 class="tab_drawer_heading d_active" rel="tab1">General Information</h3>
          <div id="tab1" class="tab_content" style="display: block;">
            <div class="supplierview-Information-outer">
              <div class="supplierview-Information-top">
                <h4>Description</h4>
                <div class="supplierview-Information-detail">
                  <p><?php echo $model->description; ?></p>
                </div>
              </div>
              <div class="supplierview-Information-top">
                <h4>Host Company Details</h4>
                <div class="supplierview-Information-detail">
                  <ul>
                    <li>Contact Name: <strong><?php echo $model->u->name; ?></strong></li>
                    <li>Company: <strong><?php echo $model->acc->company; ?></strong></li>
                    <li>E-mail: <a href="mailto:<?php echo $model->u->email; ?>"><?php echo $model->u->email; ?></a></li>
                    <li>Phone: <a href="tel:+971505342349;">+<?php echo $model->u->carriercode.$model->u->mobile;; ?></a></li>
                  </ul>
                </div>
              </div>
              <div class="supplierview-Information-top">
                <h4>Auction Summary</h4>
                <div class="supplierview-Information-detail">
                  <ul>
                    <li>Type of Auction: <strong>Reverse Auction</strong></li>
                    <li>Currency: <strong><?php echo $model->currency; ?></strong></li>
                    <li>Payment Terms: <strong><?php echo $model->payterms; ?></strong></li>
                    <li>Order Type: <strong><?php if($model->endsin=='order') echo 'Ends in Purchase Order'; else echo'Ends in Contract';?></strong></li>
                    <li>Automatic Extension Period: <strong><?php echo date('i', $model->exttime); ?> Minutes</strong></li>
                    <li>Applicable During Last: <strong><?php echo date('i', $model->exttime); ?> Minutes</strong></li>
                    <li>Minimum Bid Change: <strong><?php echo $model->lowestbid; ?> %</strong></li>
                    <li>Maximum Bid Change: <strong><?php echo $model->highestbid; ?> %</strong></li>
                    <li>"Tied Bid" Options: <strong><?php if($model->tiedbid=='equalbest') echo 'Equal Best';  if($model->tiedbid=='equalbest') echo 'Equal Worst';  else if($model->tiedbid=='bytimestamp') echo 'Seprate by time stamp';?> </strong></li>
                  </ul>
                </div>
              </div>
              <div class="supplierview-Information-top">
                <h4>Deadlines</h4>
                <div class="supplierview-Information-detail">
                  <ul>
                    <li>Questionnaire Deadline:
                        <p><?php echo date('M d, Y H:i', strtotime($model->starttime))?> UTC + 04</p>
                    </li>
                    <li>Auction Start:
                        <p><?php echo date('M d, Y H:i', strtotime($model->starttime))?> UTC + 04</p>
                    </li>
                    <li>Auction End:
                        <p><?php echo date('M d, Y H:i', strtotime($model->endtime))?> UTC + 04</p>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="supplierview-Information-top">
                <h4>Terms and Conditions</h4>
                <div class="supplierview-Information-detail">
                  <p><?php echo $model->termscond; ?></p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right text-right margin-top">
                <input name="" type="button" class="back-btn back-btn-2" value="Edit">
                <input name="" type="button" class="questionnaire-btn" value="Next">
              </div>
            </div>
          </div>
          <!-- #tab1 -->
          <h3 class="tab_drawer_heading" rel="tab2">Pre-Bid</h3>
          <div id="tab2" class="tab_content " style="display: none;">
              <div class="getSupPreBCompCont">
                  
              </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right text-right margin-top">
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-3">
                  <div class="row">
                    <input name="" type="button" class="back-btn back-btn-2" value="Back">
                  </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-8 pull-right">
                  <div class="row">
                    <input name="" type="button" class="back-btn back-btn-2" value="Edit">
                    <input name="" type="button" class="questionnaire-btn" value="Next">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- #tab2 -->
           <h3 class="tab_drawer_heading" rel="tab3">Suppliers</h3>
          <div id="tab3" class="tab_content" style="display: none;">
                        <div class="bids-received">
              <div class="bids-table-outer">
                <h4>Bids Received</h4>
                <div class="bids-received">
                  <table>
                      <?php if(isset($bidsGraph)) { $biddata=true; foreach($bidsGraph as $bdgraph){ if($bdgraph['bidstatus']==1){ $biddata=false; ?>
                    <tr>
                      <td><div class="supply">
                          <h5><?php echo $bdgraph['companyname']; ?></h5>
                          <div class="supply-detail"> <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/profile-icon.png" alt="icon"> <?php echo $bdgraph['name']; ?> </div>
                        </div></td>
                      <td>1</td>
                       <td><?php echo date('M d, H:i', strtotime($bdgraph['bidTime'])); ?></td>
                      <td>
                        <div class="dropdown">
                          <button class="btn action-button" type="button" data-toggle="dropdown">Action <span class="caret"></span></button>
                          <ul class="dropdown-menu action-button-dropdown">
                              <li onclick="fetchSupplierProfile(<?php echo $bdgraph['userid']?>,<?php echo $bdgraph['requestid']?>)" ><a  href="javascript:void(0)"  >View Bid</a></li>
                              <li onclick="enterBidManual(<?php echo $bdgraph['requestid'];?>,<?php echo $bdgraph['userid'];?>)"  ><a href="javascript:void(0)">Enter Manual Bid</a></li>
                            <li><a href="javascript:void(0)">Delete</a></li>
                          </ul>
                        </div>
                      </td>
                      <td><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/mail.png" alt="icon"></a></td>
                    </tr>
                      <?php } }  if($biddata)echo '<tr><td >No Data Found</td></tr>'; } ?>
                    
                  </table>
                </div>
              </div>
            </div>
            <div class="bids-received">
              <div class="bids-table-outer">
                <h4>Bids Not Received</h4>
                <div class="bids-received">
                  <table>
                     <?php if(isset($bidsGraph)) { $biddata = true; foreach($bidsGraph as $bdgraph){  if($bdgraph['bidstatus']==0){  $biddata=false; ?>
                     <tr>
                      <td><div class="supply">
                          <h5><?php echo $bdgraph['companyname']; ?></h5>
                          <div class="supply-detail"> <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/profile-icon.png" alt="icon"> <?php echo $bdgraph['name']; ?><strong class="not-read-bid">Not Read</strong> </div>
                        </div></td>
                      <td>&nbsp;</td>
                      <td>&nbsp</td>
                      <td><div class="dropdown">
                          <button class="btn action-button" type="button" data-toggle="dropdown">Action <span class="caret"></span></button>
                          <ul class="dropdown-menu action-button-dropdown">
                            <li><a href="javascript:void(0)">View Bid</a></li>
                            <li><a  href="javascript:void(0)">Enter Manual Bid</a></li>
                            <li><a href="javascript:void(0)">Delete</a></li>
                          </ul>
                        </div></td>
                      <td><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/mail.png" alt="icon"></a></td>
                    </tr>
                    <?php }  } if($biddata)echo '<tr><td >No Data Found</td></tr>';  } ?>
                
                  </table>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right text-right margin-top">
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                  <div class="row">
                    <input name="" type="button" class="back-btn back-btn-2" value="Back">
                  </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6 pull-right">
                  <div class="row">
                    <input name="" type="button" class="questionnaire-btn" value="Edit">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-block"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/documents.png" alt="reports"><span>Documentation</span></div>
    <div class="tab-open">
      <div class="tab-heading">
        <h1>documentation</h1>
      </div>
      <div class="rfq-overview-outer">
        <div class="drag-documents-list drag-documents-list-new">
          
            <?php if(isset($model->requestSuppliers)){
                foreach($model->requestSuppliers as $reqsup){
                   
                ?>
            <div class="drag-documents-outer">
            <?php $srdcount = 0; foreach($model->supplierReqDocs as $sreqs ){ 
                if($sreqs->uid==$reqsup->supplier->ID){
                    if($srdcount==0){
                    ?>
            <h3>Uploaded by <?php echo $reqsup->supplier->acc->company; ?></h3>
                <?php 
                $srdcount++;
                    }
                ?>
            <div class="drag-documents-outer-top">
              <div class="drag-documents-leftbar"><strong><?php echo $sreqs->d->origname; ?> </strong><span>File size : <?php echo $sreqs->d->size/1000?>KB</span></div>
              <div class="drag-documents-right"><a href="#"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/pin.png" alt="pin"></a><a href="<?php echo $sreqs->d->url.'/'.$sreqs->d->name; ?>" download>View</a></div>
            </div>
            <?php } } ?>
            
          </div>
            <?php } 
                } ?>
            
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12  pull-right text-right">
                <input name="" type="button" class="back-btn back-btn-2" value="Edit">
                <input name="" type="button" class="questionnaire-btn" value="Next">
            </div>
        </div>
      </div>
      
    </div>
        <div class="tab-block getSupAucPre getSupPreBComp "><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/auction.png" alt="reports"> Auction </div>
   <div class="tab-open  ">
             <div class="tab-heading">
            <h1>Auction</h1>
          </div>
       <div class="rfq-overview-outer">
       <div class="getSupPreBCompCont">
           
       </div>
       </div>

    </div>
    <div class="tab-block getSupBComp supplier-comparison "><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/supplier-icon.png" alt="reports"> Supplier <br>
      Comparison</div>
   <div class="tab-open supplierCompContent ">
      <div class="tab-heading">
        <h1>Supplier comparison<a href="javascript:void(0)" class="download-excel-btn">Download Excel Report</a></h1>
      </div>
      <div class="rfq-overview-outer">
        <div class="tab">
            <button class="tablinks getSupBComp active" onclick="openCity(event,'Paris')" id="defaultOpen">Compare Bid</button>
            <button class="tablinks getSupQComp " onclick="openCity(event,'London')">Compare Questionnaire</button>
        </div>
        <div id="Paris" class="tabcontent">
        </div>
        <div id="London" class="tabcontent">

            
            
     

        </div>
      </div>
    </div>
   <div class="tab-open enterBidManualCont nodisplay ">

    </div>

            <div class="modal modal_multi" id="companyProfileModal"> 
                <!-- Modal content -->
                
            </div>
    <div class="loading-gif">
        <img src="<?php echo Yii::app()->request->baseUrl?>/images/loading.gif" />
    </div>
        

<script>
      $(document).on('click','.viewBid',function(){
         
       var rid = $(this).data('rid'); 
       var uid = $(this).data('uid'); 
       fetchSupplierProfile(uid,rid);
       
    
    
    });    
    
    $(document).on('submit','#manualPriceSubmit',function(e){

            e.preventDefault();
  
        $.ajax({
            type  :'POST',
            url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array("action"=>"saveManualItemPrice","id"=>''.$model->ID)) ?>',
            data     : new FormData( this ),
            success:function(data){
                
              $('.manual-message').text('Prices have been added');
              scrollBox($('.manual-message'));
              
            },
            Error:function(data){
            alert("some thing went wrong please contact admin");
              },
            beforeSend: function() {
                // setting a timeout
                $('.loading-gif').show();
            },
            complete: function() {
                $('.loading-gif').hide();
            },    
            processData: false,
            contentType: false,
           
       });
    });
    function enterBidManual(rid,uid){
    
    $('.auction-overview').removeClass('active');
    $('.supplier-comparison').addClass('active');
    
         var exdata = {'reqId':rid,'supplierId':uid}; 
            $.ajax({
            type  :'POST',
            url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array("action"=>"getEnterBidManual","id"=>''.$model->ID)) ?>',
           data     :exdata,
            success:function(data){
               
               $('.supplierCompContent').removeClass('active');
               $('.enterBidManualCont').html(data);
               $('.enterBidManualCont').show();
               extendHeightofparent();
            },
            Error:function(data){
            alert("some thing went wrong please contact admin");
              },
            beforeSend: function() {
                // setting a timeout
                $('.loading-gif').show();
            },
            complete: function() {
                $('.loading-gif').hide();
            },
           
       });
    }
    $(document).on('click','.enterBidManual',function(){
    
     var rid = $(this).data('rid');
     var uid = $(this).data('uid');
     enterBidManual(rid,uid);
    });
    $(document).on('click','.getSupPreBComp',function(){
    refreshAuctionBids();
    });
    var supbcc = 0;
    $(document).on('click','.getSupBComp',function(){
    
    refreshBids();
    $('.enterBidManualCont').hide();
    $('.supplierCompContent').addClass('active');
    $('.supplierCompContent #Paris').addClass('active');
    $('.supplierCompContent #Paris').show();
    extendHeightofparent();    
    });
     var auctionNowPlaying = null;
    function refreshAuctionBids(){
         
              $.ajax({
            type  :'GET',
            url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array("action"=>"getSupplierAPreBidsComp","id"=>''.$model->ID)) ?>',
           
            success:function(data){
                
               $('.getSupPreBCompCont').html(data);
               $('.enterBidManualCont').hide();
               extendHeightofparent();
               arrangeBidRanks();
            },
            Error:function(data){
            alert("some thing went wrong please contact admin");
              },
            beforeSend: function() {
                // setting a timeout
            },
            complete: function() {
                  
                if(auctionNowPlaying) {
                    clearInterval(auctionNowPlaying);
                }
               
                    auctionNowPlaying = setInterval(function () {
                    refreshAuctionBids();
                   }, 5000);
            },
           
       });
    }
    
     var prevNowPlaying = null;
    function refreshBids(){
    $.ajax({
            type  :'GET',
            url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array("action"=>"getSupplierABidsComp","id"=>''.$model->ID)) ?>',
           success:function(data){
                
               $('.supplierCompContent #Paris').html(data);
               
               arrangeBidRanks();
            },
            Error:function(data){
            alert("some thing went wrong please contact admin");
            },
            beforeSend: function() {
                // setting a timeout
                if(supbcc==0){
                $('.loading-gif').show();
                supbcc++;
                }
            },
            complete: function() {
                $('.loading-gif').hide();
                
                if(prevNowPlaying) {
                    clearInterval(prevNowPlaying);
                }
               
                    prevNowPlaying = setInterval(function () {
                    refreshBids();
                   }, 5000);
                
            },
           
       });
       }
       function arrangeBidRanks(){
        $('.bid-ditm-cmp-tr').each(function(){
           var arr=[];
           var i=0;
          $(this).find('td .item-tprice').each(function(index){
              arr[index] = ($(this).text());
               
          });
          
          var sortedArray =arr.sort(function(a, b){return a - b}) ;
          
          $(this).find('td .item-tprice').each(function(index){
          var cindex =   sortedArray.indexOf($(this).text());
          var pos = cindex+1;
          
                var length = sortedArray.length;
               var parent = $(this).parent().parent().parent();
               $(parent).find('.rank').text(pos);
               if(pos==1)
                   $(parent).find('.rank').addClass('rank-green');
               else if(pos==length)
                   $(parent).find('.rank').addClass('rank-red');
               else
                   $(parent).find('.rank').addClass('rank-gray');
                   
               
          });
          
          
          
       }); 
       $('.bid-hitm-cmp-tr').each(function(){
           var arr=[];
           var i=0;
          $(this).find('th .item-tprice').each(function(index){
              arr[index] = ($(this).text());
               
          });
            var sortedArray =arr.sort(function(a, b){return a - b}) ;
           
          $(this).find('th .item-tprice').each(function(index){
              console.log($(this).text());
               var cindex =   sortedArray.indexOf($(this).text());
          var pos = cindex+1;
          console.log(pos);
                var length = sortedArray.length;
               var parent = $(this).parent().parent();
               console.log($(parent));
               $(parent).find('.rank').text(pos);
               if(pos==1)
                   $(parent).find('.rank').addClass('rank-green');
               else if(pos==length)
                   $(parent).find('.rank').addClass('rank-red');
               else
                   $(parent).find('.rank').addClass('rank-gray');
                   
               
          });
          
          console.log();
          
       }); 
        }
    $(document).on('click','.getSupQComp',function(){
     
              $.ajax({
            type  :'GET',
            url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array("action"=>"getSupplierAucQuestionnaireComp","id"=>''.$model->ID)) ?>',
           
            success:function(data){
                
               $('.supplierCompContent').addClass('active');
               $('.supplierCompContent #London').html(data);
               $('.enterBidManualCont').hide();
               extendHeightofparent();
            },
            Error:function(data){
            alert("some thing went wrong please contact admin");
              },
            beforeSend: function() {
                // setting a timeout
                $('.loading-gif').show();
            },
            complete: function() {
                $('.loading-gif').hide();
            },
           
       });
    });
    $(document).on('click','.viewAttachFile',function(){
   
        $(this).next().modal('show');
    });
    $(document).on('keyup','.manualPriceChange',function(){
        var parent = $(this).parent().parent();
        var quantity = $(parent).find('.manual-quantity').val();
        var value = $(this).val();
        var tprice = quantity*value;
        
        
        $(parent).find('.item-tprice').text(tprice);
    })
    function fetchSupplierProfile(uid,rid){
               var extdata = {'reqId':rid,'userId':uid };
       
       $.ajax({
            type  :'POST',
            url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array("action"=>"getBiddingSupplierData","id"=>""))?>',
            data  :extdata,
            success:function(data){
                
                $('#companyProfileModal').html(data);
                $('#companyProfileModal').modal('show');
            },
            Error:function(data){
            alert("some thing went wrong please contact admin");
              },
            beforeSend: function() {
                // setting a timeout
                $('.loading-gif').show();
            },
            complete: function() {
                $('.loading-gif').hide();
            },
           
       });
    }
    $(document).on('submit','#questionAnswers',function(e){
       e.preventDefault();
       // validation : check if required checkboxes are checked;
       var validationStatus = true;
       $('.purchase-outer.required').each(function(){
           var parent = $(this);
           
        if($(parent).find('input[type=checkbox]:checked').length<=0){
            
            $(parent).find('.error-checkbox').text('Please select atleast one option');
            scrollBox($(parent).find('.error-checkbox'));
            validationStatus =  false;
            
        }
        
       
        });
         var requestId = $('#reqid').val();
         if(validationStatus){
             $(parent).find('.error-checkbox').text('');
        $.ajax({
                beforeSend: function() {
                    // setting a timeout
                    $('.loading-gif').show();
                },
                complete: function() {
                    $('.loading-gif').hide();
                },     
                type     :'POST',
            url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'saveManualQuestionAnswers','id'=>'')); ?>'+requestId,
            data     : new FormData( this ),
            processData: false,
            contentType: false,
            success  :function(data){
            $('.manual-message').text('Questionnaire have been added');
            scrollBox($('.manual-message'));
            
            },
            error    : function(){
                 alert('some thing went wrong');
                   $('.loading-gif').hide();
             }
        })
        }
   });
</script>
<script>
function openCity(evt, cityName) {
    
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
    extendHeightofparent();
}
    function extendHeightofparent(){
        var height = $('.tab-block.active').next('.tab-open').height();
        
        $(".content-inner").css({
      'min-height': height+10 + 'px'
    });
    }
    $(document).ready(function(){
        extendHeightofparent();
    })
</script>


<!--enter manual bid-->

<script>
        function checkdocadded(){
            if($('.add-docs').length > 1){
              if($('.doc-req-edit').hasClass('docsedit')){
                  $('.doc-req-edit').removeClass('docsedit')
              } 
            } 
          }
var isAdvancedUpload = function() {
  var div = document.createElement('div');
  return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
}();

var $form = $('.drag-doc');

if (isAdvancedUpload) {
  $form.addClass('has-advanced-upload');
  var droppedFiles ;
  $form.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
    e.preventDefault();
    e.stopPropagation();
  })
  .on('dragover dragenter', function() {
    $form.addClass('is-dragover');
  })
  .on('dragleave dragend drop', function() {
    $form.removeClass('is-dragover');
  })
  .on('drop', function(e) {
    droppedFiles = e.originalEvent.dataTransfer.files;
    var lilen  =  $(this).siblings('.view-files').find(".doc-drag-section .drag-documents-list ul li").length;
       var qustid = $(this).data("id");
      
       var datapfirst = "<li id='dorp-li-"+lilen+"'><div class='drag-documents-leftbar'><strong>";
       var dataplast=" </strong><!--<span>File size : 12KB</span>--></div><div class='drag-documents-right '><a href='#'><img src='<?php echo Yii::app()->theme->baseUrl?>/dist/images/pin.png' alt='pin'></a><a href='javascript:void(0)' onclick='deleteDrop(this)' class='delete-drop add-docs' >Remove</a></div> <input type='file'class='nodisplayimp' id='doc_elment_"+lilen+"' multiple='multiple' name='Question["+qustid+"][doc][]' /></li>";   
      var datapmiddle = "";
    for(var i = 0; i<droppedFiles.length;  i++){
        if(i==0)
        datapmiddle = droppedFiles[i].name;
        else
        datapmiddle =datapmiddle+" ,"+ droppedFiles[i].name;
         
       
    }
    var datap= datapfirst + datapmiddle+ dataplast;
     ;
    $(this).siblings('.view-files').find(".drag-documents-list ul").append(datap);
    
     $("#dorp-li-"+lilen).find('input[type="file"]')
        .prop("files",droppedFiles);

    checkdocadded();

//    
  });
}
   function deleteDrop(el){
       $(el).parent().parent().remove(); 
    };

    function insertDocuemnt(item){
        
       $(item).parent().find('li.ext').remove();
    for(var i=0;i<item.files.length;i++){
        
        var size = item.files[i].size/1000;
        var dataap = ' <li class="ext"> <div class="drag-documents-leftbar"><strong>'+item.files[i].name+' </strong><span>File size : '+size+'KB</span></div> <div class="drag-documents-right"><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/pin.png" alt="pin"></a><a class="delete-bf-doc" href="javascript:void(0)">Remove</a></div> </li>';

        $(item).parent().append(dataap);
    }

    }
    $(document).on("click",'.delete-bf-doc',function(){
        $(this).parent().parent().remove();
    });
     
</script>