    <div class="modal-content questionnaire-popup"> 
        <span class="close close_multi" data-dismiss='modal'><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/close-btn.png" alt="close"></span>
          <h3>View Files</h3>
         
          <div class="questionnaire-popup-inner">
            <div class="view-files">
             <div class="drag-documents-list">
        <ul>
            <?php if(isset($reqItems)&&!empty($reqItems)){ foreach($reqItems as $req){ ?>
            <li>
                <div class="drag-documents-leftbar"><strong>Specification of Cleaning Materials.pdf </strong><span>File size : 12KB</span></div>
                <div class="drag-documents-right"><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/pin.png" alt="pin"></a><a href="javascript:void(0)">Remove</a></div>
             </li>
            <?php } } ?>
          <input  type="file" id='item'>
          <label  class="attach-field" >+ Attach File</label>
        </ul>
      </div>
            </div>
            <div class="question-form-btn">
            <input name="" type="button" class="questionnaire-button" value="Save">
              <input name="" type="button" class="close-btn" data-dismiss='modal' value="Cancel">
            </div>
          </div>
   
    </div>