    <a class="a-tab-block " href="<?php echo Yii::app()->createurl('requests/rfq',array('event'=>''))?>" ><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/create-icon2.png" alt="reports"> Create <br>
      RFQ <br>
       </a>
    
    <div class="tab-open ">
      <div class="tab-heading">
        <h1>REQUISITIONs list </h1>
      </div>
      <div class="rfq-overview-outer"> aaaaaaaaaaaaaaaaa </div>
    </div>
    <div class="tab-block active rfq-overview "><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/rfq-icon.png" alt="reports"> RFQ <br>
      Overview</div>
    <div class="tab-open ">
      <div class="tab-heading">
        <h1>rfq Overview </h1>
      </div>
      <div class="rfq-overview-outer">
        <ul class="tabs">
          <li class="active" rel="tab1">Bids</li>
          <li rel="tab2" class="">Suppliers</li>
        </ul>
        <div class="tab_container">
          <h3 class="tab_drawer_heading d_active" rel="tab1">Fill in Questionnaire</h3>
          <div id="tab1" class="tab_content" style="<?php echo Yii::app()->theme->baseUrl; ?>/display: block;">
            <div class="overview-bids">
              <div class="overview-bids-range">
                <ul>
                    <?php 
                    function roundUpToAny($n,$x=50) {
                        return round(($n+$x/2)/$x)*$x;
                    }
                    $tmaxbid = $maxbid/9;
                    for($i=0;$i<=8;$i++){ 
                        if($i!=0)
                    echo '<li>'.roundUpToAny($tmaxbid)*$i.'</li>';
                        else
                             echo '<li>0</li>';
                    }  ?>
                </ul>
              </div>
              <div class="riverside-outer bids-graph">
                <ul>
                    <?php if(isset($bidsGraph)) { ?>
                        <?php foreach($bidsGraph as $bdgraph) {?>
                    <li style="width:<?php if(isset($bdgraph['width'])) echo $bdgraph['width'].'%'; else  echo '0%'; ?>" class="<?php if(isset($bdgraph['class']))echo $bdgraph['class']; ?>"> 
                        <span ><?php echo $bdgraph['companyname']; ?> </span> 
 <?php  if(isset($bdgraph['class'])&& $bdgraph['class'] =='sup-min-bid') echo '<div style="left:'.($bdgraph['width']).'%" class="min-bid-vbar"></div>'; ?></li>

                        <?php } ?>
                    <?php } ?>
                </ul>
              </div>
              <div class="budget-outer">
                <div class="expected-budget"><span>Expected budget:</span> AED <?php echo $expectedBudget; ?></div>
                <div class="potential-savings"><span>Potential savings:</span> AED <?php echo $expectedBudget-$minBid; ?></div>
              </div>
              <div class="convert-totals"> Convert totals to <a href="javascript:void(0)" data-toggle="tooltip" title="" data-original-title="Convert totals to"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt="icon"></a> </div>
              <div class="bids-table-outer">
                <h4>Bids</h4>
                <div class="bids-table">
                  <table>
                    <tr>
                      <th>Company</th>
                      <th>Bids</th>
                      <th>Total</th>
                      <th>Difference</th>
                      <th>Bid time</th>
                      <th>&nbsp;</th>
                      <th>&nbsp;</th>
                    </tr>
                   
                    <?php if(isset($bdgraph) && !empty($bdgraph)){?>
                    <?php foreach($bidsGraph as $bdgraph){ if($bdgraph['bidstatus']==1){?>
                    <tr>
                      <td><?php echo $bdgraph['companyname'];?></td>
                      <td>1</td>
                      <td>AED <?php echo $bdgraph['itemTPrice']; ?></td>
                      <td><?php
                      $change = $bdgraph['itemTPrice']-$minBid;
                      $changeprcnt = ($change/$maxbid)*100;
                      if($change >0) echo '+'.$change.'<br> +'.round($changeprcnt,2).'%';
                      
                      ?></td>
                      <td><?php echo date('M d, H:i', strtotime($bdgraph['bidTime'])); ?></td>
                      <td><a href="javascript:void()" data-uid="<?php echo $bdgraph['userid']?>" data-rid="<?php echo $bdgraph['requestid']?>" class="view-bid-btn myBtn_multi viewBid">View bid</a>

                      </td>
                      <td><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/mail.png" alt="icon"></a></td>
                    </tr>
                    <?php } } ?>
                    <?php } ?>
                  </table>
                </div>
              </div>
            </div>
          </div>
            <div class="modal modal_multi" id="companyProfileModal"> 
                <!-- Modal content -->
                
            </div>
          <!-- #tab1 -->
          <h3 class="tab_drawer_heading" rel="tab2">enter quotation</h3>
          <div id="tab2" class="tab_content" style="<?php echo Yii::app()->theme->baseUrl; ?>/display: none;">
            <div class="bids-received">
              <div class="bids-table-outer">
                <h4>Bids Received</h4>
                <div class="bids-received">
                  <table>
                      <?php if(isset($bidsGraph)) { $biddata=true; foreach($bidsGraph as $bdgraph){ if($bdgraph['bidstatus']==1){ $biddata=false; ?>
                    <tr>
                      <td><div class="supply">
                          <h5><?php echo $bdgraph['companyname']; ?></h5>
                          <div class="supply-detail"> <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/profile-icon.png" alt="icon"> <?php echo $bdgraph['name']; ?> </div>
                        </div></td>
                      <td>1</td>
                       <td><?php echo date('M d, H:i', strtotime($bdgraph['bidTime'])); ?></td>
                      <td>
                        <div class="dropdown">
                          <button class="btn action-button" type="button" data-toggle="dropdown">Action <span class="caret"></span></button>
                          <ul class="dropdown-menu action-button-dropdown">
                              <li onclick="fetchSupplierProfile(<?php echo $bdgraph['userid']?>,<?php echo $bdgraph['requestid']?>)" ><a  href="javascript:void(0)"  >View Bid</a></li>
                              <li onclick="enterBidManual(<?php echo $bdgraph['requestid'];?>,<?php echo $bdgraph['userid'];?>)"  ><a href="javascript:void(0)">Enter Manual Bid</a></li>
                            <li><a href="javascript:void(0)">Delete</a></li>
                          </ul>
                        </div>
                      </td>
                      <td><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/mail.png" alt="icon"></a></td>
                    </tr>
                      <?php } }  if($biddata)echo '<tr><td >No Data Found</td></tr>'; } ?>
                    
                  </table>
                </div>
              </div>
            </div>
            <div class="bids-received">
              <div class="bids-table-outer">
                <h4>Bids Not Received</h4>
                <div class="bids-received">
                  <table>
                     <?php if(isset($bidsGraph)) { $biddata = true; foreach($bidsGraph as $bdgraph){  if($bdgraph['bidstatus']==0){  $biddata=false; ?>
                     <tr>
                      <td><div class="supply">
                          <h5><?php echo $bdgraph['companyname']; ?></h5>
                          <div class="supply-detail"> <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/profile-icon.png" alt="icon"> <?php echo $bdgraph['name']; ?><strong class="not-read-bid">Not Read</strong> </div>
                        </div></td>
                      <td>&nbsp;</td>
                      <td>&nbsp</td>
                      <td><div class="dropdown">
                          <button class="btn action-button" type="button" data-toggle="dropdown">Action <span class="caret"></span></button>
                          <ul class="dropdown-menu action-button-dropdown">
                            <li><a href="javascript:void(0)">View Bid</a></li>
                            <li><a  href="javascript:void(0)">Enter Manual Bid</a></li>
                            <li><a href="javascript:void(0)">Delete</a></li>
                          </ul>
                        </div></td>
                      <td><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/mail.png" alt="icon"></a></td>
                    </tr>
                    <?php }  } if($biddata)echo '<tr><td >No Data Found</td></tr>';  } ?>
                
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- #tab2 --> 
          
        </div>
      </div>
    </div>
    <div class="tab-block getSupBComp  supplier-comparison " onclick=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/supplier-icon.png" alt="reports"> Supplier <br>
      Comparison</div>
   <div class="tab-open supplierCompContent ">
             <div class="tab-heading">
        <h1>Supplier comparison<a href="javascript:void(0)" class="download-excel-btn">Download Excel Report</a></h1>
      </div>
      <div class="rfq-overview-outer">
        <div class="tab">
            <button class="tablinks getSupBComp active" onclick="openCity(event,'Paris')" id="defaultOpen">Compare Bid</button>
            <button class="tablinks getSupQComp" onclick="openCity(event,'London')">Compare Questionnaire</button>
        </div>
        <div id="Paris" class="tabcontent">
         </div>
          <div id="London" class="tabcontent">
              
          </div>
      </div>     

    </div>
    <div class=" tab-open enterBidManualContent">

    </div>
    <div class="loading-gif">
        <img src="<?php echo Yii::app()->request->baseUrl?>/images/loading.gif" />
    </div>
        

<script>
      $(document).on('click','.viewBid',function(){
         
       var rid = $(this).data('rid'); 
       var uid = $(this).data('uid'); 
       fetchSupplierProfile(uid,rid);
       
    
    
    });    
    
    $(document).on('submit','#manualPriceSubmit',function(e){

            e.preventDefault();
  
        $.ajax({
            type  :'POST',
            url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array("action"=>"saveManualItemPrice","id"=>''.$model->ID)) ?>',
            data     : new FormData( this ),
            success:function(data){
                
              $('.manual-message').text('Prices have been added');
              scrollBox($('.manual-message'));
              
            },
            Error:function(data){
            alert("some thing went wrong please contact admin");
              },
            beforeSend: function() {
                // setting a timeout
                $('.loading-gif').show();
            },
            complete: function() {
                $('.loading-gif').hide();
            },    
            processData: false,
            contentType: false,
           
       });
    });
    function enterBidManual(rid,uid){
    $('.rfq-overview').removeClass('active');
    $('.supplier-comparison').addClass('active');
    
         var exdata = {'reqId':rid,'supplierId':uid};
            $.ajax({
            type  :'POST',
            url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array("action"=>"getEnterBidManual","id"=>''.$model->ID)) ?>',
           data     :exdata,
            success:function(data){
                
               $('.enterBidManualContent').html(data);
               $('.enterBidManualContent').show();
               $('.supplierCompContent').hide();
               extendHeightofparent();
            },
            Error:function(data){
            alert("some thing went wrong please contact admin");
              },
            beforeSend: function() {
                // setting a timeout
                $('.loading-gif').show();
            },
            complete: function() {
                $('.loading-gif').hide();
            },
           
       });
    }
    $(document).on('click','.enterBidManual',function(){
    
     var rid = $(this).data('rid');
     var uid = $(this).data('uid');
     enterBidManual(rid,uid);
    });
    $(document).on('click','.getSupBComp',function(){
     
              $.ajax({
            type  :'GET',
            url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array("action"=>"getSupplierBidsComp","id"=>''.$model->ID)) ?>',
           
            success:function(data){
                
               $('#Paris').html(data);
               $('#Paris').show();
               
               $('.enterBidManualContent').hide();
               $('.supplierCompContent').show();
               extendHeightofparent();
            },
            Error:function(data){
            alert("some thing went wrong please contact admin");
              },
            beforeSend: function() {
                // setting a timeout
                $('.loading-gif').show();
            },
            complete: function() {
                $('.loading-gif').hide();
            },
           
       });
    });
    $(document).on('click','.getSupQComp',function(){
     
              $.ajax({
            type  :'GET',
            url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array("action"=>"getSupplierQuestionnaireComp","id"=>''.$model->ID)) ?>',
           
            success:function(data){
                
               $('#London').html(data);
               extendHeightofparent();
            },
            Error:function(data){
            alert("some thing went wrong please contact admin");
              },
            beforeSend: function() {
                // setting a timeout
                $('.loading-gif').show();
            },
            complete: function() {
                $('.loading-gif').hide();
            },
           
       });
    });
    $(document).on('click','.viewAttachFile',function(){
   
        $(this).next().modal('show');
    });
    $(document).on('keyup','.manualPriceChange',function(){
        var parent = $(this).parent().parent();
        var quantity = $(parent).find('.manual-quantity').val();
        var value = $(this).val();
        var tprice = quantity*value;
        
        
        $(parent).find('.item-tprice').text(tprice);
    })
    function fetchSupplierProfile(uid,rid){
               var extdata = {'reqId':rid,'userId':uid };
       
       $.ajax({
            type  :'POST',
            url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array("action"=>"getBiddingSupplierData","id"=>""))?>',
            data  :extdata,
            success:function(data){
                
                $('#companyProfileModal').html(data);
                $('#companyProfileModal').modal('show');
            },
            Error:function(data){
            alert("some thing went wrong please contact admin");
              },
            beforeSend: function() {
                // setting a timeout
                $('.loading-gif').show();
            },
            complete: function() {
                $('.loading-gif').hide();
            },
           
       });
    }
    $(document).on('submit','#questionAnswers',function(e){
       e.preventDefault();
       // validation : check if required checkboxes are checked;
       var validationStatus = true;
       $('.purchase-outer.required').each(function(){
           var parent = $(this);
           
        if($(parent).find('input[type=checkbox]:checked').length<=0){
            
            $(parent).find('.error-checkbox').text('Please select atleast one option');
            scrollBox($(parent).find('.error-checkbox'));
            validationStatus =  false;
            
        }
        
       
        });
         var requestId = $('#reqid').val();
         if(validationStatus){
             $(parent).find('.error-checkbox').text('');
        $.ajax({
                beforeSend: function() {
                    // setting a timeout
                    $('.loading-gif').show();
                },
                complete: function() {
                    $('.loading-gif').hide();
                },     
                type     :'POST',
            url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'saveManualQuestionAnswers','id'=>'')); ?>'+requestId,
            data     : new FormData( this ),
            processData: false,
            contentType: false,
            success  :function(data){
            $('.manual-message').text('Questionnaire have been added');
            scrollBox($('.manual-message'));
            
            },
            error    : function(){
                 alert('some thing went wrong');
                   $('.loading-gif').hide();
             }
        })
        }
   });
</script>
<script>
function openCity(evt, cityName) {
    
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
    extendHeightofparent();
}
    function extendHeightofparent(){
        var height = $('.tab-block.active').next('.tab-open').height();
        
        $(".content-inner").css({
      'min-height': height+10 + 'px'
    });
    }
    $(document).ready(function(){
        extendHeightofparent();
    })
</script>


<!--enter manual bid-->

<script>
        function checkdocadded(){
            if($('.add-docs').length > 1){
              if($('.doc-req-edit').hasClass('docsedit')){
                  $('.doc-req-edit').removeClass('docsedit')
              } 
            } 
          }
var isAdvancedUpload = function() {
  var div = document.createElement('div');
  return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
}();

var $form = $('.drag-doc');

if (isAdvancedUpload) {
  $form.addClass('has-advanced-upload');
  var droppedFiles ;
  $form.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
    e.preventDefault();
    e.stopPropagation();
  })
  .on('dragover dragenter', function() {
    $form.addClass('is-dragover');
  })
  .on('dragleave dragend drop', function() {
    $form.removeClass('is-dragover');
  })
  .on('drop', function(e) {
    droppedFiles = e.originalEvent.dataTransfer.files;
    var lilen  =  $(this).siblings('.view-files').find(".doc-drag-section .drag-documents-list ul li").length;
       var qustid = $(this).data("id");
       alert(qustid);
       var datapfirst = "<li id='dorp-li-"+lilen+"'><div class='drag-documents-leftbar'><strong>";
       var dataplast=" </strong><!--<span>File size : 12KB</span>--></div><div class='drag-documents-right '><a href='#'><img src='<?php echo Yii::app()->theme->baseUrl?>/dist/images/pin.png' alt='pin'></a><a href='javascript:void(0)' onclick='deleteDrop(this)' class='delete-drop add-docs' >Remove</a></div> <input type='file'class='nodisplayimp' id='doc_elment_"+lilen+"' multiple='multiple' name='Question["+qustid+"][doc][]' /></li>";   
      var datapmiddle = "";
    for(var i = 0; i<droppedFiles.length;  i++){
        if(i==0)
        datapmiddle = droppedFiles[i].name;
        else
        datapmiddle =datapmiddle+" ,"+ droppedFiles[i].name;
         
       
    }
    var datap= datapfirst + datapmiddle+ dataplast;
     ;
    $(this).siblings('.view-files').find(".drag-documents-list ul").append(datap);
    
     $("#dorp-li-"+lilen).find('input[type="file"]')
        .prop("files",droppedFiles);

    checkdocadded();

//    
  });
}
   function deleteDrop(el){
       $(el).parent().parent().remove(); 
    };

    function insertDocuemnt(item){
        
       $(item).parent().find('li.ext').remove();
    for(var i=0;i<item.files.length;i++){
        
        var size = item.files[i].size/1000;
        var dataap = ' <li class="ext"> <div class="drag-documents-leftbar"><strong>'+item.files[i].name+' </strong><span>File size : '+size+'KB</span></div> <div class="drag-documents-right"><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/pin.png" alt="pin"></a><a class="delete-bf-doc" href="javascript:void(0)">Remove</a></div> </li>';

        $(item).parent().append(dataap);
    }

    }
    $(document).on("click",'.delete-bf-doc',function(){
        $(this).parent().parent().remove();
    });
     
</script>