<?php
/* @var $this RequestsController */
/* @var $data Requests */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID), array('view', 'id'=>$data->ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uid')); ?>:</b>
	<?php echo CHtml::encode($data->uid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('endtime')); ?>:</b>
	<?php echo CHtml::encode($data->endtime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timezone')); ?>:</b>
	<?php echo CHtml::encode($data->timezone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('starttime')); ?>:</b>
	<?php echo CHtml::encode($data->starttime); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('exttime')); ?>:</b>
	<?php echo CHtml::encode($data->exttime); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastperiod')); ?>:</b>
	<?php echo CHtml::encode($data->lastperiod); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('highestbid')); ?>:</b>
	<?php echo CHtml::encode($data->highestbid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lowestbid')); ?>:</b>
	<?php echo CHtml::encode($data->lowestbid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reqtype')); ?>:</b>
	<?php echo CHtml::encode($data->reqtype); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('createdat')); ?>:</b>
	<?php echo CHtml::encode($data->createdat); ?>
	<br />

	*/ ?>

</div>