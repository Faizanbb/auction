<?php
/* @var $this RequestsController */
/* @var $model Requests */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'question-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
    )); ?>
            <div class="modal-content questionnaire-popup"> <span class="close close_multi"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/close-btn.png" data-dismiss='modal' alt="close"></span>
              <h3>create Question</h3>
              <div class="questionnaire-popup-inner">
                <div class="question-section">

                    <div class="question-form">
                        <label>Question</label>
                            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>350,'class'=>'form-control')); ?>
                            <?php echo $form->error($model,'name'); ?>
                        <label>Description</label>
                              <?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50,'class'=>'form-control comments-field')); ?>
            <?php echo $form->error($model,'description'); ?>
                    </div>
                </div>
                <div class="question-select">
                  <div class="question-type">
                    <h4>Question Type <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt="icon"></h4>
                    <div class="drp">
                         <?php echo $form->dropDownList($model,'qtype',array(0=>'Yes/No',1=>'Input Box',2=>'Select One',3=>'Multiple Option'),array('class'=>'form-control','onchange'=>'checkOptions(this.value)')); ?>
            <?php echo $form->error($model,'qtype'); ?>
	
                    </div>
                  </div>
                  <div class="question-type">
                    <h4>Make it a Pre-Qualification<img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt="icon"></h4>
                    <div class="drp">
                        <?php echo $form->dropDownList($model,'prequalify',array(''=>'Select One',1=>'Yes',0=>'No'),array('class'=>'form-control')); ?>
            <?php echo $form->error($model,'prequalify'); ?>
	
                    </div>
                  </div>
                  <div class="question-type">
                    <h4>Supplier can attach document <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt="icon"></h4>
                    <div class="drp">
                        <?php echo $form->dropDownList($model,'document',array(''=>'Select One',1=>'Yes',0=>'No'),array('class'=>'form-control')); ?>
            <?php echo $form->error($model,'document'); ?>
	
                    </div>
                  </div>
                  <div class="question-type">
                    <h4>Mandatory Question <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt="icon"></h4>
                    <div class="drp">
                        <?php echo $form->dropDownList($model,'mandatory',array(''=>'Select One','Yes'=>'Yes','No'=>'No'),array('class'=>'form-control')); ?>
            <?php echo $form->error($model,'mandatory'); ?>
	
                    </div>
                  </div>
                </div>
                <input type='hidden' name='SectionQuestions[parentid]' value='<?php echo $secid?>' />
                <input type='hidden' name='SectionQuestions[rid]' value='<?php echo $requestId?>' />
                <?php if(isset($qid)){ ?>
                <input type='hidden' name='SectionQuestions[questid]' value='<?php echo $qid?>' />
                <?php } ?>
                
                <div class="addOptionContent">
                    <?php if(isset($optionset)&& !empty($optionset)){
                        $count = 0;
                        foreach($optionset as $opts){
                            if($count==0){
                         ?>
                    <div class="optionBox"><input  class="form-control optionvalue width-33 d-inline-block" value="<?php echo $opts->value;?>" type="text" placeholder="value...." name="OptionValue[]"/> <span class="btn btn-default addMoreOption" type="button">+</span></div>
                             <?php
                            }else{
                            ?>
                            <div class="optbox"><input  value="<?php echo $opts->value;?>" class="form-control d-inline-block optionvalue width-33" type="text" placeholder="value...." name="OptionValue[]"/><div class="btn-delete-opt btn btn-default d-inline-block"><img src="/auction/themes/auction/dist/images/del-icon.png" alt="del"></div></div>
                <?php
                            }
                            $count++;
                        }
                    }
                ?>
                    
                </div>
                
                <div class="question-form-btn">
                  <input name="" type="button"  data-dismiss='modal'  class=" close-btn close-btn-question" value="Back">
                  <input name="" type="button" id='savenewQuestion' class="save-btn" value="Save">
                </div>
              </div>
            </div>
<!-- form -->

<?php $this->endWidget(); ?>

</div>