<?php 
//echo '<pre>';
//print_r($model);exit; 
?>
<section class="requisition-form-outer text-center">
 <div class="container-fluid">
   <div class="row">
   <div class="rfi-privateaccess">
    <div class="make-bid">
        <a href="<?php echo Yii::app()->createUrl('requests/fillq',array('id'=> $userid,'event'=> base64_encode($model->ID))) ?>" class="make-bid-btn">make bid</a> <a href="#" class="decline-bid-btn">decline</a>
    </div>
    <div class="reprehenderit-outer">
     <h1><?php echo $model->name?></h1>
    <div class="reprehenderit-list">
    <ul>
    <li>
    <div class="reprehenderit-icon"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/company-icon.png" alt=""></div>
    <div class="reprehenderit-detail"><p>Company</p><span><?php echo $model->acc->company; ?> </span></div>
    </li>
    <li>
    <div class="reprehenderit-icon"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/user.png" alt="user"></div>
    <div class="reprehenderit-detail"><p>Contact Person</p><span><?php echo $model->u->name; ?>  |  <a href="#"><?php echo '+'.$model->u->carriercode.''.$model->u->mobile; ?></a></span></div>
    </li>
    </ul>
    </div>
    <div class="time-detail">
     <ul>
      <li>Time Left:  <span>Deactive</span></li>
      <li>  End date: <span><?php echo date('M d, Y H:i', strtotime($model->endtime)); ?>  (UTC+04:00)</span></li>
     </ul>
    </div>
    
    <div class="email-preview">
<div class="email-preview-top">
<h3>Request Details</h3>
<p>
    <?php echo $model->description; ?>
</p>
<p>Additional specifications can be found from attached documents.</p>
</div>
<?php if(isset($model->documentRequests)&&!empty($model->documentRequests)){?>
<div class="email-preview-top">
<h3>Documents</h3>
<div class="documents-detail">
    <?php 
    
        foreach($model->documentRequests as $mdr){
    ?>
<div class="documents-detail-inner">
<div class="documents-detail-leftbar"><?php echo $mdr->d->origname; ?></div>
<div class="documents-detail-rightbar">
<ul>
    <li><?php  $type = explode('/', $mdr->d->typeoffile) ; echo $type[1];?></li>
<li><?php echo $mdr->d->size/1000?> KB.</li>
<li><a href="<?php echo Yii::app()->baseUrl.'/'.$mdr->d->url.'/'.$mdr->d->name;?>" download>View</a></li>
</ul>
</div>
</div>
    
    <?php } ?>
   
    
</div>
</div>
<?php } ?>
<?php
    if(isset($model->requestItems)&&!empty($model->requestItems)){
        
?>
<div class="email-preview-top">
<h3>List of items</h3>
<p>Buyer wants to receive detailed price quotations according to the list of it</p>
<a href="#" class="download-btn">Download as Excel File</a>
<div class="materials-outer">
<table>
  <tr>
    <th>#</th>
    <th>Name</th>
    <th>Quantity</th>
    <th>Unit</th>
  </tr>
  <tr class="gray-background">
   <td class="orange-color">1.</td>
   <td class="orange-color">MATERIALS</td>
   <td>&nbsp;</td>
   <td>&nbsp;</td>
  </tr>
  <?php 
  foreach($model->requestItems as $item){
  ?>
  
  <tr>
   <td>1.1</td>
   <td><?php echo $item->name;?></td>
   <td><?php echo $item->quantity?></td>
   <td><?php echo $item->unit_label?></td>
  </tr>
  
  <?php } ?>
  
</table>
</div>
</div>

    <?php }?>
        
        <?php if(isset($model->sectionQuestions)&& !empty($model->sectionQuestions)){ ?>
    <div class="email-preview-top">
    <h3>Fill In Questionnaire</h3>
    <p>The buyer wants each participant of the request to answer a list of questions to gather information.The questionnaire has to be filled when making a bid.</p>
    <div class="materials-outer">
    <table>
      <tr>
        <th>#</th>
        <th>Name</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
      </tr>
      <?php
      $scount = 1;
      foreach($model->sectionQuestions as $sq)
      {
          if($sq->parentid==0){
              $qcount = 1;
      ?>
      <tr class="gray-background">
       <td class="orange-color"><?php echo $scount;?>.</td>
       <td class="orange-color"><?php echo $sq->name?></td>
       <td>&nbsp;</td>
       <td>&nbsp;</td>
      </tr>
      <?php foreach($model->sectionQuestions as $q)
      {
          if($q->parentid==$sq->ID){
        ?>
      <tr>
       <td valign="top"><?php echo $scount.'.'.$qcount;?></td>
       <td valign="top"><strong><?php echo $q->name?></strong>
       <p><?php echo $q->description?></p></td>
       <td>&nbsp;</td>
       <td valign="top"><?php   if($q->qtype==0){echo 'Yes/No';}elseif($q->qtype==1){echo 'Text';}elseif($q->qtype==2){echo 'Choose From List';}elseif($q->qtype==3){echo 'Checkbox';} ?></td>
      </tr>
      
          <?php $qcount++;}} $scount++;} } ?>
    </table>
    </div>
    </div>
        <?php } ?>


    </div>
    
    
    </div>
   </div>
  </div>
 </div>
</section>