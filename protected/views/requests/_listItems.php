
             <?php
             
             $count = $secCount;
             for($i = $initCount; $i<$count;$i++ ){?>
        <tr data-rid ="<?php echo $i;?> " >
         <td><input  type="text" class="item-name-input" name='Items[<?php echo $i;?>][name]'></td>
         <td><input  type="text" class="item-number-input" name='Items[<?php echo $i;?>][itemnumber]'></td>
         <td >
            <button type="button" class="attach-button viewAttachFile myBtn_multi">Attach</button>
            <div class="modal modal_multi viewAttachFile" > 
            <div class="modal-content questionnaire-popup"> 
                <span class="close close_multi" data-dismiss='modal'><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/close-btn.png" alt="close"></span>
                  <h3>View Files</h3>

                  <div class="questionnaire-popup-inner">
                    <div class="view-files">
                        <div class="drag-documents-list">
                            <ul>
                               <?php if(isset($reqItems)&&!empty($reqItems)){ foreach($reqItems as $req){ ?>
                               <li>
                                   <div class="drag-documents-leftbar"><strong>Specification of Cleaning Materials.pdf </strong><span>File size : 12KB</span></div>
                                   <div class="drag-documents-right"><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/pin.png" alt="pin"></a><a class="delete-bf-doc" href="javascript:void(0)">Remove</a></div>
                                </li>
                               <?php } } ?>
                                                         <?php
                                $this->widget('CMultiFileUpload', array(
                                  'name'=>'Items['.$i.'][documents]',
                                  'accept'=>'jpg|png|PNG',
                                  'remove'=> '[x]',
                                  'duplicate'=>'Already Selected',
                                  'denied'=>'File is not allowed',
                                  'options'=>array(
                                    'afterFileSelect'=>'function(e, v, m){ }',
                                    ),
                                  'max'=>10, // max 10 files
                                    'htmlOptions'=>array('class'=>' nodisplayimp ','multiple'=>'multiple','enctype'=>'multipart/form-data','onchange'=>'insertDocuemnt(this)')

                                ));
                            ?>
                                <input type="hidden" value="<?php echo $i;?>" name="Items[<?php echo $i;?>][cc]" />
                             <label  for='Items_<?php echo $i;?>_documents' class="attach-field" >+ Attach File</label>
                           </ul>
                        </div>
                   </div>
                    <div class="question-form-btn">
                    <input  type="button" data-dismiss='modal' class="questionnaire-button" value="Save">
                      <input  type="button" data-dismiss='modal' class="close-btn" data-dismiss='modal' value="Cancel">
                    </div>
                  </div>

            </div>
            </div>
         </td>
        <td><input  type="button" value="Add" class="add-button viewaddunit myBtn_multi">
        <div class="modal modal_multi"> 
        
        <!-- Modal content -->
        <div class="modal-content questionnaire-popup unit-popup"> <span data-dismiss="modal" class="close close_multi"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/close-btn.png" alt="close"></span>
        <h3>Unit</h3>
        <div class="unit-outer">
        <div class="unit-label">
        <label>Label</label>
        <input  type="text"  name="Items[<?php echo $i;?>][unit_label]" class="unit-label-input">
        </div>
        <div class="unit-amount">
        <label>Amount</label>
        <div class="input-group spinner">
        <input type="text" name="Items[<?php echo $i;?>][unit_amount]" class="form-control" value="1">
        <div class="input-group-btn-vertical">
        <button class="btn " type="button"><i class="fa fa-caret-down"></i></button>
        <button class="btn " type="button"><i class="fa fa-caret-up"></i></button>
        </div>
        </div>
        </div>
        <div class="unit-label">
        <label>Unit</label>
        <div class="drp">
              <?php 
        
        echo CHtml::dropDownList('Items['.$i.'][unit]','units', $unitList); ?>
        </div>
        </div>
        </div>
        <div class="question-form-btn">
        <input  type="button" data-dismiss="modal" class="questionnaire-button" value="Save">
        <input  type="button" data-dismiss="modal" class="close-btn" value="Cancel">
        </div>
        </div>
        </div>
        </td>
        <td>
        <div class="input-group spinner">
        <input type="text" class="form-control"  name='Items[<?php echo $i;?>][quantity]' value="1">
        <div class="input-group-btn-vertical">
        <button class="btn btn-inc"   type="button"><i class="fa fa-caret-down"></i></button>
        <button class="btn btn-dec"   type="button"><i class="fa fa-caret-up"></i></button>
        </div>
        </div>
        </td>
        <td>
        <a href="javascript:void(0)" class="price-delete delete-bf-row"><i class="fa fa-trash" aria-hidden="true"></i></a>
            <input  type="text" name='Items[<?php echo $i;?>][price]' class="price-input">
        </td>
        </tr>
             <?php } ?>
