<?php 
$supplierId = $requestSup->supplier_id;
$requestId = $model->ID;
//echo '<pre>'; print_r($requestSup);exit;
?>

<div class="list-iteam">
   
    <h2>LIST OF ITEMS</h2>
    <div class="list-iteam-table">
      <table>
        <tr class="tr-bold">
          <td>Item Name</td>
          <td>Quantity</td>
          <td>Unit</td>
          <td>Unit Price</td>
          <td>Item Total</td>
          <td>Enter New Bid</td>
          <td>Rank</td>
        </tr>
        <?php  foreach($requestSup->req->requestItems as $reqitem){?>
        <tr  data-sid="<?php echo $requestSup->supplier_id?>" data-rid="<?php echo $requestSup->req->ID?>" data-reqitem="<?php echo $reqitem->ID; ?>">
          <td><strong><i class="fa fa-caret-down"></i> <?php echo $reqitem->name; ?></strong></td>
          <td> <span class="quantity"><?php echo $reqitem->quantity; ?></span></td>
          <td><?php echo $unitList[$reqitem->unit_id]; ?></td>
          <td >


          <?php  

              $curSupcurItem = '';
              $allPricesofItem= array();
              foreach($reqitem->requestItemPrices as $reqiprice){
              $supPriceExist = false;
              if($reqiprice->uid==$supplierId){$supPriceExist=true; $curSupcurItem= $reqiprice ;}
              $allPricesofItem[] = $reqiprice->price;
              }
              ?>


              <?php if(!empty($curSupcurItem)){ ?>
                    <div class="aed pre-price-box <?php if($reqiprice->status==0 ) echo 'nodisplay'; ?>">

                        <div class="aed-left">Aed</div>
                        <div class="aed-right"> <input  type="text" value="0" class="price-init noborder width-100 text-center pre-price  <?php if($reqiprice->status==2 ) echo 'nodisplay'; ?>" /> <?php if($reqiprice->status==2) echo $reqiprice->price; ?></div>
                    </div>
              <?php }else{ ?>
               <div class="aed pre-price-box nodisplay">

                  <div class="aed-left">Aed</div>
                  <div class="aed-right"> <input  type="text" value="0" class="price-init noborder width-100 text-center pre-price " /></div>
              </div>
              <?php } ?>

          </td>
          <td>
              <span class="item-total">
              <?php
                  if(!empty($curSupcurItem)){?> 
              <?php if($curSupcurItem->status==2) echo $curSupcurItem->price*$reqitem->quantity ?>
              <?php } ?>
                  </span> 
          </td>
          <td class="action-btns"> 
          <?php  
          if($requestSup->accept==1){

                  if(!empty($curSupcurItem)){

                      if($curSupcurItem->status==0){ ?>   
                  <a  href="javascript:void(0)" class="decline-btn ">Rejected</a>
              <?php }else if($curSupcurItem->status==2){ ?>   
             Bid Received 
              <?php }else if($curSupcurItem->status==1){ ?>
                  <a href="#" data-status="2" class="accept-btn placeRejBid  submtiBid">Submit Bid</a>
                  <?php
              }  } else{ ?>

          <a  data-status="1" href="javascript:void(0)" class="accept-btn placeRejBid">Place Bid</a><br>
          <a  data-status="0" href="javascript:void(0)" class="decline-btn placeRejBid">Reject Bid</a>                        <?php } } ?>

            </td>
            <td>
            <?php 
            if($curSupcurItem->status==2){
            $maxv = max($allPricesofItem);
            $max = array_keys($allPricesofItem, max($allPricesofItem));
            
            $min = array_keys($allPricesofItem, min($allPricesofItem));
            $minv = min($allPricesofItem);
         
            if($curSupcurItem->price==$minv)
               echo '<div class="rank-green">'.($min[0]+1).'</div>';
            else if($curSupcurItem->price==$maxv)
               echo '<div class="rank-red">'.($max[0]+1).'</div>';
            else
               echo '<div class="rank-gray">'.($min[0]+1).'</div>';
            }
              ?>
            </td>
        </tr>
        <?php } ?>
      </table>
    </div>
   
</div>
