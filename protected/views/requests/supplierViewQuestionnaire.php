<section class="requisition-form-outer text-center">
 <div class="container-fluid">
   <div class="row">
   <div class="fill-questions">
    <div class="reprehenderit-outer">
     <h1><?php echo $model->name?></h1>
    <div class="reprehenderit-list">
    <ul>
    <li>
    <div class="reprehenderit-icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/company-icon.png" alt=""></div>
    <div class="reprehenderit-detail"><span><?php echo $model->acc->company?> </span></div>
    </li>
    <li>
    <div class="reprehenderit-icon"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/user.png" alt="user"></div>
    <div class="reprehenderit-detail"><span><?php echo $model->u->name?> </a></span></div>
    </li>
    </ul>
    </div>
    <div class="time-detail">
     <ul>
      <li><strong>Time Left:</strong>  <span><strong>Deactive</strong></span></li>
      <li>  <strong>End date: </strong><span><strong><?php echo $model->endtime?></strong></span></li>
     </ul>
    </div>
    
    <div class="fill-in-questionaire-tabs">
    
<ul class="tabs">
    <?php if(isset($model->sectionQuestions)&& !empty($model->sectionQuestions)){ ?>
        <li class="active tab_questions" rel="tab1">Fill in Questionnaire</li>
    <?php } ?>
<?php
    if(isset($model->requestItems)&&!empty($model->requestItems)){
        
?>
<li rel="tab2" class="tab_items">enter quotation</li>
    <?php } ?>
</ul>


<div class="tab_container">
    <form class="" id="questionAnswers" method="POST" enctype="multipart/form-data">
  <h3 class="tab_drawer_heading d_active" rel="tab1">Fill in Questionnaire</h3>
  <div id="tab1" class="tab_content" style="display: block;">
     
          <input type="hidden" name="requestId" id="reqid" value="<?php echo $model->ID;?>" /> 
          <input type="hidden" name="supplierId"  value="<?php echo $_GET['id'];?>" /> 
    <?php
      $scount = 1;
      foreach($model->sectionQuestions as $sq)
      {
          if($sq->parentid==0){
              $qcount = 1;
      ?>
    <div class="questionnaire-general-information">
        <h3>1. General Information</h3>
    
        <?php foreach($model->sectionQuestions as $q)
        {
          if($q->parentid==$sq->ID){
        ?>
            <div class="questionnaire-general-inner">

            <div class="questionnaire-general-top">
                <h4><?php echo $scount.'.'.$qcount.'. '.$q->name?></h4>
                <p><?php echo $q->description; ?></p>
                <div class="general-answer">
                     
                    <?php   if($q->qtype==0){ ?> <select name="Question[<?php echo $q->ID?>]" class="form-control"> <option>Select one</option> <option>Yes</option> <option>No</option></select> <?php }else if($q->qtype==1){  ?> <textarea name="Question[<?php echo $q->ID?>]" cols="" rows="" placeholder="Enter your answer here..." class="answer"></textarea> <?php  }elseif($q->qtype==2){  ?> 
                    <div class="form-group">
                    <select name="Question[<?php echo $q->ID?>]" class="form-control"> 
                        <option>Select one</option> 
                        <?php if(isset($q->optionlists)&& !empty($q->optionlists)){ 
                            foreach($q->optionlists as $optl){?>
                        <option ><?php echo $optl->value; ?></option>
                        <?php } } ?>
                    </select>  
                    </div >
                    <?php  }elseif($q->qtype==3){  ?> 
                   <div class="purchase-outer">
            <ul class="checkbox-quest">
              <?php if(isset($q->optionlists)&& !empty($q->optionlists)){ 
                            foreach($q->optionlists as $optl){?>
             <li>
                 <div class="pacific-checkbox">
                            <div class="custom-checkbox">
                                <input name="Question[<?php echo $q->ID?>][]" value='<?php echo $optl->value;?>' id="cbaq<?php echo $optl->ID;?>" type="checkbox" />
                              <label  for="cbaq<?php echo $optl->ID;?>"></label>
                            </div>
                          </div><?php echo $optl->value;?>
             </li>
             <?php } } ?>
            </ul>
            </div>
                    <?php  } ?>
                    <?php if($q->document==1){ ?>
                    <div class="drag-answer">
                        <div data-id='<?php echo $q->ID; ?>' class="drag-answer-here drag-doc">
                         Drag & Drop your file here
                        </div>
                         <div class="attachment">
                             <label for="Question_<?php echo $q->ID;?>_doc" class="add-attachment-bnt">+ Add attachment</label>  
                        </div>
                        <div class="view-files bdr-right-li">
                        <div class="drag-documents-list">
                            <?php
                                $this->widget('CMultiFileUpload', array(
                                  'name'=>'Question['.$q->ID.'][doc]',
                                  'accept'=>'jpg|png|PNG',
                                  'remove'=> '[x]',
                                  'duplicate'=>'Already Selected',
                                  'denied'=>'File is not allowed',
                                  'options'=>array(
                                    'afterFileSelect'=>'function(e, v, m){ }',
                                    ),
                                  'max'=>10, // max 10 files
                                    'htmlOptions'=>array('class'=>' nodisplayimp ','multiple'=>'multiple','enctype'=>'multipart/form-data','onchange'=>'insertDocuemnt(this)')

                                ));
                            ?>
                              
                             
                            <ul >
                          
                           </ul>
                        </div>
                   </div>
                    </div>
                    <?php } ?>
                    
                </div>
            </div>


            </div>

      
        <?php $qcount++;}
        
          }?> 
    </div>
            

        <?php  $scount++;} } ?>

       <div class="request">
    <div class="request-btn-outer">
   <input type="button" onclick='$(".tab_items").trigger("click")' class="next-button btn " value="Next" />
    </div>
   </div>

  </div>
  <!-- #tab1 -->
  <h3 class="tab_drawer_heading" rel="tab2">enter quotation</h3>
  <div id="tab2" class="tab_content" style="display: none;">
      <div class="quotation-outer">
        <table>
            <thead>
                <tr>
                <th>Name</th>
                <th>Quantity Unit</th>
                <th>Unit price AED *</th>
                <th>Item total</th>
                </tr>
            </thead>
            <tbody>
                <?php if(isset($model->requestItems) &&!empty($model->requestItems)){
                    $count =1;
                    foreach($model->requestItems as $reqitem){
                    ?>   
                <tr>
                    <td>
                    <div class="form-group materials-comment-outer">
                    <label for="email"><?php echo $count.'. '.$reqitem->name?> </label>
                    <div class="materials-comment">
                    <textarea name="" cols="" rows="" placeholder="Enter your answer here..." class="answer"></textarea>
                    </div>
                    </div>
                    </td>
                    <td><input type="text" name="Items[<?php echo $reqitem->ID; ?>][quantity]" value="<?php echo $reqitem->quantity; ?>" class="form-control quantity quantity-unit-input"></td>
                    <td><input type="text" name="Items[<?php echo $reqitem->ID; ?>][price]" class="form-control price quantity-unit-input box-shadow" ></td>
                    <td><input type="text"  name="Items[<?php echo $reqitem->ID; ?>][subtotal]" class="form-control sub-t quantity-unit-input" id="email"></td>
                </tr>
 <?php 
                $count++;
                    }
                }
?>
                
                <tr>
                <td colspan="8" style="padding:0 !important;">
                    <table style="margin-top:20px;">
                <tbody><tr>
                <th class="work-total"><strong>Total of all items</strong></th>
                <th class="work-total"><strong class="pull-right ">AED <span class="total-price">00.00</span></strong></th>
                </tr>
                </tbody></table>
                </td>
                </tr>
            </tbody>
        </table>






        </td>
        </tr>
        </table>
<div class="work-documents">
 <h5>Documents</h5>
 <h6>MORE INFORMATION</h6>
 <div class="work-documents-drag">
  <div class="documents-drag">
   <div class="documents-drag-input">Drag &nbsp; Drop your file here</div> 
   <input name="" type="submit" value="Chose file" class="chose-file-btn">
   <textarea name="" cols="" rows="" class="comment-buyer" placeholder="Add comment to buyer..."></textarea>
   
   <div class="request">
    <div class="request-btn-outer">
    <a href="javascript:void(0)" onclick='$(".tab_questions").trigger("click")' class="request-btn">Back to request</a>
    <input type="submit" class="next-button btn save-answers" value='Next' />
    </div>
   </div>
  </div>
 </div>
</div>

      </div>
  </div>
  <!-- #tab2 -->
  </form>
</div>
    
    </div>
    
    
    </div>
   </div>
  </div>
 </div>
   
        
</section>
    <div class="loading-gif">
        <img src="<?php echo Yii::app()->request->baseUrl?>/images/loading.gif" />
    </div>
<script>
        function checkdocadded(){
            if($('.add-docs').length > 1){
              if($('.doc-req-edit').hasClass('docsedit')){
                  $('.doc-req-edit').removeClass('docsedit')
              } 
            } 
          }
var isAdvancedUpload = function() {
  var div = document.createElement('div');
  return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
}();

var $form = $('.drag-doc');

if (isAdvancedUpload) {
  $form.addClass('has-advanced-upload');
  var droppedFiles ;
  $form.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
    e.preventDefault();
    e.stopPropagation();
  })
  .on('dragover dragenter', function() {
    $form.addClass('is-dragover');
  })
  .on('dragleave dragend drop', function() {
    $form.removeClass('is-dragover');
  })
  .on('drop', function(e) {
    droppedFiles = e.originalEvent.dataTransfer.files;
    var lilen  =  $(this).siblings('.view-files').find(".doc-drag-section .drag-documents-list ul li").length;
       var qustid = $(this).data("id");
       alert(qustid);
       var datapfirst = "<li id='dorp-li-"+lilen+"'><div class='drag-documents-leftbar'><strong>";
       var dataplast=" </strong><!--<span>File size : 12KB</span>--></div><div class='drag-documents-right '><a href='#'><img src='<?php echo Yii::app()->theme->baseUrl?>/dist/images/pin.png' alt='pin'></a><a href='javascript:void(0)' onclick='deleteDrop(this)' class='delete-drop add-docs' >Remove</a></div> <input type='file'class='nodisplayimp' id='doc_elment_"+lilen+"' multiple='multiple' name='Question["+qustid+"][doc][]' /></li>";   
      var datapmiddle = "";
    for(var i = 0; i<droppedFiles.length;  i++){
        if(i==0)
        datapmiddle = droppedFiles[i].name;
        else
        datapmiddle =datapmiddle+" ,"+ droppedFiles[i].name;
         
       
    }
    var datap= datapfirst + datapmiddle+ dataplast;
     ;
    $(this).siblings('.view-files').find(".drag-documents-list ul").append(datap);
    
     $("#dorp-li-"+lilen).find('input[type="file"]')
        .prop("files",droppedFiles);

    checkdocadded();

//    
  });
}
   function deleteDrop(el){
       $(el).parent().parent().remove(); 
    };

</script>
<script>
    function insertDocuemnt(item){
        console.log($(item).parent());
       $(item).parent().find('li.ext').remove();
    for(var i=0;i<item.files.length;i++){
        
        var size = item.files[i].size/1000;
        var dataap = ' <li class="ext"> <div class="drag-documents-leftbar"><strong>'+item.files[i].name+' </strong><span>File size : '+size+'KB</span></div> <div class="drag-documents-right"><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/pin.png" alt="pin"></a><a class="delete-bf-doc" href="javascript:void(0)">Remove</a></div> </li>';

        $(item).parent().append(dataap);
    }

    }
    $(document).on("click",'.delete-bf-doc',function(){
        $(this).parent().parent().remove();
    });
    $(document).on('submit','#questionAnswers',function(e){
       e.preventDefault();
         var requestId = $('#reqid').val();
        
        $.ajax({
                beforeSend: function() {
                    // setting a timeout
                    $('.loading-gif').show();
                },
                complete: function() {
                    $('.loading-gif').hide();
                },     
                type     :'POST',
            url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'saveQuestionAnswers','id'=>'')); ?>'+requestId,
            data     : new FormData( this ),
            processData: false,
            contentType: false,
            success  :function(data){
                
            },
            error    : function(){
                 alert('some thing went wrong');
                   $('.loading-gif').hide();
             }
        })
     
   });
</script>
<script>
$(document).on('keyup','.price',function(){
    var price = $(this).val();
    var quantity = $(this).parent().parent().find('.quantity').val();
    var quantity = $(this).parent().parent().find('.quantity').val();
   $(this).parent().parent().find('.sub-t').val(price*quantity);
   var totalv = 0;
    $('.sub-t').each(function(){
        if($(this).val())
      totalv = parseInt(totalv) + parseInt($(this).val());
  
    });
    $('.total-price').text(totalv);
});
</script>