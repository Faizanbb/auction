
<div class="rfq-overview-outer">
        <div class="tab">
          <button class="tablinks active" onclick="openCity(event, 'ILondon')" id="defaultOpen">Enter quotation</button>
          <button class="tablinks" onclick="openCity(event, 'IParis')">Fill in Questionnaire</button>
        </div>
    <div class="manual-message"></div>
    <div id="ILondon" class="tabcontent" style="display: block;">
          <div class="pre-bids">
            <div class="pre-bids-table">
            
            <div class="time-detail">
            <ul>
              <li><strong>Time Left:</strong> <span><strong>Deactive</strong></span></li>
              <li> <strong>End date: </strong><span><strong>Sep 11, 2018 11:00 (UTC+04:00)</strong></span></li>
            </ul>
          </div>
              
                <form id="manualPriceSubmit" action="POST">
              <div class="list-iteam">
        <h2>LIST OF ITEMS</h2>
        <div class="list-iteam-table list-iteam-table-edit">
          <table class="manual-bid-table">
            <tbody>
            <tr class="row-bold">
              <td>Item Name</td>
              <td>Item Number</td>
              <td>Unit</td>
              <td>Qty</td>
              <td>Price</td>
              <td>Item Total</td>
            </tr>
                <?php foreach ($model->requestItems as $reqItem){?>
              <tr>
                <td><strong><i class="fa fa-caret-down"></i> <?php echo $reqItem->name;?></strong></td>
                <td><?php echo $reqItem->item_number;?></td>
                <td><?php echo $unitList[$reqItem->unit_id];?></td>
                <td class="text-center"><div class="input-group spinner spinner-2">
                        <input type="text" disabled="" value="<?php echo $reqItem->quantity;?>" class="form-control manual-quantity" >
<!--                    <div class="input-group-btn-vertical">
                      <button class="btn btn-inc" type="button"><i class="fa fa-caret-down "></i></button>
                      <button class="btn btn-dec" type="button"><i class="fa fa-caret-up "></i></button>
                    </div>-->
                  </div></td>
                <td><a href="#" class="edit-delete edit-delete-2"><i class="fa fa-pencil" aria-hidden="true"></i></a> 
                    <input  name="ItemPrice[<?php echo $reqItem->ID?>][price]" value="<?php if(isset($reqItem->requestItemPrices)&& !empty($reqItem->requestItemPrices[0]))echo ($reqItem->requestItemPrices[0]->price);?>" type="text" class="price-input price-input-2 manualPriceChange" placeholder="AED 300"></td>
                <td><strong class="item-tprice"><?php if(isset($reqItem->requestItemPrices)&& !empty($reqItem->requestItemPrices[0]))echo ($reqItem->requestItemPrices[0]->price)*$reqItem->quantity; ?></strong></td>
            <input type="hidden" name="ItemPrice[<?php echo $reqItem->ID?>][reqItemId]" value="<?php echo $reqItem->ID;?>" />
                
                <?php if(isset($reqItem->requestItemPrices)&& !empty($reqItem->requestItemPrices[0])){?>
            <input type="hidden" name="ItemPrice[<?php echo $reqItem->ID?>][reqItemPriceId]" value="<?php echo $reqItem->requestItemPrices[0]->ID;?>" />
            
                <?php } ?>
              </tr>
                <?php } ?>
            </tbody>
          </table>
        </div>
        <input type="hidden" name="requestId" value="<?php echo $model->ID;?>" />
        <input type="hidden" name="supplierId" value="<?php echo $supplierId;?>" />
            
      </div>
              
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right text-right margin-top">
                <div class="row">
                  <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
                    <div class="row">
                      <input name="" type="button" class="back-btn back-btn-2" onclick="$('.supplier-comparison').click()" value="Back">
                    </div>
                  </div>
                  <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6 pull-right">
                    <div class="row">
                      <input name="" type="submit" class="questionnaire-btn" value="Submit">
                    </div>
                  </div>
                </div>
              </div>
                    
                </form>
                
                
            </div>
          </div>
        </div>
        <div id="IParis" class="tabcontent" style="display: none;">
          <div class="supplier-comparison-outer">
            <div class="pre-bids">
<form class="" id="questionAnswers" method="POST" enctype="multipart/form-data">
  <h3 class="tab_drawer_heading d_active" rel="tab1">Fill in Questionnaire</h3>
  <div id="tab1" class="tab_content" style="display: block;">
     
          <input type="hidden" name="requestId" id="reqid" value="<?php echo $model->ID;?>" /> 
           <input type="hidden" name="supplierId" value="<?php echo $supplierId;?>" />
     <?php
      $scount = 1;
      foreach($model->sectionQuestions as $sq)
      {
          if($sq->parentid==0){
              $qcount = 1;
      ?>
    <div class="questionnaire-general-information mb-0">
        
        <h3><?php echo $scount.'. '.$sq->name; ?></h3>
        <?php foreach($model->sectionQuestions as $q)
        {
          if($q->parentid==$sq->ID){
        ?>
        <div class="questionnaire-general-inner">

            <div class="questionnaire-general-top">
                <h4><?php echo $scount.'.'.$qcount.'. '.$q->name?></h4>
                <p><?php echo $q->description; ?></p>
                <div class="general-answer">
                     <?php 
                     $answer = '';
                     foreach($q->questionsAnswers1 as $qans){
                         if($qans->uid==$supplierId){
                             $answer=$qans->ques_ans;
                             $slctans = $qans;
                         }
                     }
                     
                     ?>
                    <?php if(isset($slctans) && !empty($slctans)){?>
                     <input type="hidden" name="Question[<?php echo $q->ID?>][answerId]"  value="<?php echo $slctans->ID;?>" />
                    <?php } ?>
                    <?php   if($q->qtype==0){ ?> 
                    
                    <select <?php if(!empty($answer)) echo $answer ?>  <?php  if($q->mandatory=='Yes') echo 'required'; ?> name="Question[<?php echo $q->ID?>][answer]" class="form-control validate-field"> 
                        <option  value="">Select one</option> 
                        <option <?php if($answer=='Yes') echo 'selected'?>>Yes</option> 
                        <option <?php if($answer=='No') echo 'selected'?>>No</option>
                    </select>
                    
                    <?php }else if($q->qtype==1){  ?> 
                    <textarea name="Question[<?php echo $q->ID?>][answer]" cols="" rows=""  <?php  if($q->mandatory=='Yes') echo 'required'; ?>  placeholder="Enter your answer here..." class="answer"><?php if(!empty($answer)) echo $answer ?></textarea> 
                        <?php  }elseif($q->qtype==2){  ?> 
                    <div class="form-group">
                    <select  name="Question[<?php echo $q->ID?>][answer]" class="form-control validate-field"  <?php  if($q->mandatory=='Yes') echo 'required'; ?> > 
                        <option value="">Select one</option> 
                        <?php if(isset($q->optionlists)&& !empty($q->optionlists)){ 
                            foreach($q->optionlists as $optl){?>
                        <option <?php if($answer==$optl->value) echo 'selected'?> ><?php echo $optl->value; ?></option>
                        <?php } } ?>
                    </select>  
                    </div >
                    <?php  }elseif($q->qtype==3){  ?> 
                   <div class="purchase-outer <?php  if($q->mandatory=='Yes') echo 'required'; ?>">
                        <div class="error error-checkbox"></div>

                         <ul class="checkbox-quest   ">
                           <?php if(isset($q->optionlists)&& !empty($q->optionlists)){ 
                                         foreach($q->optionlists as $optl){ 
                                             $checked = '';
                                         if(!empty($answer) && $optl->value==$answer){
                                            $checked = 'selected'; 
                                         }
                                             ?>
                          <li>
                              <div class="pacific-checkbox">
                                         <div class="custom-checkbox">
                                             <input name="Question[<?php echo $q->ID?>][answer][]" value='<?php echo $optl->value;?>' <?php if(!empty($checked)) echo 'checked';?> id="cbaq<?php echo $optl->ID;?>" type="checkbox" />
                                           <label  for="cbaq<?php echo $optl->ID;?>"></label>
                                         </div>
                                       </div><?php echo $optl->value;?>
                          </li>
                          <?php } } ?>
                         </ul>


                    </div>
                    <?php  } ?>
                    <?php if($q->document==1){ ?>
                    <div class="drag-answer">
                        <div data-id='<?php echo $q->ID; ?>' class="drag-answer-here drag-doc">
                         Drag & Drop your file here
                        </div>
                         <div class="attachment">
                             <label for="Question_<?php echo $q->ID;?>_doc" class="add-attachment-bnt">+ Add attachment</label>  
                        </div>
                        <div class="view-files ">
                        <div class="drag-documents-list">
                            <?php
                                $this->widget('CMultiFileUpload', array(
                                  'name'=>'Question['.$q->ID.'][doc]',
                                  'accept'=>'jpg|png|PNG',
                                  'remove'=> '[x]',
                                  'duplicate'=>'Already Selected',
                                  'denied'=>'File is not allowed',
                                  'options'=>array(
                                    'afterFileSelect'=>'function(e, v, m){ }',
                                    ),
                                  'max'=>10, // max 10 files
                                    'htmlOptions'=>array('class'=>' nodisplayimp ','multiple'=>'multiple','enctype'=>'multipart/form-data','onchange'=>'insertDocuemnt(this)')

                                ));
                            ?>
                              
                             
                            <ul >
                          
                           </ul>
                        
                                        <?php 
            
            if(isset($q->questionsAnswers1)){
                foreach($q->questionsAnswers1 as $qans){
                    if($qans->uid==$supplierId){
            ?>
               
            
               
                                       <?php 
//                                               echo '<pre>';print_r($answers->questanswersDocuments);exit;
                                       if(isset($qans->questanswersDocuments )&&!empty($qans->questanswersDocuments )){ foreach($qans->questanswersDocuments as $doc){ ?>
                                       <li>
                                           <div class="drag-documents-leftbar"><strong><?php echo $doc->doc->origname;?></strong><span>File size : <?php echo $doc->doc->size/1000; ?> KB</span></div>
                                           <div class="drag-documents-right"><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/pin.png" alt="pin"></a><a download href="<?php echo $doc->doc->url.'/'.$doc->doc->name; ?>"  >View</a> <a data-qid='<?php echo $doc->ID?>' class='delete-af-doc' href="javascript:void(0)"  >remove</a> </div>
                                        </li>
                                       <?php } } ?>
                  
                            
                           
            <?php } } } ?>  
                       </div>                 
                   </div>
                    </div>
                    <?php } ?>
                    
                </div>
            </div>

            </div>

      
        <?php $qcount++;}
        
          }?> 
    </div>
            

        <?php  $scount++;} } ?>


    <div class="request">
    <div class="request-btn-outer">
   <input type="submit"  class="next-button btn " value="Save" />
    </div>
   </div>

  </div>
  </form>
              </div>
            </div>
          </div>
          
        </div>
       