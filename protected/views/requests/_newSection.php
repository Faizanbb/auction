<?php
/* @var $this RequestsController */
/* @var $model Requests */
/* @var $form CActiveForm */
?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'section-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
    <div class="create-section">
        <h5>Create a new section</h5>
            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>350,'class'=>'form-control')); ?>
            <?php echo $form->error($model,'name'); ?>
        <input type='hidden' name='SectionQuestions[rid]' value='<?php echo $requestId?>' >
        <input name="" type="button" value="Save" id='savenewSection'  class="save-button">
        <input name="" type="button" data-dismiss='modal' value="Cancel" class="cancel-button extbtn canceNewlSection">
    </div>

	

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $(document).ready(function(){
        $('.canceNewlSection').on('click',function(){
           $('#addSectionContent').html(''); 
        });
    });
</script>