<?php // echo '<pre>'; print_r($model);exit; ?>   

<div class="supplier-comparison-outer">
            <div class="pre-bids">
              <div class="pre-bids-table scroll-auto">
                <table  class="table-pre-bids  supplier-bid-comp">
                  <tbody>
                    <tr>
                        <td style="width:30%">&nbsp;</td>
                      <?php 
                        $supline = array();$getTotalPrices = array();
                         foreach ($model->requestSuppliers as $reqSup){ 
                             if($reqSup->accept==3)
                              $supline[]=$reqSup->supplier->ID;
                         }
                        foreach ($model->requestSuppliers as $reqSup){ 
                            if($reqSup->accept==3){
                        $getTotalPrices[$reqSup->supplier->ID] =0; 
                       ; ?>
                      <td style="width:<?php echo 70/count($supline).'%'; ?>"><h4><?php echo $reqSup->supplier->acc->company; ?></h4>
                        <p><?php echo $reqSup->supplier->name; ?><br>
                          <?php echo $reqSup->supplier->email; ?><br>
                          <?php  echo '+'.$reqSup->supplier->carriercode.' '.$reqSup->supplier->mobile; ?></p>
                   
                      </td>
                        <?php } }  ?>
                    </tr>
                    <tr>
                        <td style="width:30%">&nbsp;</td>
                      <?php foreach ($model->requestSuppliers as $reqSup){ if($reqSup->accept==3){   ?>
                        <td>
                      <a href="javascript:void(0)" class="purchase-btn">create purchase <br>
                      order <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon-blue.png" alt="icon-blue"></a> 
                            <a href="javascript:void(0)" data-rid ="<?php echo $model->ID;?>" data-uid ="<?php echo $reqSup->supplier->ID; ?>" class="enter-manual-btn enterBidManual">enter manual bid <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon-orange.png" alt="icon-blue"></a>
                    </td>
                      <?php } } ?>
                    </tr>
                    <tr class="bid-hitm-cmp-tr">
                        <th><div class="width-50 d-inline-block">Items</div><div class="width-25 d-inline-block text-center">Quantity</div><div class="width-25 d-inline-block text-right">Unit</div></th>
                        <?php foreach($model->requestSuppliers as $sup) { if($sup->accept==3){  ?>
                        <th><div class="width-50 d-inline-block ">Unit Price</div><div class="width-50 text-right d-inline-block">item total</div></th>
                        <?php } } ?>
                        
                    </tr>
                    
                    <?php $requestItems = $model->requestItems; $mcount=1; $expectedBudget = 0;
                    foreach($requestItems as $secQt){ 
                        $expectedBudget+=$secQt->price*$secQt->quantity; 
                        ?>
                    <tr class="bid-ditm-cmp-tr">
                        <td><div class="width-50 d-inline-block"><?php echo $secQt->name; ?></div><div class="width-20 d-inline-block text-center"><?php echo $secQt->quantity; ?></div><div class="width-30 d-inline-block text-right"><?php echo $unitList[$secQt->unit_id]; ?></div></td>
                               <?php  foreach($supline as $suparrnge){ 
                                   /*chcek if person has given ans other wise show empty cell*/ 
                                   $supanstatus  = true;  
                                   
                               foreach($secQt->requestItemPrices as $sup){ 
                                   if($suparrnge==$sup->uid){ 
                                   $supanstatus=false;
                                   
                                   $getTotalPrices[$suparrnge] = $getTotalPrices[$suparrnge] + $sup->price*$secQt->quantity;
                                   ?> 
                        <td><div class="width-50 d-inline-block "><?php echo number_format((float)$sup->price, 2, '.', ''); ?></div><div class="width-50 text-right d-inline-block"><span class="item-price"><?php echo ' <span class="item-tprice">'.number_format((float)$sup->price*$secQt->quantity, 2, '.', '').'</span>';?> </span></div></td>
                                                
                                  
                    <?php } } if($supanstatus) echo '<td></td>'; } ?>
                       
                    </tr>
                    <?php  } ?>

                    <tr class="bid-hitm-cmp-tr">
                        <th><div class="width-50 d-inline-block">Items Total</div></th>
                         <?php  
                        foreach($supline as $suparrnge){ 
                                   /*chcek if person has given ans other wise show empty cell*/ 
                                   $supanstatus  = true;  
                               foreach($getTotalPrices as $key=>$value){ 
                                   if($suparrnge==$key){ 
                                   $supanstatus=false;
                                   
                                   ?> 
                        <th><div class="width-50 d-inline-block "></div><div class="width-50 text-right d-inline-block"> AED <span class="item-tprice"><?php echo number_format((float)$value, 2, '.', ''); ?></span></div></th>
                         <?php } } if($supanstatus) echo '<td></td>';  } ?>
                    </tr>  
                    <tr class="bid-hitm-cmp-tr">
                        <td><div class="width-50 d-inline-block">Difference</div></td>
                         <?php  
                         $max = array_keys($getTotalPrices, max($getTotalPrices));
                        $max = $max[0];
                        $min = array_keys($getTotalPrices, min($getTotalPrices));
                        $min = $min[0];
                       
                         foreach($supline as $suparrnge){ 
                                   /*chcek if person has given ans other wise show empty cell*/ 
                                   $supanstatus  = true;  
                               foreach($getTotalPrices as $key=>$value){ 
                                   if($suparrnge==$key){ 
                                   $supanstatus=false;
                                    
                                   $dfvalue = $getTotalPrices[$max]-$value;
                                
                                   if(isset($getTotalPrices[$max])&&$getTotalPrices[$max]!=0)
                                   $dfvalueprc = ($value/$getTotalPrices[$max])*100;
                                   else
                                   $dfvalueprc = 0;
                                  
                                   if($dfvalue!=0){
                                   ?> 
                        <td><div class="width-50 d-inline-block "></div><div class="width-50 text-right d-inline-block"> <?php if($dfvalue >0) echo '+'.$dfvalue.'<br> +'.round($dfvalueprc,2).'%'; ?></div></td>
                         <?php }else  echo '<td></td>'; }   } } ?>
                    </tr>  
                    <tr class="bid-hitm-cmp-tr" >
                        <td><div class="width-50 d-inline-block">Expected Budget</div></td>
                        <?php  
                         $max = array_keys($getTotalPrices, max($getTotalPrices));
                        $max = $max[0];
                        $min = array_keys($getTotalPrices, min($getTotalPrices));
                        $min = $min[0];
                       
                         foreach($supline as $suparrnge){  ?>
                        <td class="text-right"> <?php echo $model->currency;?> <?php echo $expectedBudget; ?></td>
                             
                            <?php } ?>
                    </tr>  
                    <tr class="bid-hitm-cmp-tr">
                        <td><div class="d-inline-block">Difference from expected budget</div></td>
                         <?php  
                      
                         foreach($supline as $suparrnge){ 
                                   /*chcek if person has given ans other wise show empty cell*/ 
                                   $supanstatus  = true;  
                               foreach($getTotalPrices as $key=>$value){ 
                                   if($suparrnge==$key){ 
                                   $supanstatus=false;
                                   
                                   $dfvalue = $value-$expectedBudget;
                                   
                                   if(isset($expectedBudget)&&!empty($expectedBudget)&&$value!=0){
                                   if($dfvalue >0)
                                   $dfvalueprc = ($value/$expectedBudget)*100;
                                   else
                                   $dfvalueprc = ($expectedBudget/$value)*100;
                                   }else
                                       $dfvalueprc = 0;
                                   if($dfvalue!=0){
                                   ?> 
                        <td><div class="width-50 d-inline-block "></div><div class="width-50 text-right d-inline-block"> <?php if($dfvalue >0) echo '+'.$dfvalue.'<br> (+'.round($dfvalueprc,2).'%)'; else echo $dfvalue.'<br> (-'.round($dfvalueprc,2).'%)'; ?></div></td>
                         <?php }else  echo '<td></td>'; }   } } ?>
                    </tr>  
                    <tr class="bid-hitm-cmp-tr">
                        <td><div class="d-inline-block">Documents attached to bid</div></td>
                         <?php  
                      
                         foreach($supline as $suparrnge){ 
                                   /*chcek if person has given ans other wise show empty cell*/ 
                                   $supanstatus  = true;  
                               foreach($getTotalPrices as $key=>$value){ 
                                 $dfvalue=0;
                                   if($dfvalue!=0){
                                   ?> 
                        <td><div class="width-50 d-inline-block "></div><div class="width-50 text-right d-inline-block"> <?php if($dfvalue >0) echo '+'.$dfvalue.'<br> (+'.round($dfvalueprc,2).'%)'; else echo $dfvalue.'<br> (-'.round($dfvalueprc,2).'%)'; ?></div></td>
                         <?php } } echo '<td></td>';}   ?>
                    </tr>  
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right text-right margin-top">
              <div class="col-lg-2">
                <div class="row">
                  <input name="" type="button" class="back-btn back-btn-2 back-btn-n" value="Back">
                </div>
              </div>
              <div class="col-lg-2 pull-right">
                <div class="row">
                  <input name="" type="button" class="questionnaire-btn" value="Next">
                </div>
              </div>
            </div>
          </div>
<script>
    $(document).ready(function(){
     
       $('.bid-ditm-cmp-tr').each(function(){
           var arr=[];
           var i=0;
          $(this).find('td .item-tprice').each(function(index){
              arr[index] = ($(this).text());
               
          });
          var min = Math.min.apply(min,arr);
          var max = Math.max.apply(max,arr);
          
          $(this).find('td .item-tprice').each(function(index){
              if($(this).text()==min)
                  $(this).addClass('min-price');
              if($(this).text()==max)
                  $(this).addClass('max-price');
               
          });
          
          console.log();
          
       }); 
       $('.bid-hitm-cmp-tr').each(function(){
           var arr=[];
           var i=0;
          $(this).find('th .item-tprice').each(function(index){
              arr[index] = ($(this).text());
               
          });
          var min = Math.min.apply(min,arr);
          var max = Math.max.apply(max,arr);
          $(this).find('th .item-tprice').each(function(index){
              if($(this).text()==min)
                  $(this).addClass('min-price');
              if($(this).text()==max)
                  $(this).addClass('max-price');
               
          });
          
          console.log();
          
       }); 
    });
    </script>