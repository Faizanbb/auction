    <a class="a-tab-block  active" href='javascript:void(0)'><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/iocn-2.png" alt="reports"> Create <br>
      RFI </a>
    
    <div class="tab-block  general-block active">General Information</div>
    <div class="tab-open">
      <div class="tab-heading">
        <h1>create rfI </h1>
      </div>
      <div class="general-Information">
        <h2>enter general information</h2>
        

        <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'requests-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // There is a call to performAjaxValidation() commented in generated controller code.
                // See class documentation of CActiveForm for details on this.
                'enableAjaxValidation'=>false,
        )); ?>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-condensed-left">
            <div class="form-group">
              <label for="email">Name <sup><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/star-icon.png" alt="Star"></sup> <a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt="icon"></a></label>
        	<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>350,'class'=>'form-control text-field')); ?>
		<?php echo $form->error($model,'name'); ?>
	      <p>Example: Purchases of Office Supplies; AMC for Facilities Management, <br>
                Cleaning Services.</p>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-condensed-right">
            <div class="form-group">
              <label for="email">End Time <sup><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/star-icon.png" alt="Star"></sup> <a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt="icon"></a></label>
              <div class="end-time">
                <div class="date date-picker">
                  <div class="date-pic">
                    <div class="input-group" id="datetimepicker1">
                        
                	<?php 
                        if(isset($model->endtime)&& !empty($model->endtime)){
                        if($model->endtime=='0000-00-00 00:00:00')
                            $endtime = '';
                        else
                        $endtime = date('Y-m-d', strtotime($model->endtime));
                        
                        $model->endtime = $endtime;
                        }
                        echo $form->textField($model,'endtime',array('class'=>'')); ?>
                        <?php echo $form->error($model,'endtime'); ?>
	      <p>Deadline for suppliers to submit their <br>
                        bids. </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="time-pick ">
                  <div class='drp'>
                		<?php 
                foreach($timezone as $tz)
                $timezonelist[$tz->ID] = $tz->name;
                echo $form->dropDownList($model,'timezone',$timezonelist,array('class'=>'')); ?><?php echo $form->error($model,'timezone'); ?>
                      </div>
<p>Timezone: Dubai, Asia <br>
                  (UTC+04:00)</p>
              </div>
            </div>
          </div>
          <div class="form-group col-lg-12">
            <div class="row">
              <label for="email">Description <sup><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/star-icon.png" alt="Star"></sup> <a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt="icon"></a></label>
            	<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50,'class'=>'form-control comments-field')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>
            <div class="row">
                <input type="hidden" class='requestId' name='Requests[rid]' value='<?php echo $requestId; ?>' />
                <input name="" type="button" class="questionnaire-btn questionnaire-btn-new" value="Next: Questionnaire">
            </div>
          </div>
        <?php  $this->endWidget(); ?>
      </div>
    </div>
    <div class="tab-block request-block general-block questionnaire-block" >Questionnaire</div>
    <div class="tab-open">
      <div class="tab-heading">
        <h1>CREATE RFI</h1>
      </div>
      <div class="general-Information">
        <h2>Questionnaire</h2>
            <button name="" type="button" id='questSection' class="add-question-btn  myBtn_multi" value="">Add New Question</button>
          <div class="modal " id='questionnaireModal'> 
            
            <!-- Modal content -->
            
            
          </div>
          
<!--          Pacific Trade Corp pop up
          <input name="" type="button" class="add-question-btn add-question-btn-has allquestion myBtn_multi" value="Pacific Trade Corp.">-->
          <div class="modal modal_multi" id='questionnaireModalHas'> 
            
            <!-- Modal content -->

          </div>
          <!--Pacific Trade Corp pop up-->
          <div class="col-lg-12 <?php if($questcount==0) echo 'nodisplay';?> questionnaire-box add-question-btn-new">
            <div class="row">
              <div class="open-questionnaire"> <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/text-icon.png" alt="">
                <h4>Open Questionnaire</h4>
                <p> <span class="questionnaire-count"><?php echo $questcount;?></span> Questions have been added to request</p>
              </div>
            </div>
          </div>
         
          <div class=" col-lg-12">
            <div class="row">
              <input name="" type="button" class="tab-block-btn back-btn" value="Back">
              <input name="" type="button" class="questionnaire-btn questsection-btn" value="Next: Questionnaire">
            </div>
          </div>
      </div>
    </div>
    <div class="tab-block request-block general-block documentation-block">Documentation</div>
    <div class="tab-open">
      <div class="tab-heading">
        <h1>CREATE RFI</h1>
      </div>
      <div class="requisition-inner">
        <div class="general-Information">
            
    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'document-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
        'clientOptions'=>array(
        'validateOnSubmit'=>true,
        ),
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),

)); 
  
?>
          <h2>Attach documentation</h2>          
          <div class="requisition-inner text-center">
               <div class="drag-documents">
        <div class="drag-documents-input">Drag documents here</div>
        <label for="Documents_name" id="Documents_name_label" class="approver-btn">Add document...</label>
         <?php
                    $this->widget('CMultiFileUpload', array(
                      'model'=>$docModel,
                      'attribute'=>'name',
                      'accept'=>'jpg|png|PNG',
                      'remove'=> '[x]',
                      'duplicate'=>'Already Selected',
                      'denied'=>'File is not allowed',
                      'options'=>array(
                        'afterFileSelect'=>'function(e, v, m){ console.log(m);'
                          . 'if($("#"+m.current.id+"-li").length==0){'
                          . 'var data = " <li id=\'"+m.current.id+"-li\'>  <div class=\'drag-documents-leftbar\'><strong>"+v+" </strong></div><div class=\'drag-documents-right \'><a href=\'#\'><img src=\'./images/pin.png\' alt=\'pin\'></a><a href=\'javascript:void(0)\' class=\'unploded-doc\' onclick=\' dcd(this) \'  data-uuid=\'"+m.current.id+"\'  >Remove</a></div></li>";   $(".drag-documents-list ul").append(data);  '
                          . '}else{'
                          . ' var curtxt =  $("#"+m.current.id+"-li").find(".drag-documents-leftbar").find("strong").text();  $("#"+m.current.id+"-li").find(".drag-documents-leftbar").find("strong").text(curtxt+" ,"+v);'
                          . '}'
                          . ';fileLabelloyality();checkdocadded();}',
                          
                        ),
                      'max'=>10, // max 10 files
                        'htmlOptions'=>array('class'=>' nodisplayimp add-docs','multiple'=>'multiple','enctype'=>'multipart/form-data')

                ));
        ?>
        <div class="drag-documents-list">
         <ul> <?php
             if(isset($reqDocs) && !empty($reqDocs)){
//                    print_r($reqDocs);exit;
                    foreach($reqDocs as $d){
                       
                ?>
         <li>
         <div class="drag-documents-leftbar">
             <strong><?php echo $d->d->origname;?> </strong>
             <!--<span>File size : 12KB</span>-->
         </div>
             <div class="drag-documents-right "><a href="#"><img src="<?php echo Yii::app()->theme->baseUrl?>/dist/images/pin.png" alt="pin"></a><a href="javascript:void(0)" class="delete-doc"  data-id="<?php echo $d->ID?>" data-did="<?php echo$d->d->ID;?>">Remove</a></div>
         </li>
         <?php   }
                }
                ?>
         </ul>
         </div>
        </div>
            <div class="error phone-error-container"></div>
            <input type="hidden" value='<?php echo $requestId; ?>' class='requestId' name='Request[rid]' />
              <div class="text-left">
                <input name="" type="button" class="tab-block-btn back-btn" value="Back">
                <input name="" type="submit"  id='savedocuments' class="questionnaire-btn" value="Next: suppliers">
              </div>
	 </div>
          <?php $this->endWidget(); ?>
        </div>
      </div>
    </div>
    <div class="tab-block request-block general-block suppliers-block">Suppliers</div>
    <div class="tab-open">
      <div class="tab-heading ">
        <h1>CREATE RFI</h1>
      </div>
        <form enctype="multipart/form-data" id="publishForm" action='<?php echo Yii::app()->createUrl('requests/publish')?>' method="POST" >
      <div class="general-Information">
        <h2>invite suppliers</h2>
        <div class="suppliers-list">
          <ul>
            <li>
              <label>
                <input name="Suppliers[type]" type="radio" value="manual">
                Add Participants Manually <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt="">
              </label>
              <div class="hide-div supplier-box manual-email-box nodisplay manual">
                <h3>User E-mail * <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt="icon"></h3>
                <div class="input-group">
                  <div class="input-group-addon">@</div>
                  <input type="email" name='Suppliers[manual][]' class="form-control text-field" id="exampleInputAmount">
                  <button type="button" class="btn add-btn add-manual-email">Add</button>
                </div>
                <p class="error manual-error"></p>
              </div>
            </li>
            <li>
              <label>
                <input name="Suppliers[type]" type="radio" value="upload">
                Upload Participants <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt=""></label>
              <div class="hide-div supplier-box  nodisplay upload">
                  <div class="supplier-box-docu">
                  <label id="supplierDocLabel" for="Suppliers_uploaddoc"> </label>
                <div class="input-group " > 
                            <span class="input-group-btn">
                  <button   class="btn btn-default btn-choose" type="button">Upload file</button>
                           <?php
                    $this->widget('CMultiFileUpload', array(
                        'name'=>'Suppliers[uploaddoc]',
                        'accept'=>'xlsx',
                        'remove'=> '[x]',
                        'duplicate'=>'Already Selected',
                        'denied'=>'File is not allowed',
                        'options'=>array(
                          'afterFileSelect'=>'function(e, v, m){ $(".upload-field").val(v);}'

                          ),
                        'max'=>10, // max 10 files
                          'htmlOptions'=>array('class'=>'nodisplayimp  ','enctype'=>'multipart/form-data')

                  ));
          ?>
                  </span>
                  <input type="text" class="form-control upload-field" />
                  <button class="btn  btn-reset" type="button">Upload List</button>
                </div>
                  
                </div>
                 <p>Please use this format <a href="<?php echo Yii::app()->request->baseUrl?>/uploads/supplier.xlsx" download>download</a></p>
                 <p class="upload-error eror"></p>
              </div>
                 
            </li>
            <li>
              <label>
                <input name="Suppliers[type]" type="radio" value="pickgroup">
                Pick Group <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt=""></label>
              <div class="hide-div supplier-box  nodisplay pickgroup">
                
              <ul >
              <?php if(isset($supplierGroups)&&!empty($supplierGroups)){
                  foreach($supplierGroups as $sa){
                  ?>
                    <li>
                        <div class="pacific-checkbox">
                        <div class="custom-checkbox">
                         <input id="sg<?php echo $sa->ID;?>" value="<?php echo $sa->ID;?>" type="checkbox" name="Suppliers[pickgroup][]"><label for="sg<?php echo $sa->ID;?>"></label></div>
                       </div> <span><?php echo $sa->name;?></span> 
                       <!--<button type="submit" class="btn add-btn">Add</button>-->
                    </li>
                  <?php } }else{ echo '<li> No Group Found <li>'; } ?>
              </ul>
                
                   <div class="width-100 d-inline-block">
                    <p class="pickgroup-error error"></p>
                </div> 
              </div>
               
            </li>
            <li>
              <label>
                <input name="Suppliers[type]" type="radio" value="pickdatabase">
                Pick From Participants Database <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt=""></label>
              <div class="hide-div-outer supplier-box nodisplay supplier-box pickdatabase">
              <div class="hide-div">
              <ul>
                <?php if(isset($accountSuppliers)&&!empty($accountSuppliers)){
                    foreach($accountSuppliers as $as){
                    ?>
                  
                <li>
                    <div class="pacific-checkbox">
                    <div class="custom-checkbox">
                    <input id="pfpd<?php echo $as->ID;?>"  name="Suppliers[pfpd][]" value="<?php echo $as->user->ID; ?>" type="checkbox"><label for="pfpd<?php echo $as->ID;?>"></label></div>
                    </div> <span><?php echo $as->user->acc->company?>: <?php echo $as->user->name.' '.$as->user->email;?>  </span>
                </li>
                
                <?php }}else{echo '<li>No Participant Found</li>';} ?>
                
              </ul>
              
              </div>
<!--              <div class="add-btn-outer">
              <button type="submit" class="btn add-btn">Add</button>
              </div>-->
                <div class="width-100">
                    <p class="error pickdatabase-error"></p>
                </div>
              </div>

                
            </li>
          </ul>
            <p class="error suppliertype-error"></p>
        </div>
        <div class="email-invitation">
          <h3>Customize Invitation Email</h3>
          <label for="Suppliers_cusEmail_yes" class="yes-btn emailbtn ">Yes</label>
          <label for="Suppliers_cusEmail_no" class="yes-btn emailbtn active">No</label>
          <input id="Suppliers_cusEmail_yes" name="Suppliers[cusEmail]" value="yes" type="radio" class="nodisplay" >
          <input id="Suppliers_cusEmail_no" name="Suppliers[cusEmail]" value="no" type="radio" class="nodisplay" >
          
          <div class="invitation-email nodisplay">
           <div class="invitation-email-subject">
            <label>Subject <sup><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/star.png" alt="star"></sup> <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt=""></label>
            <input type="text" name='Suppliers[subject]' class="form-control text-field" id="email" placeholder="{$name}">
            <input name="" type="button" class="insert-field" value="+ Insert Field">
            <div class="email-content">
             <label>Email Content <sup><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/star.png" alt="star"></sup> <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt=""></label>
             <small><br>Use these variables {want_to_participate},{dont_want_to_participate}</small>
             <textarea name="Suppliers[emailbody]" id="emailEditor" cols="" rows="" class="content-msg"></textarea>
            </div>
           </div>
          </div>
          
          <div class="privacy-settings">
            <h4>RFI Privacy Settings</h4>
            <ul>
              <li>
                <label>
                  <input name="Suppliers[privacy]" type="radio" value="public">
                  Public Request <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt="icon"></label>
              </li>
              <li>
                <label>
                  <input name="Suppliers[privacy]" type="radio" value="restricted">
                  Restricted Public Request <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt="icon"></label>
              </li>
              <li>
                <label>
                  <input name="Suppliers[privacy]" type="radio" value="private">
                  Private Requestt <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt="icon"></label>
              </li>
            </ul>
            <div class="width-100 d-inline-block"><p class="privacy-error error"></p> </div>
          </div>
          <div class="buttons-rfi"> 
          <a href="#" id="previewRFIBtn" class="rfi-btn myBtn_multi">Preview RFI <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt=""></a> 
          <div class="modal modal_multi" id="previewRFIModal"> 
            
            <!-- Modal content -->
            <div class="modal-content questionnaire-popup"> <span class="close close_multi" data-dismiss="modal"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/close-btn.png" alt="close"></span>
              <h3>Preview RFI</h3>
              <div class="questionnaire-popup-inner">
                <div class="preview-supplier">
                 <ul>
                  <li>
                  <h4>Preview Supplier View</h4>
                  <p>This is how suppliers will see the RFI bid request. Please check all the details to <br>ensure accuracy.</p>
                  <a href="javascript:void(0)"  class="supplier-view-btn open-supplier-btn">Open Supplier View</a>
                  </li>
                  
                  <li>
                  <h4>Preview Invitation Email</h4>
                  <p>Please enter your email address below to see the invitation that suppliers will <br>receive.</p>
                  
                  <div class="form-group">
              <label for="email">Email Address </label>
              <input type="email" class="form-control text-field" id="email">
              </div>
                  
                  <a href="javascript:void(0)" id="previewModalBtn"  class="supplier-view-btn myBtn_multi">Preview Invitation</a>
                  
                  
                  
                  
                  </li>
                 </ul>
                </div>
                <div class="question-form-btn">
                  <input data-dismiss="modal" data-dismiss="modal" name="" type="button" class="close-btn" value="Close">
                </div>
              </div>
            </div>
          </div>
          
          
          <a href="#" class="rfi-btn">save RFI <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/icon.png" alt=""></a> 
          <input class="publish-btn" type="submit" value='publish'> </div>
        </div>
      </div>
            <input class="requestId" type="hidden" name='Suppliers[requestId]' value="<?php echo $requestId?>"/>
    
        </form>
        
    </div>
    
    <input class="requestId" type="hidden" id="reqid" value="<?php echo $requestId?>"/>
    <div class="modal modal_multi" id="previewModalView"> 
            
            <!-- Modal content -->
            <div class="modal-content questionnaire-popup"> <span class="close close_multi close-invitemail"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/close-btn.png" alt="close"></span>
              <h3>Preview the invitation email</h3>
               <div class="preview-invitation-email">
               Subject: <strong>Business Bid DMCC invites you to participate in an RFI for Cleaning Services</strong><br>
               Date: <strong>Aug 24, 2018 12:17</strong>
               </div>
                <div class="preview-invitation-email-content">
                <div class="invitation-email-content-top">
                <p>Hi</p>
                <p>I am inviting you to participate on a request below. Click the name of the request to see details and please submit your bid through our online platform.</p>
                <p><span>RFI for Cleaning Services </span>
                The deadline for making bids is: 31. August 2018 12:00 (UTC+04:00)</p>
                <p><strong>Description of the request:</strong><br>
                Annual Contract for Cleaning Services for Office 503, Indigo Icon Tower, Cluster F, JLT. Dubai.</p>
                </div>
                
                <div class="invitation-email-content-top">
                <p><strong>If you would like to participate in this request, please <a href="#">click here</a> so we will know to wait for your bid.</strong>
                
                However if this request is of no interest to you, <span>just let me know.</span></p>
                <div class="participate-btn">
                <a href="#">Want to Participate</a> <a href="#">Dont want to Participat</a>
                </div>
                </div>
                <div class="address">
                Kind regards,<br>
                Salman <br>
                Business Bid DMCC<br>
                +971505342349
                </div>
                </div>
            </div>
          </div>
         
    <div class="loading-gif">
        <img src="<?php echo Yii::app()->request->baseUrl?>/images/loading.gif" />
    </div>
        
    <script>
    
        $('.add-question-btn-new').click(function(){
            $('#questSection').trigger('click')
        });
        $('.add-question-btn-has').click(function(){
            $('#questionnaireModalHas').modal('show'); 
        });
        
        $('.questionnaire-btn-new').on('click',function(){
            
//            validate request form
            if(!$('#Requests_name').val()){
                $('#Requests_name').addClass("errorb");
                scrollBox($('#Requests_name'));
                $('#Requests_name').focus();
                return false;
            }$('#Requests_name').removeClass("errorb");
            
            if(!$('#Requests_endtime').val()){
                $('#Requests_endtime').addClass("errorb");
                scrollBox($('#Requests_endtime'));
                $('#Requests_endtime').focus();
                return false;
            }$('#Requests_endtime').removeClass("errorb");
            if(!$('#Requests_description').val()){
                $('#Requests_description').addClass("errorb");
                scrollBox($('#Requests_description'));
                $('#Requests_description').focus();
                return false;
            }$('#Requests_description').removeClass("errorb");
            
            
            
            var req = $('#requests-form').serialize();
            var ndata = {'Requests':req};
           $.ajax({
                        beforeSend: function() {
             // setting a timeout
             $('.loading-gif').show();
         },
         complete: function() {
             $('.loading-gif').hide();
         },
               type:'POST',
               data : req,
               url:'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'saveRFIRequest','id'=>''))?>',
               success: function(data){
                $('.requestId').each(function(){
                    $(this).val(data);    
                });
                if(data)
                    $('.request-block').removeClass('disabled-block');
                    $('.questionnaire-block').trigger('click');
                },
               error:{
               }
           })
        });
    </script>
    <script>
$(document).ready(function(){
   
   $('#questSection').on('click',function(e){
    var requestId = $('#reqid').val();

    $.ajax({
                beforeSend: function() {
            // setting a timeout
            $('.loading-gif').show();
        },
        complete: function() {
            $('.loading-gif').hide();
        },
           type     :'GET',
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'fetchSecQues','id'=>'')); ?>'+requestId,
           success  :function(data){
               
                   $('#questionnaireModal').html(data);
                    $('#questionnaireModal').modal('show'); 
                    $('.questionnaire-count').text($(".pacific-list ul").children().length);
                    if($(".pacific-list ul").children().length>0)
                    $('.questionnaire-box').show();
           },
           error    : function(){
           alert('some thing went wrong');
            }
       })
   });
   $('#getRequestDocument').on('click',function(){
      var requestId = $('#reqid').val();
     $.ajax({
                    beforeSend: function() {
               // setting a timeout
               $('.loading-gif').show();
           },
           complete: function() {
               $('.loading-gif').hide();
           },
           type     :'GET',
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'requestDocument','id'=>'')); ?>'+requestId,
           success  :function(data){
               
                   $('#questionnaireModal').html(data);
         
           },
           error    : function(){
           alert('some thing went wrong');
            }
       })
   });
   $(document).on('click','#save-as-template',function(){

    var requestId = $('#reqid').val();
        $.ajax({
                    beforeSend: function() {
            // setting a timeout
            $('.loading-gif').show();
        },
        complete: function() {
            $('.loading-gif').hide();
        },
            type     : 'GET',
            url      : '<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'getNewTemplateForm','id'=>'')); ?>'+requestId,
            success  : function(data){
                
                 $('#questionnaireModal').html(data);
            },
            error    : function(){
                alert('some thing went wrong');
            }
        })
   });
   $(document).on('click','#loadTemplates',function(){

    var requestId = $('#reqid').val();
        $.ajax({
                    beforeSend: function() {
            // setting a timeout
            $('.loading-gif').show();
        },
        complete: function() {
            $('.loading-gif').hide();
        },
            type     : 'GET',
            url      : '<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'getQTemplates','id'=>'')); ?>'+requestId,
            success  : function(data){
                
                 $('#questionnaireModal').html(data);
            },
            error    : function(){
                alert('some thing went wrong');
            }
        })
   });
   
   $(document).on('click','#addSection',function(){

    var requestId = $('#reqid').val();
        $.ajax({
                    beforeSend: function() {
            // setting a timeout
            $('.loading-gif').show();
        },
        complete: function() {
            $('.loading-gif').hide();
        },
            type     : 'GET',
            url      : '<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'newSection','id'=>'')); ?>'+requestId,
            success  : function(data){
                 $('#addSectionContent').html(data);
            },
            error    : function(){
                alert('some thing went wrong');
            }
        })
   });
   
   $(document).on('click','#putNewTemplateQ',function(){
        var formData = $("#use-template-question-form").serialize();
        var requestId = $('#reqid').val();
        $.ajax({
                    beforeSend: function() {
            // setting a timeout
            $('.loading-gif').show();
        },
        complete: function() {
            $('.loading-gif').hide();
        },
           type     :'POST',
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'usenewTemplate','id'=>'')); ?>'+requestId,
           data     : formData,
           success  :function(data){
                $('#questSection').trigger('click');
           },
           error    : function(){
                alert('some thing went wrong');
            }
       })
   });
   $(document).on('click','#savenewTemplateQ',function(){
        var formData = $("#template-question-form").serialize();
        var requestId = $('#reqid').val();
        $.ajax({
                    beforeSend: function() {
            // setting a timeout
            $('.loading-gif').show();
        },
        complete: function() {
            $('.loading-gif').hide();
        },
           type     :'POST',
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'savenewTemplate','id'=>'')); ?>'+requestId,
           data     : formData,
           success  :function(data){
                $('#questSection').trigger('click');
           },
           error    : function(){
                alert('some thing went wrong');
            }
       })
   });
   $(document).on('click','#savenewSection',function(){
        var formData = $("#section-form").serialize();
        console.log(formData);
         var requestId = $('#reqid').val();
        $.ajax({
                    beforeSend: function() {
            // setting a timeout
            $('.loading-gif').show();
        },
        complete: function() {
            $('.loading-gif').hide();
        },
           type     :'POST',
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'savenewSection','id'=>'')); ?>'+requestId,
           data     : formData,
           success  :function(data){
                $('#questSection').trigger('click');
           },
           error    : function(){
                alert('some thing went wrong');
            }
       })
   });
   
   $(document).on('submit','#document-form',function(e){
       e.preventDefault();
         var requestId = $('#reqid').val();
        
        $.ajax({
                    beforeSend: function() {
            // setting a timeout
            $('.loading-gif').show();
        },
        complete: function() {
            $('.loading-gif').hide();
        },
            type     :'POST',
            url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'saverequestDocument','id'=>'')); ?>'+requestId,
            data     : new FormData( this ),
            processData: false,
            contentType: false,
            success  :function(data){
                console.log(data);
                console.log(JSON.parse(data));
                var avdata = JSON.parse(data);
                var apdata= '';
                for(var i=0;i<avdata.length;i++){
                 var apdata =apdata+'<li>         <div class="drag-documents-leftbar">             <strong>'+avdata[i].name+'</strong>           </div>             <div class="drag-documents-right "><a href="#"><img src="'+avdata[i].src+'/dist/images/pin.png" alt="pin"></a><a href="javascript:void(0)" class="delete-doc"  data-id="'+avdata[i].ID+'>" data-did="'+avdata[i].did+'">Remove</a></div>         </li>';
             }
               
                $(".drag-documents-list ul").html('');
                $(".drag-documents-list ul").append(apdata);
                $('#Documents_name-wrap').find('.add-docs').each(function(){
                    if(!$(this).is(':last-child'))
                        $(this).remove();
                });
                
                $('.suppliers-block').trigger("click");
            },
            error    : function(){
                 alert('some thing went wrong');
             }
        })
     
   });
   $(document).on('click','#savenewQuestion',function(){
        var formData = $("#question-form").serialize();
      var requestId = $('#reqid').val();
     $.ajax({
                    beforeSend: function() {
               // setting a timeout
               $('.loading-gif').show();
           },
           complete: function() {
               $('.loading-gif').hide();
           },
           type     :'POST',
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'savenewQuestion','id'=>'')); ?>'+requestId,
           data     : formData,
           success  :function(data){
                $('#questSection').trigger('click');
           },
           error    : function(){
                alert('some thing went wrong');
            }
       })
   });
   $(document).on('click','.addQuestion',function(){
   
        var qsecid = $(this).data('secid');
         var requestId = $('#reqid').val();
        var ndata = {'secId':qsecid};
        $.ajax({
                    beforeSend: function() {
            // setting a timeout
            $('.loading-gif').show();
        },
        complete: function() {
            $('.loading-gif').hide();
        },
           type     :'GET',
           data     : ndata,
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'addQuestion','id'=>'')); ?>'+requestId,
           success  :function(data){
                $('.section').each(function(){
                        $('#questionnaireModal').html(data);
                    
                });
           },
           error    : function(){
           alert('some thing went wrong');
            }
        });
   });
   $(document).on('click','.edit-question',function(){
   
        var qid = $(this).data('qid');
        var qsecid = $(this).data('secid');
        var ndata = {'qId':qid,'secId':qsecid};
        var requestId = $('#reqid').val();
        $.ajax({
                    beforeSend: function() {
            // setting a timeout
            $('.loading-gif').show();
        },
        complete: function() {
            $('.loading-gif').hide();
        },
           type     :'GET',
           data     : ndata,
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'editQuestion','id'=>'')); ?>'+requestId,
           success  :function(data){
                   
                    $('#questionnaireModal').html(data);;
                           },
           error    : function(){
           alert('some thing went wrong');
            }
        });
   });
   $(document).on('click','.delete-question',function(){
   
        var qid = $(this).data('qid');
        var qsecid = $(this).data('secid');
        var ndata = {'qId':qid,'secId':qsecid};
        var requestId = $('#reqid').val();
        $.ajax({
                    beforeSend: function() {
            // setting a timeout
            $('.loading-gif').show();
        },
        complete: function() {
            $('.loading-gif').hide();
        },
           type     :'GET',
           data     : ndata,
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'deleteQuestion','id'=>'')); ?>'+requestId,
           success  :function(data){
                   
                $('#questSection').trigger('click');
           },
           error    : function(){
           alert('some thing went wrong');
            }
        });
   });
   $(document).on('click','.clear-all-questions',function(){
   
        var requestId = $('#reqid').val();
        var ndata = {'reqId':requestId};
        $.ajax({
                    beforeSend: function() {
            // setting a timeout
            $('.loading-gif').show();
        },
        complete: function() {
            $('.loading-gif').hide();
        },
           type     :'POST',
           data     : ndata,
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'deleteAllQuestion','id'=>'')); ?>',
           success  :function(data){
                   
                $('#questSection').trigger('click');
           },
           error    : function(){
           alert('some thing went wrong');
            }
        });
   });
   
   $('.questsection-btn').on('click',function(){
        $('.documentation-block').trigger('click');
    });
   
});    
</script>

<script>
    $(document).on('click','.close-btn-question',function(){
       $('#questSection').trigger('click');
    });
    $(document).on('click','.btn-delete-opt',function(){
       $(this).parent().remove(); 
    });
    $(document).on('click','.addMoreOption',function(){
        var apenddata = '<div class="optbox"><input class="form-control d-inline-block optionvalue width-33" type="text" placeholder="value...." name="OptionValue[]"/><div class="btn-delete-opt btn btn-default d-inline-block"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/del-icon.png" alt="del"></div></div>';
        $(this).parent().parent().append(apenddata); 
        
    });
    function checkOptions(val){
        if(val==2||val==3){
        var apenddata = '<div class="optionBox"><input  class="form-control optionvalue width-33 d-inline-block" type="text" placeholder="value...." name="OptionValue[]"/> <span class="btn btn-default addMoreOption" type="button">+</span></div>';
        $('.addOptionContent').html(apenddata); 
            
        }else{
            $(".addOptionContent").html('');
        }
    }
    
</script>
<script>
        function checkdocadded(){
         if($('.add-docs').length > 1){
           if($('.doc-req-edit').hasClass('docsedit')){
               $('.doc-req-edit').removeClass('docsedit')
           } 
         } 
       }
var isAdvancedUpload = function() {
  var div = document.createElement('div');
  return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
}();

var $form = $('.drag-documents-input');

if (isAdvancedUpload) {
  $form.addClass('has-advanced-upload');
  var droppedFiles ;
  $form.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
    e.preventDefault();
    e.stopPropagation();
  })
  .on('dragover dragenter', function() {
    $form.addClass('is-dragover');
  })
  .on('dragleave dragend drop', function() {
    $form.removeClass('is-dragover');
  })
  .on('drop', function(e) {
    droppedFiles = e.originalEvent.dataTransfer.files;
    var lilen  =  $(".drag-documents-list ul li").length;
       var datapfirst = "<li id='dorp-li-"+lilen+"'><div class='drag-documents-leftbar'><strong>";
       var dataplast=" </strong><!--<span>File size : 12KB</span>--></div><div class='drag-documents-right '><a href='#'><img src='<?php echo Yii::app()->theme->baseUrl?>/dist/images/pin.png' alt='pin'></a><a href='javascript:void(0)' onclick='deleteDrop(this)' class='delete-drop add-docs' >Remove</a></div> <input type='file'class='nodisplayimp' id='doc_elment_"+lilen+"' multiple='multiple' name='Documents[name][]' /></li>";   
      var datapmiddle = "";
    for(var i = 0; i<droppedFiles.length;  i++){
        if(i==0)
        datapmiddle = droppedFiles[i].name;
        else
        datapmiddle =datapmiddle+" ,"+ droppedFiles[i].name;
         
       
    }
    var datap= datapfirst + datapmiddle+ dataplast;
    $(".drag-documents-list ul").append(datap);
    
     $("#dorp-li-"+lilen).find('input[type="file"]')
        .prop("files",droppedFiles);

    checkdocadded();

//    
  });
}

</script>
<script>
      function dcd(el){
                $(el).parent().parent().remove();
        var nid = $(el).data('uuid');
        $('#'+nid).remove();
        fileLabelloyality();
    };
    function fileLabelloyality(){
       var id =  $('#Documents_name-wrap').find('input').last().attr('id');
        console.log(id);
        $('#Documents_name_label').attr('for',id);
        
    }
    function deleteDrop(el){
       $(el).parent().parent().remove(); 
    };

  $(document).on('click','.delete-doc',function(){
    var status = confirm("Are you sure you want to delete it.");   
    var curElement = $(this);
    
    if( status){
       var id = $(this).data('id'); 
       var did = $(this).data('did'); 
       var extdata = {'reqdid':id,'docid':did};
       $.ajax({
                    beforeSend: function() {
             // setting a timeout
             $('.loading-gif').show();
         },
         complete: function() {
             $('.loading-gif').hide();
         },
          type  :'POST',
          url   :'<?php echo Yii::app()->createUrl("requests/questionnaire",array('action'=>'deleteDocument','id'=>''))?>',
          data  :extdata,
          success:function(data){
                $(curElement).parent().parent().remove();
         },
          Error:function(data){
          alert("some thing went wrong please contact admin");
            }
           
       });
    
    }
    });
    
</script>
<script>
    $('input[name="Suppliers[type]"]').change(function(){
        
        var value = $( 'input[name="Suppliers[type]"]:checked' ).val();
        $('.supplier-box').hide();
        $('.'+value).show();
         extendHeightofparent();
    });    
    $('input[name="Suppliers[cusEmail]"]').change(function(){
        
        var value = $( 'input[name="Suppliers[cusEmail]"]:checked' ).val();
        console.log(value)
       if(value=='yes'){
           $(".invitation-email").show();
          
        }else{
            $(".invitation-email").hide();
       }
        extendHeightofparent();
    });    
    function extendHeightofparent(){
        var height = $('.tab-block.active').next('.tab-open').height();
        console.log(height);
        $(".content-inner").css({
      'min-height': height+10 + 'px'
    });
    }
    $(".emailbtn").click(function(){
        $('.emailbtn').removeClass('active');
        $(this).addClass("active");
    });
</script>
<script>
    $('#previewRFIBtn').on('click',function(){
       $('#previewRFIModal').modal('show'); 
    });
    $('#previewModalBtn').on('click',function(){
       $('#previewModalView').modal('show'); 
       $('#previewRFIModal').modal('hide'); 
    });
    $('.close-invitemail').on('click',function(){
       $('#previewRFIModal').modal('show'); 
       $('#previewModalView').modal('hide'); 
    });
</script>

<script>
$(".open-supplier-btn").on('click',function(){
    var requestid = $('#reqid').val();
    window.open('<?php echo Yii::app()->createUrl('requests/supplierview',array('id'=>''));?>'+requestid, '_blank');
    
});    
</script>

<script>
    $('.add-manual-email').on('click',function(){
        var apdata = '<div class="input-group mt-10px"><div class="input-group-addon">@</div><input name="Suppliers[manual][]"  type="email" class="form-control text-field" id="exampleInputAmount"><button type="button" class="btn add-btn delete-manual-email">-</button></div>';
        $(".manual-email-box").append(apdata);
        
    });
    $(document).on("click",'.delete-manual-email',function(){
        $(this).parent().remove();
    })
    
    
</script>
<script>
    
    $('#publishForm').on('submit',function(e){
        if(!validationPublish())
        e.preventDefault();
        
    })
    function validationPublish(){
//        check if supplier type section is selected

        if ($('input[name="Suppliers[type]"]:checked').length!=0) {
             $('.suppliertype-error').text('');
            console.log($('input[name="Suppliers[type]"]:checked').val());
            var supplierType = $('input[name="Suppliers[type]"]:checked').val();
            if(supplierType=='manual'){
                $('input[name="Suppliers[manual][]"]').each(function(){
                    if($(this).val()==''){
                        $('.manual-error').text('Please Fill all participan emails');
                        scrollBox($('.manual-error'));
                        return false;
                    }$('.manual-error').text('');
                });
            }
            if(supplierType=='upload'){
                if(!$('#Suppliers_uploaddoc').val()){
                    $('.upload-error').text('Please attach a file of suppliers');
                    scrollBox($('.upload-error'));
                    return false;
                }$('.upload-error').text('');
            }
            if(supplierType=='pickgroup'){
                if($('input[name="Suppliers[pickgroup][]"]:checked').length==0){
                    $('.pickgroup-error').text('Please select a group');
                    scrollBox($('.pickgroup-error'));
                    return false;
                }$('.pickgroup-error').text('');
            }
            if(supplierType=='pickdatabase'){
                if($('input[name="Suppliers[pfpd][]"]:checked').length==0){
                    $('.pickdatabase-error').text('Please select participants');
                    scrollBox($('.pickdatabase-error'));
                    return false;
                }$('.pickdatabase-error').text('');
            }
            
            
         
         
        }
        else {
              $('.suppliertype-error').text('Please select suppliers');
                    scrollBox($('.suppliertype-error'));
                    return false;
        } $('.suppliertype-error').text('');
        
          if($('input[name="Suppliers[privacy]"]:checked').length==0){
                $('.privacy-error').text('Please select suitable option');
                scrollBox($('.privacy-error'));
                return false;
            }$('.privacy-error').text('');
            
            return true;
    }
</script>
