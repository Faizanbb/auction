<?php
/* @var $this RequisitionController */
/* @var $model Requisition */

?>




<div class="requisition-list">

      <div class="tab-heading">
        <h1>Requests list
        
        <div class="dropdown automobiles">
          <button class="btn   create-requisition" type="button" data-toggle="dropdown">Create Request <div class="caret-block"><span class="caret"></span></div></button>
          <ul class="dropdown-menu create-requisition-menu">
              <li class="create-link"><a href="<?php echo Yii::app()->createurl('requests/rfi',array('event'=>''))?>"  class="">Create RFI</a></li>
            <li class="create-link"><a href="<?php echo Yii::app()->createurl('requests/rfq',array('event'=>''))?>"  class="">Create RFQ</a></li>
            <li class="create-link"><a href="<?php echo Yii::app()->createurl('requests/reverseauction',array('event'=>''))?>"  class="">Reverse Auction</a></li>
          
          </ul>
        </div>
        </h1>
      </div>
      <div class="tab-requisitions">
        <div class="col-lg-12 requisitions-search">
          <div class="row">
              <div class="requisitions-search-leftbar">
                <input name="" id="reqSearchDescription" type="text" class="searchbar" placeholder="Search Requests">
              </div>
              <div class="search-rightbar">
                <button class="btn filter-bnt"> Filter</button>
              </div>
          </div>
        </div>
        
        <div class="col-lg-12 find-requisitions">
          <div class="row">
            <div class="date-box">
              <div class="date-left">Date</div>
              <div class="date-pic">
                <div class='input-group' >
                    <input type='text'  class="date" id="reqDateSearch" />
                </div>
              </div>
            </div>
            <div class="requisition-no">
              <div class="date-left">Request NO</div>
              <div class="requisition-rightbar">
                <input name="" type="text" id="reqIDSearch" class="requisition-input"placeholder="Find Requisition No.">
              </div>
            </div>
            <div class="requisition-status">
              <div class="date-left">Status</div>
              <div class="status-rightbar">
              
     
                <div class="custom-select select-requisitions ">
                  <select id="reqStatusSearch" class="select-requisition requisition-input ">
                    <option value="">Select</option>
                    <option >Draft</option>
                    <option >Published</option>
                    <option >Completed</option>
                  </select>
                </div>
              </div>
            </div>
            <a  href="javascript:void(0)" class="close-filter">Close Filter</a> 
          </div>
        </div>
          
        <div class="requisition-table">
          <table id="dataTableRequsition">
              <thead>
                    <tr>
                      <th>REQUISITION NO.</th>
                      <th>Date</th>
                      <th>Description</th>
                      <th>REQUEST TYPE</th>
                      <th>STATUS</th>
                      <th>Action</th>
                    </tr>
              </thead>
              <tbody>
            <?php 
            foreach($model as $req){

                ?>  
            <tr  data-id="<?php echo $req->ID?>" class='<?php if($req->status==1) echo "published";?>'>
                <td class="id"><?php echo $req->ID?></td>
                <td><?php
    //            echo date('j M, Y g:ia', strtotime($req->createdat));
                echo date('j M, Y H:i', strtotime($req->createdat));
                ?></td>
                <td class="description light-blue-color"><?php echo $req->name?></td>
                <td class="reqtype">
                    
                    <?php 
//                echo '<pre>';print_r($req);exit;
                    
                    echo $req->reqtype;
//                        echo 123;exit;
                    ?>
                </td>
                <td class="status">
                    
                    <?php 
//                echo '<pre>';print_r($req);exit;
                   
                    $status= '';
                    if($req->status==0)
                        $status = 'Draft';
                    else if($req->status==1)
                        $status = 'Published';
                    else if($req->status==2)
                        $status = 'Completed';
                    
                   echo $status;
//                        echo 123;exit;
                    ?>
                </td>
                 <td>
                <div class="dropdown action">
                <a href="javascript:void(0)" class="btn  action-btn" role="button" type="button" data-toggle="dropdown" >Select
                <span class="caret"></span></a>
                    <ul class="dropdown-menu action-menu" role="menu">
                        <?php if($req->status == 0 ){
                            if($req->reqtype=='RFQ'){
                            ?>
                            <li><a href="<?php  echo Yii::app()->createUrl('requests/rfq',array('event'=>$req->ID)); ?>"> View / Edit Request</a></li>
                                <?php }else  if($req->reqtype=='RFI') { ?>
                            
                            <li><a href="<?php  echo Yii::app()->createUrl('requests/rfi',array('event'=>$req->ID)); ?>"> View / Edit Request</a></li><?php } else  if($req->reqtype=='AUCTION') { ?>
                            
                            <li><a href="<?php  echo Yii::app()->createUrl('requests/reverseauction',array('event'=>$req->ID)); ?>"> View / Edit Request</a></li><?php } ?>
                        <!--<li onclick="changeAction(this)" data-status="1"><a href="javascript:void(0)">Approve Requisition</a></li>-->
                        <li class="duplicate-btn-req" data-id="<?php echo $req->ID;?>"><a href="javascript:void(0)">Duplicate Request</a></li>
                        <li class="delete-btn-req" onclick="deleteReq(<?php echo $req->ID;?>)"><a href="javascript:void(0)" class="">Delete Request</a></li>
                        <?php }else{ ?> 
                            <?php if($req->reqtype=="RFI"){ ?>

                            <li><a href="javascript:void(0)">Create RFQ</a></li>
                            <li><a href="javascript:void(0)">Create Reverse Auction</a></li>
                            <li><a href="javascript:void(0)">Create Purchase Order</a></li>
                            <?php } ?>
                        
                            <?php if($req->reqtype=="RFQ"){ ?>

                            <li><a href="javascript:void(0)">Create Reverse Auction</a></li>
                            <li><a href="javascript:void(0)">Create Purchase Order</a></li>
                            <?php } ?>
                        
                            <?php if($req->reqtype=="AUCTION"){ ?>

                            <li><a href="javascript:void(0)">Create Purchase Order</a></li>
                            <?php } ?>
                        
                        <?php } ?>
                    </ul>
                </div> 
              </td>
            <?php } ?>
          
              </tbody>
          </table>
         
        </div>
          
      </div>
 

<div class="modal modal_multi" id="myDeleteModal">
    <!-- Modal content -->
    <div class="modal-content create-popup">
    <span class="close close_multi"  data-dismiss="modal"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/close-btn.png" alt="close"></span>
    <h3>Delete requisition</h3>
    <div class="create-popup-inner text-center">
    <span>Are you sure you want to delete requisition?</span>
    <input type="hidden" id="deleteReqId" />
    <a  href="javascript:void(0)" class="ok-btn" onclick="deleteReqOk()" >Ok</a> <a href="javascript:void(0)" class="cancel-btn" data-dismiss="modal">Cancel</a>
    </div>
    </div>
</div>
</div>
      <div class="modal modal_multi" id='inviteSupplierModal'> 
              <div class="modal-content questionnaire-popup unit-popup"> <span class="close close_multi"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/close-btn.png" data-dismiss="modal" alt="close"></span>  
            <!-- Modal content -->
              <form id="inviteNewSupplier" method="post">
                <h3>Invite Suppliers</h3>
    <div class="create-popup-inner text-center">
      
                <select class="selectpicker" id="inviteUserSelect"  name="supplier[]" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">
                   
                </select> 
        <input type="hidden" name="reqId" id="requestId"/>
      
    </div>


            
        <div class="question-form-btn">
        <input name="" type="submit" class="questionnaire-button add-dext-items" value="Invite Supplier">
        <input name="" type="button"  data-dismiss="modal" class="close-btn" value="Cancel">
        </div>
              </form>    
        </div>
        </div> 
                  

<script type="text/javascript">

     $(document).ready(function() {
         
       // Requestion Table / DataTable
      var table =   $('#dataTableRequsition').DataTable({
            "paging": true
        });
        
//        Requisition Search based on Requisition ID
        $('#reqIDSearch').keyup( function() {
        table.columns( 0 ).search( this.value ).draw();
        } );
//        Requisition Search based on Requisition ID
        $('#reqSearchDescription').keyup( function() {
        table.columns( 2 ).search( this.value ).draw();
        } );
        
//        Requisition Search based on Status
        $('#reqStatusSearch').on('change', function() {
            table.columns( 4 ).search( this.value ).draw();
        } );
        // requestion search bases on date / datepicker 
         $('#reqDateSearch').change(function(){
            getResultonDate();
        });
        
        function getResultonDate(seldate){
             $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        var celldate = data[1]  // use data for the age column
                        var celldate = new Date(celldate);
                        var d = celldate.getDate();
                        var m = (celldate.getMonth()+1);
                        if(d<10)
                            d = "0"+d;
                        if(m<10)
                            m = "0"+m;
                        var cdate = celldate.getFullYear()+"-"+m+"-"+d;
                        if($('#reqDateSearch').val()!='')
                        var seldate = $('#reqDateSearch').val();
                        else
                        var seldate = '';
                    
                    if(seldate==''){
                              return true;
                        }else if ( cdate==seldate)
                        {
                            return true;
                        } 
                        return false;
                    }
                );
               table.draw();
        }
        // make row clickable and open view/edit page
//        $('#dataTableRequsition tbody tr td').on('click',function(e){
//            if($(this).is(':last-child')){
//               return false;
//            }
//            var id = $(this).parent().data('id');
//            
//            window.location.replace('<?php echo Yii::app()->createUrl('requisition/update',array('id'=>''))?>'+id);
//        });
//        
              
  
        
    });

</script>


<script>
    
    function changeAction(el){
        var parenttr = $(el).parent().parent().parent().parent();

        var thid= parenttr.data('id');
        var reqStatus  = $(el).data('status');

        var data = {id:thid,status:reqStatus}
        $.ajax({
            type        : 'POST',
            url         : '<?php echo Yii::app()->createUrl("requisition/updateStatus")?>',
            data        : data,
            success     : function(data) {
                data = JSON.parse(data);
                if(data.status==true){

                    var status = '';
                    if(reqStatus==0)
                        status = 'Pending';
                    else if (reqStatus==1)
                        status = 'Approved';
                    else if (reqStatus==2)
                        status = 'Rejected';

                    var newel = $(parenttr).find('.status')
                    newel.html(status);

                }
            },
            error : function (xhr) {
                alert("Error occured.please try again");
            }
      });
         
    }
    $('.duplicate-btn-req').on('click',function(){
    var id = $(this).data('id');
                           var form=document.createElement("form");form.setAttribute("method","post"),form.setAttribute("action",'<?php echo Yii::app()->createUrl('requests/duplicate')?>');var hiddenField=document.createElement("input");hiddenField.setAttribute("name","id"),hiddenField.setAttribute("value",id),hiddenField.setAttribute("type","hidden"),form.appendChild(hiddenField),document.body.appendChild(form),form.submit();
    });
    function deleteReqOk(){
    var thid = $("#deleteReqId").val();
    var data = {id:thid}
                
               $.ajax({
                    type        : 'POST',
                    url         : '<?php echo Yii::app()->createUrl("requests/DeleteReq")?>',
                    data        : data,
                    success     : function(data) {
                        data = JSON.parse(data);
                        if(data.status==true){
                        //hide row
                            $('#dataTableRequsition tbody tr').each(function(){
                                if($(this).data('id')==thid){
                                    $(this).remove();
                              }
                           });
                           $('#myDeleteModal').modal('hide'); 
                       }
                    },
                    error : function (xhr) {
                        alert("Error occured.please try again");
                    }
              });
        }
</script>
<script>
     
     function deleteReq(id){
         
            console.log(123);
            $("#deleteReqId").val(id);
            console.log(id);
            $('#myDeleteModal').modal('show'); 
            $('.dropdown').removeClass('open'); 
        };
        
$(document).ready(function(){

       
 
 $('.close-filter').click(function(){
     $(".find-requisitions").fadeToggle();
 })
});
</script>
<script>
//    Request List
$(document).click(function(e){
        // if the target of the click isn't the container nor a descendant of the container
        var container = $('.requisition-table table');
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        $('.request-stats').remove();
    }
  
});

$('.requisition-table table tr').on("click",function(event){
   
    if($(this).hasClass('published')){
        var requestId = $(this).data('id');
        var el = $(this);
        $.ajax({
           type: 'GET',
           url : "<?php echo Yii::app()->createUrl('requests/requestbidstatus',array('id'=>''));?>"+requestId,
           success:function(data){
               var data = JSON.parse(data);
               $('.request-stats').remove();
               $(data['listhtml']).insertAfter($(el));
               console.log(data['selecthtml']);
               $('#inviteUserSelect').html(data['selecthtml']);
               $('#requestId').val(data['requestId']);
              
               
           },
           error: function(){
           alert('Some thing went wrong');
            },    
            beforeSend: function() {
                // setting a timeout
                $('.loading-gif').show();
            },
            complete: function() {
                $('.loading-gif').hide();
            },
        });
    }
});
$(document).on('click','.inviteSupplierModal',function(){

    $('#inviteSupplierModal').modal('show');
});
    
</script>

<!--_requestListBidStats-->
<script>
            $(document).ready(function(){
               $(document).on("click",".resendemail-btn",function(){
                    $('.emailsent').hide();
                   var reqsup = $(this).data('id');
                   var dataa = {reqSupplier:reqsup};
                   console.log(reqsup);
                  $.ajax({
                      type:'POST',
                      data : dataa,
                      url: "<?php echo Yii::app()->createUrl('requests/resendsupplierEmail');?>",
                      success: function(data){
                        $('.emailsent').show();
                        $('.emailsent').text('Email Sent !!');
                        
                      },
                      error: function(){
                       alert("Something went wrong");   
                        },
                        beforeSend: function() {
                            // setting a timeout
                            $('.loading-gif').show();
                        },
                        complete: function() {
                            $('.loading-gif').hide();
                        },
                  })
                  
               }); 
               
               $(document).on('submit','#inviteNewSupplier',function(e){
                e.preventDefault();
                 $.ajax({
                      type:'POST',
                      data : new FormData(this),
                       processData: false,
                        contentType: false,
                      url: "<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'inviteNewSupplier','id'=>''));?>",
                      success: function(data){
                        $('.emailsent').show();
                        $('.emailsent').text('Email Sent !!');
                        $('#requestId').val();
                      },
                      error: function(){
                       alert("Something went wrong");   
                        },
                    beforeSend: function() {
                        // setting a timeout
                        $('.loading-gif').show();
                    },
                    complete: function() {
                        $('.loading-gif').hide();
                    },
                  })
                });
            });
            </script>
        