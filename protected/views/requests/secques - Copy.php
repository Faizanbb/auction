<?php
/* @var $this RequestsController */
/* @var $model Requests */
/* @var $form CActiveForm */


?>

<div class='row'>
    <div class='col-sm-12 ' id='questSectionContent'>
        
    </div>
</div>



<button class="btn btn-primary" id='getRequestDocument'>Fetch docs</button>
<button class="btn btn-primary" id='questSection'>Fetch Questionnaire</button>

<script>
$(document).ready(function(){
   
   $('#questSection').on('click',function(){
       $.ajax({
           type     :'GET',
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'fetchSecQues','id'=>$requestId)); ?>',
           success  :function(data){
               
                   $('#questSectionContent').html(data);
         
           },
           error    : function(){
           alert('some thing went wrong');
            }
       })
   });
   $('#getRequestDocument').on('click',function(){
       $.ajax({
           type     :'GET',
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'requestDocument','id'=>$requestId)); ?>',
           success  :function(data){
               
                   $('#questSectionContent').html(data);
         
           },
           error    : function(){
           alert('some thing went wrong');
            }
       })
   });
   $(document).on('click','#addSection',function(){

        $.ajax({
            type     : 'GET',
            url      : '<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'newSection','id'=>$requestId)); ?>',
            success  : function(data){
                 $('#addSectionContent').html(data);
            },
            error    : function(){
                alert('some thing went wrong');
            }
        })
   });
   $(document).on('click','#savenewSection',function(){
        var formData = $("#section-form").serialize();
       $.ajax({
           type     :'POST',
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'savenewSection','id'=>$requestId)); ?>',
           data     : formData,
           success  :function(data){
                $('#questSection').trigger('click');
           },
           error    : function(){
                alert('some thing went wrong');
            }
       })
   });
   $(document).on('submit','#document-form',function(e){
        $.ajax({
            type     :'POST',
            url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'saverequestDocument','id'=>$requestId)); ?>',
            data     : new FormData( this ),
            processData: false,
            contentType: false,
            success  :function(data){
                $('#getRequestDocument').trigger("click");
            },
            error    : function(){
                 alert('some thing went wrong');
             }
        })
         e.preventDefault();
   });
   $(document).on('click','#savenewQuestion',function(){
        var formData = $("#question-form").serialize();
       $.ajax({
           type     :'POST',
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'savenewQuestion','id'=>$requestId)); ?>',
           data     : formData,
           success  :function(data){
                $('#questSection').trigger('click');
           },
           error    : function(){
           alert('some thing went wrong');
            }
       })
   });
   $(document).on('click','.addQuestion',function(){
   
        var qsecid = $(this).data('secid');
        var ndata = {'secId':qsecid};
        $.ajax({
           type     :'GET',
           data     : ndata,
           url      :'<?php echo Yii::app()->createUrl('requests/questionnaire',array('action'=>'addQuestion','id'=>$requestId)); ?>',
           success  :function(data){
                   
                $('.section').each(function(){
                    if($(this).data('secid')==qsecid)
                    $(this).find('.addQuestionContent').html(data);;
                });
           },
           error    : function(){
           alert('some thing went wrong');
            }
        });
   });
   
});    
</script>


