        <!-- Modal content -->
        <div class="modal-content questionnaire-popup unit-popup"> <span class="close close_multi"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/close-btn.png" data-dismiss="modal" alt="close"></span>
        <h3>ADD list ITEMS</h3>
        <div class="list-items">
        <ul id="myTabs"  role="tablist" data-tabs="tabs">
    <li class="active grp-cat"><a href="#Category" data-toggle="tab">Group by Category</a></li>
    <li class=" grp-sup"><a href="#Suppliers" data-toggle="tab">Group by Suppliers</a></li>
  </ul>
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="Category">
     <div class="group-category">
      <div class="group-category-search">
      
      <p>Search by item name/number/category:</p>
      <input name="" type="text" class="search-category searchByCategory cat-srch">
      
      </div>
         <?php if(isset($categoryItems)){?>
         <?php foreach($categoryItems as $catitem){?>
            <div class="cleaning-equipment category-srch">
                <h4><strong>Category</strong>: <span class="catey"><?php echo $catitem->name;?></span> <a href="javascript:void(0)">Hide</a></h4>
             <div class="cleaning-equipment-list">
             <ul>
         <?php foreach($catitem->categoriesItems as $item){?>
                  <li>
                      <div class="pacific-checkbox">
                          <div class="custom-checkbox">
                          <input id="cbd<?php echo $item->ID.$item->item->ID; ?>" class="category-ckbx" name="dataextc[]" value="<?php echo $item->item->ID; ?>"type="checkbox"><label for="cbd<?php echo $item->ID.$item->item->ID; ?>"></label>
                          </div>
                      </div>
                      <span class="numy nodisplay"> <?php echo $item->item->lotnum; ?></span>
                      <span class="name"> <?php echo $item->item->name; ?> </span>
                      <?php if(isset($item->item->description)&& !empty($item->item->description)){ ?>
                      
                      <a href="javascript:void(0)" class="show-description">Show Description</a>
                      <div class="description">
                      <?php echo $item->item->description; ?>

                      </div>
                      <?php } ?>
                  </li>
         <?php } ?>
             </ul>
             </div>
            </div>
      
         <?php } } ?>
     </div>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="Suppliers">
    <div class="group-category">
      <div class="group-category-search">
      <p>Search by item name/number/supplier:</p>
      <input name="" type="text" class="search-category searchByCategory sup-srch">
      </div>
    <?php if(isset($supplierItems)&& !empty($supplierItems)){?>
         <?php foreach($supplierItems as $supitem){     if(isset($supitem->user->supplierItems)&&!empty($supitem->user->supplierItems)){            ?>
         
      <div class="cleaning-equipment supplier-search">
          <h4><strong>Supplier</strong>: <span class="catey"> <?php  echo $supitem->user->name; ?></span> <a href="javascript:void(0)">Hide</a></h4>
       <div class="cleaning-equipment-list">
       <ul>
           <?php  foreach($supitem->user->supplierItems as $item){ ?>
                      <li>
                      <div class="pacific-checkbox">
                          <div class="custom-checkbox">
                          <input id="dbd<?php echo $item->ID.$item->item->ID; ?>" class="supplier-ckbx"  name="dataexts[]" value="<?php echo $item->item->ID; ?>"type="checkbox"><label for="dbd<?php echo $item->ID.$item->item->ID; ?>"></label>
                          </div>
                      </div>
                       <span class="numy nodisplay"> <?php echo $item->item->lotnum; ?></span>
                      <span class="name"> <?php echo $item->item->name; ?> </span>
                     <?php if(isset($item->item->description)&& !empty($item->item->description)){ ?>
                          <a href="javascript:void(0)" class="show-description">Show Description</a>
                      <div class="description">
                      <?php echo $item->item->description; ?>

                      </div>
                      <?php } ?>
                  </li>
           <?php } ?>
       
       </ul>
       </div>
      </div>
        
         <?php } } }else{ echo  '<div class="cleaning-equipment"> <h4>No Data</h4></div>' ;} ?>
     </div>
    
    </div>
  </div>
        </div>
        <div class="question-form-btn">
        <input name="" type="button" class="questionnaire-button add-dext-items" value="Add Items">
        <input name="" type="button"  data-dismiss="modal" class="close-btn" value="Cancel">
        </div>
        </div>