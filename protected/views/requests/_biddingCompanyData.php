<?php $supplier =  $model->requestSuppliers[0] ;
$bidTime = $model->requestItems[0]->requestItemPrices[0]->createdat;;
//echo $bidTime;exit;
$bidTotal = 0;
foreach($model->requestItems as $reqItem){
    $bidTotal+=$reqItem->quantity*$reqItem->requestItemPrices[0]->price;
}

?>

<div class="modal-content create-popup bid-popup-outer"> <span  onclick="$('#companyProfileModal').modal('hide')"  class="close close_multi" data-index="1"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/close-btn.png" alt="close"></span>
                  <h3><?php  echo $supplier->supplier->acc->company ?></h3>
                  <div class="bid-popup">
                    <div class="pdf-buttons-outer"> <a href="#" class="load-template-btn">Download Excel</a> <a href="#" class="load-template-btn">Download PDF</a> </div>
                    <div class="company-detail"> <a href="#" class="company-logo"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/logo-company.png" alt="logo-company"></a>
                      <div class="company-detail-top">
                        <ul>
                          <li><span>Company</span> <?php  echo $supplier->supplier->acc->company ?></li>
                          <li><span>Contact</span> <strong><?php  echo $supplier->supplier->name ?> <br>
                            <?php  echo '+'.$supplier->supplier->carriercode.' '.$supplier->supplier->mobile ?></strong></li>
                          <li><span>Bid made</span> <?php  echo date('M ,d Y H:i',strtotime($bidTime)) ?></li>
                          <li><span>Grand total</span> <?php echo $model->currency.' '.$bidTotal;?></li>
                        </ul>
                      </div>
                      <div class="supplier-comments"> <strong>Supplier comments</strong>
                        <p>We have all the proper certificates that you can find from attached documents, as well as company overview.</p>
                        <br>
                        <strong>Best regards,</strong>
                        <p>Morgan Cooper</p>
                        <p>Morgan Supply Star Ltd.</p>
                      </div>
                    </div>
                    <div class="supplier-tabs">
                      <div class="tab-blocks tbblock" id = "tab-block">
                        <ul class="tab-mnu">
                          <li class = "active" >List of Items</li>
                          <li>Questionnaire</li>
                        </ul>
                        <div class="tab-cont">
                          <div class="tab-pane">
                            <table class="adhesive-table table-b-lrb">
                              <thead class="fixedHeader tr-bold">
                                <tr>
                                  <td>Item Name</td>
                                  <td>Item Number</td>
                                  <td>Spec. Files</td>
                                  <td>Unit</td>
                                  <td>Qty</td>
                                  <td>Price</td>
                                </tr>
                              </thead>
                              <tbody class="scrollContent">
                                  <?php foreach($model->requestItems as $item){ ?>
                                <tr>
                                  <td><span class="caret"></span> <strong><?php echo $item->name;?></strong></td>
                                  <td><?php echo $item->item_number?></td>
                                  <td><a href="javascript:void(0)" class="view-btn viewAttachFile">View</a>
                                  
                                      <div class="modal modal_multi vattfile" id="viewAttachFile" style="z-index:1000;" > 
                                    <div class="modal-content questionnaire-popup"> 
                                        <span class="close close_multi" onclick="$(this).parent().parent().modal('hide');"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/close-btn.png" alt="close"></span>
                                          <h3>View Files</h3>

                                          <div class="questionnaire-popup-inner">
                                            <div class="view-files">
                                                <div class="drag-documents-list">
                                                 
                                                    <ul>
                                                       <?php 
                        //                               echo '<pre>';print_r($item->reqItemDocs);exit;
                                                       if(isset($item->reqItemDocs)&&!empty($item->reqItemDocs)){ foreach($item->reqItemDocs as $doc){ ?>
                                                       <li>
                                                           <div class="drag-documents-leftbar"><strong><?php echo $doc->doc->origname;?></strong><span>File size : <?php echo $doc->doc->size/1000; ?> KB</span></div>
                                                           <div class="drag-documents-right"><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/pin.png" alt="pin"></a><a href="javascript:void(0)" class="delete-af-doc" data-idid="<?php echo $doc->ID?>" >Remove</a></div>
                                                        </li>
                                                       <?php } } ?>
                                                   </ul>
                                                </div>
                                           </div>
                                            <div class="question-form-btn">

                                            <!--<input  type="button" data-dismiss="modal" class="questionnaire-button" value="Save">-->
                                              <input  type="button" class="close-btn" onclick="$('.vattfile').modal('hide');" value="Cancel">
                                            </div>
                                          </div>

                                    </div>
                                    </div>
                                  </td>
                                  <td>Pcs</td>
                                  <td>500</td>
                                  <td>AED 300</td>
                                </tr>
                                <?php     } ?>

                              </tbody>
                            </table>
                            <div> </div>
                          </div>
                          <div class="tab-pane">
                            <div class="questionnaire-general">
                            <?php $sectionQuestions = $model->sectionQuestions; $mcount=1; foreach($sectionQuestions as $secQt){ ?>
                            <?php if($secQt->parentid==0){ $scount=1;?>    
                              <div class="questionnaire-top">
                                <h5><?php echo $mcount.'. '.$secQt->name?></h5>
                                <?php foreach($sectionQuestions as $subQt){ ?> 
                                <?php if($subQt->parentid==$secQt->ID){?>
                                <div class="questionnaire-inner"> <span><?php echo $mcount.'.'.$scount.' '.$subQt->name?></span>
                                  <p><?php echo $subQt->description?></p>
                                  <?php if(isset($subQt->questionsAnswers1)){ foreach($subQt->questionsAnswers1 as $answers) {?>
                                  <?php if($answers->uid==$supplierId){?>
                                  <strong><?php echo $answers->ques_ans ?></strong> 
                                  <?php } } } ?>
                                
                                    <div class="questionnaire-popup-inner">
                                    <div class="view-files">
                                        <div class="drag-documents-list">
                                            <ul>
                                               <?php 
//                                               echo '<pre>';print_r($answers->questanswersDocuments);exit;
                                               if(isset($answers->questanswersDocuments )&&!empty($answers->questanswersDocuments )){ foreach($answers->questanswersDocuments as $doc){ ?>
                                               <li>
                                                   <div class="drag-documents-leftbar"><strong><?php echo $doc->doc->origname;?></strong><span>File size : <?php echo $doc->doc->size/1000; ?> KB</span></div>
                                                   <div class="drag-documents-right"><a href="javascript:void(0)"><img src="<?php echo Yii::app()->theme->baseUrl;?>/dist/images/pin.png" alt="pin"></a><a download href="<?php echo $doc->doc->url.'/'.$doc->doc->name; ?>"  >View</a></div>
                                                </li>
                                               <?php } } ?>
                                           </ul>
                                        </div>
                                   </div>
                                  </div>
                                  </div>
                                <?php $scount++;} } ?>
                              </div>
                                
                            <?php $mcount++;} ?>
                            <?php  } ?>
                                
                            </div>
                          </div>
                          <input name="" type="button" onclick="$('#companyProfileModal').modal('hide')" class="close-btn" value="Close">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
<script>
    $(document).ready(function(){
  $('.tbblock').each(function(){
      var tabWrapper = $(this),
      tabMnu = tabWrapper.find('.tab-mnu  li'),
      tabContent = tabWrapper.find('.tab-cont > .tab-pane');
    


  tabContent.not(':first-child').hide();
  
  tabMnu.each(function(i){
    $(this).attr('data-tab','tab'+i);
  });
  tabContent.each(function(i){
    $(this).attr('data-tab','tab'+i);
  });
  
  tabMnu.click(function(){
    var tabData = $(this).data('tab');
    tabWrapper.find(tabContent).hide();
    tabWrapper.find(tabContent).filter('[data-tab='+tabData+']').show(); 
  });
  
  $('.tab-mnu > li').click(function(){
    var before = $('.tab-mnu li.active');
    before.removeClass('active');
    $(this).addClass('active');
   });
    });
});
</script>