<?php
/* @var $this RequisitionController */
/* @var $model Requisition */

$this->breadcrumbs=array(
	'Requisitions'=>array('index'),
	'Manage',
);


?>






    <a class="a-tab-block " href="<?php echo Yii::app()->createUrl('requisition/list');?>"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/requistions-icon.png" alt="reports"> Requisitions <br>
      List</a>
    

<div class="modal modal_multi" id="myDeleteModal">
    <!-- Modal content -->
    <div class="modal-content create-popup">
    <span class="close close_multi"  data-dismiss="modal"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/close-btn.png" alt="close"></span>
    <h3>Delete requisition</h3>
    <div class="create-popup-inner text-center">
    <span>Are you sure you want to delete requisition?</span>
    <input type="hidden" id="deleteReqId" />
    <a  href="javascript:void(0)" class="ok-btn" onclick="deleteReqOk()" >Ok</a> <a href="#" class="cancel-btn" data-dismiss="modal">Cancel</a>
    </div>
    </div>
</div>

      
      <a href="<?php echo Yii::app()->createUrl('requisition/create');?>" class="a-tab-block ">
          <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/create-icon.png" alt="reports" /> Create <br>
      Requisition</a>  
      <div class="tab-block active ">
          <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/approve-icon.png" alt="reports" /> Approve <br>
      Requisition</div>  
      <div class="tab-open">
      <div class="tab-heading">
        <h1>REQUISITIONs list
        
        <div class="dropdown automobiles">
          <button class="btn   create-requisition" type="button" data-toggle="dropdown">Create Requisition <div class="caret-block"><span class="caret"></span></div></button>
          <ul class="dropdown-menu create-requisition-menu">
            <li class="create-link"><a href="#"  class="myBtn_multi">Create Web Form Link</a></li>
            <div class="modal modal_multi" id="webModel">

        <!-- Modal content -->
        <div class="modal-content create-popup" >
            <span  data-dismiss="modal" class="close close_multi"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/close-btn.png" alt="close"></span>
            <h3>Create Requisition web Form Link</h3>
            <div class="create-popup-inner">
            <p>Share this link to create a purchase requisition</p>
            <form action="" method="get">
            <input name="" class="form-control text-field" value="<?php $accountId= Yii::app()->user->accountId; echo Yii::app()->createAbsoluteUrl('Requisition/extlink',array('id'=>$accountId),'http')?>"  type="text" placeholder="https://www.businessbid.com/db/requisition/create/token_93af148c68391e9903ae0ed2e/languge_en_GB">
            <input name="" type="button" class="copy-link-btn" value="Copy link">
            </form>
            </div>
        </div>

    </div>
          </ul>
        </div>
        </h1>
      </div>
      <div class="tab-requisitions">

          
        <div class="requisition-table">
          <table id="dataTableRequsition">
              <thead>
            <tr>
              <th>REQUISITION NO.</th>
              <th>Date</th>
              <th>Description</th>
              <th>APPROVER NAME</th>
              <th>Action</th>
            </tr>
              </thead>
              <tbody>
            <?php 
            foreach($model as $req){

            ?>  
            <tr  data-id="<?php echo $req->ID?>">
                <td class="id"><?php echo $req->ID?></td>
                <td><?php
    //            echo date('j M, Y g:ia', strtotime($req->createdat));
                echo date('j M, Y H:i', strtotime($req->createdat));
                ?></td>
                <td><?php echo $req->description?></td>
                <td >
                  Muhammad Faizan
                </td>
                <?php if($req->status=='APPROVED'){ ?>
                <td>Approved</td>
                <?php }else if($req->status=='PENDING'){ ?>
                <td class="status">
                    <a href="javascript:void(0)" onclick="popUp(<?php echo $req->ID?>,1)" class="approve-btn myBtn_multi">Approve <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/green-checkbtn.png" alt=""></a> 
                

                <a href="javascript:void(0)" onclick="popUp(<?php echo $req->ID?>,3)"  class="approve-btn myBtn_multi">Reject <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/reject-btn.png" alt=""></a>
                
                </td>
                <?php } ?>
            <?php } ?>
          
              <tbody>
          </table>
          <div class="pagination">
           <ul>
           <li><a href="#">Preview</a></li>
           <li><a href="#">1</a></li>
           <li><a href="#">2</a></li>
           <li><a href="#">3</a></li>
           <li><a href="#">Next</a></li>
           </ul>
          </div>
        </div>
      </div>
      </div>
<div class="modal modal_multi" id="approveModal">
    <!-- Modal content -->
    <div class="modal-content create-popup">
    <span class="close close_multi" data-dismiss="modal"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/close-btn.png" alt="close"></span>
    <h3>Approve requisition r</h3>
    <div class="create-popup-inner text-center">
    <span>Are you sure you want to Approve requisition?</span>
    <a href="javascript:void(0)" onclick="changeStatus()" class="ok-btn">Ok</a> <a href="#" class="cancel-btn" data-dismiss="modal">Cancel</a>
    </div>
    </div>
</div>

<div class="modal modal_multi" id="rejectModal">
    <div class="modal-content create-popup">
    <span class="close close_multi" data-dismiss="modal"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/close-btn.png" alt="close"></span>
    <h3>Delete requisition</h3>
    <div class="create-popup-inner text-center">
    <span>Are you sure you want to delete requisition?</span>
    <a href="javascript:void(0)" onclick="changeStatus()" class="ok-btn">Ok</a> <a href="#" class="cancel-btn" data-dismiss="modal">Cancel</a>
    </div>
    </div>
</div>
    <input type="hidden" id="modalStatus" /> 
    <input type="hidden" id="modalId" /> 

<script type="text/javascript">
    function popUp(id,status){
        if(status==1)
        $("#approveModal").modal("show");
        else if(status==3)
        $("#rejectModal").modal("show");
    console.log(id+"-"+status);
        $('#modalId').val(id);
        $('#modalStatus').val(status);
    }
    

    function changeStatus(){
        
       var thid =  $('#modalId').val();
       var val =  $('#modalStatus').val();
        if(val==1){
                var data = {id:thid,status:val}
                $.ajax({
                    type        : 'POST',
                    url         : '<?php echo Yii::app()->createUrl("requisition/updateStatus")?>',
                    data        : data,
                    success     : function(data) {
                        data = JSON.parse(data);
                        if(data.status==true){
                            $('#dataTableRequsition tbody tr').each(function(){
                               var curid =  $(this).data("id");
                               if(curid==thid){
                               $(this).find(".status").text("Approved");
                                }
                            });
                        $("#approveModal").modal("hide");
                            }
                    },
                    error : function (xhr) {
                        alert("Error occured.please try again");
                    }
              });
        }else if(val==3){
        
              var data = {id:thid}
               $.ajax({
                    type        : 'POST',
                    url         : '<?php echo Yii::app()->createUrl("requisition/DeleteReq")?>',
                    data        : data,
                    success     : function(data) {
                        data = JSON.parse(data);
                        if(data.status==true){
                             $("#rejectModal").modal("hide");
                             $('#dataTableRequsition tbody tr').each(function(){
                               var curid =  $(this).data("id");
                               if(curid==thid){
                               $(this).remove();
                                }
                            });
                        }
                    },
                    error : function (xhr) {
                        alert("Error occured.please try again");
                    }
              });
                
                
                     
        }else{
            console.log('operation');
        }
    }
</script>
<script>
     $(document).ready(function() {
         
        
       // Requestion Table / DataTable
      var table =   $('#dataTableRequsition').DataTable({
            "paging": false
        });
        
//        Requisition Search based on Requisition ID
        $('#reqIDSearch').keyup( function() {
        table.columns( 0 ).search( this.value ).draw();
        } );
//        Requisition Search based on Requisition ID
        $('#reqSearchDescription').keyup( function() {
        table.columns( 2 ).search( this.value ).draw();
        } );
        
//        Requisition Search based on Status
        $('#reqStatusSearch').on('change', function() {
            table.columns( 3 ).search( this.value ).draw();
        } );
        // requestion search bases on date / datepicker 
         $('#reqDateSearch').change(function(){
            getResultonDate();
        });
        
        function getResultonDate(seldate){
             $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        var celldate = data[1]  // use data for the age column
                        var celldate = new Date(celldate);
                        var d = celldate.getDate();
                        var m = (celldate.getMonth()+1);
                        if(d<10)
                            d = "0"+d;
                        if(m<10)
                            m = "0"+m;
                        var cdate = celldate.getFullYear()+"-"+m+"-"+d;
                        if($('#reqDateSearch').val()!='')
                        var seldate = $('#reqDateSearch').val();
                        else
                        var seldate = '';
                    
                    if(seldate==''){
                              return true;
                        }else if ( cdate==seldate)
                        {
                            return true;
                        } 
                        return false;
                    }
                );
               table.draw();
        }
        // make row clickable and open view/edit page
        $('#dataTableRequsition tbody tr td').on('click',function(e){
            if($(this).is(':last-child')){
               return false;
            }
            var id = $(this).parent().data('id');
            
            window.location.replace('<?php echo Yii::app()->createUrl('requisition/update',array('id'=>''))?>'+id);
        });
        
              
  
        
    });

</script>


<script>
    
    function changeAction(el){
            var parenttr = $(el).parent().parent().parent().parent();
            
            var thid= parenttr.data('id');
            var reqStatus  = $(el).data('status');
        if(reqStatus==0||reqStatus==1||reqStatus==2){
      
                var data = {id:thid,status:reqStatus}
                $.ajax({
                    type        : 'POST',
                    url         : '<?php echo Yii::app()->createUrl("requisition/updateStatus")?>',
                    data        : data,
                    success     : function(data) {
                        data = JSON.parse(data);
                        if(data.status==true){
                            
                            var status = '';
                            if(reqStatus==0)
                                status = 'Pending';
                            else if (reqStatus==1)
                                status = 'Approved';
                            else if (reqStatus==2)
                                status = 'Rejected';

                            var newel = $(parenttr).find('.status')
                            newel.html(status);

                        }
                    },
                    error : function (xhr) {
                        alert("Error occured.please try again");
                    }
              });
                
                
                
            
        }else if($(el).val()==3){
                      
        }else{
            console.log('operation');
        }
        
        
    }
    function deleteReqOk(){
    var thid = $("#deleteReqId").val();
    var data = {id:thid}
                
               $.ajax({
                    type        : 'POST',
                    url         : '<?php echo Yii::app()->createUrl("requisition/DeleteReq")?>',
                    data        : data,
                    success     : function(data) {
                        data = JSON.parse(data);
                        if(data.status==true){
                        //hide row
                            $('#dataTableRequsition tbody tr').each(function(){
                                if($(this).data('id')==thid){
                                    $(this).remove();
                              }
                           });
                           $('#myDeleteModal').modal('hide'); 
                       }
                    },
                    error : function (xhr) {
                        alert("Error occured.please try again");
                    }
              });
        }
</script>
<script>
$(document).ready(function(){
        $('.create-link').click(function(){
            $('#webModel').modal('show'); 
         });
        $('.delete-btn-req').click(function(){
            
               
          var id = $(this).data('id');
          $("#deleteReqId").val(id);
            $('#myDeleteModal').modal('show'); 
        $('.dropdown').removeClass('open'); 
        });
 
 $('.close-filter').click(function(){
     $(".find-requisitions").fadeToggle();
 })
});
</script>