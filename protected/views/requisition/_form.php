<?php
/* @var $this RequisitionController */
/* @var $model Requisition */
/* @var $form CActiveForm */
?>

<div class="form" style="margin-left: 50px;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'requisition-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
        'clientOptions'=>array(
        'validateOnSubmit'=>true,
        ),
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),

)); ?>
    <div class="row">
        <div class="col-sm-8">
            

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
        <div class="row">
        <div class="form-group col-sm-12">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>250,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="form-group col-sm-12">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="form-group col-sm-6">
		<?php echo $form->labelEx($model,'delivery_date'); ?>
		<?php echo $form->textField($model,'delivery_date',array('class'=>'form-control datepicker')); ?>
		<?php echo $form->error($model,'delivery_date'); ?>
	</div>

	<div class="form-group col-sm-6">
		<?php echo $form->labelEx($model,'quantity'); ?>
		<?php echo $form->textField($model,'quantity',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'quantity'); ?>
	</div>

	<div class="form-group col-sm-6">
		<?php echo $form->labelEx($model,'capex_opex'); ?>
		<?php echo $form->dropDownList($model,'capex_opex',array('capex'=>'capex','opex'=>'opex','not-sure'=>'Not Sure'),array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'capex_opex'); ?>
	</div>

	<div class="form-group col-sm-6 ">
		<?php echo $form->labelEx($model,'budgeted'); ?>
		<?php echo $form->dropDownList($model,'budgeted',array(0=>'Please select one',1=>'Budgeted',2=>'Non-Budgeted',3=>'Not Sure'),array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'budgeted'); ?>
	</div>

	<div class="form-group col-sm-2">
		<?php echo $form->labelEx($model,'budget'); ?>
		<?php echo $form->textField($model,'budget',array('size'=>15,'maxlength'=>15,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'budget'); ?>
	</div>

	<div class="form-group col-sm-4">
		<?php echo $form->labelEx($model,'budget_currency'); ?>
		<?php echo $form->dropDownList($model,'budget_currency',array('AED'=>'AED - United Arab Emirates','Pound'=>'Pound - United Kingdom'),array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'budget_currency'); ?>
	</div>

	<div class="form-group col-sm-6">
		<?php echo $form->labelEx($model,'cost_center'); ?>
		<?php echo $form->textField($model,'cost_center',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'cost_center'); ?>
	</div>
            
	<div class="form-group col-sm-12">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textArea($model,'address',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>
            <?php if(Yii::app()->user->isGuest) { ?>
            
            <div class="form-group">
                <h1>Contact Form</h1>
            </div>
        <?php } ?>
            <div class="form-group col-sm-12">
                <label>Approver Name</label>
                <select class="form-control">
                    <option>Select one</option>
                </select>
            </div>
            <div class="form-group col-sm-12">
                <?php
                    $this->widget('CMultiFileUpload', array(
                      'model'=>$model,
                      'attribute'=>'additional_documents',
                      'accept'=>'jpg|png|PNG',
                      'remove'=> '<i class="fa fa-trash-o "></i>',
                      'duplicate'=>'Already Selected',
                      'denied'=>'File is not allowed',
                      'max'=>10, // max 10 files

                ));
                if(isset($reqDocs)){
//                    print_r($reqDocs);exit;
                    foreach($reqDocs as $d){
                        echo '<div class="prev-doc-file">'
                        . '<img height="30" src="'.Yii::app()->baseUrl.'/'.$d->d->url.'/'.$d->d->name.'" />'
                                . '<span>'.$d->d->origname.'</span>'
                                . '<div class="btn btn-danger delete-doc" data-id="'.$d->ID.'" data-did="'.$d->d->ID.'">delete</div></div>';
                    }
                }
                
                ?>
        </div>
        
        <div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
	</div>

        </div>
        </div>
    </div>
<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
$(document).ready(function(){
    $('.delete-doc').on('click',function(){
    var status = confirm("Are you sure you want to delete it.");   
    var curElement = $(this);
    
    if( status){
       var id = $(this).data('id'); 
       var did = $(this).data('did'); 
       var extdata = {'reqdid':id,'docid':did};
       $.ajax({
          type  :'POST',
          url   :'<?php echo Yii::app()->createUrl("requisition/ajax/action/delete/type/doc")?>',
          data  :extdata,
          success:function(data){
              var data = JSON.parse(data);
              console.log(data);
              if(data.status)
                $(curElement).parent().remove();
         },
          Error:function(data){
          alert("some thing went wrong please contact admin");
            }
           
       });
    
    }
    });
    
})  ;  
    
</script>