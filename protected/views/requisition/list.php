<?php
/* @var $this RequisitionController */
/* @var $model Requisition */

$this->breadcrumbs=array(
	'Requisitions'=>array('index'),
	'Manage',
);


?>
    <div class="tab-block active requisition-list"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/requistions-icon.png" alt="reports"> Requisitions <br>
      List</div>
    <div  class="tab-open requisition-list-content">   
        <div class="tab-heading">
    <h1>REQUISITIONs list       <select class="selectpicker create-requisition">
    <option>Create Requisition</option>
    <option>Create Requisition 2</option>
    <option>Create Requisition 3</option>
    </select></h1>
    </div>
    
    <div class="tab-requisitions">
     <div class="col-lg-12 requisitions-search">
      <div class="row">
          <div class="requisitions-search-leftbar">
           <input name="" type="text" id="reqSearchDescription" class="searchbar" placeholder="Search Purchase Requisitions">
          </div>
          
          <div class="search-rightbar"><button class="btn filter-bnt"> Filter</button></div>
      </div>
     </div>
<div class="col-lg-12 find-requisitions">
<div class="row">
<div class="date">
<div class="date-left">Date</div><div class="date-pic"><input id="reqDateSearch" class="datepicker" name="" type="text"></div>
</div>

<div class="requisition-no">
<div class="date-left">REQUISITION NO</div>
<div class="requisition-rightbar">
<input name="" type="text"  id="reqIDSearch" class="requisition-input"placeholder="Find Requisition No.">
</div>
</div>

<div class="requisition-status">
<div class="date-left">Status</div>
<div class="status-rightbar">
<div class="custom-select">
<select id="reqStatusSearch">
<option>Select</option>
<option>Approved</option>
<option>Pending</option>
<option>Rejected</option>
</select> </div><input name="" type="submit" value="SEARCH" class="search-btn">
</div>
</div>
<a  href="#" class="close-filter">Close Filter</a>
</div>
</div>
     <div class="requisition-table">
    <table id="dataTableRequsition" >
        <thead>
            <tr>
                <th>REQUISITION NO.</th>
                <th>Date</th>
                <th>Description</th>
                <th>STATUS</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach($model as $req){

            ?>  
            <tr>
                <td class="id" data-id="<?php echo $req->ID?>"><?php echo $req->ID?></td>
                <td><?php
    //            echo date('j M, Y g:ia', strtotime($req->createdat));
                echo date('j M, Y H:i', strtotime($req->createdat));
                ?></td>
                <td><?php echo $req->description?></td>
                <td class="status">
                    <?php 
                    if($req->status==0)
                        $status = 'Pending';
                    else if($req->status==1)
                        $status = 'Approved';
                    else if($req->status==2)
                        $status = 'Rejected';
                    echo $status;
                    ?>
                </td>
                <td>
                    <div class="select_mate" data-mate-select="active" >
                        <select name="" onchange="" onclick="return false;" onchange="changeAction(this,);" id="">
                        <option value=""  >Select </option>
                        <option value="1">View / Edit Requisition</option>
                        <option value="2" >Approve Requisition</option>
                        <option value="3">Delete Requisition</option>
                        <option value="4">Create RFQ</option>
                        <option value="5">Create RFI</option>
                        <option value="6">Create Reverse Auction</option>
                        <option value="7">Create Purchase Order</option>
                        </select>
                        <p class="selecionado_opcion"  onclick="open_select(this)" ></p><span onclick="open_select(this)" class="icon_select_mate" ><svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z"/>
                        <path d="M0-.75h24v24H0z" fill="none"/>
                        </svg></span>
                        <div class="cont_list_select_mate">
                        <ul class="cont_select_int">  </ul> 
                        </div>
                    </div>
                </td>
            </tr>

            <?php
                }
            ?>
            </tbody>
    </table>

     </div>
     
    </div>


    </div>
      
      <a href="<?php echo Yii::app()->createUrl('requisition/create');?>" class="a-tab-block ">
          <img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/create-icon.png" alt="reports" /> Create <br>
      Requisition</a>  


 
<script type="text/javascript">

     $(document).ready(function() {
         

        
       // Requestion Table / DataTable
      var table =   $('#dataTableRequsition').DataTable({
            "paging": false
        });
        
//        Requisition Search based on Requisition ID
        $('#reqIDSearch').keyup( function() {
        table.columns( 0 ).search( this.value ).draw();
        } );
//        Requisition Search based on Requisition ID
        $('#reqSearchDescription').keyup( function() {
        table.columns( 2 ).search( this.value ).draw();
        } );
        
//        Requisition Search based on Status
        $('#reqStatusSearch').on('change', function() {
        table.columns( 3 ).search( this.value ).draw();
        } );
        // requestion search bases on date / datepicker 
        
        $('#reqDateSearch').on('change',function(){
            getResultonDate();
               
        });
        function getResultonDate(seldate){
             $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        var celldate = data[1]  // use data for the age column
                        celldate = celldate+'';
                        celldate = celldate.split(" ");
                        celldate = celldate[1]+" "+celldate[0]+" "+celldate[2]
                        var celldate = new Date(celldate);
                        celldate = celldate.getTime();
                        if($('#reqDateSearch').val()!='')
                        var seldate = new Date($('#reqDateSearch').val());
                        else
                        var seldate = '';
                    
                        if(seldate==''){
                              return true;
                        }else if ( celldate==seldate.getTime())
                        {
                            return true;
                        } 
                        return false;
                    }
                );
               table.draw();
        }
        // make row clickable and open view/edit page
        $('#dataTableRequsition tbody tr td').on('click',function(e){
            if($(this).is(':last-child')){
               return false;
            }
            var id = $(this).parent().find('.id').data('id');
            
            window.location.replace('<?php echo Yii::app()->createUrl('requisition/update',array('id'=>''))?>'+id);
        });
        
              
  
        
    });

</script>


<script>
    
    function changeAction(el){
        var thid = $(el).parent().siblings('.id').text();
            
        if($(el).val()==0||$(el).val()==1||$(el).val()==2){
            var r = confirm("Are you sure you want to change status");
            if (r == true) {
                
                var data = {id:thid,status:$(el).val()}
                $.ajax({
                    type        : 'POST',
                    url         : '<?php echo Yii::app()->createUrl("requisition/updateStatus")?>',
                    data        : data,
                    success     : function(data) {
                        data = JSON.parse(data);
                        if(data.status==true){
                            
                            var status = '';
                            if($(el).val()==0)
                                status = 'Pending';
                            else if ($(el).val()==1)
                                status = 'Approved';
                            else if ($(el).val()==2)
                                status = 'Rejected';

                            var newel = $(el).parent().siblings('.status')
                            newel.html(status);

                        }
                    },
                    error : function (xhr) {
                        alert("Error occured.please try again");
                    }
              });
                
                
                
            }
        }else if($(el).val()==3){
            
            var r = confirm("Are you sure you want to delete this requisition");
            
            if (r == true) {
            var data = {id:thid}
                
               $.ajax({
                    type        : 'POST',
                    url         : '<?php echo Yii::app()->createUrl("requisition/DeleteReq")?>',
                    data        : data,
                    success     : function(data) {
                        data = JSON.parse(data);
                        if(data.status==true){
                          $(el).parent().parent().remove();
                         
                        }
                    },
                    error : function (xhr) {
                        alert("Error occured.please try again");
                    }
              });
                
                
            }          
        }else{
            console.log('operation');
        }
    }
</script>



