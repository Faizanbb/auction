<?php
/* @var $this RequisitionController */
/* @var $model Requisition */

$this->breadcrumbs=array(
	'Requisitions'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Requisition', 'url'=>array('index')),
	array('label'=>'Create Requisition', 'url'=>array('create')),
	array('label'=>'Update Requisition', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Requisition', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Requisition', 'url'=>array('admin')),
);
?>

<h1>View Requisition #<?php echo $model->ID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID',
		'user_id',
		'name',
		'description',
		'delivery_date',
		'quantity',
		'capex_opex',
		'budgeted',
		'budget',
		'budget_currency',
		'cost_center',
		'additional_documents',
		'address',
		'createdat',
		'status',
	),
)); ?>
