<?php
/* @var $this RequisitionController */
/* @var $model Requisition */

$this->breadcrumbs=array(
	'Requisitions'=>array('index'),
	'Manage',
);


?>

<h1>Approve Requisitions</h1>


<table id="dataTableRequsition" class="table table-striped table-bordered dataTable">
    <thead>
        <tr>
            <td>ID</td>
            <td>Date</td>
            <td>Description</td>
            <td>Status</td>
            <td>Action</td>
        </tr>
    </thead>
    <tbody>
        <?php 
        foreach($model as $req){
          
        ?>  
        <tr>
            <td class="id"><?php echo $req->ID?></td>
            <td><?php
//            echo date('j M, Y g:ia', strtotime($req->createdat));
            echo date('j M, Y H:i', strtotime($req->createdat));
            ?></td>
            <td><?php echo $req->description?></td>
            <td class="status">
                <?php 
                if($req->status==0)
                    $status = 'Pending';
                else if($req->status==1)
                    $status = 'Approved';
                else if($req->status==2)
                    $status = 'Rejected';
                echo $status;
                ?>
            </td>
            <td>
                <button class="btn btn-primary" onclick="changeStatus(this,1)">Approve</button>
                <button  class="btn btn-warning" onclick="changeStatus(this,2)">Reject</button>
                <button  class="btn btn-danger" onclick="changeStatus(this,3)">Delete</button>
            </td>
        </tr>
        
        <?php
            }
        ?>
    </tbody>
</table>

 
<script type="text/javascript">

     $(document).ready(function() {
       
      var table =   $('#dataTableRequsition').DataTable({
            responsive: true,
            
        });
        
        
         $('#reqDateSearch').datepicker({
            "onSelect": function(date) {
                seldate = date;
                seldate = new Date(seldate);
                console.log('first '+seldate.getTime());   
                $.fn.dataTable.ext.search.push(
                    function( settings, data, dataIndex ) {
                        var celldate = data[1]  // use data for the age column
                        celldate = celldate+'';
                        celldate = celldate.split(" ");
                        celldate = celldate[1]+" "+celldate[0]+" "+celldate[2]
                        var celldate = new Date(celldate);
                        celldate = celldate.getTime();
                        
                    if ( celldate==seldate.getTime())
                        {
                            return true;
                        }
                        return false;
                    }
                );
                console.log(" sec "+seldate.getTime())
               table.draw();
              }
              
                
        });
    });

</script>


<script>

    function changeStatus(el,val){
        
        if(val==1){
                var data = {id:thid,status:val}
                $.ajax({
                    type        : 'POST',
                    url         : '<?php echo Yii::app()->createUrl("requisition/updateStatus")?>',
                    data        : data,
                    success     : function(data) {
                        data = JSON.parse(data);
                        if(data.status==true){
                        
                            }
                    },
                    error : function (xhr) {
                        alert("Error occured.please try again");
                    }
              });
        }else if(val==3){
              var data = {id:thid}
                
               $.ajax({
                    type        : 'POST',
                    url         : '<?php echo Yii::app()->createUrl("requisition/DeleteReq")?>',
                    data        : data,
                    success     : function(data) {
                        data = JSON.parse(data);
                        if(data.status==true){
                          $(el).parent().parent().remove();
                         
                        }
                    },
                    error : function (xhr) {
                        alert("Error occured.please try again");
                    }
              });
                
                
                     
        }else{
            console.log('operation');
        }
    }
</script>


