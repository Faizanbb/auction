<?php
/* @var $this RequisitionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Requisitions',
);

$this->menu=array(
	array('label'=>'Create Requisition', 'url'=>array('create')),
	array('label'=>'Manage Requisition', 'url'=>array('admin')),
);
?>

<h1>Requisitions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
