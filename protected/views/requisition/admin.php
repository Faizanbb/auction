<?php
/* @var $this RequisitionController */
/* @var $model Requisition */

$this->breadcrumbs=array(
	'Requisitions'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Requisition', 'url'=>array('index')),
	array('label'=>'Create Requisition', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#requisition-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Requisitions</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'requisition-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-striped table-bordered dataTable no-footer',
	'columns'=>array(
		'ID',
                array(
                    'name'=>'user_id',
                    'value'=>'$data->user->name'
                ),
		'name',
		'description',
		'delivery_date',
		'quantity',
		/*
		'capex_opex',
		'budgeted',
		'budget',
		'budget_currency',
		'cost_cater',
		'additional_documents',
		'createdat',
		'status',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
