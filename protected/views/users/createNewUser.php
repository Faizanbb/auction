<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Profile', 'url'=>array('profile')),
	array('label'=>'Manage Users', 'url'=>array('index')),
	array('label'=>'Create New Group', 'url'=>array('//groups/create')),
	array('label'=>'Groups List', 'url'=>array('//groups/index')),
	array('label'=>'Manage Groups', 'url'=>array('//groups/admin')),
	// array('label'=>'Manage Users', 'url'=>array('admin')),
);
?>

<h1>Create New User</h1>

<?php $this->renderPartial('_formNewUser', array('model'=>$model, 'group_model'=>$group_model)); ?>