<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<!-- <div class="row">
		<?php //echo $form->labelEx($model,'accid'); ?>
		<?php //echo $form->textField($model,'accid'); ?>
		<?php //echo $form->error($model,'accid'); ?>
	</div> -->

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>350)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>350)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>350)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>350)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mobile'); ?>
		<?php echo $form->textField($model,'mobile',array('size'=>60,'maxlength'=>350)); ?>
		<?php echo $form->error($model,'mobile'); ?>
	</div>
	<?php if ($model->role == 'USER') { ?>
		<div class="row">
			<?php echo $form->labelEx($model,'user group'); ?>
			<select name="group">
				<option value="">-- Assign Group --</option>
				<?php foreach ($groupModel as $gm) { ?>
					<option value="<?php echo $gm['name'];?>"><?php echo $gm['name']; ?></option>
				<?php } ?>
			</select>
			<?php //echo $form->textField($model,'usergroup',array('size'=>60,'maxlength'=>350)); ?>
			<?php echo $form->error($model,'usergroup'); ?>
		</div>
	<?php } ?>
		
	<?php if (isset($account_model)) { ?>
		<div class="row">
			<?php echo $form->labelEx($account_model,'company name'); ?>
			<?php echo $form->textField($account_model,'company',array('size'=>60,'maxlength'=>350)); ?>
			<?php echo $form->error($account_model,'company_name'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($account_model,'country'); ?>
			<?php echo $form->textField($account_model,'country',array('size'=>60,'maxlength'=>350)); ?>
			<?php echo $form->error($account_model,'country'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($account_model,'city'); ?>
			<?php echo $form->textField($account_model,'city',array('size'=>60,'maxlength'=>350)); ?>
			<?php echo $form->error($account_model,'city'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($account_model,'address'); ?>
			<?php echo $form->textField($account_model,'address',array('size'=>60,'maxlength'=>350)); ?>
			<?php echo $form->error($account_model,'address'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($account_model,'zip code'); ?>
			<?php echo $form->textField($account_model,'zipcode',array('size'=>60,'maxlength'=>350)); ?>
			<?php echo $form->error($account_model,'zipcode'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($account_model,'business type'); ?>
			<?php echo $form->textField($account_model,'businesstype',array('size'=>60,'maxlength'=>350)); ?>
			<?php echo $form->error($account_model,'businesstype'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($account_model,'employees'); ?>
			<?php echo $form->textField($account_model,'employees',array('size'=>60,'maxlength'=>350)); ?>
			<?php echo $form->error($account_model,'employees'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($account_model,'turn over'); ?>
			<?php echo $form->textField($account_model,'turnover',array('size'=>60,'maxlength'=>350)); ?>
			<?php echo $form->error($account_model,'turnover'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($account_model,'established'); ?>
			<?php echo $form->textField($account_model,'established',array('size'=>60,'maxlength'=>350)); ?>
			<?php echo $form->error($account_model,'established'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($account_model,'company introduction'); ?>
			<?php echo $form->textField($account_model,'companyintroduction',array('size'=>60,'maxlength'=>350)); ?>
			<?php echo $form->error($account_model,'companyintroduction'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($account_model,'language'); ?>
			<?php echo $form->textField($account_model,'language',array('size'=>60,'maxlength'=>350)); ?>
			<?php echo $form->error($account_model,'language'); ?>
		</div>
	<?php } ?>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->