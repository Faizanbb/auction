<?php
/* @var $this UsersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Users',
);

$this->menu=array(
	array('label'=>'Profile', 'url'=>array('profile')),
	array('label'=>'Create New User', 'url'=>array('create_new_user')),
	array('label'=>'Create New Group', 'url'=>array('//groups/create')),
	array('label'=>'Manage Groups', 'url'=>array('//groups/index')),
);
?>

<h1>All Users</h1>
<br>
	<!-- Flash messages -->
	<?php
		foreach (Yii::app()->user->getFlashes() as $type => $flash) {
			echo "<div class='{$type} text-center' style='color:red;'>{$flash}</div><br>";
		}
	?>
<?php //print_r($dataProvider);exit; ?>
<?php $userID = Yii::app()->user->id; // Get user session id ?>

<div class="col-md-12">
	<table class="table table-striped">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Email</th>
			<th>Group Name</th>
			<th>Actions</th>
		</tr>
		<?php foreach ($users as $user) { ?>
			<?php if ($user['ID'] != $userID) {?>
				<tr>
					<td><?php echo $user['ID']; ?></td>
					<td><?php echo $user['name']; ?></td>
					<td><?php echo $user['email']; ?></td>
					<td><?php echo $user['usergroup']; ?></td>
					<td>
						<a href="<?php echo Yii::app()->createUrl('users/update_user&id='.$user['ID']);?>" class="btn btn-xs btn-primary">edit</a>
						| <a href="<?php echo Yii::app()->createUrl('users/view&id='.$user['ID']);?>" class="btn btn-xs btn-success">View</a>
						| <a href="<?php echo Yii::app()->createUrl('users/delete&id='.$user['ID']);?>"  class="btn btn-xs btn-danger" onclick="return confirm('Are you sure you want to delete this item?');">del</a>
					</td>
				</tr>
			<?php } ?>
		<?php } ?>
	</table>
</div>






<?php
/* @var $this UsersController */
/* @var $dataProvider CActiveDataProvider */

// $this->breadcrumbs=array(
// 	'Users',
// );

// $this->menu=array(
// 	array('label'=>'Create Users', 'url'=>array('create')),
// 	array('label'=>'Manage Users', 'url'=>array('admin')),
// );
?>

<!-- <h1>Users</h1> -->

<?php
 // $this->widget('zii.widgets.CListView', array(
	// 'dataProvider'=>$dataProvider,
	// 'itemView'=>'_view',
// )); 
?>
