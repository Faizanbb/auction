<?php
/* @var $this UsersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Users',
);
	if (isset($account_model)) {
		$this->menu=array(
			array('label'=>'Profile', 'url'=>array('profile')),
			// array('label'=>'Create New User', 'url'=>array('create_new_user')),
			array('label'=>'Manage Users', 'url'=>array('index')),
			array('label'=>'Manage Groups', 'url'=>array('//groups/admin')),
		);
	}
?>

<h1>Profile</h1>
<br>

<div class="col-md-6">
	<!-- Flash messages -->
	<?php
		foreach (Yii::app()->user->getFlashes() as $type => $flash) {
			echo "<div class='{$type} text-center' style='color:red;'>{$flash}</div><br>";
		}
	?>
	<a href="<?php echo Yii::app()->createUrl('users/update&id='.$model->ID);?>" class="btn btn-primary btn-xs pull-right">Update Profile</a>
	<?php if (isset($account_model)) {?>
		<a href="<?php echo Yii::app()->createUrl('users/create_new_user');?>" class="btn btn-success btn-xs pull-left">Create New User</a>
		<a href="<?php echo Yii::app()->createUrl('groups/create');?>" class="btn btn-warning btn-xs pull-left">Create New Group</a>
	<?php } ?>
	<br><br>
	
	<table class="table table-striped">
		<tr>
			<th>Full Name:</th>
			<td><?php echo $model->name; ?></td>
			<th>Email:</th>
			<td><?php echo $model->email; ?></td>
		</tr>
		<tr>
			<th>Title:</th>
			<td><?php echo $model->title; ?></td>
			<th>Phone:</th>
			<td><?php echo $model->phone; ?></td>
		</tr>
		<?php if (isset($account_model)) { ?>
			<tr>
				<th>Mobile:</th>
				<td><?php echo $model->mobile; ?></td>
				<th>Company:</th>
				<td><?php echo $account_model->company; ?></td>
			</tr>
			<tr>
				<th>Country:</th>
				<td><?php echo $account_model->country; ?></td>
				<th>City:</th>
				<td><?php echo $account_model->city; ?></td>
			</tr>
			<tr>
				<th>Address:</th>
				<td><?php echo $account_model->address; ?></td>
				<th>Language:</th>
				<td><?php echo $account_model->language; ?></td>
			</tr>
		<?php } else {?>
			<tr>
				<th>Mobile:</th>
				<td><?php echo $model->mobile; ?></td>
			</tr>
		<?php } ?>
	</table>
</div>
<?php
/* @var $this UsersController */
/* @var $model Users */

// $this->breadcrumbs=array(
// 	'Users'=>array('index'),
// 	$model->name,
// );

// $this->menu=array(
// 	array('label'=>'List Users', 'url'=>array('index')),
// 	array('label'=>'Create Users', 'url'=>array('create')),
// 	array('label'=>'Update Users', 'url'=>array('update', 'id'=>$model->ID)),
// 	array('label'=>'Delete Users', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
// 	array('label'=>'Manage Users', 'url'=>array('admin')),
// );
?>
