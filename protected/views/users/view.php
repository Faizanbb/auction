<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Profile', 'url'=>array('profile')),
	array('label'=>'Create New User', 'url'=>array('create_new_user')),
	// array('label'=>'Update Users', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete New User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this user account?')),
	array('label'=>'Manage Users', 'url'=>array('index')),
	// array('label'=>'Manage Users', 'url'=>array('admin')),
);
?>

<h1>View New User <?php //echo $model->ID; ?></h1>

	<!-- Flash messages -->
	<?php
		foreach (Yii::app()->user->getFlashes() as $type => $flash) {
			echo "<div class='{$type} text-center' style='color:red;'>{$flash}</div><br>";
		}
	?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		// 'ID',
		// 'accid',
		'name',
		'email',
		// 'password',
		'title',
		'phone',
		'mobile',
		'usergroup',
		// 'createdat',
		// 'status',
		'role',
		// 'logincount',
		// 'lastaccess',
		// 'updatedat',
	),
)); ?>










<?php
/* @var $this UsersController */
/* @var $model Users */

// $this->breadcrumbs=array(
// 	'Users'=>array('index'),
// 	$model->name,
// );

// $this->menu=array(
// 	array('label'=>'List Users', 'url'=>array('index')),
// 	array('label'=>'Create Users', 'url'=>array('create')),
// 	array('label'=>'Update Users', 'url'=>array('update', 'id'=>$model->ID)),
// 	array('label'=>'Delete Users', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
// 	array('label'=>'Manage Users', 'url'=>array('admin')),
// );
?>

<!-- <h1>View Users #<?php echo $model->ID; ?></h1> -->

<?php 
// $this->widget('zii.widgets.CDetailView', array(
// 	'data'=>$model,
// 	'attributes'=>array(
// 		'ID',
// 		'accid',
// 		'name',
// 		'email',
// 		'password',
// 		'title',
// 		'phone',
// 		'mobile',
// 		'carriercode',
// 		'mobileregion',
// 		'usergroup',
// 		'createdat',
// 		'status',
// 		'role',
// 		'logincount',
// 		'lastaccess',
// 		'updatedat',
// 	),
// )); 
?>
