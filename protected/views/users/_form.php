<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>350)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>350)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>350)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'Re-enter password'); ?>
		<label for="password2">Re-enter Password</label>
		<input type="password" name="password2" id="password2" size="60" maxlength="350">
		<?php //echo $form->passwordField($model,'password2',array('size'=>60,'maxlength'=>350)); ?>
		<?php //echo $form->error($model,'password2'); ?>
	</div>


	<div class="row">
		<label for="company_name">Company Name</label>
		<input type="text" name="company_name" id="company_name" size="60" maxlength="350">
		<?php //echo $form->error($model,'company_name'); ?>
	</div>

	<div class="row">
		<label for="country">Country</label>
		<input type="text" name="country" id="country" size="60" maxlength="350">
		<?php //echo $form->error($model,'country'); ?>
	</div>

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'accid'); ?>
		<?php echo $form->textField($model,'accid'); ?>
		<?php echo $form->error($model,'accid'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>350)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>350)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mobile'); ?>
		<?php echo $form->textField($model,'mobile',array('size'=>60,'maxlength'=>350)); ?>
		<?php echo $form->error($model,'mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'carriercode'); ?>
		<?php echo $form->textField($model,'carriercode'); ?>
		<?php echo $form->error($model,'carriercode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mobileregion'); ?>
		<?php echo $form->textField($model,'mobileregion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'mobileregion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usergroup'); ?>
		<?php echo $form->textField($model,'usergroup',array('size'=>60,'maxlength'=>350)); ?>
		<?php echo $form->error($model,'usergroup'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'createdat'); ?>
		<?php echo $form->textField($model,'createdat'); ?>
		<?php echo $form->error($model,'createdat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'role'); ?>
		<?php echo $form->textField($model,'role',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'role'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'logincount'); ?>
		<?php echo $form->textField($model,'logincount'); ?>
		<?php echo $form->error($model,'logincount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lastaccess'); ?>
		<?php echo $form->textField($model,'lastaccess'); ?>
		<?php echo $form->error($model,'lastaccess'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updatedat'); ?>
		<?php echo $form->textField($model,'updatedat'); ?>
		<?php echo $form->error($model,'updatedat'); ?>
	</div> -->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->