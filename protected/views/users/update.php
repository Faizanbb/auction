<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->name=>array('view','id'=>$model->ID),
	'Update',
);

	if (isset($account_model)) {
		$this->menu=array(
			array('label'=>'Profile', 'url'=>array('profile')),
			array('label'=>'Create User', 'url'=>array('create')),
			array('label'=>'Create Group', 'url'=>array('//groups/create')),
			array('label'=>'Manage Users', 'url'=>array('index')),
			array('label'=>'Manage Groups', 'url'=>array('//groups/index')),
			array('label'=>'View Users', 'url'=>array('view', 'id'=>$model->ID)),
			// array('label'=>'Manage Users', 'url'=>array('admin')),
		);
	}
?>

<h1>Update Profile</h1>

<?php 
$dt  = array('model'=>$model, 'groupModel'=>$groupModel);
if (isset($account_model)) {
	$dt['account_model'] = $account_model;
}
$this->renderPartial('_formUpdate', $dt); ?>







<?php
/* @var $this UsersController */
/* @var $model Users */

// $this->breadcrumbs=array(
// 	'Users'=>array('index'),
// 	$model->name=>array('view','id'=>$model->ID),
// 	'Update',
// );

// $this->menu=array(
// 	array('label'=>'List Users', 'url'=>array('index')),
// 	array('label'=>'Create Users', 'url'=>array('create')),
// 	array('label'=>'View Users', 'url'=>array('view', 'id'=>$model->ID)),
// 	array('label'=>'Manage Users', 'url'=>array('admin')),
// );
?>

<!-- <h1>Update Users <?php echo $model->ID; ?></h1> -->

<?php //$this->renderPartial('_form', array('model'=>$model)); ?>