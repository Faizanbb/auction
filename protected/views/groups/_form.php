<?php
/* @var $this GroupsController */
/* @var $model Groups */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'groups-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<!-- <div class="row">
		<?php //echo $form->labelEx($model,'account_id'); ?>
		<?php //echo $form->textField($model,'account_id'); ?>
		<?php //echo $form->error($model,'account_id'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'user_id'); ?>
		<?php //echo $form->textField($model,'user_id'); ?>
		<?php //echo $form->error($model,'user_id'); ?>
	</div> -->

	<div class="row">
		<?php echo $form->labelEx($model,'group name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	<br>
	<?php if (isset($roles) && !empty($roles)) { ?>
		<div class="row">
			<h5>Rights</h5>
		</div>
		<hr>
		<div class="col-md-12">
			<div class="row">
				<?php foreach ($roles as $r) { ?>
					<?php if ($r['parent'] == 0) { ?>
						<div class="col-md-4">
							<h4><?php echo $r['name']; ?></h4>
							<?php foreach ($roles as $role) { ?>
								<?php if ($role['parent'] == $r['ID']) { ?>
									<input type="checkbox" name="role[]" id="<?php echo $role['ID']; ?>" value="<?php echo $role['ID']; ?>">
									<label for="<?php echo $role['ID']; ?>"><?php echo $role['name']; ?></label>
									<br>
								<?php } ?>
							<?php } ?>
							<br>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	<?php } ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->