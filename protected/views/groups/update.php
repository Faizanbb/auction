<?php
/* @var $this GroupsController */
/* @var $model Groups */

$this->breadcrumbs=array(
	'Groups'=>array('index'),
	$model->name=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'Profile', 'url'=>array('//users/profile')),
	array('label'=>'Create New User', 'url'=>array('//users/create_new_user')),
	array('label'=>'Manage Users', 'url'=>array('//users/index')),
	array('label'=>'Manage Groups', 'url'=>array('index')),
	array('label'=>'Create Groups', 'url'=>array('create')),
	array('label'=>'View Groups', 'url'=>array('view', 'id'=>$model->ID)),
	// array('label'=>'Manage Groups', 'url'=>array('admin')),
);
?>

<h1>Update Groups <?php echo $model->ID; ?></h1>

<?php $this->renderPartial('_formUpdate', array('model'=>$model, 'roles'=>$roles, 'groupsRoles'=>$groupsRoles)); ?>