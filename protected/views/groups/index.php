<?php
/* @var $this GroupsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Groups',
);

$this->menu=array(
	array('label'=>'Profile', 'url'=>array('//users/profile')),
	array('label'=>'Create New User', 'url'=>array('//users/create_new_user')),
	array('label'=>'Manage Users', 'url'=>array('//users/index')),
	array('label'=>'Create Group', 'url'=>array('create')),
	// array('label'=>'Manage Groups', 'url'=>array('index')),
);
?>

<h1>Groups</h1>

<?php $userID = Yii::app()->user->id; // Get user session id ?>

<div class="col-md-12">
	<table class="table table-striped">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Actions</th>
		</tr>
		<?php foreach ($groups as $group) { ?>
			<?php //if ($user['ID'] != $userID) {?>
				<tr>
					<td><?php echo $group['ID']; ?></td>
					<td><?php echo $group['name']; ?></td>
					<td>
						<a href="<?php echo Yii::app()->createUrl('groups/update&id='.$group['ID']);?>" class="btn btn-xs btn-primary">edit</a>
						| <a href="<?php echo Yii::app()->createUrl('groups/view&id='.$group['ID']);?>" class="btn btn-xs btn-success">View</a>
						| <a href="<?php echo Yii::app()->createUrl('groups/delete&id='.$group['ID']);?>"  class="btn btn-xs btn-danger" onclick="return confirm('Are you sure you want to delete this item?');">del</a>
					</td>
				</tr>
			<?php //} ?>
		<?php } ?>
	</table>
</div>
