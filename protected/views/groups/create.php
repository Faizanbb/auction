<?php
/* @var $this GroupsController */
/* @var $model Groups */

$this->breadcrumbs=array(
	'Groups'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Profile', 'url'=>array('//users/profile')),
	array('label'=>'Create New User', 'url'=>array('//users/create_new_user')),
	array('label'=>'Manage Users', 'url'=>array('//users/index')),
	// array('label'=>'List Groups', 'url'=>array('index')),
	array('label'=>'Manage Groups', 'url'=>array('index')),
);
?>

<h1>Create Groups</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'roles'=>$roles)); ?>