
	<?php foreach ($items as $i) { ?>
		<tr>
			<td>
				<b>Added on:</b> <?php echo Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($i['createdat'], 'yyyy-MM-dd'), 'medium', null); ?>
				<br><br>
				<b>
					<a href="<?php echo Yii::app()->createUrl('items/view&id='.$i['ID']); ?>">
						<?php echo $i['name']; ?>
					</a>
				</b>
			</td>
			<td><br><br><?php echo $i['lotnum']; ?> Roll</td>
			<td>
				<br><br>
				<?php if (isset($doc) && !empty($doc)) {?>
					<?php if (in_array($i['ID'], $itemID) && in_array('application/pdf', $doc)) { ?>
						<a href="<?php echo Yii::app()->createUrl('items/view&id='.$i['ID']); ?>" class="btn btn-primary btn-sm">View</a>
					<?php }else{ ?>
						<a href="<?php echo Yii::app()->createUrl('items/attachFile&id='.$i['ID']); ?>" class="btn btn-warning btn-sm bb" id="myBtn">Attach File</a>
					<?php } ?>
				<?php } ?>
			</td>
			<td><br><br><?php echo $i['unitname']; ?></td>
			<td><br><br><b><?php echo $i['quantity']; ?></b></td>
			<td><a href="<?php echo Yii::app()->createUrl('items/update&id='.$i['ID']); ?>">e</a> | <a href="<?php echo Yii::app()->createUrl('items/delete&id='.$i['ID']); ?>">d</a><br><br><b>AED <?php echo $i['itemprice']; ?></b></td>
		</tr>
	<?php } ?>
