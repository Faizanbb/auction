<?php
/* @var $this ItemsController */
/* @var $model Items */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'items-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
		'method' => 'post'
	)
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
	
	<div class="row">
		<?php echo $form->labelEx($model,'name/Classification'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>350)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Item Number'); ?>
		<?php echo $form->textField($model,'lotnum'); ?>
		<?php echo $form->error($model,'lotnum'); ?>
	</div>

	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'Item Categories'); ?>
		<?php echo $form->dropDownList($model,'parentid', $categoriesList); ?>
		<?php echo $form->error($model,'parentid'); ?>
	</div>

	<div class="row">
		<label>Suppliers</label>
		<div>
			<?php if (isset($suppliers) && !empty($suppliers)) { ?>
				<?php foreach ($suppliers as $s) { ?>
					<input type="checkbox" name="suppliers[]" value="<?php echo $s['ID']; ?>"
						<?php echo in_array($s['ID'],$checkedSup)?' checked="checked"':'';?>
					>
					<?php echo $s->name; ?> <br>
				<?php } ?>
			<?php } ?>
		</div>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Purchase Price'); ?>
		<?php echo $form->textField($model,'itemprice'); ?>
		<?php echo $form->error($model,'itemprice'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<label>Item Image</label>
		<input type="file" name="item_image[]" accept="image/*" multiple>
		<?php foreach ($getItemDocs as $value) { ?>
			<?php if ($value['typeoffile'] == 'image/jpeg') { ?>
				<input  onclick="toggle('.<?php echo $value['did'];?>', this)" type="checkbox" name="item_image[]" value="<?php echo $value['dname']; ?>"> <?php echo $value['origname']; ?> <a class="<?php echo $value['did'];?>" href="<?php echo Yii::app()->createUrl('items/remove&id='.$value['did']); ?>">remove</a><br>
			<?php } ?>
		<?php } ?>
	</div>

	<div class="row">
		<label>Specifications Files</label>
		<input type="file" name="item_docs[]" accept="application/pdf, image/*" multiple>
		<?php foreach ($getItemDocs as $value) { ?>
			<?php if ($value['typeoffile'] == 'application/pdf') { ?>
				<input type="checkbox" name="item_image[]" value="<?php echo $value['dname']; ?>" onclick="toggle('.<?php echo $value['did'];?>', this)">
				<?php echo $value['origname']; ?>
				<a class="<?php echo $value['did'];?>" href="<?php echo Yii::app()->createUrl('items/remove&id='.$value['did']); ?>">remove</a>
				<br>
			<?php } ?>
			<script type="text/javascript">
				$(document).ready(function(){
					$('.<?php echo $value['did'];?>').hide();
				});
				function toggle(className, obj) {
				    var $input = $(obj);
				    if ($input.prop('checked'))
			    	{
			    		$(className).show();
			    	}
				    else{
				    	$(className).hide();
				    }
				}
			</script>
		<?php } ?>
	</div><br>
	
	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<select name="status">
			<option value="1">Active</option>
			<option value="0">Closed</option>
		</select>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Quantity'); ?>
		<?php echo $form->textField($model,'quantity'); ?>
		<?php echo $form->error($model,'quantity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'unit label eg: box'); ?>
		<?php echo $form->textField($model,'unit_label',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'unit_label'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'units'); ?>
		<?php echo $form->dropDownList($model,'unit', $unitList); ?>
		<?php echo $form->error($model,'unit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'unit amount eg: 5'); ?>
		<?php echo $form->textField($model,'unit_amount'); ?>
		<?php echo $form->error($model,'unit_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'notes'); ?>
		<?php echo $form->textArea($model,'notes',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'notes'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


