<?php
/* @var $this ItemsController */
/* @var $model Items */

$this->breadcrumbs=array(
	'Items'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Items', 'url'=>array('index')),
	array('label'=>'Create Items Heading', 'url'=>array('create_heading')),
	array('label'=>'Manage Items', 'url'=>array('admin')),
);
?>

<h1>Import Data From Excel</h1>
<hr>
<a href="<?php echo Yii::app()->createUrl('items/download_template'); ?>" class="btn btn-sm btn-warning">Download Template</a>
<br>
	<!-- Flash messages -->
	<?php
		foreach (Yii::app()->user->getFlashes() as $type => $flash) {
			echo "<div class='{$type} text-center' style='color:red;'>{$flash}</div><br>";
		}
	?>
	
<?php $this->renderPartial('_formImportExcel'); ?>