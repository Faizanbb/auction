<?php
/* @var $this ItemsController */
/* @var $model Items */

$this->breadcrumbs=array(
	'Items'=>array('index'),
	$model->name=>array('view','id'=>$model->ID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Items', 'url'=>array('index')),
	array('label'=>'Create Items Heading', 'url'=>array('create_heading')),
	array('label'=>'Create Items', 'url'=>array('create')),
	array('label'=>'View Items', 'url'=>array('view', 'id'=>$model->ID)),
	array('label'=>'Manage Items', 'url'=>array('admin')),
);
?>

<h1>Update Items <?php echo $model->ID; ?></h1>
<!-- Flash messages -->
	<?php
		foreach (Yii::app()->user->getFlashes() as $type => $flash) {
			echo "<div class='{$type} text-center' style='color:red;'>{$flash}</div><br>";
		}
	?>
	
<?php $this->renderPartial('_formSubItemUpdate', array('model'=>$model, 'categoriesList'=>$categoriesList, 'getItemDocs'=>$getItemDocs, 'checkedSup'=>$checkedSup, 'suppliers'=>$suppliers, 'unitList'=>$unitList)); ?>