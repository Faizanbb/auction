<?php
/* @var $this ItemsController */
/* @var $model Items */

$this->breadcrumbs=array(
	'Items'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Items', 'url'=>array('index')),
	array('label'=>'Create Items Heading', 'url'=>array('create_heading')),
	array('label'=>'Create Items', 'url'=>array('create')),
	array('label'=>'Update Items', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete Items', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Items', 'url'=>array('admin')),
);
?>

<h1>Item Details</h1>
<hr>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading" style="background-color: #f9f9f9;">
			Added on <?php echo Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($getItem['createdat'], 'yyyy-MM-dd'),'medium',null); ?>
		</div>
		<div class="panel-body">
			<div class="col-md-12">
				<div class="col-md-3">
					<?php if (isset($getItemDocs)) { ?>
						<?php foreach ($getItemDocs as $value) { ?>
							<?php if ($value['typeoffile'] != 'application/pdf') { ?>
								<img src="<?php echo Yii::app()->getBaseUrl(true) . '/uploads/items/'.$value['dname']; ?>" class="img img-responsive">
							<?php break; } ?>
						<?php } ?>
					<?php } ?>
				</div>
				<div class="col-md-9" id="responce1">
					<p style="color: #37377d;"><b><?php echo $getItem['name']; ?></b></p>
					<p><b>Item Number :</b> <?php echo $getItem['lotnum']; ?> Roll</p>
					<p><b>Unit : </b><?php echo $getItem['unitname']; ?></p>
					<p><b>Qty : </b><?php echo $getItem['quantity']; ?></p>
					<p><b>Spec.Files</b> <a href="#" class="btn btn-primary btn-sm">View</a></p>
					<p><b>Price :</b> AED <?php echo $getItem['itemprice']; ?></p>
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-info">
		<div class="panel-heading">
			<strong>Supplier</strong>
		</div>
		<div class="panel-body" style="padding: 0;">
			<div class="col-md-12" style="padding: 15px;">
				<strong>SAGE</strong>
			</div>
			<table class="table" id="responce2">
				<thead>
				<tr>
					<th>Item Name</th>
					<th>Item Number</th>
					<th>Spec.Files</th>
					<th>Unit</th>
					<th>Qty</th>
					<th>Price</th>
					<th class="savebtnth" style="display: none;">Save Changes</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<td><br><b><?php echo $getItem['name']; ?></b></td>
					<td><br><b><?php echo $getItem['lotnum']; ?></b></td>
					<td><br><a href="#" class="btn btn-primary btn-smm">View</a></td>
					<td><br><?php echo $getItem['unitname']; ?></td>
					<td class="td">
						<br>
						<input type="number" name="quantity" style="display: none;">
						<span><b><?php echo $getItem['quantity']; ?></b></span>
					</td>
					<td class="td">
						<a id="edit" style="cursor: pointer;">Edit</a>
						<br>
						<input type="text" name="itemprice" style="display: none;">
						<span>
							<b>AED <?php echo $getItem['itemprice']; ?></b>
						</span>
					</td>
					<td class="savebtn" style="display: none;">
						<br>
						<button class="btn btn-sm btn-primary" type="submit">Save</button>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#edit').click(function(){
		    $(this).parent().parent().find('input').show();
		    $(this).parent().parent().find('span').hide();
		    $(this).parent().parent().find('.savebtn').show();
		    $(this).parent().parent().parent().find('.savebtnth').show();
		    $(this).parent().find('a').hide();
		});
	});

	$(document).ready(function(){
		$('button').click(function(){
			var id = '<?php echo $getItem['ID']; ?>';
		    var qty = $(this).parent().parent().find('input[name=quantity]').val();
		    var itmpr = $(this).parent().parent().find('input[name=itemprice]').val();
		    // alert(id+' '+itmpr+' '+qty);
		    $.ajax({
		    	type : 'POST',
		    	url : '<?php echo Yii::app()->createUrl('items/changes'); ?>',
		    	data: { 'id':id, 'qty':qty, 'itmpr':itmpr },
		    	success:function(resp1){
		    		// alert(resp);
		    		$.ajax({
				    	type : 'POST',
				    	url : '<?php echo Yii::app()->createUrl('items/changes'); ?>',
				    	data: { 'id2':id, 'qty2':qty, 'itmpr2':itmpr },
				    	success:function(resp2){
				    		// alert(resp2);
				    		if (resp2) {	
							  	$('#responce1').html(resp2);
				    		}
				    	}
				    });
		    		if (resp1) {	
					  	$('table#responce2 tbody').html(resp1);
		    		}
		    	}
		    });
		});
	});
</script>

