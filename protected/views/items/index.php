<?php
/* @var $this ItemsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Items',
);

$this->menu=array(
	array('label'=>'Create Items', 'url'=>array('create')),
	array('label'=>'Create Items Heading', 'url'=>array('create_heading')),
	array('label'=>'Manage Items', 'url'=>array('admin')),
);
?>
<!-- Flash messages -->
	<?php
		foreach (Yii::app()->user->getFlashes() as $type => $flash) {
			echo "<div class='{$type} text-center' style='color:red;'>{$flash}</div><br>";
		}
	?>
	
<div class="col-md-12">
	<div class="col-md-2">
		<h1>ITEMS</h1>	
	</div>
	<div class="col-md-10">
		<p><span><a href="<?php echo Yii::app()->createUrl('items/create'); ?>" class="btn btn-warning pull-right"> + ADD NEW ITEM</a></span> <span><a href="<?php echo Yii::app()->createUrl('items/import_excel'); ?>" class="btn btn-default pull-right">IMPORT FROM EXCEL</a></span> <span> <a href="<?php echo Yii::app()->createUrl('items/export_csv'); ?>" class="btn btn-primary pull-right">EXPORT TO CSV</a></span></p> 
	</div>
</div>
<!-- SEARCH BOX -->
<div class="col-md-12" style="margin-bottom: 10px;">
<hr>
	<form class="form-group" method="GET" enctype="multipart/form-data" action="<?php echo Yii::app()->createUrl('items/search'); ?>">
		<input type="hidden" value="items/search" name="r">
		<div class="col-md-8">
			<input type="text" name="query" class="form-control" required>
		</div>
		<div class="col-md-4" style="padding-left: 0;">
			<button type="submit" class="btn btn-primary">SEARCH</button>
		</div>
	</form>
</div>
<!-- SELECT MENU -->
<div class="col-md-12" style="margin-bottom: 20px;">
	<div class="col-md-3">
		<select class="form-control" id="branchOrCompany">
			<option value="1">View My Branch Items</option>
			<option value="2">View All Company Items</option>
		</select>
	</div>
	<div class="col-md-2">
		<select class="form-control" id="selectSupplier">
			<option value="">All Suppliers</option>
			<?php if (isset($suppliers) && !empty($suppliers)) { ?>
	  			<?php foreach ($suppliers as $sup) { ?>
				    <option value="<?php echo $sup['ID'] ?>"><?php echo $sup['name']; ?></option>
				<?php } ?>
			<?php } ?>
		</select>
	</div>
	<div class="col-md-3">
		<select class="form-control" id="activeInactive">
			<option value="viewAll">Active and Inactive Items</option>
		    <option value="1">Active Items</option>
		    <option value="2">Inactive Items</option>
		</select>
	</div>
</div>
<div class="col-md-12">
	<div class="panel panel-info">
		<div class="panel-heading">
			Items
		</div>
		<div class="body">
			<table class="table">
				<thead>
					<tr>
						<th>Item Name</th>
						<th>Item Number</th>
						<th>Spec.Files</th>
						<th>Unit</th>
						<th>Qty</th>
						<th>Price</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($items as $i) { ?>
						<tr id="main">
							<td>
								<b>Added on:</b> <?php echo Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($i['createdat'], 'yyyy-MM-dd'), 'medium', null); ?>
								<br><br>
								<b>
									<a href="<?php echo Yii::app()->createUrl('items/view&id='.$i['ID']); ?>">
										<?php echo $i['name']; ?>
									</a>
								</b>
							</td>
							<td><br><br><?php echo $i['lotnum']; ?> Roll</td>
							<td>
								<br><br>
								<?php if (isset($doc) && !empty($doc)) {?>
									<?php if (in_array($i['ID'], $itemID) && in_array('application/pdf', $doc)) { ?>
										<a href="<?php echo Yii::app()->createUrl('items/view&id='.$i['ID']); ?>" class="btn btn-primary btn-sm">View</a>
									<?php }else{ ?>
										<a href="<?php echo Yii::app()->createUrl('items/attachFile&id='.$i['ID']); ?>" class="btn btn-warning btn-sm bb" id="myBtn">Attach File</a>
									<?php } ?>
								<?php } ?>
							</td>
							<td><br><br><?php echo $i['unitname']; ?></td>
							<td><br><br><b><?php echo $i['quantity']; ?></b></td>
							<td><a href="<?php echo Yii::app()->createUrl('items/update&id='.$i['ID']); ?>">e</a> | <a href="<?php echo Yii::app()->createUrl('items/delete&id='.$i['ID']); ?>">d</a><br><br><b>AED <?php echo $i['itemprice']; ?></b></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>


		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#branchOrCompany").change(function() {
			var view = $("#branchOrCompany option:selected").val();
			if (view == 2) {
				$.ajax({ 
					url: "<?php echo Yii::app()->createUrl('items/index&view='); ?>"+view,
					data:{'view':view},
					success: function(resp){
						if (resp) {
							$('table.table tbody').html(resp);
						}
					}
				});
			}
		});
	});

	$(document).ready(function(){
		$("#selectSupplier").change(function() {
			var supId = $("#selectSupplier option:selected").val();
			if (supId >= 0) {
				$.ajax({ 
					url: "<?php echo Yii::app()->createUrl('items/index&supId='); ?>"+supId,
					data:{'supId':supId},
					success: function(resp){
						if (resp) {
							$('table.table tbody').html(resp);
						}
					}
				});
			}
		});
	});

	$(document).ready(function(){
		$("#activeInactive").change(function() {
			var activeInactive = $("#activeInactive option:selected").val();
			if (activeInactive == 'viewAll') {
				console.log(activeInactive);
			}
			if (activeInactive > 0) {
				$.ajax({ 
					url: "<?php echo Yii::app()->createUrl('items/index&activeInactive='); ?>"+activeInactive,
					data:{'activeInactive':activeInactive},
					success: function(resp){
						if (resp) {
							$('table.table tbody').html(resp);
						}
					}
				});
			}
		});
	});
</script>

