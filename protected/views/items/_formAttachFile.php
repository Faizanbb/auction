<?php
/* @var $this ItemsController */
/* @var $model Items */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'items-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
		'method' => 'post'
	)
)); ?>

<br>
	<div class="row">
		<label for="fileToUpload">Attach File</label>
		<input type="file" name="fileToUpload[]" id="fileToUpload" multiple>
	</div>
<br>
	<div class="row buttons">
		<button type="submit">Save</button>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->