-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 08, 2018 at 09:16 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bb_auctionsbid`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `ID` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `company` varchar(350) NOT NULL,
  `country` varchar(350) NOT NULL,
  `city` varchar(350) NOT NULL,
  `address` text NOT NULL,
  `zipcode` varchar(350) NOT NULL,
  `businesstype` varchar(250) NOT NULL,
  `employees` varchar(150) NOT NULL,
  `turnover` varchar(150) NOT NULL,
  `established` varchar(150) NOT NULL,
  `companyintroduction` longtext NOT NULL,
  `language` varchar(350) NOT NULL,
  `status` enum('Active','Block','Pending') NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`ID`, `uid`, `company`, `country`, `city`, `address`, `zipcode`, `businesstype`, `employees`, `turnover`, `established`, `companyintroduction`, `language`, `status`, `createdat`) VALUES
(1, 1, 'Faizan co', 'UAE', 'Dubai', 'address', '123456', 'bbtype', '50', '5000', '1993', 'intro', 'English', 'Active', '2018-08-30 18:05:23'),
(2, 9, 'supplier 1 co.', 'uae', 'dubai', 'address', '1324', 'services', '20', '25000', '2000', 'company intro', 'English', 'Active', '2018-09-11 12:30:21'),
(3, 10, 'supplier 2 co.', 'uae', 'dubai', 'address', '1324', 'services', '20', '25000', '2000', 'company intro', 'English', 'Active', '2018-09-11 12:30:21'),
(4, 11, 'supplier 3 co.', 'uae', 'dubai', 'address', '1324', 'services', '20', '25000', '2000', 'company intro', 'English', 'Active', '2018-09-11 12:30:21'),
(5, 13, 'supplier one co', '', '', '', '', '', '', '', '', '', '', 'Active', '2018-09-11 17:59:59'),
(6, 14, 'supplier 2 co', '', '', '', '', '', '', '', '', '', '', 'Active', '2018-09-11 18:00:01'),
(7, 0, '', '', '', '', '', '', '', '', '', '', '', 'Active', '2018-09-11 19:35:52'),
(8, 15, '', '', '', '', '', '', '', '', '', '', '', 'Active', '2018-09-11 19:38:33'),
(15, 16, '', '', '', '', '', '', '', '', '', '', '', 'Active', '2018-09-11 19:44:11'),
(16, 17, '', '', '', '', '', '', '', '', '', '', '', 'Active', '2018-09-11 19:44:12'),
(17, 18, 'faizan co.', '', '', '', '', '', '', '', '', '', '', 'Active', '2018-09-16 17:57:56');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `ID` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `acc_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`ID`, `user_id`, `acc_id`, `name`, `createdat`) VALUES
(1, 1, 1, 'Category 1', '2018-09-18 17:13:37'),
(2, 1, 1, 'category 2', '2018-09-18 17:13:37');

-- --------------------------------------------------------

--
-- Table structure for table `categories_items`
--

CREATE TABLE `categories_items` (
  `ID` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories_items`
--

INSERT INTO `categories_items` (`ID`, `cat_id`, `item_id`) VALUES
(1, 1, 17),
(2, 1, 18),
(3, 1, 19),
(4, 2, 18),
(5, 1, 19),
(6, 2, 18);

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `ID` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `url` varchar(350) NOT NULL,
  `typeoffile` varchar(250) NOT NULL,
  `origname` varchar(250) NOT NULL,
  `size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`ID`, `uid`, `name`, `url`, `typeoffile`, `origname`, `size`) VALUES
(8, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'corporate-event-planner.png', 0),
(9, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'fbauth.PNG', 0),
(10, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'fill-form-arrow.png', 0),
(11, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'Capture1.PNG', 0),
(12, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'corporate-photography.png', 0),
(13, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'dev.bbid performance.PNG', 0),
(14, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'glass-almunium.png', 0),
(15, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'glass-works-2.png', 0),
(16, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'corporate-event-planner.png', 0),
(17, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'corporate-photography.png', 0),
(18, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'dev.bbid performance.PNG', 0),
(19, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'corporate-event-planner.png', 0),
(20, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'corporate-photography.png', 0),
(21, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'dev.bbid performance.PNG', 0),
(22, 1, '1100000.jpeg', 'uploads/reqdocs', 'image/jpeg', 'amir.jpg', 0),
(23, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'corporate-event-planner.png', 0),
(24, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'corporate-event-planner.png', 0),
(25, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'Capture.PNG', 0),
(26, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'glass-works-2.png', 0),
(27, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'Capture.PNG', 0),
(28, 1, '125467.png', 'uploads/reqdocs', 'image/png', 'residential-fitout.png', 0),
(29, 1, '189065.jpeg', 'uploads/reqdocs', 'image/jpeg', 'travelling_in_style.jpg', 0),
(30, 1, '1818182.png', 'uploads/reqdocs', 'image/png', 'uniforms.png', 0),
(31, 1, '1834678.png', 'uploads/reqdocs', 'image/png', 'corporate-event-planner.png', 0),
(32, 1, '1677845.png', 'uploads/reqdocs', 'image/png', 'corporate-photography.png', 0),
(33, 1, '1150124.png', 'uploads/reqdocs', 'image/png', 'dev.bbid performance.PNG', 0),
(34, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'corporate-event-planner.png', 0),
(35, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'corporate-photography.png', 0),
(36, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'dev.bbid performance.PNG', 0),
(37, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'corporate-photography.png', 0),
(38, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'corporate-photography.png', 0),
(39, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'pending.PNG', 0),
(40, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'uniforms.png', 0),
(41, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'outdoor-gardens-build.png', 0),
(42, 1, '199999.jpeg', 'uploads/reqdocs', 'image/jpeg', 'amir.jpg', 0),
(43, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'Capture.PNG', 0),
(44, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'Capture1.PNG', 0),
(45, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'corporate-photography.png', 0),
(46, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'dev.bbid performance.PNG', 0),
(47, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'office-flowers.png', 0),
(48, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'residential-interior-design-  square-images.png', 0),
(49, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'uniforms.png', 0),
(50, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'office-flowers.png', 0),
(51, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'office-flowers-2.png', 0),
(52, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'corporate-event-planner.png', 0),
(53, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'corporate-photography.png', 0),
(54, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'fbauth.PNG', 0),
(55, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'office-flowers.png', 0),
(56, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'office-flowers-1.png', 0),
(57, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'office-flowers-1.png', 0),
(58, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'residential-fitout.png', 0),
(59, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'glass-almunium.png', 0),
(60, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'glass-works-2.png', 0),
(61, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'google.png', 0),
(62, 1, '1100000.jpeg', 'uploads/reqdocs', 'image/jpeg', 's_XL_22357418_30310603.jpg', 0),
(63, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'glass-almunium.png', 0),
(64, 1, '1172844.png', 'uploads/reqdocs', 'image/png', 'dev.bbid performance.PNG', 0),
(65, 1, '1838998.jpeg', 'uploads/reqdocs', 'image/jpeg', 'finance-blog.jpg', 0),
(66, 1, '1297984.png', 'uploads/reqdocs', 'image/png', 'Capture1.PNG', 0),
(67, 1, '199999.jpeg', 'uploads/reqdocs', 'image/jpeg', 'fblog.jpg', 0),
(68, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'fill-form-arrow.png', 0),
(69, 1, '199999.jpeg', 'uploads/reqdocs', 'image/jpeg', 'finance-blog.jpg', 0),
(70, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'dev.bbid performance.PNG', 0),
(71, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'fill-form-arrow.png', 0),
(72, 1, '1100000.jpeg', 'uploads/reqdocs', 'image/jpeg', 'finance-blog.jpg', 0),
(73, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'residential-interior-design-  square-images.png', 0),
(74, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'residential-fitout.png', 0),
(75, 1, '1100000.jpeg', 'uploads/reqdocs', 'image/jpeg', 'fblog.jpg', 0),
(76, 1, '1100000.jpeg', 'uploads/reqdocs', 'image/jpeg', 'fblog.jpg', 0),
(77, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'fill-form-arrow.png', 0),
(78, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'office-flowers.png', 0),
(79, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'residential-fitout.png', 0),
(80, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'residential-interior-design-  square-images.png', 0),
(81, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'residential-fitout.png', 0),
(82, 1, '1100000.png', 'uploads/reqdocs', 'image/png', 'residential-interior-design-  square-images.png', 0),
(83, 1, '199999.png', 'uploads/reqdocs', 'image/png', 'residential-fitout.png', 0),
(84, 1, '197282.png', 'uploads/reqdocs', 'image/png', 'residential-interior-design-  square-images.png', 0),
(85, 1, '1198192.jpeg', 'uploads/reqdocs', 'image/jpeg', 'travelling_in_style.jpg', 0),
(86, 1, '1812502.png', 'uploads/reqdocs', 'image/png', 'dev.bbid performance.PNG', 0),
(87, 1, '1952235.png', 'uploads/reqdocs', 'image/png', 'residential-interior-design-  square-images.png', 0),
(88, 1, '1698843.png', 'uploads/reqdocs', 'image/png', 'residential-fitout.png', 0),
(89, 1, '1419408.png', 'uploads/requests', 'image/png', 'residential-fitout.png', 0),
(90, 1, '1102690.png', 'uploads/requests', 'image/png', 'residential-fitout.png', 0),
(91, 1, '1808574.png', 'uploads/requests', 'image/png', 'residential-fitout.png', 0),
(92, 1, '1416719.png', 'uploads/requests', 'image/png', 'corporate-photography.png', 0),
(93, 1, '1202210.png', 'uploads/requests', 'image/png', 'office-flowers-1.png', 0),
(94, 1, '1389316.png', 'uploads/requests', 'image/png', 'office-flowers.png', 0),
(95, 1, '1844588.png', 'uploads/requests', 'image/png', 'uniforms.png', 0),
(96, 5, '5843500.png', 'uploads/reqdocs', 'image/png', 'corporate-photography.png', 0),
(97, 5, '5587570.png', 'uploads/reqdocs', 'image/png', 'fbauth.PNG', 0),
(98, 5, '5395812.jpeg', 'uploads/reqdocs', 'image/jpeg', 'finance-blog.jpg', 0),
(99, 5, '5578083.png', 'uploads/reqdocs', 'image/png', 'contracting-services.png', 0),
(100, 5, '511449.png', 'uploads/reqdocs', 'image/png', 'residential-interior-design-  square-images.png', 0),
(101, 5, '5788543.png', 'uploads/reqdocs', 'image/png', 'residential-fitout.png', 0),
(102, 6, '6597329.png', 'uploads/reqdocs', 'image/png', 'residential-fitout.png', 0),
(103, 6, '6299343.png', 'uploads/reqdocs', 'image/png', 'residential-interior-design-  square-images.png', 0),
(104, 6, '6226018.png', 'uploads/reqdocs', 'image/png', 'box_design.png', 0),
(105, 7, '7171393.png', 'uploads/reqdocs', 'image/png', 'residential-fitout.png', 0),
(106, 9, '9649445.png', 'uploads/reqdocs', 'image/png', 'corporate-photography.png', 0),
(107, 1, '1986556.png', 'uploads/requests', 'image/png', 'office-flowers-1.png', 0),
(108, 1, '1727363.png', 'uploads/requests', 'image/png', 'office-flowers.png', 0),
(109, 1, '1365569.png', 'uploads/requests', 'image/png', 'office-flowers-1.png', 0),
(110, 1, '1267107.png', 'uploads/requests', 'image/png', 'office-flowers.png', 0),
(111, 1, '1596090.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 0),
(112, 1, '1179793.png', 'uploads/requests', 'image/png', 'uniforms.png', 0),
(113, 1, '1761111.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 0),
(114, 1, '1749177.png', 'uploads/requests', 'image/png', 'uniforms.png', 0),
(115, 1, '1732288.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 0),
(116, 1, '1237196.png', 'uploads/requests', 'image/png', 'uniforms.png', 0),
(117, 1, '1892474.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 0),
(118, 1, '1774676.png', 'uploads/requests', 'image/png', 'uniforms.png', 0),
(119, 1, '1379044.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 0),
(120, 1, '1529290.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 0),
(121, 1, '1493791.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 0),
(122, 1, '1568869.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 0),
(123, 1, '1136256.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 0),
(124, 1, '1857398.png', 'uploads/requests', 'image/png', 'residential-interior-design-  square-images.png', 0),
(125, 1, '1431795.png', 'uploads/requests', 'image/png', 'residential-interior-design-  square-images.png', 0),
(126, 1, '1286714.png', 'uploads/requests', 'image/png', 'Capture.PNG', 0),
(127, 1, '1117917.png', 'uploads/requests', 'image/png', 'Capture.PNG', 0),
(128, 1, '1272122.png', 'uploads/requests', 'image/png', 'residential-interior-design-  square-images.png', 0),
(129, 1, '1705520.jpeg', 'uploads/requests', 'image/jpeg', 'finance-blog.jpg', 0),
(130, 1, '1851083.png', 'uploads/requests', 'image/png', 'glass-works-2.png', 0),
(131, 1, '1382276.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 0),
(132, 1, '192750.jpeg', 'uploads/requests', 'image/jpeg', 'fblog.jpg', 0),
(133, 1, '1570258.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 0),
(134, 1, '1318679.jpeg', 'uploads/requests', 'image/jpeg', 'fblog.jpg', 0),
(135, 1, '1946736.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 0),
(136, 1, '1192301.png', 'uploads/requests', 'image/png', 'download.png', 0),
(137, 1, '1422912.png', 'uploads/requests', 'image/png', 'Capture3.PNG', 0),
(138, 1, '1175321.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 0),
(139, 1, '188098.png', 'uploads/requests', 'image/png', 'download.png', 0),
(140, 1, '1217800.png', 'uploads/requests', 'image/png', 'Capture3.PNG', 0),
(141, 1, '1471131.png', 'uploads/requests', 'image/png', 'Capture1.PNG', 0),
(142, 1, '148066.png', 'uploads/requests', 'image/png', 'Capture3.PNG', 0),
(143, 1, '1353635.png', 'uploads/requests', 'image/png', 'download.png', 0),
(144, 1, '1250218.png', 'uploads/requests', 'image/png', 'download.png', 0),
(145, 1, '1541708.jpeg', 'uploads/requests', 'image/jpeg', 'fblog.jpg', 0),
(146, 1, '1966042.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 0),
(147, 1, '1703344.png', 'uploads/requests', 'image/png', 'office-flowers-1.png', 0),
(148, 1, '1268919.png', 'uploads/requests', 'image/png', 'office-flowers.png', 0),
(149, 1, '1941479.png', 'uploads/requests', 'image/png', 'pending.PNG', 0),
(150, 1, '1578355.png', 'uploads/requests', 'image/png', 'glass-works-2.png', 0),
(151, 1, '1520619.png', 'uploads/requests', 'image/png', 'office-flowers-1.png', 0),
(152, 1, '1646938.png', 'uploads/requests', 'image/png', 'Capture1.PNG', 0),
(153, 1, '1354511.png', 'uploads/requests', 'image/png', 'Capture.PNG', 0),
(154, 1, '1577660.png', 'uploads/requests', 'image/png', 'glass-work-banner.png', 212489),
(155, 1, '1429408.png', 'uploads/requests', 'image/png', 'flooring-banner.png', 349240),
(156, 1, '1892897.jpeg', 'uploads/requests', 'image/jpeg', 'index-footer.jpg', 27371),
(157, 1, '1356717.jpeg', 'uploads/requests', 'image/jpeg', 'index.jpg', 36295),
(158, 1, '1398078.jpeg', 'uploads/requests', 'image/jpeg', 'index.jpg', 36295),
(159, 1, '1849482.jpeg', 'uploads/requests', 'image/jpeg', 'index.jpg', 36295),
(160, 1, '190364.png', 'uploads/requests', 'image/png', 'residential-fitout.png', 104007),
(161, 1, '1374028.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 175344),
(162, 1, '1245626.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 175344),
(163, 1, '1344904.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 175344),
(164, 1, '1180216.png', 'uploads/requests', 'image/png', 'corporate-photography.png', 91235),
(165, 1, '1343151.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 175344),
(166, 1, '1638901.png', 'uploads/requests', 'image/png', 'office-flowers-1.png', 104540),
(167, 1, '1375720.png', 'uploads/requests', 'image/png', 'office-flowers-2.png', 46765),
(168, 1, '1136105.png', 'uploads/requests', 'image/png', 'residential-interior-design-  square-images.png', 77841),
(169, 1, '1616272.png', 'uploads/requests', 'image/png', 'office-flowers.png', 104540),
(170, 1, '1461494.png', 'uploads/requests', 'image/png', 'office-flowers-1.png', 104540),
(171, 1, '1590833.png', 'uploads/requests', 'image/png', 'office-flowers-2.png', 46765),
(172, 1, '1565847.png', 'uploads/requests', 'image/png', 'corporate-photography.png', 91235),
(173, 1, '1858818.png', 'uploads/requests', 'image/png', 'corporate-photography.png', 91235),
(174, 1, '1230791.png', 'uploads/requests', 'image/png', 'corporate-photography.png', 91235),
(175, 1, '1867640.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 175344),
(176, 1, '1140516.png', 'uploads/requests', 'image/png', 'dev.bbid performance.PNG', 175344),
(177, 1, '1282032.png', 'uploads/requests', 'image/png', 'fbauth.PNG', 14422),
(178, 1, '1708964.png', 'uploads/requests', 'image/png', 'Capture1.PNG', 175234),
(179, 1, '1254387.png', 'uploads/requests', 'image/png', 'doc-div.PNG', 175344),
(180, 1, '1523308.png', 'uploads/requests', 'image/png', 'doc-res.png', 104007),
(181, 1, '1675005.png', 'uploads/requests', 'image/png', 'outdoor-gardens-build.png', 377689),
(182, 1, '1552131.png', 'uploads/requests', 'image/png', 'pending.PNG', 77526),
(183, 1, '1694039.png', 'uploads/requests', 'image/png', 'corporate-photography.png', 91235),
(184, 1, '1554004.png', 'uploads/requests', 'image/png', 'glass-works-2.png', 92437),
(185, 1, '1681652.png', 'uploads/requests', 'image/png', 'glass-works-2.png', 92437),
(186, 1, '1533158.png', 'uploads/requests', 'image/png', 'outdoor-gardens-build.png', 377689),
(187, 1, '1934047.png', 'uploads/requests', 'image/png', 'Capture.PNG', 148830),
(188, 1, '1851053.png', 'uploads/requests', 'image/png', 'Capture1.PNG', 175234),
(189, 1, '123352.png', 'uploads/requests', 'image/png', 'glass-works-2.png', 92437),
(190, 1, '1198857.png', 'uploads/requests', 'image/png', 'fill-form-arrow.png', 19339),
(191, 1, '1379557.png', 'uploads/requests', 'image/png', 'handyman-logo.png', 13799),
(192, 1, '1941056.png', 'uploads/requests', 'image/png', 'firstpage-watermark.png', 63905),
(193, 1, '1878033.png', 'uploads/requests', 'image/png', 'fbauth.PNG', 14422),
(194, 1, '117219.png', 'uploads/requests', 'image/png', 'glass-almunium.png', 200017),
(195, 1, '1814254.png', 'uploads/requests', 'image/png', 'glass-works-2.png', 92437),
(196, 1, '1948277.png', 'uploads/requests', 'image/png', 'auditing.png', 46361),
(197, 1, '1437595.png', 'uploads/requests', 'image/png', 'business-funding.png', 34183),
(198, 1, '1940482.png', 'uploads/requests', 'image/png', 'carpenter.png', 65717),
(199, 18, '18842775.png', 'uploads/requests', 'image/png', 'business-funding.png', 34183),
(200, 18, '1855619.png', 'uploads/requests', 'image/png', 'business-funding.png', 34183),
(201, 18, '18560319.png', 'uploads/requests', 'image/png', 'outdoor-gardens-build.png', 377689),
(202, 18, '18735702.png', 'uploads/requests', 'image/png', 'office-flowers-2.png', 46765),
(203, 18, '18170729.png', 'uploads/requests', 'image/png', 'business-funding.png', 34183),
(204, 18, '18948790.png', 'uploads/requests', 'image/png', 'carpenter.png', 65717),
(205, 18, '18849361.png', 'uploads/requests', 'image/png', 'corporate-catering.png', 61999),
(206, 18, '18453004.png', 'uploads/requests', 'image/png', 'business-funding.png', 34183),
(207, 18, '18423426.png', 'uploads/requests', 'image/png', 'carpenter.png', 65717),
(208, 18, '18587087.png', 'uploads/requests', 'image/png', 'corporate-catering.png', 61999),
(209, 18, '1844531.png', 'uploads/requests', 'image/png', 'business-funding.png', 34183),
(210, 18, '18697513.png', 'uploads/requests', 'image/png', 'carpenter.png', 65717),
(211, 18, '1820059.png', 'uploads/requests', 'image/png', 'corporate-catering.png', 61999),
(212, 18, '18152873.png', 'uploads/requests', 'image/png', 'business-funding.png', 34183),
(213, 18, '18843319.png', 'uploads/requests', 'image/png', 'cleaning-services.png', 30639),
(214, 9, '9908124.png', 'uploads/requests', 'image/png', 'document-storage.png', 32406),
(215, 11, '11160215.png', 'uploads/requests', 'image/png', 'business-funding.png', 34183),
(216, 1, '1279826.png', 'uploads/requests', 'image/png', 'business-funding.png', 34183),
(217, 1, '1418954.png', 'uploads/requests', 'image/png', 'business-funding.png', 34183),
(218, 1, '1631892.png', 'uploads/requests', 'image/png', 'cleaning-services.png', 30639),
(219, 1, '196467.png', 'uploads/requests', 'image/png', 'business-funding.png', 34183),
(220, 13, '13608719.png', 'uploads/requests', 'image/png', 'business-funding.png', 34183),
(221, 13, '13287893.png', 'uploads/requests', 'image/png', 'cleaning-services.png', 30639),
(222, 13, '1314319.png', 'uploads/requests', 'image/png', 'document-storage.png', 32406),
(223, 14, '14593401.png', 'uploads/requests', 'image/png', 'business-funding.png', 34183),
(224, 14, '14644672.png', 'uploads/requests', 'image/png', 'cleaning-services.png', 30639),
(225, 14, '14401431.png', 'uploads/requests', 'image/png', 'corporate-catering.png', 61999),
(226, 13, '13137435.png', 'uploads/requests', 'image/png', 'outdoor-gardens-build.png', 377689),
(227, 1, '1799903.png', 'uploads/requests', 'image/png', 'moving.png', 204512),
(228, 1, '1460708.png', 'uploads/requests', 'image/png', 'residential-interior-design-  square-images.png', 77841),
(229, 1, '1678902.png', 'uploads/requests', 'image/png', 'fbauth.PNG', 14422),
(230, 1, '1590229.png', 'uploads/requests', 'image/png', 'glass-almunium.png', 200017),
(231, 1, '137190.png', 'uploads/requests', 'image/png', 'glass-works-2.png', 92437),
(232, 1, '1809934.png', 'uploads/requests', 'image/png', 'glass-works-2.png', 92437),
(234, 18, '18692196.png', 'uploads/requests', 'image/png', 'Capture.PNG', 148830),
(235, 18, '18182965.png', 'uploads/requests', 'image/png', 'corporate-catering.png', 94972),
(236, 18, '18101421.png', 'uploads/requests', 'image/png', 'corporate-catering.png', 94972),
(237, 18, '18991058.png', 'uploads/requests', 'image/png', 'glass-works-2.png', 92437),
(238, 18, '18109579.png', 'uploads/requests', 'image/png', 'glass-almunium.png', 200017),
(239, 18, '18407262.png', 'uploads/requests', 'image/png', 'Capture1.PNG', 175234),
(240, 1, '1941962.png', 'uploads/requests', 'image/png', 'Capture1.PNG', 175234),
(241, 1, '1383545.png', 'uploads/requests', 'image/png', 'Capture.PNG', 148830),
(242, 18, '18620743.png', 'uploads/requests', 'image/png', 'Capture1.PNG', 175234),
(243, 18, '18144504.png', 'uploads/requests', 'image/png', 'glass-works-2.png', 92437),
(244, 18, '18132208.png', 'uploads/requests', 'image/png', 'glass-almunium.png', 200017),
(245, 1, '1680111.png', 'uploads/requests', 'image/png', 'glass-works-2.png', 92437),
(246, 1, '1103204.png', 'uploads/requests', 'image/png', 'glass-almunium.png', 200017),
(247, 18, '18239432.png', 'uploads/requests', 'image/png', 'glass-works-2.png', 92437),
(248, 18, '18238979.png', 'uploads/requests', 'image/png', 'glass-almunium.png', 200017),
(249, 1, '1479258.png', 'uploads/requests', 'image/png', 'glass-works-2.png', 92437),
(250, 1, '1856582.png', 'uploads/requests', 'image/png', 'fill-form-arrow.png', 19339),
(251, 1, '1884287.png', 'uploads/requests', 'image/png', 'Capture1.PNG', 175234),
(252, 1, '1733013.png', 'uploads/requests', 'image/png', 'Capture.PNG', 148830);

-- --------------------------------------------------------

--
-- Table structure for table `document_request`
--

CREATE TABLE `document_request` (
  `ID` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `rid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document_request`
--

INSERT INTO `document_request` (`ID`, `uid`, `did`, `rid`) VALUES
(49, 1, 150, 21),
(50, 1, 151, 21),
(51, 1, 152, 21),
(52, 1, 153, 21),
(61, 1, 154, 50),
(62, 1, 155, 50),
(63, 1, 156, 49),
(64, 1, 157, 49),
(65, 1, 158, 56),
(66, 1, 158, 57),
(67, 1, 159, 51),
(68, 1, 160, 61),
(75, 1, 178, 66),
(76, 1, 179, 66),
(77, 1, 180, 66),
(79, 1, 186, 67),
(80, 1, 192, 54),
(81, 1, 196, 68),
(82, 1, 197, 68),
(83, 1, 198, 68),
(84, 1, 227, 70),
(85, 1, 228, 70),
(86, 1, 229, 68),
(87, 1, 230, 68),
(88, 1, 240, 71),
(89, 1, 241, 71),
(90, 1, 245, 72),
(91, 1, 246, 72);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `ID` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups_roles`
--

CREATE TABLE `groups_roles` (
  `ID` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `ID` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `accid` int(11) NOT NULL,
  `parentid` int(11) NOT NULL,
  `lotnum` int(11) NOT NULL,
  `name` varchar(350) NOT NULL,
  `description` longtext NOT NULL,
  `notes` longtext NOT NULL,
  `unit` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `saved` enum('YES','NO') NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL,
  `itemprice` double NOT NULL,
  `unit_label` varchar(100) NOT NULL,
  `unit_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`ID`, `uid`, `accid`, `parentid`, `lotnum`, `name`, `description`, `notes`, `unit`, `quantity`, `saved`, `createdat`, `status`, `itemprice`, `unit_label`, `unit_amount`) VALUES
(1, 1, 1, 1, 1, 'item', '', '', 0, 1, 'YES', '2018-09-17 13:39:35', 1, 0, '', 0),
(2, 1, 1, 1, 2, 'item', '', '', 0, 1, 'YES', '2018-09-17 13:40:07', 1, 0, '', 0),
(3, 1, 1, 1, 2, 'item2', '', '', 0, 2, 'YES', '2018-09-17 13:40:07', 1, 0, '', 0),
(4, 1, 1, 1, 3, 'item3', '', '', 0, 3, 'YES', '2018-09-17 13:40:07', 1, 0, '', 0),
(5, 1, 1, 1, 1, 'item', '', '', 0, 1, 'YES', '2018-09-17 14:51:28', 1, 0, '', 0),
(6, 1, 1, 1, 2, 'item2', '', '', 0, 2, 'YES', '2018-09-17 14:51:28', 1, 0, '', 0),
(7, 1, 1, 1, 3, 'item3', '', '', 0, 3, 'YES', '2018-09-17 14:51:28', 1, 0, '', 0),
(8, 1, 1, 1, 2, 'item', '', '', 0, 1, 'YES', '2018-09-17 14:51:59', 1, 0, '', 0),
(9, 1, 1, 1, 2, 'item2', '', '', 0, 2, 'YES', '2018-09-17 14:51:59', 1, 0, '', 0),
(10, 1, 1, 1, 3, 'item3', '', '', 0, 3, 'YES', '2018-09-17 14:51:59', 1, 0, '', 0),
(11, 1, 1, 1, 2, 'item', '', '', 0, 1, 'YES', '2018-09-17 14:52:15', 1, 0, '', 0),
(12, 1, 1, 1, 2, 'item2', '', '', 0, 2, 'YES', '2018-09-17 14:52:15', 1, 0, '', 0),
(13, 1, 1, 1, 3, 'item3', '', '', 0, 3, 'YES', '2018-09-17 14:52:15', 1, 0, '', 0),
(14, 1, 1, 1, 5, 'item4', '', '', 0, 20, 'YES', '2018-09-17 15:00:00', 1, 5, '', 0),
(15, 1, 1, 1, 20, 'item new', '', '', 0, 3, 'YES', '2018-09-17 15:59:20', 1, 600, '', 0),
(16, 1, 1, 1, 2, 'item 1', '', '', 3, 1, 'YES', '2018-09-18 10:50:36', 1, 1, 'item label unit', 1),
(17, 1, 1, 1, 2, 'item 2', '', '', 13, 1, 'YES', '2018-09-18 10:50:36', 1, 2, 'new label', 3),
(18, 1, 1, 1, 2, 'item', '', '', 14, 1, 'YES', '2018-09-18 12:28:41', 1, 4, 'label', 2),
(19, 1, 1, 1, 41, 'item ext get this', ' ', '', 42, 1, 'YES', '2018-09-18 12:28:41', 1, 0, 'square inches', 1),
(20, 1, 1, 1, 0, '', '', '', 2, 1, 'YES', '2018-09-19 11:13:15', 1, 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `item_documents`
--

CREATE TABLE `item_documents` (
  `ID` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `doc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_documents`
--

INSERT INTO `item_documents` (`ID`, `item_id`, `doc_id`) VALUES
(1, 2, 163),
(3, 3, 165),
(4, 14, 166),
(5, 14, 167),
(7, 15, 169),
(10, 16, 181),
(11, 16, 182),
(12, 17, 183),
(13, 18, 184),
(14, 19, 185);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `ID` int(11) NOT NULL,
  `mto` int(11) NOT NULL,
  `subject` text NOT NULL,
  `message` longtext NOT NULL,
  `mfrom` int(11) NOT NULL,
  `sentdate` datetime NOT NULL,
  `readdate` datetime NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `optionlist`
--

CREATE TABLE `optionlist` (
  `ID` int(11) NOT NULL,
  `qid` int(11) NOT NULL,
  `value` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `optionlist`
--

INSERT INTO `optionlist` (`ID`, `qid`, `value`) VALUES
(17, 44, 'opt 1'),
(18, 44, 'opt 2'),
(19, 44, 'opt3'),
(44, 111, 'optin 1'),
(45, 111, 'option 2'),
(46, 111, 'option 3'),
(47, 112, 'option 1'),
(48, 112, 'option 2'),
(49, 112, 'option 3'),
(50, 112, 'option 4'),
(57, 134, 'val 1'),
(58, 134, 'val 2'),
(59, 134, 'val 3'),
(60, 138, 'value 1'),
(61, 138, 'value 2'),
(62, 138, 'value 3'),
(64, 143, 'value 1'),
(65, 143, 'value 2'),
(66, 143, 'value 3'),
(70, 193, '5'),
(71, 193, '3'),
(72, 193, '4'),
(73, 215, 'option 1 '),
(74, 215, 'option 2'),
(75, 215, 'option 3'),
(76, 215, 'option 4'),
(77, 212, 'option 11'),
(78, 212, 'option 12'),
(79, 212, 'option 13'),
(83, 219, '5'),
(84, 219, '3'),
(85, 219, '4');

-- --------------------------------------------------------

--
-- Table structure for table `package`
--

CREATE TABLE `package` (
  `ID` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `packageplan` varchar(250) NOT NULL,
  `startdate` date NOT NULL,
  `enddate` date NOT NULL,
  `publishrequests` int(11) NOT NULL,
  `paymentmethod` varchar(250) NOT NULL,
  `status` enum('Active','Block','Pending','Rejected') NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questanswers_document`
--

CREATE TABLE `questanswers_document` (
  `ID` int(11) NOT NULL,
  `qans_id` int(11) NOT NULL,
  `doc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questanswers_document`
--

INSERT INTO `questanswers_document` (`ID`, `qans_id`, `doc_id`) VALUES
(1, 13, 200),
(2, 14, 201),
(3, 14, 202),
(4, 21, 203),
(5, 22, 204),
(6, 22, 205),
(7, 24, 206),
(8, 25, 207),
(9, 25, 208),
(10, 27, 209),
(12, 28, 211),
(15, 36, 214),
(16, 40, 215),
(17, 42, 220),
(18, 42, 221),
(19, 43, 222),
(20, 45, 223),
(21, 46, 224),
(22, 46, 225),
(27, 125, 242);

-- --------------------------------------------------------

--
-- Table structure for table `questions_answers`
--

CREATE TABLE `questions_answers` (
  `ID` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `rid` int(11) NOT NULL,
  `qid` int(11) NOT NULL,
  `ques_ans` longtext NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions_answers`
--

INSERT INTO `questions_answers` (`ID`, `uid`, `rid`, `qid`, `ques_ans`, `createdat`) VALUES
(26, 18, 54, 212, 'option 11', '2018-09-23 18:39:14'),
(27, 18, 54, 214, 'Yes', '2018-09-23 18:39:14'),
(28, 18, 54, 215, 'option 2', '2018-09-23 18:39:15'),
(29, 18, 68, 217, 'Yes', '2018-09-23 19:18:21'),
(30, 18, 68, 219, '3', '2018-09-23 19:18:21'),
(31, 18, 68, 220, 'Yes', '2018-09-23 19:18:21'),
(32, 18, 68, 222, 'No', '2018-09-23 19:18:21'),
(33, 9, 68, 217, 'Yes', '2018-09-23 19:19:17'),
(34, 9, 68, 219, '5', '2018-09-23 19:19:17'),
(35, 9, 68, 220, 'No', '2018-09-23 19:19:17'),
(36, 9, 68, 222, 'Yes', '2018-09-23 19:19:17'),
(37, 11, 68, 217, 'No', '2018-09-23 19:20:39'),
(38, 11, 68, 219, '3', '2018-09-23 19:20:39'),
(39, 11, 68, 220, 'Yes', '2018-09-23 19:20:39'),
(40, 11, 68, 222, 'Yes', '2018-09-23 19:20:39'),
(41, 13, 54, 212, 'option 12 ,option 13', '2018-09-24 17:00:04'),
(42, 13, 54, 214, 'Yes', '2018-09-24 17:00:04'),
(43, 13, 54, 215, 'option 2', '2018-09-24 17:00:05'),
(45, 14, 54, 214, 'No', '2018-09-24 17:01:35'),
(46, 14, 54, 215, 'option 2', '2018-09-24 17:01:35'),
(98, 13, 54, 212, 'option 11 ,option 13', '2018-10-07 11:05:30'),
(99, 13, 54, 214, 'Yes', '2018-10-07 11:05:30'),
(100, 13, 54, 215, 'option 2', '2018-10-07 11:05:31'),
(101, 13, 54, 212, 'option 12', '2018-10-07 11:05:51'),
(102, 13, 54, 214, 'Yes', '2018-10-07 11:05:51'),
(103, 13, 54, 215, 'option 2', '2018-10-07 11:05:51'),
(104, 13, 54, 212, 'option 12', '2018-10-07 11:06:18'),
(105, 13, 54, 214, 'Yes', '2018-10-07 11:06:18'),
(106, 13, 54, 215, 'option 2', '2018-10-07 11:06:18'),
(121, 18, 61, 155, 'No', '2018-10-07 11:53:32'),
(122, 18, 61, 157, 'No', '2018-10-07 11:53:32'),
(123, 18, 61, 158, 'No', '2018-10-07 11:53:32'),
(124, 18, 71, 234, 'Yes', '2018-10-07 15:54:41'),
(125, 18, 71, 236, 'No', '2018-10-07 15:54:41'),
(126, 18, 71, 237, 'No', '2018-10-07 15:54:42'),
(127, 18, 72, 239, 'Yes', '2018-10-07 17:08:04'),
(128, 18, 72, 241, 'No', '2018-10-07 17:08:04'),
(129, 18, 72, 242, 'No', '2018-10-07 17:08:04');

-- --------------------------------------------------------

--
-- Table structure for table `questions_documents`
--

CREATE TABLE `questions_documents` (
  `ID` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `qid` int(11) NOT NULL,
  `doc_name` varchar(350) NOT NULL,
  `doc_url` varchar(350) NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `q_answer`
--

CREATE TABLE `q_answer` (
  `ID` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `qid` int(11) NOT NULL,
  `rid` int(11) NOT NULL,
  `q_ans` varchar(250) NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `ID` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `accid` int(11) NOT NULL,
  `name` varchar(350) NOT NULL,
  `description` longtext NOT NULL,
  `endtime` datetime NOT NULL,
  `timezone` varchar(250) NOT NULL,
  `starttime` datetime NOT NULL,
  `exttime` int(11) NOT NULL,
  `lastperiod` int(11) NOT NULL,
  `highestbid` int(11) NOT NULL,
  `lowestbid` int(11) NOT NULL,
  `tiedbid` enum('equalbest','equalworst','bytimestamp','') NOT NULL,
  `reqtype` enum('RFQ','RFI','AUCTION') NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL,
  `privacy` enum('public','restricted','private','') NOT NULL,
  `termscond` text NOT NULL,
  `currency` varchar(50) NOT NULL,
  `mcurrency` tinyint(4) NOT NULL,
  `payterms` text NOT NULL,
  `isauction` tinyint(4) NOT NULL,
  `endsin` enum('order','contract') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`ID`, `uid`, `accid`, `name`, `description`, `endtime`, `timezone`, `starttime`, `exttime`, `lastperiod`, `highestbid`, `lowestbid`, `tiedbid`, `reqtype`, `createdat`, `status`, `privacy`, `termscond`, `currency`, `mcurrency`, `payterms`, `isauction`, `endsin`) VALUES
(21, 1, 1, 'RFI Go', 'Description', '2018-09-12 00:00:00', '4', '0000-00-00 00:00:00', 0, 0, 0, 0, 'equalbest', 'RFI', '2018-09-10 12:28:08', 1, 'public', '', '', 0, '', 0, 'order'),
(49, 1, 1, 'New RFi', 'description', '2018-09-18 00:00:00', '5', '0000-00-00 00:00:00', 0, 0, 0, 0, 'equalbest', 'RFI', '2018-09-12 09:38:26', 1, 'public', '', '', 0, '', 0, 'order'),
(50, 1, 1, 'RFI Name', 'Description of event', '2018-09-04 00:00:00', '5', '0000-00-00 00:00:00', 0, 0, 0, 0, 'equalbest', 'RFI', '2018-09-12 09:18:27', 1, '', '', '', 0, '', 0, 'order'),
(51, 1, 1, 'Request For Cleaning Material', 'description goes here', '2018-09-12 00:00:00', '5', '0000-00-00 00:00:00', 0, 0, 0, 0, 'equalbest', 'RFI', '2018-09-12 14:40:27', 1, 'public', '', '', 0, '', 0, 'order'),
(53, 1, 1, 'product 2', 'description', '2018-09-16 00:00:00', '3', '0000-00-00 00:00:00', 0, 0, 0, 0, 'equalbest', 'RFQ', '2018-09-16 16:23:45', 0, 'public', '', 'AED', 0, '', 0, 'order'),
(54, 1, 1, 'product name', 'description', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', 0, 0, 0, 0, 'equalbest', 'RFQ', '2018-09-12 15:00:36', 1, 'public', '', 'AED', 0, '', 0, 'order'),
(55, 1, 1, 'product name', 'description', '1970-01-01 00:00:00', '1', '0000-00-00 00:00:00', 0, 0, 0, 0, 'equalbest', 'RFI', '2018-09-12 15:28:18', 0, 'public', '', '', 0, '', 0, 'order'),
(56, 1, 1, 'product name', 'description up', '1970-01-01 00:00:00', '1', '0000-00-00 00:00:00', 0, 0, 0, 0, 'equalbest', 'RFI', '2018-09-16 07:18:08', 0, 'public', '', '', 0, '', 0, 'order'),
(57, 1, 1, 'product name', 'description dup', '1970-01-01 00:00:00', '1', '0000-00-00 00:00:00', 0, 0, 0, 0, 'equalbest', 'RFI', '2018-09-16 07:34:44', 0, 'public', '', '', 0, '', 0, 'order'),
(58, 1, 1, 'item 2', 'description', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', 0, 0, 0, 0, 'equalbest', 'RFQ', '2018-09-16 14:31:30', 0, 'public', '', '', 0, '', 0, 'order'),
(59, 1, 1, 'product 2', 'description', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', 0, 0, 0, 0, 'equalbest', 'RFQ', '2018-09-16 14:31:38', 0, 'public', '', '', 0, '', 0, 'order'),
(61, 1, 1, 'RFQ one', 'description Description questLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, ', '2018-09-10 00:00:00', '4', '0000-00-00 00:00:00', 0, 0, 0, 0, 'equalbest', 'RFQ', '2018-09-18 14:30:42', 1, 'public', 'TermsquestLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, ', 'Pound', 1, 'Payment Terms questLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, ', 0, 'order'),
(66, 1, 1, 'item 2', 'description', '2018-09-19 08:18:18', '1', '0000-00-00 00:00:00', 0, 0, 0, 0, 'equalbest', 'RFQ', '2018-09-17 17:23:48', 1, 'public', 'terms ', 'Pound', 0, 'payment', 0, 'contract'),
(67, 1, 1, 'item 2', 'description', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', 0, 0, 0, 0, 'equalbest', 'RFQ', '2018-09-18 13:10:03', 0, 'public', '', '', 0, '', 0, 'order'),
(68, 1, 1, 'First Auction', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,', '2018-10-02 00:00:00', '', '2018-10-01 06:15:21', 120, 600, 5, 10, 'bytimestamp', 'AUCTION', '2018-09-22 14:32:00', 1, 'private', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,', '28800', 0, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,', 0, 'contract'),
(69, 1, 1, 'new auctoin', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', 600, 300, 5, 10, 'equalworst', 'AUCTION', '2018-09-24 13:43:55', 0, 'public', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries', '7200', 0, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries', 0, 'contract'),
(70, 1, 1, 'RFQ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,', '2018-09-30 00:00:00', '5', '0000-00-00 00:00:00', 0, 0, 0, 0, 'equalbest', 'RFQ', '2018-09-30 15:37:10', 1, 'public', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,', 'Pound', 0, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,', 0, 'contract'),
(71, 1, 1, 'RFQ Request', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ', '2018-10-08 00:00:00', '3', '0000-00-00 00:00:00', 0, 0, 0, 0, 'equalbest', 'RFQ', '2018-10-07 13:37:31', 1, 'private', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ', 'Pound', 0, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ', 1, 'order'),
(72, 1, 1, 'Reverse Auction', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, ', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', 300, 300, 5, 10, 'equalbest', 'AUCTION', '2018-10-07 15:04:04', 1, 'private', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, ', '1800', 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, ', 0, 'order');

-- --------------------------------------------------------

--
-- Table structure for table `request_invitation_message`
--

CREATE TABLE `request_invitation_message` (
  `ID` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `rid` int(11) NOT NULL,
  `name` text NOT NULL,
  `email_message` longtext NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_invitation_message`
--

INSERT INTO `request_invitation_message` (`ID`, `uid`, `rid`, `name`, `email_message`, `createdat`) VALUES
(1, 1, 54, 'Request Email', '<link href=\"/auction/themes/auction/dist/css/style.css\" rel=\"stylesheet\">\n<link href=\"/auction/themes/auction/dist/css/stylesheet.css\" rel=\"stylesheet\" type=\"text/css\">\n\n<div class=\"preview-invitation-email-content\">\n    <div class=\"invitation-email-content-top\">\n    <p>Hi</p><br>\n    <p>I am inviting you to participate on a request below. Click the name of the request to see details and please submit your bid through our online platform.</p>\n    <p><span>product name</span>\n        The deadline for making bids is: Jan 01, 1970 01:00  </p>\n    <p><strong>Description of the request:</strong><br>\n    Annual Contract for Cleaning Services for Office 503, Indigo Icon Tower, Cluster F, JLT. Dubai.</p>\n    </div>\n\n    <div class=\"invitation-email-content-top\">\n        <p><strong>If you would like to participate in this request, please <a href=\"http://localhost/auction/index.php?r=requests/supplierview&id=NTQ%3D\">click here</a> so we will know to wait for your bid.</strong>\n\n    However if this request is of no interest to you, <span>just let me know.</span></p>\n    <div class=\"participate-btn\">\n    <a href=\"#\">Want to Participate</a> <a href=\"#\">Dont want to Participat</a>\n    </div>\n    </div>\n    <div class=\"address\">\n    Kind regards,<br>\n    Muhammad Faizan <br>\n    Faizan co<br>\n    +971527886386    </div>\n</div>', '2018-09-29 14:37:48'),
(2, 1, 70, 'Request Email', '<link href=\"/auction/themes/auction/dist/css/style.css\" rel=\"stylesheet\">\r\n<link href=\"/auction/themes/auction/dist/css/stylesheet.css\" rel=\"stylesheet\" type=\"text/css\">\r\n\r\n<div class=\"preview-invitation-email-content\">\r\n    <div class=\"invitation-email-content-top\">\r\n    <p>Hi</p><br>\r\n    <p>I am inviting you to participate on a request below. Click the name of the request to see details and please submit your bid through our online platform.</p>\r\n    <p><span>RFQ</span>\r\n        The deadline for making bids is: Sep 30, 2018 00:00  (GMT -9:00) Alaska</p>\r\n    <p><strong>Description of the request:</strong><br>\r\n    Annual Contract for Cleaning Services for Office 503, Indigo Icon Tower, Cluster F, JLT. Dubai.</p>\r\n    </div>\r\n\r\n    <div class=\"invitation-email-content-top\">\r\n        <p><strong>If you would like to participate in this request, please <a href=\"http://localhost/auction/index.php?r=requests/supplierview&id=NzA%3D\">click here</a> so we will know to wait for your bid.</strong>\r\n\r\n    However if this request is of no interest to you, <span>just let me know.</span></p>\r\n    <div class=\"participate-btn\">\r\n    <a href=\"#\">Want to Participate</a> <a href=\"#\">Dont want to Participat</a>\r\n    </div>\r\n    </div>\r\n    <div class=\"address\">\r\n    Kind regards,<br>\r\n    Muhammad Faizan <br>\r\n    Faizan co<br>\r\n    +971527886386    </div>\r\n</div>', '2018-09-30 15:42:59'),
(3, 1, 71, 'Request Email', '<link href=\"/auction/themes/auction/dist/css/style.css\" rel=\"stylesheet\">\r\n<link href=\"/auction/themes/auction/dist/css/stylesheet.css\" rel=\"stylesheet\" type=\"text/css\">\r\n\r\n<div class=\"preview-invitation-email-content\">\r\n    <div class=\"invitation-email-content-top\">\r\n    <p>Hi</p><br>\r\n    <p>I am inviting you to participate on a request below. Click the name of the request to see details and please submit your bid through our online platform.</p>\r\n    <p><span>RFQ Request</span>\r\n        The deadline for making bids is: Oct 08, 2018 00:00  (GMT -10:00) Hawaii</p>\r\n    <p><strong>Description of the request:</strong><br>\r\n    Annual Contract for Cleaning Services for Office 503, Indigo Icon Tower, Cluster F, JLT. Dubai.</p>\r\n    </div>\r\n\r\n    <div class=\"invitation-email-content-top\">\r\n        <p><strong>If you would like to participate in this request, please <a href=\"http://localhost/auction/index.php?r=requests/supplierview&id=NzE%3D\">click here</a> so we will know to wait for your bid.</strong>\r\n\r\n    However if this request is of no interest to you, <span>just let me know.</span></p>\r\n    <div class=\"participate-btn\">\r\n    <a href=\"#\">Want to Participate</a> <a href=\"#\">Dont want to Participat</a>\r\n    </div>\r\n    </div>\r\n    <div class=\"address\">\r\n    Kind regards,<br>\r\n    Muhammad Faizan <br>\r\n    Faizan co<br>\r\n    +971527886386    </div>\r\n</div>', '2018-10-07 13:44:39'),
(4, 1, 72, 'Request Email', '<link href=\"/auction/themes/auction/dist/css/style.css\" rel=\"stylesheet\">\r\n<link href=\"/auction/themes/auction/dist/css/stylesheet.css\" rel=\"stylesheet\" type=\"text/css\">\r\n\r\n<div class=\"preview-invitation-email-content\">\r\n    <div class=\"invitation-email-content-top\">\r\n    <p>Hi</p><br>\r\n    <p>I am inviting you to participate on a request below. Click the name of the request to see details and please submit your bid through our online platform.</p>\r\n    <p><span>Reverse Auction</span>\r\n        The deadline for making bids is: Jan 01, 1970 01:00  </p>\r\n    <p><strong>Description of the request:</strong><br>\r\n    Annual Contract for Cleaning Services for Office 503, Indigo Icon Tower, Cluster F, JLT. Dubai.</p>\r\n    </div>\r\n\r\n    <div class=\"invitation-email-content-top\">\r\n        <p><strong>If you would like to participate in this request, please <a href=\"http://localhost/auction/index.php?r=requests/supplierview&id=NzI%3D\">click here</a> so we will know to wait for your bid.</strong>\r\n\r\n    However if this request is of no interest to you, <span>just let me know.</span></p>\r\n    <div class=\"participate-btn\">\r\n    <a href=\"#\">Want to Participate</a> <a href=\"#\">Dont want to Participat</a>\r\n    </div>\r\n    </div>\r\n    <div class=\"address\">\r\n    Kind regards,<br>\r\n    Muhammad Faizan <br>\r\n    Faizan co<br>\r\n    +971527886386    </div>\r\n</div>', '2018-10-07 15:06:40');

-- --------------------------------------------------------

--
-- Table structure for table `request_items`
--

CREATE TABLE `request_items` (
  `ID` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` varchar(150) NOT NULL,
  `item_number` varchar(50) NOT NULL,
  `unit_label` varchar(150) NOT NULL,
  `unit_amount` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_items`
--

INSERT INTO `request_items` (`ID`, `request_id`, `name`, `description`, `item_number`, `unit_label`, `unit_amount`, `unit_id`, `quantity`, `price`) VALUES
(4, 66, '', '', '', '', 0, 0, 0, 0),
(5, 66, '', '', '', '', 0, 0, 0, 0),
(6, 53, '', '', '', '', 0, 0, 0, 0),
(7, 53, '', '', '', '', 0, 0, 0, 0),
(10, 67, '', '', '', '', 0, 0, 0, 0),
(11, 67, '', '', '', '', 0, 0, 0, 0),
(13, 54, 'up item new', '', '1', 'sadf', 1, 6, 21, 20),
(19, 54, 'item4', '', '5', '', 0, 2, 20, 5),
(20, 54, 'item4', '', '5', '', 0, 2, 20, 5),
(21, 54, 'item 2', '', '2', 'new label', 3, 13, 1, 2),
(22, 54, 'new item', '', '4', 'label', 1, 7, 10, 600),
(41, 61, 'item 1', '', 'item number', 'label ', 1, 13, 1, 3),
(44, 61, 'item', '', '2', 'label', 2, 16, 1, 4),
(45, 61, 'item ext get this', ' ', '41', 'square inches', 1, 42, 1, 0),
(46, 61, 'item new', '', 'item s', '', 1, 2, 1, 0),
(52, 68, 'item 1', '', 'item3', '', 1, 2, 1, 0),
(55, 70, 'item 2', '', '2', 'new label', 3, 13, 1, 2),
(56, 70, 'item', '', '2', 'label', 2, 14, 1, 4),
(57, 70, 'item ext get this', ' ', '41', 'square inches', 1, 42, 1, 0),
(58, 68, 'item', '', '2', 'label', 2, 14, 10, 4),
(59, 68, 'item ext get this', ' ', '41', 'square inches', 1, 42, 20, 0),
(60, 71, 'item', '', '2', 'label', 2, 14, 9, 600),
(61, 71, 'item ext get this', ' ', '41', 'square inches', 1, 42, 5, 2500),
(62, 72, 'item', '', '2', 'label', 2, 14, 3, 2500),
(63, 72, 'item4', '', '5', '', 0, 2, 20, 500),
(64, 72, 'item 2', '', '2', 'new label', 3, 13, 1, 400),
(65, 72, 'item', '', '2', 'label', 2, 14, 1, 600),
(66, 72, 'item ext get this', ' ', '41', 'square inches', 1, 42, 1, 900);

-- --------------------------------------------------------

--
-- Table structure for table `request_item_prices`
--

CREATE TABLE `request_item_prices` (
  `ID` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `rid` int(11) NOT NULL,
  `reqitem_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `status` tinyint(4) NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_item_prices`
--

INSERT INTO `request_item_prices` (`ID`, `uid`, `rid`, `reqitem_id`, `price`, `status`, `createdat`) VALUES
(6, 18, 54, 13, 60, 2, '2018-09-23 18:39:15'),
(7, 18, 54, 19, 5, 2, '2018-09-23 18:39:15'),
(8, 18, 54, 20, 30, 2, '2018-09-23 18:39:15'),
(9, 18, 54, 21, 6, 2, '2018-09-23 18:39:16'),
(10, 18, 54, 22, 10, 2, '2018-09-23 18:39:16'),
(20, 13, 54, 13, 40, 2, '2018-09-24 17:00:05'),
(21, 13, 54, 19, 55, 2, '2018-09-24 17:00:05'),
(22, 13, 54, 20, 5, 2, '2018-09-24 17:00:05'),
(23, 13, 54, 21, 55, 2, '2018-09-24 17:00:05'),
(24, 13, 54, 22, 4, 2, '2018-09-24 17:00:06'),
(25, 14, 54, 13, 50, 2, '2018-09-24 17:01:36'),
(26, 14, 54, 19, 20, 2, '2018-09-24 17:01:36'),
(27, 14, 54, 20, 20, 2, '2018-09-24 17:01:36'),
(28, 14, 54, 21, 20, 2, '2018-09-24 17:01:36'),
(29, 14, 54, 22, 80, 2, '2018-09-24 17:01:36'),
(55, 18, 68, 52, 40, 2, '2018-10-03 12:37:19'),
(56, 18, 68, 58, 60, 2, '2018-10-03 12:37:59'),
(57, 18, 68, 59, 0, 0, '2018-10-03 12:41:55'),
(58, 18, 61, 41, 50, 0, '2018-10-07 14:13:43'),
(59, 18, 61, 44, 8, 0, '2018-10-07 14:13:43'),
(60, 18, 61, 45, 60, 0, '2018-10-07 14:13:43'),
(61, 18, 61, 46, 9, 0, '2018-10-07 14:13:43'),
(62, 18, 71, 60, 20, 0, '2018-10-07 15:54:26'),
(63, 18, 71, 61, 50, 0, '2018-10-07 15:54:26'),
(64, 18, 72, 62, 400, 2, '2018-10-07 17:07:32'),
(65, 18, 72, 63, 40, 2, '2018-10-07 17:07:37'),
(66, 18, 72, 64, 40, 2, '2018-10-07 17:07:41'),
(67, 18, 72, 65, 0, 0, '2018-10-07 17:07:48'),
(68, 18, 72, 66, 9000, 2, '2018-10-07 17:07:49'),
(69, 13, 72, 62, 40, 2, '2018-10-08 10:27:22'),
(70, 13, 72, 63, 90, 2, '2018-10-08 10:27:32'),
(71, 13, 72, 64, 40, 2, '2018-10-08 10:27:36'),
(72, 13, 72, 65, 600, 2, '2018-10-08 10:27:40'),
(73, 13, 72, 66, 12000, 2, '2018-10-08 10:27:44');

-- --------------------------------------------------------

--
-- Table structure for table `request_supplier`
--

CREATE TABLE `request_supplier` (
  `ID` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `req_id` int(11) NOT NULL,
  `accept` int(11) NOT NULL,
  `viewed` int(11) NOT NULL,
  `description` text NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_supplier`
--

INSERT INTO `request_supplier` (`ID`, `supplier_id`, `req_id`, `accept`, `viewed`, `description`, `createdat`) VALUES
(1, 9, 51, 0, 0, '', '0000-00-00 00:00:00'),
(2, 10, 51, 0, 0, '', '0000-00-00 00:00:00'),
(3, 11, 51, 0, 0, '', '0000-00-00 00:00:00'),
(4, 18, 61, 3, 1, '', '0000-00-00 00:00:00'),
(5, 18, 61, 0, 0, '', '0000-00-00 00:00:00'),
(6, 18, 66, 0, 1, '', '0000-00-00 00:00:00'),
(7, 9, 68, 0, 0, '', '0000-00-00 00:00:00'),
(8, 11, 68, 0, 0, '', '0000-00-00 00:00:00'),
(9, 18, 68, 1, 1, '', '0000-00-00 00:00:00'),
(10, 13, 54, 1, 1, '', '2018-09-24 16:58:29'),
(11, 14, 54, 1, 1, '', '2018-09-24 16:58:30'),
(12, 18, 54, 1, 1, '', '2018-09-24 16:58:31'),
(17, 18, 70, 0, 0, '', '2018-09-30 17:42:58'),
(18, 11, 71, 0, 0, '', '2018-10-07 15:44:37'),
(19, 13, 71, 0, 0, '', '2018-10-07 15:44:38'),
(20, 18, 71, 3, 1, '', '2018-10-07 15:44:39'),
(21, 13, 72, 3, 1, '', '2018-10-07 17:06:39'),
(22, 18, 72, 3, 1, '', '2018-10-07 17:06:40');

-- --------------------------------------------------------

--
-- Table structure for table `requisition`
--

CREATE TABLE `requisition` (
  `ID` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `delivery_date` date NOT NULL,
  `quantity` int(11) NOT NULL,
  `capex_opex` varchar(100) NOT NULL,
  `budgeted` tinyint(4) NOT NULL COMMENT '0: none , 1:budgeted, non-budget',
  `budget` varchar(15) NOT NULL,
  `budget_currency` varchar(20) NOT NULL,
  `cost_center` varchar(100) NOT NULL,
  `additional_documents` tinyint(4) NOT NULL,
  `address` text NOT NULL,
  `createdat` datetime NOT NULL,
  `status` enum('PENDING','APPROVED','RFI','RFQ','AUCTION','ORDER') NOT NULL DEFAULT 'PENDING' COMMENT '0:pending, 1:approve,2:rejec'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `requisition`
--

INSERT INTO `requisition` (`ID`, `user_id`, `account_id`, `name`, `description`, `delivery_date`, `quantity`, `capex_opex`, `budgeted`, `budget`, `budget_currency`, `cost_center`, `additional_documents`, `address`, `createdat`, `status`) VALUES
(12, 1, 0, 'sdfg update', 'sdfg', '2018-08-24', 500, 'capex', 0, '500', 'AED', 'cost app', 1, 'address', '2018-09-01 17:45:59', 'PENDING'),
(15, 1, 0, 'Product Name', 'Description', '2018-09-12', 500, 'opex', 2, '500', 'Pound', 'cost center', 1, 'delivery address', '2018-09-02 08:00:09', 'AUCTION'),
(18, 1, 1, 'name', 'description', '2018-09-19', 500, 'capex', 3, '500', 'Pound', 'cost center', 1, 'delivery address', '2018-09-03 15:16:17', 'RFQ'),
(20, 1, 1, 'product 2 ', 'description', '2018-09-06', 500, 'capex', 2, '5', 'Pound', 'cost', 0, 'delivery', '2018-09-03 15:23:10', 'PENDING'),
(21, 2, 1, 'product ', 'description', '2018-09-28', 5, 'capex', 2, '500', 'Pound', 'cost center', 1, 'address', '2018-09-03 15:49:56', 'ORDER'),
(22, 1, 1, 'name', 'descript', '2018-09-27', 50, 'capex', 0, '500', 'Pound', 'cost', 0, 'delivery', '2018-09-04 08:51:59', 'AUCTION'),
(23, 1, 1, 'product name', 'description', '2018-09-28', 600, 'capex', 2, '500', 'Pound', '30', 0, 'delivery address', '2018-09-04 09:20:50', 'RFI'),
(24, 4, 1, 'Item 2', 'descritpion', '2018-09-21', 5, 'capex', 2, '500', 'Pound', '600', 1, 'delivery address', '2018-09-06 09:11:02', 'RFQ'),
(25, 5, 1, 'item 2', 'description', '2018-09-06', 6, 'capex', 2, '123', 'Pound', 'cost center', 1, 'delivery address', '2018-09-06 09:12:36', 'RFQ'),
(26, 6, 1, 'product 2', 'description', '2018-09-13', 600, 'opex', 2, '500', 'Pound', 'cost center', 1, 'delivery address', '2018-09-06 18:42:00', 'RFQ'),
(27, 7, 1, 'asdkh', 'asdkjfh', '2018-09-19', 500, 'capex', 2, '400', 'Pound', '500', 1, 'delivery address', '2018-09-06 18:44:14', 'RFI'),
(29, 9, 1, 'Product Name 08', 'description 08', '2018-09-08', 500, 'capex', 1, '500', 'Pound', 'cost center', 1, 'delivery address', '2018-09-08 10:48:23', 'PENDING');

-- --------------------------------------------------------

--
-- Table structure for table `requisition_approver`
--

CREATE TABLE `requisition_approver` (
  `ID` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `req_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `requisition_document`
--

CREATE TABLE `requisition_document` (
  `ID` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `rid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `requisition_document`
--

INSERT INTO `requisition_document` (`ID`, `uid`, `did`, `rid`) VALUES
(53, 1, 59, 15),
(54, 1, 60, 15),
(55, 1, 61, 15),
(56, 1, 62, 15),
(57, 1, 63, 15),
(68, 1, 74, 12),
(69, 1, 75, 12),
(70, 1, 76, 12),
(71, 1, 77, 12),
(72, 1, 78, 12),
(73, 1, 79, 12),
(74, 1, 80, 12),
(78, 1, 84, 18),
(80, 1, 86, 21),
(81, 1, 87, 21),
(82, 1, 88, 21),
(83, 5, 96, 25),
(84, 5, 97, 25),
(85, 5, 98, 25),
(86, 5, 99, 25),
(87, 5, 100, 25),
(88, 5, 101, 25),
(89, 6, 102, 26),
(90, 6, 103, 26),
(91, 6, 104, 26),
(92, 7, 105, 27),
(93, 9, 106, 29);

-- --------------------------------------------------------

--
-- Table structure for table `req_item_doc`
--

CREATE TABLE `req_item_doc` (
  `ID` int(11) NOT NULL,
  `reqitem_id` int(11) NOT NULL,
  `doc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `req_item_doc`
--

INSERT INTO `req_item_doc` (`ID`, `reqitem_id`, `doc_id`) VALUES
(3, 13, 189),
(6, 13, 193),
(10, 19, 166),
(11, 19, 167),
(12, 20, 166),
(13, 20, 167),
(15, 22, 194),
(16, 22, 195),
(28, 41, 218),
(31, 44, 184),
(32, 45, 185),
(33, 46, 219),
(36, 55, 183),
(37, 56, 184),
(38, 57, 185),
(39, 58, 184),
(40, 59, 185),
(41, 60, 184),
(42, 61, 185),
(43, 62, 184),
(44, 63, 166),
(45, 63, 167),
(46, 64, 183),
(47, 65, 184),
(48, 66, 185);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `ID` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `parent` tinyint(4) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `section_questions`
--

CREATE TABLE `section_questions` (
  `ID` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `rid` int(11) NOT NULL,
  `parentid` int(11) NOT NULL,
  `name` longtext NOT NULL,
  `description` longtext NOT NULL,
  `qtype` varchar(250) NOT NULL,
  `mandatory` enum('Yes','No') NOT NULL,
  `document` varchar(350) NOT NULL,
  `prequalify` tinyint(4) NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `section_questions`
--

INSERT INTO `section_questions` (`ID`, `uid`, `rid`, `parentid`, `name`, `description`, `qtype`, `mandatory`, `document`, `prequalify`, `createdat`) VALUES
(42, 1, 21, 0, 'section first', '', '', 'Yes', '', 0, '2018-09-09 08:50:41'),
(43, 1, 21, 42, 'question 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to m', '0', 'Yes', '1', 0, '2018-09-09 08:51:13'),
(44, 1, 21, 42, 'question 2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to m', '2', 'No', '0', 0, '2018-09-09 08:51:43'),
(45, 1, 21, 0, 'section second', '', '', 'Yes', '', 0, '2018-09-09 08:51:57'),
(46, 1, 21, 45, 'question 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to m', '1', 'No', '1', 0, '2018-09-09 08:52:17'),
(107, 1, 50, 0, 'new section', '', '', 'Yes', '', 0, '2018-09-12 09:18:37'),
(108, 1, 50, 107, 'question 1.1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '0', 'No', '1', 1, '2018-09-12 09:19:30'),
(109, 1, 50, 107, 'question 1.2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '0', 'No', '1', 0, '2018-09-12 09:19:26'),
(110, 1, 50, 0, 'section second', '', '', 'Yes', '', 0, '2018-09-12 09:19:43'),
(111, 1, 50, 110, 'question 2.1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '2', 'No', '0', 0, '2018-09-12 09:20:20'),
(112, 1, 50, 110, 'question 2.2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '3', 'No', '1', 0, '2018-09-12 09:21:17'),
(131, 1, 49, 0, 'section 1', '', '', 'Yes', '', 0, '2018-09-12 12:51:55'),
(132, 1, 49, 131, 'q1', 'section1', '0', 'No', '0', 1, '2018-09-12 12:51:55'),
(133, 1, 49, 0, 'section 2', '', '', 'Yes', '', 0, '2018-09-12 12:51:55'),
(134, 1, 49, 133, 'q2', 'd2', '2', 'Yes', '0', 1, '2018-09-12 12:51:55'),
(135, 1, 49, 133, 'q1', 'd1', '0', 'Yes', '1', 1, '2018-09-12 12:51:56'),
(136, 1, 51, 0, 'general information goes here', '', '', 'Yes', '', 0, '2018-09-12 14:40:40'),
(137, 1, 51, 136, 'question 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '1', 'No', '0', 0, '2018-09-12 14:41:10'),
(138, 1, 51, 136, 'question 2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '2', 'No', '0', 0, '2018-09-12 14:41:32'),
(139, 1, 51, 0, 'section second', '', '', 'Yes', '', 0, '2018-09-12 14:41:41'),
(140, 1, 51, 139, 'Lorem Ipsum', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '1', 'Yes', '0', 1, '2018-09-12 14:41:57'),
(141, 1, 51, 0, 'section 3rd', '', '', 'Yes', '', 0, '2018-09-12 14:42:10'),
(142, 1, 51, 141, 'Lorem Ipsum', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '1', 'Yes', '1', 0, '2018-09-12 14:42:26'),
(143, 1, 51, 141, 'Lorem Ipsum', 'Lorem Ipsum', '2', 'Yes', '1', 0, '2018-09-12 14:43:04'),
(144, 1, 56, 0, 'section 1', '', '', 'Yes', '', 0, '2018-09-16 07:18:33'),
(145, 1, 56, 144, 'q1', 'section1', '0', 'No', '0', 1, '2018-09-16 07:18:33'),
(146, 1, 56, 0, 'section 2', '', '', 'Yes', '', 0, '2018-09-16 07:18:33'),
(147, 1, 56, 146, 'q1', 'd1', '0', 'Yes', '1', 1, '2018-09-16 07:18:33'),
(148, 1, 56, 146, 'q2', 'd2', '0', 'Yes', '0', 1, '2018-09-16 07:18:33'),
(149, 1, 57, 0, 'section 1', '', '', 'Yes', '', 0, '2018-09-16 07:34:35'),
(150, 1, 57, 149, 'q1', 'section1', '0', 'No', '0', 1, '2018-09-16 07:34:35'),
(151, 1, 57, 0, 'section 2', '', '', 'Yes', '', 0, '2018-09-16 07:34:35'),
(152, 1, 57, 151, 'q1', 'd1', '0', 'Yes', '1', 1, '2018-09-16 07:34:35'),
(153, 1, 57, 151, 'q2', 'd2', '0', 'Yes', '0', 1, '2018-09-16 07:34:35'),
(154, 1, 61, 0, 'section 1', '', '', 'Yes', '', 0, '2018-09-16 15:56:00'),
(155, 1, 61, 154, 'q1', 'section1', '0', 'No', '0', 1, '2018-09-16 15:56:00'),
(156, 1, 61, 0, 'section 2', '', '', 'Yes', '', 0, '2018-09-16 15:56:00'),
(157, 1, 61, 156, 'q1', 'd1', '0', 'Yes', '1', 1, '2018-09-16 15:56:00'),
(158, 1, 61, 156, 'q2', 'd2', '0', 'Yes', '0', 1, '2018-09-16 15:56:00'),
(159, 1, 66, 0, 'section 1', '', '', 'Yes', '', 0, '2018-09-17 09:27:08'),
(160, 1, 66, 159, 'q1', 'section1', '0', 'No', '0', 1, '2018-09-17 09:27:08'),
(161, 1, 66, 0, 'section 2', '', '', 'Yes', '', 0, '2018-09-17 09:27:08'),
(162, 1, 66, 161, 'q1', 'd1', '0', 'Yes', '1', 1, '2018-09-17 09:27:08'),
(163, 1, 66, 161, 'q2', 'd2', '0', 'Yes', '0', 1, '2018-09-17 09:27:08'),
(190, 1, 67, 0, 'section 1', '', '', 'Yes', '', 0, '2018-09-18 13:10:03'),
(191, 1, 67, 190, 'q1', 'section1', '0', 'No', '0', 1, '2018-09-18 13:10:03'),
(192, 1, 67, 0, 'section 2', '', '', 'Yes', '', 0, '2018-09-18 13:10:03'),
(193, 1, 67, 192, 'q1', 'd1', '2', 'Yes', '0', 0, '2018-09-18 13:10:03'),
(194, 1, 67, 192, 'q2', 'd2', '0', 'Yes', '0', 1, '2018-09-18 13:10:04'),
(195, 1, 67, 0, 'section ext', '', '', 'Yes', '', 0, '2018-09-18 13:10:04'),
(206, 1, 55, 0, 'section 1', '', '', 'Yes', '', 0, '2018-09-19 15:28:19'),
(207, 1, 55, 206, 'q1', 'section1', '0', 'No', '0', 1, '2018-09-19 15:28:19'),
(208, 1, 55, 0, 'section 2', '', '', 'Yes', '', 0, '2018-09-19 15:28:19'),
(209, 1, 55, 208, 'q1', 'd1', '0', 'Yes', '1', 1, '2018-09-19 15:28:19'),
(210, 1, 55, 208, 'q2', 'd2', '0', 'Yes', '0', 1, '2018-09-19 15:28:19'),
(211, 1, 54, 0, 'section 1', '', '', 'Yes', '', 0, '2018-09-21 13:57:48'),
(212, 1, 54, 211, 'Lorem Ipsum', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, ', '3', 'Yes', '0', 0, '2018-09-21 13:59:38'),
(213, 1, 54, 0, 'section 2', '', '', 'Yes', '', 0, '2018-09-21 13:57:49'),
(214, 1, 54, 213, 'Lorem Ipsum', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, ', '0', 'Yes', '1', 1, '2018-09-21 13:59:50'),
(215, 1, 54, 213, 'Lorem Ipsum', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, ', '2', 'Yes', '1', 0, '2018-09-21 13:58:56'),
(216, 1, 68, 0, 'section 1', '', '', 'Yes', '', 0, '2018-09-22 16:29:57'),
(217, 1, 68, 216, 'q1', 'section1', '0', 'No', '0', 1, '2018-09-22 16:29:58'),
(218, 1, 68, 0, 'section 2', '', '', 'Yes', '', 0, '2018-09-22 16:29:58'),
(219, 1, 68, 218, 'Lorem Ipsum is', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '2', 'Yes', '0', 0, '2018-09-22 16:32:30'),
(220, 1, 68, 218, 'Lorem Ipsum ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '0', 'Yes', '0', 1, '2018-09-22 16:32:16'),
(221, 1, 68, 0, 'section ext', '', '', 'Yes', '', 0, '2018-09-22 16:29:58'),
(222, 1, 68, 221, 'Lorem Ipsum', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s', '0', 'No', '1', 0, '2018-09-22 16:32:04'),
(223, 1, 69, 0, 'section 1', '', '', 'Yes', '', 0, '2018-09-24 13:44:03'),
(224, 1, 69, 223, 'q1', 'section1', '0', 'No', '0', 1, '2018-09-24 13:44:03'),
(225, 1, 69, 0, 'section 2', '', '', 'Yes', '', 0, '2018-09-24 13:44:03'),
(226, 1, 69, 225, 'q1', 'd1', '0', 'Yes', '1', 1, '2018-09-24 13:44:03'),
(227, 1, 69, 225, 'q2', 'd2', '0', 'Yes', '0', 1, '2018-09-24 13:44:03'),
(228, 1, 70, 0, 'section 1', '', '', 'Yes', '', 0, '2018-09-30 15:37:29'),
(229, 1, 70, 228, 'q1', 'section1', '0', 'No', '0', 1, '2018-09-30 15:37:29'),
(230, 1, 70, 0, 'section 2', '', '', 'Yes', '', 0, '2018-09-30 15:37:29'),
(231, 1, 70, 230, 'q1', 'd1', '0', 'Yes', '1', 1, '2018-09-30 15:37:29'),
(232, 1, 70, 230, 'q2', 'd2', '0', 'Yes', '0', 1, '2018-09-30 15:37:29'),
(233, 1, 71, 0, 'section 1', '', '', 'Yes', '', 0, '2018-10-07 13:41:05'),
(234, 1, 71, 233, 'q1', 'section1', '0', 'No', '0', 1, '2018-10-07 13:41:06'),
(235, 1, 71, 0, 'section 2', '', '', 'Yes', '', 0, '2018-10-07 13:41:06'),
(236, 1, 71, 235, 'q1', 'd1', '0', 'Yes', '1', 1, '2018-10-07 13:41:06'),
(237, 1, 71, 235, 'q2', 'd2', '0', 'Yes', '0', 1, '2018-10-07 13:41:06'),
(238, 1, 72, 0, 'section 1', '', '', 'Yes', '', 0, '2018-10-07 15:04:10'),
(239, 1, 72, 238, 'q1', 'section1', '0', 'No', '0', 1, '2018-10-07 15:04:10'),
(240, 1, 72, 0, 'section 2', '', '', 'Yes', '', 0, '2018-10-07 15:04:10'),
(241, 1, 72, 240, 'q1', 'd1', '0', 'Yes', '1', 1, '2018-10-07 15:04:10'),
(242, 1, 72, 240, 'q2', 'd2', '0', 'Yes', '0', 1, '2018-10-07 15:04:10');

-- --------------------------------------------------------

--
-- Table structure for table `suppliergroup`
--

CREATE TABLE `suppliergroup` (
  `ID` int(11) NOT NULL,
  `acc_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `createdat` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppliergroup`
--

INSERT INTO `suppliergroup` (`ID`, `acc_id`, `user_id`, `name`, `createdat`) VALUES
(1, 1, 1, 'Supplier Group 1', '2018-09-11 00:00:00'),
(2, 1, 1, 'Supplier group 2', '2018-09-11 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_accounts`
--

CREATE TABLE `supplier_accounts` (
  `ID` int(11) NOT NULL,
  `acc_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_accounts`
--

INSERT INTO `supplier_accounts` (`ID`, `acc_id`, `user_id`) VALUES
(1, 1, 9),
(2, 1, 10),
(3, 1, 11),
(4, 1, 13),
(5, 1, 14),
(6, 8, 15),
(7, 15, 16),
(8, 16, 17),
(9, 1, 18);

-- --------------------------------------------------------

--
-- Table structure for table `supplier_group_rel`
--

CREATE TABLE `supplier_group_rel` (
  `ID` int(11) NOT NULL,
  `supgroup_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_group_rel`
--

INSERT INTO `supplier_group_rel` (`ID`, `supgroup_id`, `supplier_id`) VALUES
(1, 1, 9),
(2, 1, 11),
(3, 2, 9),
(4, 2, 10),
(5, 2, 11);

-- --------------------------------------------------------

--
-- Table structure for table `supplier_items`
--

CREATE TABLE `supplier_items` (
  `ID` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `item_num` varchar(20) NOT NULL,
  `price` double NOT NULL,
  `description` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_items`
--

INSERT INTO `supplier_items` (`ID`, `item_id`, `supplier_id`, `item_num`, `price`, `description`) VALUES
(1, 17, 10, '1', 10, ''),
(2, 14, 11, '0', 0, ''),
(3, 17, 14, '1', 10, ''),
(4, 14, 10, '2', 3, '');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_req_doc`
--

CREATE TABLE `supplier_req_doc` (
  `ID` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `rid` int(11) NOT NULL,
  `did` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_req_doc`
--

INSERT INTO `supplier_req_doc` (`ID`, `uid`, `rid`, `did`) VALUES
(3, 18, 68, 237),
(4, 18, 68, 238),
(5, 18, 61, 239),
(6, 18, 71, 243),
(7, 18, 71, 244),
(8, 18, 72, 247),
(9, 18, 72, 248);

-- --------------------------------------------------------

--
-- Table structure for table `template_optionlist`
--

CREATE TABLE `template_optionlist` (
  `ID` int(11) NOT NULL,
  `qid` int(11) NOT NULL,
  `value` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `template_optionlist`
--

INSERT INTO `template_optionlist` (`ID`, `qid`, `value`) VALUES
(1, 10, 'val 1'),
(2, 10, 'val 2'),
(3, 10, 'val 3'),
(4, 14, '5'),
(5, 14, '3'),
(6, 14, '4');

-- --------------------------------------------------------

--
-- Table structure for table `template_questionnaire`
--

CREATE TABLE `template_questionnaire` (
  `ID` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `accid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `createdat` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `template_questionnaire`
--

INSERT INTO `template_questionnaire` (`ID`, `uid`, `accid`, `name`, `createdat`) VALUES
(1, 1, 1, 'template 1', '2018-09-12 10:58:30'),
(2, 1, 1, 'tempalte 2', '2018-09-12 11:00:41'),
(3, 1, 1, 'template 3', '2018-09-12 11:00:55'),
(4, 1, 1, 'template 4', '2018-09-12 11:01:47'),
(5, 1, 1, 'request 49 template', '2018-09-12 12:47:15'),
(6, 1, 1, 'new temp', '2018-09-18 13:05:00');

-- --------------------------------------------------------

--
-- Table structure for table `template_section_questions`
--

CREATE TABLE `template_section_questions` (
  `ID` int(11) NOT NULL,
  `tempid` int(11) NOT NULL,
  `parentid` int(11) NOT NULL,
  `name` longtext NOT NULL,
  `description` longtext NOT NULL,
  `qtype` varchar(250) NOT NULL,
  `mandatory` enum('Yes','No') NOT NULL,
  `document` varchar(350) NOT NULL,
  `prequalify` tinyint(4) NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `template_section_questions`
--

INSERT INTO `template_section_questions` (`ID`, `tempid`, `parentid`, `name`, `description`, `qtype`, `mandatory`, `document`, `prequalify`, `createdat`) VALUES
(1, 4, 0, 'section 1', '', '', 'Yes', '', 0, '2018-09-12 11:01:47'),
(2, 4, 1, 'q1', 'section1', '0', 'No', '0', 1, '2018-09-12 11:01:47'),
(3, 4, 0, 'section 2', '', '', 'Yes', '', 0, '2018-09-12 11:01:47'),
(4, 4, 3, 'q1', 'd1', '0', 'Yes', '1', 1, '2018-09-12 11:01:47'),
(5, 4, 3, 'q2', 'd2', '0', 'Yes', '0', 1, '2018-09-12 11:01:47'),
(6, 5, 0, 'section 1', '', '', 'Yes', '', 0, '2018-09-12 12:47:16'),
(7, 5, 6, 'q1', 'section1', '0', 'No', '0', 1, '2018-09-12 12:47:16'),
(8, 5, 0, 'section 2', '', '', 'Yes', '', 0, '2018-09-12 12:47:16'),
(9, 5, 8, 'q1', 'd1', '0', 'Yes', '1', 1, '2018-09-12 12:47:16'),
(10, 5, 8, 'q2', 'd2', '2', 'Yes', '0', 1, '2018-09-12 12:47:16'),
(11, 6, 0, 'section 1', '', '', 'Yes', '', 0, '2018-09-18 13:05:00'),
(12, 6, 11, 'q1', 'section1', '0', 'No', '0', 1, '2018-09-18 13:05:00'),
(13, 6, 0, 'section 2', '', '', 'Yes', '', 0, '2018-09-18 13:05:00'),
(14, 6, 13, 'q1', 'd1', '2', 'Yes', '0', 0, '2018-09-18 13:05:00'),
(15, 6, 13, 'q2', 'd2', '0', 'Yes', '0', 1, '2018-09-18 13:05:01'),
(16, 6, 0, 'section ext', '', '', 'Yes', '', 0, '2018-09-18 13:05:01');

-- --------------------------------------------------------

--
-- Table structure for table `timezone`
--

CREATE TABLE `timezone` (
  `ID` int(11) NOT NULL,
  `value` varchar(10) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timezone`
--

INSERT INTO `timezone` (`ID`, `value`, `name`) VALUES
(1, '-12:00', '(GMT -12:00) Eniwetok, Kwajalein'),
(2, '-11:00', '(GMT -11:00) Midway Island, Samoa'),
(3, '-10:00', '(GMT -10:00) Hawaii'),
(4, '-09:50', '(GMT -9:30) Taiohae'),
(5, '-09:00', '(GMT -9:00) Alaska'),
(6, '-08:00', '(GMT -8:00) Pacific Time (US & Canada)'),
(7, '-07:00', '(GMT -7:00) Mountain Time (US & Canada)'),
(8, '-06:00', '(GMT -6:00) Central Time (US & Canada), Mexico City'),
(9, '-05:00', '(GMT -5:00) Eastern Time (US & Canada), Bogota, Lima'),
(10, '-04:50', '(GMT -4:30) Caracas'),
(11, '-04:00', '(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz'),
(12, '-03:50', '(GMT -3:30) Newfoundland'),
(13, '-03:00', '(GMT -3:00) Brazil, Buenos Aires, Georgetown'),
(14, '-02:00', '(GMT -2:00) Mid-Atlantic'),
(15, '-01:00', '(GMT -1:00) Azores, Cape Verde Islands'),
(16, '+00:00', '(GMT +0:00) Western Europe Time, London, Lisbon, Casablanca'),
(17, '+01:00', '(GMT +1:00) Brussels, Copenhagen, Madrid, Paris'),
(18, '+02:00', '(GMT +2:00) Kaliningrad, South Africa'),
(19, '+03:00', '(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg'),
(20, '+03:50', '(GMT +3:30) Tehran'),
(21, '+04:00', '(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi'),
(22, '+04:50', '(GMT +4:30) Kabul'),
(23, '+05:00', '(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent'),
(24, '+05:50', '(GMT +5:30) Bombay, Calcutta, Madras, New Delhi'),
(25, '+05:75', '(GMT +5:45) Kathmandu, Pokhara'),
(26, '+06:00', '(GMT +6:00) Almaty, Dhaka, Colombo'),
(27, '+06:50', '(GMT +6:30) Yangon, Mandalay'),
(28, '+07:00', '(GMT +7:00) Bangkok, Hanoi, Jakarta'),
(29, '+08:00', '(GMT +8:00) Beijing, Perth, Singapore, Hong Kong'),
(30, '+08:75', '(GMT +8:45) Eucla'),
(31, '+09:00', '(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk'),
(32, '+09:50', '(GMT +9:30) Adelaide, Darwin'),
(33, '+10:00', '(GMT +10:00) Eastern Australia, Guam, Vladivostok'),
(34, '+10:50', '(GMT +10:30) Lord Howe Island'),
(35, '+11:00', '(GMT +11:00) Magadan, Solomon Islands, New Caledonia'),
(36, '+11:50', '(GMT +11:30) Norfolk Island'),
(37, '+12:00', '(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka'),
(38, '+12:75', '(GMT +12:45) Chatham Islands'),
(39, '+13:00', '(GMT +13:00) Apia, Nukualofa'),
(40, '+14:00', '(GMT +14:00) Line Islands, Tokelau');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `ID` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`ID`, `name`, `parent`) VALUES
(1, 'Pieces', 0),
(2, 'pieces (pcs)', 1),
(3, 'pallets', 1),
(4, 'cartons', 1),
(5, 'each', 1),
(6, 'pairs', 1),
(7, 'sections', 1),
(8, 'bundles', 1),
(9, 'sets', 1),
(10, 'sheets', 1),
(11, 'Weight', 0),
(12, 'ounces (US) (oz)', 11),
(13, 'pounds (US) (lb)', 11),
(14, 'tons (US) (ton)', 11),
(15, 'grams (g)', 11),
(16, 'kilograms (kg)', 11),
(17, 'tonnes (metric) (ton)', 11),
(18, 'Volume', 0),
(19, 'cubic millimetres (cu mm)', 18),
(20, 'cubic centimetres (cu cm)', 18),
(21, 'cubic inches (cu in)', 18),
(22, 'cubic feet (cu ft)', 18),
(23, 'cubic yards (cu yd)', 18),
(24, 'cubic metres (cu m)', 18),
(25, 'fluid ounces (US) (fl oz)', 18),
(26, 'pints (US) (pint)', 18),
(27, 'quarts (US) (qt)', 18),
(28, 'gallons (US) (gL)', 18),
(29, 'litres (L)', 18),
(30, 'millilitres (mL)', 18),
(31, 'board feet (BF)', 18),
(32, 'Length', 0),
(33, 'millimetres (mm)', 32),
(34, 'centimetres (cm)', 32),
(35, 'metres (m)', 32),
(36, 'inches (in)', 32),
(37, 'feet (ft)', 32),
(38, 'yards (yds)', 32),
(39, 'Area', 0),
(40, 'square millimetres (sq mm)', 39),
(41, 'square centimetres (sq cm)', 39),
(42, 'square inches (sq in)', 39),
(43, 'square feet (sq ft)', 39),
(44, 'square yards (sq yd)', 39),
(45, 'square metres (sq m)', 39),
(46, 'Time', 0),
(47, 'hours (hrs)', 46),
(48, 'days', 46),
(49, 'months', 46),
(50, 'years (yrs)', 46);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `accid` int(11) NOT NULL,
  `name` varchar(350) DEFAULT NULL,
  `email` varchar(350) NOT NULL,
  `password` varchar(350) NOT NULL,
  `title` varchar(350) NOT NULL,
  `phone` varchar(350) NOT NULL,
  `mobile` varchar(350) NOT NULL,
  `carriercode` int(11) NOT NULL,
  `mobileregion` varchar(10) NOT NULL,
  `usergroup` varchar(350) NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Approved','Block','Pending') DEFAULT 'Pending',
  `role` enum('SUPPERADMIN','ADMIN','SUPPLIER') NOT NULL,
  `logincount` int(11) NOT NULL,
  `lastaccess` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `accid`, `name`, `email`, `password`, `title`, `phone`, `mobile`, `carriercode`, `mobileregion`, `usergroup`, `createdat`, `status`, `role`, `logincount`, `lastaccess`, `updatedat`) VALUES
(1, 1, 'Muhammad Faizan', 'faizan@businessbid.ae', '81dc9bdb52d04dc20036dbd8313ed055', 'Developer', '0527886386', '527886386', 971, 'ae', '0', '2018-08-30 17:53:57', 'Approved', '', 0, '2018-08-30 00:00:00', '2018-08-30 00:00:00'),
(2, 1, 'Sahar Akram', 'sahar@businessbid.ae', '81dc9bdb52d04dc20036dbd8313ed055', 'Select Department', '0527886386', '9715212312', 1, 'us', '', '2018-09-03 17:21:26', 'Pending', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 1, 'Tahir', 'tahir@businessbid.ae', '81dc9bdb52d04dc20036dbd8313ed055', 'Select Department', '', '521233211', 971, 'ae', '', '2018-09-06 11:11:02', 'Pending', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 1, 'User 1', 'user1@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Select Department', '', '523222123', 971, 'ae', '', '2018-09-06 11:12:36', 'Pending', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 1, 'name', 'user123@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Select Department', '', '1231222', 971, 'ae', '', '2018-09-06 20:42:00', 'Pending', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 1, 'adkj', 'user11@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Select Department', '', '1233211', 971, 'ae', '', '2018-09-06 20:44:14', 'Pending', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 1, 'name', 'user12@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Select Department', '', '1234555', 971, 'ae', '', '2018-09-06 20:45:46', 'Pending', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 2, 'supplier1', 'supplier1@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Select Department', '', '521233322', 971, 'ae', '', '2018-09-08 12:48:23', 'Pending', 'SUPPLIER', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 3, 'Supplier 2', 'supplier2@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Select Department', '', '521233322', 971, 'ae', '', '2018-09-08 12:48:23', 'Pending', 'SUPPLIER', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 4, 'Supplier 3', 'supplier3@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Select Department', '', '521233322', 971, 'ae', '', '2018-09-08 12:48:23', 'Pending', 'SUPPLIER', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 5, 'usersupplier 1', 'usersupplier1@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '', '', '', 0, '', '', '2018-09-11 15:59:59', 'Pending', 'SUPPLIER', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 6, 'user supplier2', 'usersupplier2@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '', '', '', 0, '', '', '2018-09-11 16:00:01', 'Pending', 'SUPPLIER', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 8, NULL, 'usersupplier3@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '', '', '', 0, '', '', '2018-09-11 17:38:33', 'Pending', 'SUPPLIER', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 15, NULL, 'suppliernew1@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '', '', '', 0, '', '', '2018-09-11 17:44:11', 'Pending', 'SUPPLIER', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 16, NULL, 'suppliernew2@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '', '', '', 0, '', '', '2018-09-11 17:44:12', 'Pending', 'SUPPLIER', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 17, 'faizan', 'm.faizan1517@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '', '', '', 0, '', '', '2018-09-16 15:57:56', 'Pending', 'SUPPLIER', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `_supplier_item_prices`
--

CREATE TABLE `_supplier_item_prices` (
  `ID` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `rid` int(11) NOT NULL,
  `qid` int(11) NOT NULL,
  `ques_ans` longtext NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `categories_items`
--
ALTER TABLE `categories_items`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `cat_id` (`cat_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `document_request`
--
ALTER TABLE `document_request`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `uid` (`uid`),
  ADD KEY `did` (`did`),
  ADD KEY `rid` (`rid`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `groups_roles`
--
ALTER TABLE `groups_roles`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `uid` (`uid`),
  ADD KEY `rid` (`accid`),
  ADD KEY `unit` (`unit`);

--
-- Indexes for table `item_documents`
--
ALTER TABLE `item_documents`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `doc_id` (`doc_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `optionlist`
--
ALTER TABLE `optionlist`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `qid` (`qid`);

--
-- Indexes for table `package`
--
ALTER TABLE `package`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `questanswers_document`
--
ALTER TABLE `questanswers_document`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `qans_id` (`qans_id`),
  ADD KEY `doc_id` (`doc_id`);

--
-- Indexes for table `questions_answers`
--
ALTER TABLE `questions_answers`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `uid` (`uid`),
  ADD KEY `rid` (`rid`),
  ADD KEY `qid` (`qid`);

--
-- Indexes for table `questions_documents`
--
ALTER TABLE `questions_documents`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `uid` (`uid`),
  ADD KEY `qid` (`qid`);

--
-- Indexes for table `q_answer`
--
ALTER TABLE `q_answer`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `uid` (`uid`),
  ADD KEY `qid` (`qid`),
  ADD KEY `rid` (`rid`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `uid` (`uid`),
  ADD KEY `accid` (`accid`);

--
-- Indexes for table `request_invitation_message`
--
ALTER TABLE `request_invitation_message`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `request_items`
--
ALTER TABLE `request_items`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `request_id` (`request_id`),
  ADD KEY `unit_id` (`unit_id`),
  ADD KEY `unit_id_2` (`unit_id`);

--
-- Indexes for table `request_item_prices`
--
ALTER TABLE `request_item_prices`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `uid` (`uid`),
  ADD KEY `rid` (`rid`),
  ADD KEY `qid` (`reqitem_id`);

--
-- Indexes for table `request_supplier`
--
ALTER TABLE `request_supplier`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `req_id` (`req_id`);

--
-- Indexes for table `requisition`
--
ALTER TABLE `requisition`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `requisition_approver`
--
ALTER TABLE `requisition_approver`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `req_id` (`req_id`);

--
-- Indexes for table `requisition_document`
--
ALTER TABLE `requisition_document`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `uid` (`uid`),
  ADD KEY `did` (`did`),
  ADD KEY `rid` (`rid`);

--
-- Indexes for table `req_item_doc`
--
ALTER TABLE `req_item_doc`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `reqitem_id` (`reqitem_id`),
  ADD KEY `doc_id` (`doc_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `account_id` (`account_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `section_questions`
--
ALTER TABLE `section_questions`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `uid` (`uid`),
  ADD KEY `rid` (`rid`);

--
-- Indexes for table `suppliergroup`
--
ALTER TABLE `suppliergroup`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `acc_id` (`acc_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `supplier_accounts`
--
ALTER TABLE `supplier_accounts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `acc_id` (`acc_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `supplier_group_rel`
--
ALTER TABLE `supplier_group_rel`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `supgroup_id` (`supgroup_id`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `supplier_items`
--
ALTER TABLE `supplier_items`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `supplier_req_doc`
--
ALTER TABLE `supplier_req_doc`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `uid` (`uid`),
  ADD KEY `rid` (`rid`),
  ADD KEY `did` (`did`);

--
-- Indexes for table `template_optionlist`
--
ALTER TABLE `template_optionlist`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `qid` (`qid`);

--
-- Indexes for table `template_questionnaire`
--
ALTER TABLE `template_questionnaire`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `uid` (`uid`),
  ADD KEY `accid` (`accid`);

--
-- Indexes for table `template_section_questions`
--
ALTER TABLE `template_section_questions`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `tempid` (`tempid`);

--
-- Indexes for table `timezone`
--
ALTER TABLE `timezone`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `accid` (`accid`);

--
-- Indexes for table `_supplier_item_prices`
--
ALTER TABLE `_supplier_item_prices`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `uid` (`uid`),
  ADD KEY `rid` (`rid`),
  ADD KEY `qid` (`qid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categories_items`
--
ALTER TABLE `categories_items`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=253;
--
-- AUTO_INCREMENT for table `document_request`
--
ALTER TABLE `document_request`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `groups_roles`
--
ALTER TABLE `groups_roles`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `item_documents`
--
ALTER TABLE `item_documents`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `optionlist`
--
ALTER TABLE `optionlist`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `package`
--
ALTER TABLE `package`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `questanswers_document`
--
ALTER TABLE `questanswers_document`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `questions_answers`
--
ALTER TABLE `questions_answers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;
--
-- AUTO_INCREMENT for table `questions_documents`
--
ALTER TABLE `questions_documents`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `q_answer`
--
ALTER TABLE `q_answer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `request_invitation_message`
--
ALTER TABLE `request_invitation_message`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `request_items`
--
ALTER TABLE `request_items`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `request_item_prices`
--
ALTER TABLE `request_item_prices`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `request_supplier`
--
ALTER TABLE `request_supplier`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `requisition`
--
ALTER TABLE `requisition`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `requisition_approver`
--
ALTER TABLE `requisition_approver`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `requisition_document`
--
ALTER TABLE `requisition_document`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT for table `req_item_doc`
--
ALTER TABLE `req_item_doc`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `section_questions`
--
ALTER TABLE `section_questions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=248;
--
-- AUTO_INCREMENT for table `suppliergroup`
--
ALTER TABLE `suppliergroup`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `supplier_accounts`
--
ALTER TABLE `supplier_accounts`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `supplier_group_rel`
--
ALTER TABLE `supplier_group_rel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `supplier_items`
--
ALTER TABLE `supplier_items`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `supplier_req_doc`
--
ALTER TABLE `supplier_req_doc`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `template_optionlist`
--
ALTER TABLE `template_optionlist`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `template_questionnaire`
--
ALTER TABLE `template_questionnaire`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `template_section_questions`
--
ALTER TABLE `template_section_questions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `timezone`
--
ALTER TABLE `timezone`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `_supplier_item_prices`
--
ALTER TABLE `_supplier_item_prices`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories_items`
--
ALTER TABLE `categories_items`
  ADD CONSTRAINT `categories_items_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `categories_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `items` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `documents_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `document_request`
--
ALTER TABLE `document_request`
  ADD CONSTRAINT `document_request_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `document_request_ibfk_2` FOREIGN KEY (`rid`) REFERENCES `requests` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `document_request_ibfk_3` FOREIGN KEY (`did`) REFERENCES `documents` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `groups_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `groups_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `groups_roles`
--
ALTER TABLE `groups_roles`
  ADD CONSTRAINT `groups_roles_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `groups_roles_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `items_ibfk_2` FOREIGN KEY (`accid`) REFERENCES `accounts` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `item_documents`
--
ALTER TABLE `item_documents`
  ADD CONSTRAINT `item_documents_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `item_documents_ibfk_2` FOREIGN KEY (`doc_id`) REFERENCES `documents` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `optionlist`
--
ALTER TABLE `optionlist`
  ADD CONSTRAINT `optionlist_ibfk_1` FOREIGN KEY (`qid`) REFERENCES `section_questions` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `package`
--
ALTER TABLE `package`
  ADD CONSTRAINT `package_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `questanswers_document`
--
ALTER TABLE `questanswers_document`
  ADD CONSTRAINT `questanswers_document_ibfk_1` FOREIGN KEY (`doc_id`) REFERENCES `documents` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `questanswers_document_ibfk_2` FOREIGN KEY (`qans_id`) REFERENCES `questions_answers` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `questions_answers`
--
ALTER TABLE `questions_answers`
  ADD CONSTRAINT `questions_answers_ibfk_2` FOREIGN KEY (`rid`) REFERENCES `requests` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `questions_answers_ibfk_3` FOREIGN KEY (`qid`) REFERENCES `section_questions` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `questions_answers_ibfk_4` FOREIGN KEY (`uid`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `questions_documents`
--
ALTER TABLE `questions_documents`
  ADD CONSTRAINT `questions_documents_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `questions_documents_ibfk_2` FOREIGN KEY (`qid`) REFERENCES `section_questions` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `q_answer`
--
ALTER TABLE `q_answer`
  ADD CONSTRAINT `q_answer_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `q_answer_ibfk_2` FOREIGN KEY (`qid`) REFERENCES `section_questions` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `q_answer_ibfk_3` FOREIGN KEY (`rid`) REFERENCES `requests` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `requests`
--
ALTER TABLE `requests`
  ADD CONSTRAINT `requests_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `requests_ibfk_2` FOREIGN KEY (`accid`) REFERENCES `accounts` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_invitation_message`
--
ALTER TABLE `request_invitation_message`
  ADD CONSTRAINT `request_invitation_message_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_items`
--
ALTER TABLE `request_items`
  ADD CONSTRAINT `request_items_ibfk_2` FOREIGN KEY (`request_id`) REFERENCES `requests` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_item_prices`
--
ALTER TABLE `request_item_prices`
  ADD CONSTRAINT `request_item_prices_ibfk_1` FOREIGN KEY (`rid`) REFERENCES `requests` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `request_item_prices_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `request_item_prices_ibfk_3` FOREIGN KEY (`reqitem_id`) REFERENCES `request_items` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_supplier`
--
ALTER TABLE `request_supplier`
  ADD CONSTRAINT `request_supplier_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `request_supplier_ibfk_2` FOREIGN KEY (`req_id`) REFERENCES `requests` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `requisition`
--
ALTER TABLE `requisition`
  ADD CONSTRAINT `requisition_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `requisition_approver`
--
ALTER TABLE `requisition_approver`
  ADD CONSTRAINT `requisition_approver_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `requisition_approver_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `requisition_approver_ibfk_3` FOREIGN KEY (`req_id`) REFERENCES `requisition` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `requisition_document`
--
ALTER TABLE `requisition_document`
  ADD CONSTRAINT `requisition_document_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `requisition_document_ibfk_3` FOREIGN KEY (`did`) REFERENCES `documents` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `requisition_document_ibfk_4` FOREIGN KEY (`rid`) REFERENCES `requisition` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `req_item_doc`
--
ALTER TABLE `req_item_doc`
  ADD CONSTRAINT `req_item_doc_ibfk_1` FOREIGN KEY (`reqitem_id`) REFERENCES `request_items` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `req_item_doc_ibfk_2` FOREIGN KEY (`doc_id`) REFERENCES `documents` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `roles_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `section_questions`
--
ALTER TABLE `section_questions`
  ADD CONSTRAINT `section_questions_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `section_questions_ibfk_2` FOREIGN KEY (`rid`) REFERENCES `requests` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `suppliergroup`
--
ALTER TABLE `suppliergroup`
  ADD CONSTRAINT `suppliergroup_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `suppliergroup_ibfk_2` FOREIGN KEY (`acc_id`) REFERENCES `accounts` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `supplier_accounts`
--
ALTER TABLE `supplier_accounts`
  ADD CONSTRAINT `supplier_accounts_ibfk_1` FOREIGN KEY (`acc_id`) REFERENCES `accounts` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `supplier_accounts_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `supplier_group_rel`
--
ALTER TABLE `supplier_group_rel`
  ADD CONSTRAINT `supplier_group_rel_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `supplier_group_rel_ibfk_2` FOREIGN KEY (`supgroup_id`) REFERENCES `suppliergroup` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `supplier_items`
--
ALTER TABLE `supplier_items`
  ADD CONSTRAINT `supplier_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `supplier_items_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `supplier_req_doc`
--
ALTER TABLE `supplier_req_doc`
  ADD CONSTRAINT `supplier_req_doc_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `supplier_req_doc_ibfk_2` FOREIGN KEY (`rid`) REFERENCES `requests` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `supplier_req_doc_ibfk_3` FOREIGN KEY (`did`) REFERENCES `documents` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `template_optionlist`
--
ALTER TABLE `template_optionlist`
  ADD CONSTRAINT `template_optionlist_ibfk_1` FOREIGN KEY (`qid`) REFERENCES `template_section_questions` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `template_section_questions`
--
ALTER TABLE `template_section_questions`
  ADD CONSTRAINT `template_section_questions_ibfk_1` FOREIGN KEY (`tempid`) REFERENCES `template_questionnaire` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`accid`) REFERENCES `accounts` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
