<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
<title>Businnesbid</title>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/css/style.css" rel="stylesheet">
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/css/stylesheet.css" rel="stylesheet" type="text/css">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/customStyle.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link href="<?php echo Yii::app()->request->baseUrl; ?>/lib/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
    

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>
<body>
<!--header part start-->
<header>
  <div class="container-fluid">
    <div class="col-lg-9 col-md-7 col-sm-7 col-xs-12 logo">
      <div class="row"><a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/logo.png" alt="Logo"></a></div>
    </div>
    <div class="col-lg-3 col-md-5 col-sm-5 col-xs-12 header-right">
      <div class="row">
        <div class="notification">
          <ul>
            <li><a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/notification-icon-1.png" alt="Notification"></a></li>
            <li><a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/notification-icon-2.png" alt="Notification"></a><span class="notification-dot"></span></li>
            <li><a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/notification-icon-3.png" alt="Notification"></a><span class="notification-dot notification-dot-green"></span></li>
          </ul>
        </div>
        <div class="dropdown automobiles">
          <button class="btn  dropdown-toggle automobiles-btn" type="button" data-toggle="dropdown">al nabooda automobiles <span class="caret"></span></button>
          <ul class="dropdown-menu">
            <li><a href="#">HTML</a></li>
            <li><a href="#">CSS</a></li>
            <li><a href="#">JavaScript</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</header>
<!--header part end--> 
<!--nav part start-->
<nav role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <div class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu"> <span class="bar1"></span> <span class="bar2"></span> <span class="bar3"></span> </div>
    </div>
    <div class="collapse navbar-collapse" id="main-menu">
      <ul>
        <li><a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/dashboard-icon.png" alt="Dashboard">dashboard</a></li>
        <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/transactions-icon.png" alt="Dashboard"> transactions <span class="caret caret-nav"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#" class="">Action</a> </li>
            <li><a href="#" class="">Another action</a> </li>
            <li><a href="#" class="">Something else here</a> </li>
            <li class="divider"></li>
            <li><a href="#" class="">Separated link</a> </li>
          </ul>
        </li>
        <li><a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/suppliers-icon.png" alt="Dashboard">Suppliers</a></li>
        <li><a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/catalogues-icon.png" alt="catalogues">catalogues</a></li>
        <li><a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/reports-icon.png" alt="reports">reports</a></li>
      </ul>
    </div>
  </div>
</nav>
<!--nav part end--> 
<!--content part start-->
<section class="content">
  <div class="content-inner">

        <?php echo $content;?>
    
    
  </div>
</section>
<!--content part end--> 
<!--footer part start-->
<footer>
  <div class="container-fluid"> Copyright &copy; BusinessBid 2014-2018. All Rights Reserved. </div>
</footer>
<!--footer part end--> 
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/js/customize.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/js/index.js"></script> 
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/js/selectbox.js"></script> 
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> 
<!-- DataTables JavaScript -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/vendor/multifile/jquery.MultiFile.min.js"></script>
 <script src="<?php echo Yii::app()->request->baseUrl; ?>/lib/datetimepicker/build/jquery.datetimepicker.full.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/custom.js"></script>
 <script
        src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>

        
</body>
</html>
