<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
<title>Businnesbid</title>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/css/style.css" rel="stylesheet">
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/css/media.css" rel="stylesheet">
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/css/intlInputPhone.min.css" rel="stylesheet">
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/css/stylesheet.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/css/bootstrap-datetimepicker.min.css'>

<!--Custom-->
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/customStyle.css" rel="stylesheet">

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/js/intlInputPhone.min.js"></script> 

</head>
<body>
<!--header part start-->
<header>
  <div class="container-fluid">
    <div class="col-lg-9 col-md-7 col-sm-7 col-xs-12 logo">
      <div class="row"><a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/images/logo.png" alt="Logo"></a></div>
    </div>
    <div class="col-lg-3 col-md-5 col-sm-5 col-xs-12 header-right">
      <div class="row">
        
            
            <div class="collapse navbar-collapse topmenu-line" id="main-menu">
                <ul>
                    <li><a href="#">Sign Up</a></li>
                    <li><a href="#">Login</a></li>
                </ul>
            </div>
            
        
      </div>
    </div>
  </div>
</header>
<!--header part end--> 

<!--content part start-->
<section class="content">
  <?php echo $content;?>
</section>
<!--content part end-->  
<!--footer part start-->
<footer>
  <div class="container-fluid"> Copyright &copy; BusinessBid 2014-2018. All Rights Reserved. </div>
  
</footer>
<!--footer part end--> 
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/js/customize.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/custom.js"></script> 
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/js/index.js"></script> 
<script  src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/js/date.js"></script> 
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/js/selectbox.js"></script> 
<script src='http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.min.js'></script> 
<script src='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.3/js/bootstrap-datetimepicker.min.js'></script>
<!-- Custom DataTables JavaScript -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/vendor/multifile/jquery.MultiFile.js"></script>
<!-- Custom Theme JavaScript -->
 <script
        src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous"></script>


    <script>
    $(document).ready(function(){
    $(".filter-bnt").click(function(){
        $(".find-requisitions").fadeToggle();
    });
});

    </script>
    

<script>
function myFunction() {
    var x = document.getElementById(".find-requisitions");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
</script>
<script>
if(navigator.userAgent.indexOf('Mac') > 0)
$('body').addClass('mac-os');
if(navigator.userAgent.indexOf('Safari') > 0)
$('body').addClass('safari');
if(navigator.userAgent.indexOf('Chrome') > 0)
$('body').addClass('chrome');
</script> 
    
</body>
</html>
