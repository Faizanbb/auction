$(document).ready(function(){
   var requestId = $("#reqid").val();
   if(!requestId){
       $('.request-block').addClass('disabled-block');
   }
   
});
$('section .tab-block').click(function(event) {
    event.preventDefault();
    var requestId = $("#reqid").val();
    if($(this).hasClass('disabled-block')){
        return false;
    }
   
    $(this).addClass('active');
    $(this).siblings().removeClass('active');

  var ph = $(this).parent().parent().height();

  var ch = $(this).next().height();

  if (ch > ph) {
    $(this).parent().css({
      'min-height': ch + 'px'
    });
  } else {
    $(this).parent().css({
      'height': 'auto'
    });
  }
});

function tabParentHeight() {
  var ph = $('section').height();
  var ch = $('section .tab-open').height();
  if (ch > ph) {
    $('.content-inner').css({
      'min-height': ch+10 + 'px'
    });
  } else {
    $(this).parent().css({
      'height': 'auto'
    });
  }
}

$(window).resize(function() {
  tabParentHeight();
});

$(document).resize(function() {
  tabParentHeight();
});
tabParentHeight();